$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Android_Login.feature");
formatter.feature({
  "line": 1,
  "name": "Verify Sign In  screen functionality",
  "description": "",
  "id": "verify-sign-in--screen-functionality",
  "keyword": "Feature"
});
formatter.before({
  "duration": 56201616300,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "#Combined cases"
    },
    {
      "line": 4,
      "value": "#To verify that the Sign In screen is displayed"
    },
    {
      "line": 5,
      "value": "#To verify that clicking the Back button opens the Welcome Screen"
    },
    {
      "line": 6,
      "value": "#To verify that clicking the Help icon opens the Help Screen"
    },
    {
      "line": 7,
      "value": "#To verify that the \u0027Mobile number\u0027 field is displayed"
    },
    {
      "line": 8,
      "value": "#To verify that the help text \u0027Mobile number\u0027 in field is displayed"
    },
    {
      "line": 9,
      "value": "#To verify that inputing data in the field removes the help text \u0027Mobile number\u0027."
    },
    {
      "line": 10,
      "value": "#To verify that inputing data in the field removes the help text \u0027Mobile number\u0027  when the user enter data in the field"
    },
    {
      "line": 11,
      "value": "#To verify clicking the NEXT button an error is displayed"
    },
    {
      "line": 12,
      "value": "#To verify that clicking the Face ID/Fingerprint icon opens Warning Pop up"
    },
    {
      "line": 13,
      "value": "#To verify that clicking the OK button closes the Warning Pop up"
    },
    {
      "line": 14,
      "value": "#To verify the Legal stuff link is displayed and if clicking the Legal Stuff link opens the Legal Stuff Screen"
    },
    {
      "line": 15,
      "value": "#To verify the Legal stuff text is displayed on top of the screen"
    },
    {
      "line": 16,
      "value": "#To verify the help icon is displayed on top right corner and if clicking user redirect to support screen"
    },
    {
      "line": 17,
      "value": "#To verify the (X) button is displayed and if clicking the X button user redirect to sign In screen"
    },
    {
      "line": 18,
      "value": "#To verify if clicking the device back button user redirect to Sign In screen"
    },
    {
      "line": 19,
      "value": "#To verify clicking the NEXT button the Verification Screen is displayed"
    },
    {
      "line": 20,
      "value": "#To verify that clicking the Back button opens the Login Screen"
    },
    {
      "line": 21,
      "value": "#To verify that clicking the Help icon opens the Support Screen"
    },
    {
      "line": 22,
      "value": "#To verify that clicking the X button closes the support screen"
    },
    {
      "line": 23,
      "value": "#To verify if the user can input data in the OTP field"
    },
    {
      "line": 24,
      "value": "#To verify that clicking the NEXT button logs in the user"
    }
  ],
  "line": 28,
  "name": "",
  "description": "",
  "id": "verify-sign-in--screen-functionality;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@Test9888"
    },
    {
      "line": 27,
      "name": "@Login"
    }
  ]
});
formatter.step({
  "line": 29,
  "name": "I update the test rail case ID \"9888\"",
  "keyword": "Given "
});
formatter.step({
  "line": 30,
  "name": "I install the application",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I verify I am on splash screen",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I tap on EXISTING CUSTOMER button",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I verify I am on \"Sign In\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "I tap on back button",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "I verify Welcome screen is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "I tap on EXISTING CUSTOMER button",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "I tap on help Icon",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "I verify I am on help screen",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "I tap on Cross button",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "I verify mobile number field display",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "I verify help text displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "I enter \"96874411\" mobile number",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "I tap on next button",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "I verfiy \"This number is not an active FRiENDi mobile number. Please try again.\" text is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 45,
  "name": "I click on \"OK\" button",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "I enter mobile number",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "I verify help text removed",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 48,
      "value": "#When I tap on fingerprint icon"
    },
    {
      "line": 49,
      "value": "#Then I verify warning pop up displayed"
    },
    {
      "line": 50,
      "value": "#When I click on \"OK\" button"
    }
  ],
  "line": 51,
  "name": "I click on \"LEGAL STUFF\" button",
  "keyword": "When "
});
formatter.step({
  "line": 52,
  "name": "I verify I am on \"Legal Stuff\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "I tap on help Icon",
  "keyword": "When "
});
formatter.step({
  "line": 54,
  "name": "I verify I am on help screen",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "I tap on Cross button",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "I tap on Cross button",
  "keyword": "When "
});
formatter.step({
  "line": 57,
  "name": "I verify I am on \"Sign In\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 58,
  "name": "I click on \"LEGAL STUFF\" button",
  "keyword": "When "
});
formatter.step({
  "line": 59,
  "name": "I tap on device back button",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I verify I am on \"Sign In\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 61,
  "name": "I tap on next button",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "I verify I am on \"Verification\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 63,
  "name": "I tap on back button present screen",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "I verify I am on \"Sign In\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 65,
  "name": "I enter mobile number",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "I tap on next button",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "I tap on help Icon",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "I verify I am on help screen",
  "keyword": "Then "
});
formatter.step({
  "line": 69,
  "name": "I tap on Cross button",
  "keyword": "When "
});
formatter.step({
  "line": 70,
  "name": "I verify I am on \"Verification\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 71,
  "name": "I verify able to input \"123456\" data in OTP field",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "I enter verification code",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "I click on next button",
  "keyword": "When "
});
formatter.step({
  "line": 74,
  "name": "I verify I am on dashboard",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "9888",
      "offset": 32
    }
  ],
  "location": "Android_WelcomeGetAsimStepDef.i_update_the_test_rail_case_id_something(String)"
});
formatter.result({
  "duration": 272665699,
  "status": "passed"
});
formatter.match({
  "location": "Android_WelcomeGetAsimStepDef.i_install_the_application()"
});
formatter.result({
  "duration": 177299,
  "status": "passed"
});
formatter.match({
  "location": "Android_WelcomeGetAsimStepDef.i_verify_i_am_on_splash_screen()"
});
formatter.result({
  "duration": 1067880699,
  "status": "passed"
});
formatter.match({
  "location": "Android_WelcomeGetAsimStepDef.i_tap_on_existing_customer_button()"
});
formatter.result({
  "duration": 1512086100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sign In",
      "offset": 18
    }
  ],
  "location": "Android_WelcomeGetAsimStepDef.i_verify_i_am_on_something_screen(String)"
});
formatter.result({
  "duration": 22270806800,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_tap_on_back_button()"
});
formatter.result({
  "duration": 1715131399,
  "status": "passed"
});
formatter.match({
  "location": "Android_WelcomeGetAsimStepDef.i_verify_welcome_screen_is_displayed()"
});
formatter.result({
  "duration": 1698979700,
  "status": "passed"
});
formatter.match({
  "location": "Android_WelcomeGetAsimStepDef.i_tap_on_existing_customer_button()"
});
formatter.result({
  "duration": 2833487900,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_tap_on_help_icon()"
});
formatter.result({
  "duration": 1665384300,
  "status": "passed"
});
formatter.match({
  "location": "Android_InternationalCallsStepDef.i_verify_i_am_on_help_screen()"
});
formatter.result({
  "duration": 2717407001,
  "status": "passed"
});
formatter.match({
  "location": "Android_MoreHelpStepDef.i_tap_on_cross_button()"
});
formatter.result({
  "duration": 2149245300,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_verify_mobile_number_field_display()"
});
formatter.result({
  "duration": 892365399,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_verify_help_text_displayed()"
});
formatter.result({
  "duration": 687950900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "96874411",
      "offset": 9
    }
  ],
  "location": "Android_InternationalCallsStepDef.i_enter_something_mobile_number(String)"
});
formatter.result({
  "duration": 2446630100,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_tap_on_next_button()"
});
formatter.result({
  "duration": 1449979201,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "This number is not an active FRiENDi mobile number. Please try again.",
      "offset": 10
    }
  ],
  "location": "Android_MoreAccountSettingStepDef.i_verfiy_something_text_is_displayed(String)"
});
formatter.result({
  "duration": 1674558899,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "OK",
      "offset": 12
    }
  ],
  "location": "Android_MoreAccountSettingStepDef.i_click_on_something_button(String)"
});
formatter.result({
  "duration": 2016240000,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_enter_mobile_number()"
});
formatter.result({
  "duration": 3379344601,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_verify_help_text_removed()"
});
formatter.result({
  "duration": 745873801,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "LEGAL STUFF",
      "offset": 12
    }
  ],
  "location": "Android_MoreAccountSettingStepDef.i_click_on_something_button(String)"
});
formatter.result({
  "duration": 3319496400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Legal Stuff",
      "offset": 18
    }
  ],
  "location": "Android_WelcomeGetAsimStepDef.i_verify_i_am_on_something_screen(String)"
});
formatter.result({
  "duration": 22406604499,
  "status": "passed"
});
formatter.match({
  "location": "Android_LoginStepDef.i_tap_on_help_icon()"
});
formatter.result({
  "duration": 1749827400,
  "status": "passed"
});
formatter.match({
  "location": "Android_InternationalCallsStepDef.i_verify_i_am_on_help_screen()"
});
formatter.result({
  "duration": 1858588401,
  "status": "passed"
});
formatter.match({
  "location": "Android_MoreHelpStepDef.i_tap_on_cross_button()"
});
