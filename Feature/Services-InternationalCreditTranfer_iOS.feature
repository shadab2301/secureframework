Feature: Verify International Credit Transfer screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	#Combined cases
	#To verify if the International Credit Transfer screen is displayed
	#To verify if the user is navigated to the Other Services screen after clicking on the Back button from the International Credit transfer screen
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that Top up button navigates to the Top up screen
	#To verify if a field with help text "Transfer credit to..." is displayed on the Credit Transfer screen
	#To verify if the user is able to input data in the field with help text "Transfer credit to..."
	#To verify if the help text will disappear when the user enters data in the field
	#To verify if Text "The number should start with the country code in any of these formats: +968 or 00968" is displayed on international credit transfer screen.
	#To verify if the user is navigated to the native Contacts application after clicking on the button
	#To verify if the phone number is displayed in the field after selecting a number from the native Contacts application
	#To verify if a CONTINUE button is displayed on the International Credit Transfer screen
	#To verify if an error message with Cancel button is displayed after clicking on the CONTINUE  button with not valid phone number
	#To verify if the error message pop-up will disappear if the user clicks on the OK button
	#To verify if the CONTINUE  button is enabled only when there is enough money in the wallet and entered phone number in the field is in correct format
	#International Credit Transfer - Selection Screen is displayed after clicking on the CONTINUE button 
	#international Credit Transfer - Selection screen is displayed
	#To verify if the user is navigated to the International Credit Transfer screen after clicking on the Back button from the International Credit transfer-Selection screen
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that current wallet balance (Total balance) is displayed in the header
	#To verify that Top up button navigates to the Top up screen
	#To verify that phone number is displayed
	#To verify that the International Credit Transfer Screen is displayed with clicking the EDIT button
	#To verify that the default value under the total balance screen is displayed
	#To verify that the dropdown list with predefined values of destination currency amounts should be expanded
	#To verify that the balance that will remain in the wallet if the transfer was executed is displayed
	#To verify that clicking the TRANSFER button a pop up is displayed that the user does not have sufficient balance in the wallet
	#To verify that clicking the Recharge button, the user is navigated to the TOP UP screens
	#To verify that clicking the CANCEL button, the pop up is closed
	#To verify that clicking the TRANSFER button ,user navigated to Summary Screen.
	#To verify if a You'll transfer : RO x.xx is displayed
	#To verify if the user is navigated to the Verification screen after clicking on the CONFIRM button
	#To verify if the user is navigated to the Credit Transfer Confirmation screen 
	#To verify if an OTP field is displayed on the screenß
	#To verify if the user is able to input data in the OTP field
	#To verify if a CONFIRM/NEXT button is displayed
	#To verify if an error popup message that the OTP is not correct is displayed after clicking on the CONFIRM button with invalid OTP
	#To verify if the user will receive an OTP by SMS
@InternationalCreditTransferJenkins 
@test10274 
Scenario: 
	Given I update the test rail case ID "10274" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "International Credit Transfer" button present on screen 
	Then I verify "International Credit Transfer" Screen appears 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "International Credit Transfer" button present on screen 
	Then I verify "International Credit Transfer" Screen appears 
	And I verify "TOTAL BALANCE" text present on screen 
	And I verify RO value display over screen 
	When I click on plus button 
	Then I verify "Recharge" Screen appears 
	When I tap on back button 
	Then I verify Transfer credit to text present on screen 
	And I verify user can enter "12345678910" this data in the field 
	And I verify the help text in the field in disappeared from field 
	And I verify "The international number should start with the country code in any of these formats: +(country code) or 00(country code)" text is present on screen using value 
	When I click on contacts button 
	Then I verify "Contacts" Screen appears 
	When I click on "Anna Haro" button present on screen 
	Then I verify "International Credit Transfer" Screen appears 
	And I verify "+9685555228243" text is present on screen using value 
	And I verify "CONTINUE" button is present on screen 
	When I enter number "0096879566999" in the field 
	When I click on "Done" button present on screen 
	When I click on "CONTINUE" button present on screen 
	Then I verify "Error" text present on screen 
	Then I verify "Sorry, the credit transfer service is not available to this country/operator at the moment." text is present on screen using value 
	When I click on "Close" button present on screen 
	When I enter number "00966570738031" in the field 
	When I click on "Done" button present on screen 
	Then I verify continue button get enabled 
	When I click on "CONTINUE" button present on screen 
	Then I verify "Maximum amount you can transfer in a month is RO 30." text is present on screen using value 
	When I tap on back button 
	#And I verify "The international number should start with the country code in any of these formats: +(country code) or 00(country code)" text is present on screen using value 
	When I click on "CONTINUE" button present on screen 
	And I verify "TOTAL BALANCE" text present on screen 
	And I verify RO value display over screen 
	When I click on plus button 
	Then I verify "Recharge" Screen appears 
	When I tap on back button 
	Then I verify "00966570738031" text is present on screen using value 
	When I click on EDIT button 
	#And I verify "The international number should start with the country code in any of these formats: +(country code) or 00(country code)" text is present on screen using value 
	When I click on "CONTINUE" button present on screen 
	#Then I verify "10 SAR (1.39 RO)" text is present on screen using value 
	When I click on drop down present on screen 
	Then I verify dropdown list with predefind values diaplayed 
	When I scroll Ro value 
	When I click on Done button 
	Then I verify "TRANSFER" text present on screen 
	Then I verify remaining balance in wallet is display 
	When I click on "TRANSFER" button present on screen 
	Then I verify Confirmation popup is present on screen 
	When I click on CANCEL button present on screen 
	Then I verify Maximum amount info is present on screen 
	When I click on TRANSFER button present on screen 
	When I click on RECHARGE button present on screen 
	Then I verify Recharge Screen appears 
	When I tap on back button 
	Then I verify "International Credit Transfer" Screen appears 
	When I tap on back button 
	When I click on "CONTINUE" button present on screen 
	When I click on "TRANSFER" button present on screen 
	Then I verify "Summary" Screen appears 
	#And I verify "You'll transfer" text present on screen 
	When I click on "CONTINUE" button present on screen 
	#When I tap on back button 
	#Then I verify "Summary" Screen appears
	#When I click on "CONTINUE" button present on screen
	Then I verify "Verification" Screen appears 
	Then I verify OTP field display 
	And I verify able to input data in OTP field 
	Then I verify "NEXT" text present on screen 
	When I enter invalid OTP in OTP field 
	When I click on "NEXT" button present on screen 
	Then I verify "Error" text is present on screen using value 
	When I click on "OK" button present on screen 
	Then I verify OTP field display 
	# Then I verify "SEND AGAIN" text present on screen 
    