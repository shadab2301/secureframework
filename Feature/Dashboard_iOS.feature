Feature: Verify Dashboard screen functionality

Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
 
############################## Dashboard #################################################    
 #Combine Case
#1. DASHBOARD: Verify the Credit Balance Section is displayed below the Manage SIMs section
#2. DASHBOARD: Verify the Credit Balance is displayed below the Manage SIMs section
#3. DASHBOARD: Verify if clicking the add credit button should displayed the Recharge screen
#4. DASHBOARD: Verify the Bonus Wallet Section is displayed below the Wallet Balance section
#5. DASHBOARD: Verify the Bonus Wallet arrow should navigate to Your Rewards screen
#6. DASHBOARD: Verify if Dashboard is displayed when the back button is clicked
#7. DASHBOARD: Verify if the Previous months' bonuses and the Help button is displayed when History button is clicked
#8. DASHBOARD: Verify if the Help screen is displayed when Help button is clicked 
#9. DASHBOARD: Verify if the Your Rewards screen is displayed when Back button is clicked
#10. DASHBOARD: Verify if the Recharge screen with voucher and one-off top up should be displayed 
#11. DASHBOARD: Verify if the detailed view of promo bonus per month is listed
#12. DASHBOARD: Verify the Add Credit button is displayed below the Manage SIMs section
#13. DASHBOARD: Verify the back button is displayed and user is navigated back to dashboard after clicking on back button
#14. DASHBOARD: Verify the Enter Amount field is displayed
#15. DASHBOARD: To verify if the user is able to input data in the field
#16. DASHBOARD: Verify that the field has character validation
#17. DASHBOARD: Payment method should be displayed in the ADD CREDIT screen
#18. DASHBOARD: The Add new Card Screen should be displayed
#19. DASHBOARD: To  verify that the error message is displayed when user enter invalid card information
#20. DASHBOARD: To verify if the PAY button should be displayed
#21. DASHBOARD: To verify if the  PAY button should be enabled when payment method is selected
#22. DASHBOARD: Verify if the detailed view of promo bonus per month is closed
#23. DASHBOARD: Verify if the detailed view of promo bonus can be opened for more than one month at a time
#24. DASHBOARD: Verify if the amounts per days are correctly summed up in the monthly amount 
#25. DASHBOARD: Verify if the current month amounts are matching

  @DashboardiOSJenkins @10517
  Scenario: C7997,C7998,C7999,C8000,C8001,C8003,C8006,C8007,C8008,C8009,C8010,C8015,C8018,C8019
    Given I update the test rail case ID "10517"
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    And I verify "Credit balance" text is present on screen using value
    And I verify total balance is displayed
    And I verify "ADD CREDIT" text is present on screen using value
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I tap on back button 
    Then I verify user is on dashboard
    #And I verify bonus wallet section is displayed
    #And I verify "5% bonus credit" text is present on screen using value
    When I click on bonus wallet section
    Then I verify "Your rewards" Screen appears 
    And I verify Total amount of SR and Your bonus balance is displayed
    When I tap on back button
    Then I verify user is on dashboard
    When I click on bonus wallet section
    Then I verify "Your rewards" Screen appears  
    When I click on "HISTORY" button using value
    Then I verify "Rewards history" Screen appears
    And I verify the previous months are displayed
    And I verify bonuses are displayed
    And I verify amount per day are summed correctly
    And I verify bonuses are closed now
    And I verify more than one month details can be opened at a time
    When I click on help button present
    Then I verify "FAQ" Screen appears
    When I tap on back button
    Then I verify "Rewards history" Screen appears   
    When I tap on back button
    Then I verify "Your rewards" Screen appears 
    When I click on recharge your account button
    Then I verify "Recharge" Screen appears 
    And I verify "Recharge with voucher" text is present on screen using value
    And I verify "VOUCHER NUMBER" text is present on screen using value
    When I tap on back button
    When I tap on back button
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    And I verify "Recharge account with" text is present on screen using value
    And I verify amount field is displayed 
    Then I verify "Pay | RO 0.500" text is present on screen using value
    And I verify pay button is enabled
    #And I verify a payment method is displayed
    And I verify user able to enter this "123" data in recharge wallet field
    And I verify there is character validation present in the field
    
#1. DASHBOARD: To verify if the user can enter amount in the Recharge field and if the cash back amount dynamically displayed on the same screen
#2. DASHBOARD: To verify if the Done button should be displayed         
#3. DASHBOARD: To verify if the  User should do the top up with 0.5 RO
#4. DASHBOARD: To verify if the  User should do the top up with 1 RO
#5. DASHBOARD: To verify if the  User should do the top up with 2 RO
#6. DASHBOARD: To verify if the  User should do the top up with 3 RO
#7. DASHBOARD: To verify if the  User should do the top up with 4 RO
#8. DASHBOARD: To verify if the  User should do the top up with 5 RO
#9. DASHBOARD: To verify if the User should do the top up with 10 RO
#10. DASHBOARD: To verify if the User should do the top up with 20 RO
#11. DASHBOARD: To verify if the User should do the top up with 50 RO
@DashboardiOSJenkins @10544
  Scenario: C8032,C8033,C8034,C8035,C8036,C8037,C8038,C8039,C8040
    Given I update the test rail case ID "10544"
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    When I click on "More" button present on screen
    Then I verify "Settings" Screen appears using value
    When I click on "Settings" button using value
    When I click on "Log out" button present on screen
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter this "79517436" mobile number in the field
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    When I close the Welcome popup
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    When I tap for guide popup
    And I verify "Credit balance" text is present on screen using value
    And I verify total balance is displayed
    And I verify "ADD CREDIT" text is present on screen using value
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    And I verify user able to enter this "100" data in recharge wallet field    
    And I verify "RO 5.000" text is present on screen using value
    When I click on "Pay | RO 100.000" button using value
    Then I verify "Recharge" Screen appears
    Then I verify "DONE" text is present on screen using value
    When I tap on back button
    Then I verify "Recharge" Screen appears
    And I verify user able to enter this "0.5" data in recharge wallet field 
    When I click on "Pay | RO 0.500" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '1' times
    When I click on "Pay | RO 1.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '2' times
    When I click on "Pay | RO 2.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '3' times
    When I click on "Pay | RO 3.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '4' times
    When I click on "Pay | RO 4.000" button using value
    Then I verify "Recharge" Screen appears 
    When I click on "DONE" button
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '5' times
    When I click on "Pay | RO 5.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '6' times
    When I click on "Pay | RO 10.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I click on plus button on screen '7' times
    When I click on "Pay | RO 10.000" button using value
    Then I verify "Recharge" Screen appears
    When I click on "DONE" button 
    Then I verify user is on dashboard
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    When I enter wrong details in add new card
    Then I verify "Error" text is present on screen using value
    
#1. DASHBOARD: Verify that the user can input data in the field
#2. DASHBOARD: Verify that the error message is displayed when user tries to use Data or Recharge voucher that the voucher alrady has been used when user clicks on the Use Voucher button
#3. DASHBOARD: Verify that the error message is displayed when user enters incorrect voucher code when user clicks on the Use Voucher button
#4. DASHBOARD: Verify that the field has character validation
#5. DASHBOARD: Verify that the field has character limit
#6. DASHBOARD: Verify that the recharge wallet screen is displayed
#7. DASHBOARD: Verify that the Use Voucher button is disabled
#8. DASHBOARD: Verify that the Voucher Number field is displayed
#9. DASHBOARD: Verify that the Use Voucher button is enabled
#10. DASHBOARD: To verify if the home screen notifications are displayed under the Recharge section
#11. DASHBOARD: To verify if the home screen notifications are stacked one on top of other
#12. DASHBOARD: To verify that the arrow for expanding should be displayed and if clicked expands the notifications     
#13. DASHBOARD: To verify that the CLEAR ALL button should be displayed and if clicked on CLEAR ALL button all home screen notifications cleared
#14. DASHBOARD: To verify that the free WhatsApp and free on-net minutes view is displayed on dashboard when the user scrolls down 
#15. DASHBOARD: To verify that the status is displayed when one of the recharge promos has expired/was consumed
#16. To verify that the status is displayed when both of the recharge promos has expired/was consumed
@DashboardiOSJenkins @10726
  Scenario: C8032,C8033,C8034,C8035,C8036,C8037,C8038,C8039,C8040
    Given I update the test rail case ID "10726"
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    And I verify "ADD CREDIT" text is present on screen using value
    When I click on "ADD CREDIT" button using value
    Then I verify "Recharge" Screen appears
    And I verify "VOUCHER NUMBER" text is present on screen using value
    And I verify "USE VOUCHER" text is present on screen using value
    And I verify use voucher button is disabled
    And I verify there is character validation present in the voucher field
    And I verify character limit is also present in the  voucher field
    When User enters this "580717557166" data in voucher field
    And I verify "5807-1755-7166" text is present on screen using value
    And I verify use voucher button is enabled
    When I click on "USE VOUCHER" button present on screen
    Then I verify "Error" Screen appears
    #And I verify "This recharge voucher has already been used or is invalid. Please try to input the voucher again or use another one." text is present on screen using value
    When I click on "Close" button present on screen
    When I tap on back button
    Then I verify user is on dashboard
    And I verify notifications are displayed in a stack
    And I verify notifications can be expanded
    And I verify clear all button is displayed
    When I click on clear all button
    Then I verify all notifications are cleared
    #And I verify "Free FRiENDi Calls" text is present on screen using value
    #And I verify "Free WhatsApp" text is present on screen using value
    And I verify the status of free whatsapp and free friendi call 
    
#1. DASHBOARD: To verify that the graph with details 3 different service types
#2. DASHBOARD: To verify that selecting service type from the graph shows only the selected service type on the dashboard
#3. DASHBOARD: To verify if the user is able to navigate through the PAYG rates 
#4. DASHBOARD: To verify if the International "View Rates >" button navigates the user to the Selec Country screen
#5. DASHBOARD: To verify if the Toggle button is displayed in the Data PAYG rates 
#6. DASHBOARD: To verify if the Toggle button is selected in the Data PAYG rates the Data PAYG rates are enabled
#7. DASHBOARD: To verify if the Toggle button is selected in the Data PAYG rates the Data PAYG rates are disabled
#8. DASHBOARD: To verify if the SEE FULL DETAILS button is displayed
#9. DASHBOARD: To verify if the SEE FULL DETAILS button is NOT displayed when all PAYG rates are visible
#10. DASHBOARD: To verify if all PAYG rates will be displayed by clicking on the SEE FULL DETAILS button
#11. DASHBOARD: To verify if the SEE FULL DETAILS button is NOT displayed when all PAYG rates are visible
#12. DASHBOARD: To verify if all PAYG rates will be displayed by clicking on the SEE FULL DETAILS button
#13. DASHBOARD: To verify if expanded section with all PAYG rates displayed will collapse and the user will see reduced amount of PAYG items

@DashboardiOSJenkins @11217
  Scenario: C8032,C8033,C8034,C8035,C8036,C8037,C8038,C8039,C8040
    Given I update the test rail case ID "11217"    
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    When I tap for guide popup
    When I tap for guide popup
    #When I scroll upward
    #And I verify "Data" text is present on screen using value
    #And I verify "Local" text is present on screen using value
    #And I verify "International calls" text is present on screen using value
    #When I click on "Data" button using value
    #Then I verify "MB" Screen appears
    #When I click on "Local" button using value
    #Then I verify "MB" Screen appears
    #When I click on "Local" button using value
    #Then I verify "Local Minutes" Screen appears
    #When I click on "International calls" button using value
    #Then I verify "International Minutes" Screen appears
    When I scroll upward "8" times
    And I verify "SMS" text is present on screen using value
    #And I verify "Local" text is present on screen using value
    And I verify "RO 0.011/SMS" text is present on screen using value
    And I verify "International" text is present on screen using value
    And I verify "RO 0.053/SMS" text is present on screen using value
    #And I verify "Data" text is present on screen using value
    And I verify "RO 0.011/MB" text is present on screen using value
    And I verify "SEE FULL DETAILS" text is present on screen using value
    When I click on "SEE FULL DETAILS" button using value
    When I scroll upward "1" times
    And I verify "Local Minutes" text is present on screen using value
    And I verify "FRiENDi" text is present on screen using value
    And I verify "RO 0.027/Min" text is present on screen using value
    And I verify "Other" text is present on screen using value
    And I verify "RO 0.048/Min" text is present on screen using value
    And I verify "International Minutes" text is present on screen using value
    And I verify "VIEW RATES" text is present on screen using value
    And I verify "SHOW LESS" text is present on screen using value
    And I verify toggle button is present
    And I verify toggle button is disabled
    And I verify toggle is enabled now
    When I click on "SHOW LESS" button using value
    And I verify "SEE FULL DETAILS" text is present on screen using value
    When I click on "SEE FULL DETAILS" button using value
    When I scroll upward "2" times
    And I verify "VIEW RATES" text is present on screen using value
    And I click on "VIEW RATES" button using value
    And I verify "Select Country" Screen appears
    
#1. To verify if the Navigation bar is displayed
#2. To verify if the "Home" option in the Navigation Bar is displayed and user navigates to the dashboard screen once clicked on Home option
#3. To verify if the "Plans" option in the Navigation Bar is displayed and user navigates the packs screen once clicked on plans option.
#4. To verify if the "History" option in the Navigation Bar is displayed and user navigates the usage history screen once clicked on History option
#5. To verify if the "More" option in the Navigation Bar is displayed and user navigates the more screen once clicked on More tab  
@DashboardiOSJenkins @12084
Scenario: C8214,C8215,C8216,C8217,C8218
    Given I update the test rail case ID "12084"    
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    #When I tap for guide popup
    #When I tap for guide popup
    Then I verify navigation bar is displayed
    When I click on "Home" button present on screen
    Then I verify user is on dashboard
    When I click on "Plans" button present on screen
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify "Data" button is present on screen
    Then I verify "Minutes" button is present on screen
    Then I verify "Mix" button is present on screen
    When I click on "History" button present on screen
    Then I verify "Usage History" Screen appears
    When I click on "More" button present on screen
    Then I verify "Settings" Screen appears using value

 
    
    
    