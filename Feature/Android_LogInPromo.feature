Feature: Verify LoginPromo screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	
	#Combined cases
	#To verify that the Dashboard promo popup is displayed
	#To verify that the promo pop-up is closed after clicking "X" button
@Test9153 
@loginpromo 
Scenario: 
	Given I update the test rail case ID "9153" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	Then I verify promo popup coach marks is displayed 
	When I tap on cross button 
	Then I verify I am on dashboard 
	
	
@Test6567 
@loginpromo 
Scenario: 
	To verify that the promo pop-up is displayed after user refuse the promo 
	Given I update the test rail case ID "6567" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	Then I verify promo popup coach marks is displayed 

@Test6568
@loginpromo 
Scenario: 
	To verify that the user is navigated to Dashboard if promo data is claimed 
	Given I update the test rail case ID "6568" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	Then I verify I am on dashboard
	
	#Combined cases
	#To verify that Dashboard success claim pop-up is displayed 
	#To verify that claimed data is displayed in the dashboard properly
	
@Test9194 
@loginpromo 
Scenario: 
	Given I update the test rail case ID "9194" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I tap on claim button 
	Then I verify success with done button displayed 
	When I tap on done button 
	When I scroll the screen upwards 
	And I verify promo data display 
	
	
	
	
	
	
	
	
	
	