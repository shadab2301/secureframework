Feature: Verify Manage SIM screen functionality

  Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen


  ############################## Dashboard - Manage SIM #################################################
  #Combine Case
  #Dashboard: After entering valid credentials Dashboard is displayed 
  #Dashboard: To verify if the 'Mobile' text is displayed in the header after entering valid credentials
  #Dashboard: To verify if the 'SIM Name" is displayed in the header after entering valid credentials
  #Dashboard:To verify if the MSISDN is displayed in the header after entering valid credentials
  #Dashboard:To verify if the MSISDN drop down is displayed in the header and when clicked the Manage SIM options are displayed
  #Dashboard:To verify Credit: RO XX -Recharge option within the Manage SIM is displayed
	#Dashboard:To verify if an "Edit name" option within the Manage SIM is displayed
  #Dashboard:To verify if the user is redirected to the "Recharge" screen after clicking on the Credit X.X RO - Recharge option 
  #Dashboard:To verify if an "Edit name" option within the Manage SIM is displayed
  #Dashboard:To verify if an "Edit Name" pop-up is displayed after clicking on the Edit name option 
  #Dashboard:To verify if the "Name" field is displayed on the pop-up
  #Dashboard:To verify if the if the cancel button appears and the popup disappears when the user click on cancel button 
  
  
  @ManageSIMJenkins @10322
  Scenario: C6540,C6542,C6543,C6544,C6548,C6550,C6551,C6552
    Given I update the test rail case ID "10322"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    When I close the Welcome popup   
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    Then I verify user is on dashboard
    #And I verify "Mobile" text is present on screen using value
    #And I verify "079 525 424" text is present on screen using value
    When I expand the header  
    Then I verify recharge option is displayed
    And I verify "Edit name" text is present on screen using value
    When I click on recharge option
    Then I verify "Recharge" Screen appears
    When I tap on back button
    Then I verify user is on dashboard
    When I expand the header
    Then I verify "Edit name" text is present on screen using value
    When I click on "Edit name" button using value
    Then I verify "Edit Name" Screen appears using value
    And I verify "You can update your line name" text is present on screen using value
    And I verify name field is present
    #And I verify "Test" this name can be entered in the field 
    Then I verify "CANCEL" button is present on screen
    When I click on "CANCEL" button present on screen
    Then I verify user is on dashboard
    
    
    
    