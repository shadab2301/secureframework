Feature: Verify International Call Rates screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	#Combined cases
	#To verify if the Data Transfer screen is displayed
	#To verify if the Data Transfer Shares screen is displayed
	#To verify if the Help screen is displayed
	#To verify if the Back button navigates the user to the Services screen
	#To verify if the Close button navigates the user to the Services screen
@DataTransferJenkins 
@test10693 
Scenario: 
	Given I update the test rail case ID "10693" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number in field 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "Data Transfer" button using value 
	Then I verify "Share your data" Screen appears 
	And I verify You cannot transfer data that already transferred to you text display 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "Data Transfer" button using value 
	When I click on help button present 
	Then I verify "Other Services" text is present on screen using value 
	When I tap on back button 
	And I verify CLOSE button display 
	When I tap CLOSE button 
	Then I verify Other services Screen appears 
	
	#Combined cases
	#To verify if the Data Transfer Shares screen is displayed
	#To verify if Help screen is displayed
	#To verify if clicking on the Back button navigates the user to the previous screen
	#To verify if Learn more will open Data transfer FAQ section
	#To verify if the Data carousel should be scrollable displaying predefined MB values
	#To verify if the Data carousel should be scrollable displaying predefined GB values
	#To verify if each data transfer will apply its own fee
	#To verify if the data validity is correctly displayed (limited or unlimited)
	#To verify if the Continue button will navigate the user to the next Enter number screen
	#To verify if the Help screen is displayed
	#To verify if the Back button navigates the user to the previous screen
	#To verify if the input field is enabled to put data into
	#To verify if the input field will be filled with a selected number from the address book
	#To verify if clicking the Recently used list will display the numbers that previously data is transferred to
	#To verify if the Recently used list can be collapsed once expanded
	#To verify if the valid number will enable the Continue button to proceed to next screen
	#To verify if the Help screen is displayed
	#To verify if the selected data can be reviewed and changed 
	
@DataTransferJenkins 
@test10738 
Scenario: 
	Given I update the test rail case ID "10738" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number in field 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "Data Transfer" button using value 
	Then I verify "Share your data" Screen appears 
	When I click on help button present 
	Then I verify "Other Services" text is present on screen using value 
	When I tap on back button 
	Then I verify "Share your data" Screen appears 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "Data Transfer" button using value 
	Then I verify "Share your data" Screen appears 
	Then I verify "Learn more" text is present on screen using value 
	When I click on "Learn more" button using value 
	Then I verify "Other Services" text is present on screen using value 
	When I tap on back button 
	Then I verify "Share your data" Screen appears 
	And I verify "50, MB" text is present on screen using value 
	And I verify "FEE" text is present on screen using value 
	And I verify "RO 0.110" text is present on screen using value 
	And I verify "VALIDITY" text is present on screen using value 
	And I verify "10 DAYS" text is present on screen using value 
	When I scroll the data carousel right present on screen 
	Then I verify "100, MB" text is present on screen using value 
	And I verify "RO 0.110" text is present on screen using value 
	And I verify "10 DAYS" text is present on screen using value 
	When I scroll the data carousel right present on screen 
	Then I verify "200, MB" text is present on screen using value 
	And I verify "RO 0.110" text is present on screen using value 
	And I verify "10 DAYS" text is present on screen using value 
	When I scroll the data carousel right present on screen 
	Then I verify "500, MB" text is present on screen using value 
	And I verify "RO 0.220" text is present on screen using value 
	And I verify "10 DAYS" text is present on screen using value 
	When I scroll the data carousel right present on screen 
	Then I verify "1, GB" text is present on screen using value 
	And I verify "RO 0.220" text is present on screen using value 
	And I verify "10 DAYS" text is present on screen using value 
	When I click on "CONTINUE" button using value 
	Then I verify Add your friend number screen display 
	When I click on help button present 
	Then I verify "Other Services" text is present on screen using value 
	When I tap on back button 
	Then I verify "Share your data" Screen appears 
	When I tap on back button 
	Then I verify How much data do you want to share screen display 
	When I click on "CONTINUE" button using value 
	Then I verify contacts button is displayed on data transfer screen 
	When I click on contacts button present on screen 
	Then I verify "Contacts" Screen appears 
	When I click on "Anna Haro" button present on screen 
	Then I verify "Share your data" Screen appears 
	And I verify "5555228243" text is present on screen using value 
	When I click on Recently used dropdown button 
	Then I verify previously used number display on screen 
	When I click on Recently used dropdown button 
	Then I verify Recently used number list collapsed 
	And I verify user can enter "79566458" this data in the tap in number field
	When I tap for guide popup  
	And I verify "CONTINUE" text is present on screen using value 
	When I click on "CONTINUE" button using value 
	And I verify "Data you’re going to share" text is present on screen using value 
	When I click on help button present 
	Then I verify "Other Services" text is present on screen using value 
	When I tap on back button 
	Then I verify "Share your data" Screen appears 
	And I verify "1 GB" text is present on screen using value 
	When I tap on Change button to change data 
	Then I verify How much data do you want to share screen display 
	When I scroll the data carousel left present on screen 
	When I scroll the data carousel left present on screen 
	When I scroll the data carousel left present on screen 
	Then I verify "100, MB" text is present on screen using value 
	When I click on "CONTINUE" button using value 
	Then I verify Add your friend number screen display 
	When I click on "CONTINUE" button using value 
	Then I verify "Data you’re going to share" text is present on screen using value 
	And I verify "100 MB" text is present on screen using value 
	When I tap on Change button to change number 
	Then I verify Add your friend number screen display 
	When I click on "CONTINUE" button using value 
	Then I verify "Data you’re going to share" text is present on screen using value 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "Data Transfer" button using value 
	When I click on "CONTINUE" button using value 
	Then I verify Add your friend number screen display 
	And I verify user can enter "79566458" this data in the tap in number field
	When I tap for guide popup  
	When I click on "CONTINUE" button using value 
	Then I verify "Data you’re going to share" text is present on screen using value 
	When I click on "SHARE DATA" button using value 
	Then I verify "You’ve successfully shared" text is present on screen using value 
	And I verify "AWESOME" text is present on screen using value 
	When I click on "AWESOME" button using value 
	Then I verify "Other services" Screen appears 
	
	