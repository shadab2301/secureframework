Feature: Verify More Payment screen functionality

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	################################# Redeem Rewards ####################################
	#Combine Case
#To verify if Redeem Rewards screen with details is displayed when click on redeem reward button.
#To verify if the Back button navigates the user to the redeem reward screen.
#To verify if "CODE HERE" text appears after removing "100MBFREE" code from code field
#To verify if apply button enables when user enters valid data in redeem rewards code field.
#To verify if an error message is displayed when user enters invalid code.
#To verify if congratulations popup displayed when user click on submit button
#To verify if congratulations popup disappears when user click on (X) button
#To verify if saved popup is displayed when click on save for later over congratulation popup
#To verify if More screen is displayed when click on ok button in saved popup.
#To verify if Promo redeemed popup appears when clicked on Apply now button
#To verify if redeem rewards code field should be pre filled when user selects save for later option on congratulations pop up
#To verify if user gets redirected to more screen when clicked on back button
@RedeemRewardsJenkins @12801
Scenario: C7536,C7537,C7538,C7540,C7542,C7543,C7549,C7550,C7541,C7551,C7552 
	Given I update the test rail case ID "12801" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	And I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Redeem Rewards" text is present on screen using value 
	When I click on "Redeem Rewards" button using value 
#	Then I verify "REDEEM" Screen appears using value 
#	Then I verify "REWARD" Screen appears using value 
	And I verify "Got a promo code?" text is present on screen using value
	And I verify "Enter the code to get your rewards" text is present on screen using value
    And I verify "Apply" text is present on screen using value
    When I tap on back button on screen
    Then I verify "More" text is present on screen using value 
    And I verify "Redeem Rewards" text is present on screen using value 
	When I click on "Redeem Rewards" button using value 
#	Then I verify "REDEEM" Screen appears using value 
#	Then I verify "REWARD" Screen appears using value 
	Then I verify "Got a promo code?" text is present on screen using value
	And I verify entered "20MBFREE" text displays in field
	When I click on "Apply" button present on screen 
	Then I verify "This promo code has already been used or is invalid" text present on screen
	And I verify entered "100MBFREE" text displays in field
	When I click on "Apply" button present on screen 
	Then I verify "Congratulations" text is present on screen using value
	And I verify "Awesome! Enjoy your reward." text is present on screen using value
	And I verify "APPLY NOW" text is present on screen using value
	And I verify "SAVE FOR LATER" text is present on screen using value
	When I tap on back button on screen
#	Then I verify "REDEEM" Screen appears using value 
#	Then I verify "REWARD" Screen appears using value 
	And I verify "Got a promo code?" text is present on screen using value
	When I click on "SAVE FOR LATER" button present on screen 
	And I verify "Saved" Screen appears using value 
	And I verify "No rush! your promo has been saved. Come back here when you want to use it." text is present on screen using value
	And I verify "OK" text is present on screen using value
	When I click on "SAVE FOR LATER" button present on screen 
	Then I verify "OK" Screen appears using value 
	And I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Redeem Rewards" text is present on screen using value 
	When I click on "Redeem Rewards" button using value 
	Then I verify "REDEEM" Screen appears using value 
	And I verify "REWARD" Screen appears using value 
	And I verify "Got a promo code?" text is present on screen using value
	And I verify "100MBFREE" text is present on screen using value
	When I click on "Apply" button present on screen 
	Then I verify "Congratulations" text is present on screen using value
#	When I click on "APPLY NOW" button present on screen 
#	And I verify user is on dashboard 
	
	
	
	
	
	
	
	
	
	