Feature: Verify Roaming Serives screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	#Combined Cases
	#To verify if the user is navigated to Roaming service screen from Services in More screen
	#To verify that user is navigated to country picker screen when search field is clicked
	#To verify if the search field is working properly
	#To verify if the Roaming package screen is displayed properly
	#To verify that Change button navigates to country picker
@Test9368 
@Android_RoamingServices
Scenario: 
	Given I update the test rail case ID "9368" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "Roaming Services" button 
	Then I verify I am on "Roaming Services" screen 
	When I tap on search icon present on screen 
	Then I verify the list of countries is displayed 
	And I verify that the search list with "YEMEN" in it 
	When I enter country name in enter country field 
	When I select country 
	Then I verify country logo display 
	And I verify "CHANGE" button is Displayed 
	When I click on "CHANGE" button 
	Then I verify country list screen display 
	
	
		