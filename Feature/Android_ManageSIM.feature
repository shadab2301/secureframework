Feature: Verify dashboard manage sim screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	
	#COMBINED CASES
	#To verify that the Dashboard is displayed
	#To verify if the 'Mobile' text is displayed in the header
	#To verify if the MSISDN is displayed in the header
	#To verify if the MSISDN is displayed in the header with +968 prefix
	#To verify if the MSISDN drop down is displayed and clicked in the header
	#To verify if the Manage SIM options appears when the user clicks on the number
	#To verify Credit: RO XX -Recharge option within the Manage SIM is displayed
	#To verify if the user is redirected to the "Recharge" screen after clicking on the Credit X.X RO - Recharge option 
	#To verify if an "Edit name" option within the Manage SIM is displayed
	#To verify if an "Edit Name" pop-up is displayed after clicking on the Edit name option 
	#To verify if the "Name" field is displayed on the pop-up
	#To verify if the "Name" field is displayed on the pop-up and the data can be entered in the field
	#To verify if the if the cancel button appears and the popup disappears when the user click on cancel button 
	#To verify if the if the confirm button appears and the name gets saved when user click on the confirm button after editing the name
	#To verify if a "Copy PUK" option within the Manage SIM is displayed 
	#To verify if the SIM Name is displayed in the header
@Android_ManageSim 
@Test8703 
Scenario: 
	Given I update the test rail case ID "8703" 
	Then I verify I am on dashboard 
	When I tap for guide popup 
	Then I verify "Mobile" text displayed 
	And I verify if the MSISDN is displayed in the header 
	When I tap on expand button in top right corner 
	Then I verify the user is able to expand the number dropdown when clicks on it 
	Then I verify ManageSIM options displayed 
	And I verify "Credit: RO XX -Recharge" display 
	When I tap on credit recharge options 
	Then I verify I am on "Recharge" screen 
	When I tap back button from recharge screen  
	When I tap on expand button in top right corner 
	Then I verify "Edit name" button is Displayed 
	When I click on "Edit name" button 
	Then I verify Edit name popup display 
	And I verify Name field display 
	And I verify able to enter in field 
	Then I verify "CANCEL" button is Displayed 
	When I click on "CANCEL" button 
	Then I verify I am on dashboard 
	When I tap on expand button in top right corner  
	When I click "Edit name" text present on the screen 
	When I enter "Mobile" in name field 
	Then I verify "CONFIRM" button is Displayed 
	When I click on "CONFIRM" button 
	Then I verify entered text is saved 
	And I verify SIM Name is displayed in the header 
	When I tap on expand button in top right corner 
	Then I verify "Copy PUK : 23309655" button is Displayed 
	
	
	
	
	
	