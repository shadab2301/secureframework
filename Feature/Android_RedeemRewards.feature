Feature: Verify Redeem Rewards screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Redeem Rewards" text is displayed 
	
	#COMBINED CASES
	#To verify if Redeem Rewards screen with details is displayed when click on redeem reward button.
	#To verify if  the Back button navigates the user to the  redeem reward screen.
	#To verify if "CODE HERE" text appears after removing "100MBFREE" code from code field
	#To verify if apply button enables when user enters valid data in redeem rewards code field.
	#To verify if an error message is displayed when user enters invalid code.
	#To verify if congratulations popup displayed when user click on submit button
	#To verify if congratulations popup disappears when user click on (X) button
	#To verify if saved popup is displayed when click on save for later over congratulation popup
	#To verify if dashboard screen is displayed when click on ok button in saved popup.
	#To verify if redeem rewards code field should be pre-filled when user selects save for later option on congratulations pop up
	#To verify if Promo redeemed popup appears when clicked on Apply now button
	#To verify if user gets redirected to more screen when clicked on back button
	
@AndroidRedeemRwards 
@test12793 
Scenario: 
	Given I update the test rail case ID "12793" 
	When I click on "Redeem Rewards" button 
	Then I verfiy "REDEEM" text is displayed 
	And I verfiy "REWARD" text is displayed 
	And I verfiy "Got a promo code?" text is displayed 
	And I verfiy "Enter the code to get your rewards" text is displayed 
	When I tap back button present screen 
	Then I verfiy "Settings" text is displayed 
	When I click on "Redeem Rewards" button 
	Then I verfiy Apply button for code is displayed 
	And I verify enterd "20MBFREE" code display 
	When I click on Apply button for code 
	Then I verfiy "This promo code has already been used or is invalid" text is displayed 
	And I verify enterd "100MBFREE" code display 
	When I click on Apply button for code 
	Then I verify congratulations popup display 
	When I close the popup 
	Then I verfiy "Got a promo code?" text is displayed 
	When I click on Apply button for code 
	Then I verify congratulations popup display 
	When I tap on Save For Later button 
	Then I verify Saved pop up displayed 
	When I click on "OK" button 
	Then I verfiy "Settings" text is displayed 
	When I click on "Redeem Rewards" button 
	Then I verfiy "100MBFREE" text is displayed 
	When I tap back button present screen 
	Then I verfiy "Settings" text is displayed 
	When I click on "Redeem Rewards" button 
	Then I verfiy "REDEEM" text is displayed 
	And I verfiy "REWARD" text is displayed 
	When I click on Apply button for code 
	Then I verify congratulations popup display 
	#When I click on "APPLY NOW" button 
	#Then I verfiy "Promo redeemed" text is displayed 
	#And I verfiy "Awesome! Your promo has been redeemed." text is displayed 
	#When I click on "OK" button 
	#Then I verify I am on dashboard 
	
	#COMBINED CASES
	#To verify if the "Available Offers" text appears when the user redirects to redeem reward screen.
	#To verify if Apply button is visible within Available Offers section
	
@AndroidRedeemRwards 
@test12921
Scenario: 
	Given I update the test rail case ID "12921" 
	When I click on "Redeem Rewards" button 
	Then I verfiy "REDEEM" text is displayed 
	And I verfiy "REWARD" text is displayed 
	And I verfiy "AVAILABLE OFFERS" text is displayed 
	And I verfiy Apply button for offer displayed 
	
	