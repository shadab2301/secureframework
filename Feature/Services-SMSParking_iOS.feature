Feature: Verify Services - SMS Parking screen functionality

  Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen

  ############################## Services - SMS Parking #################################################
  #Combine Case
  #Services>>Sms Parking:  verify if the SIMS Parking screen is displayed after clicking on the option
  #Services>>Sms Parking:  verify if the "Hassle free SIMS parking service from FRiENDi mobile. Type your car number followed by the car code and SIMS it to 90091" text is displayed
  #Services>>Sms Parking:  verify if the rates [minutes and price] are displayed
  #Services>>Sms Parking:  verify if the minimum rate is 30Minutes
  
  @SMSParkingJenkins @10321
  Scenario: C7497,C7498,C7499,C7500
    Given I update the test rail case ID "10321"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    When I close the Welcome popup    
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    And I verify user is on dashboard
    When I click on "More" button present on screen
    Then I verify "More" Screen appears
    When I click on "Services" button using value
    Then I verify "Other services" Screen appears
    When I click on "SMS Parking" button present on screen
    Then I verify "SMS Parking" Screen appears
    When I tap on back button
    When I click on "SMS Parking" button present on screen
    Then I verify "SMS Parking" Screen appears
    Then I verify "Hassle free SMS parking service from FRiENDi mobile. Type your car number followed by the car code and SMS it to 90091." text present on screen
    When I scroll upward "3" times
    Then I verify the "30" minutes and ro "RO 0.110" price is displayed
    And I verify the "60" minutes and ro "0.210" price is displayed
    And I verify the "90" minutes and ro "0.310" price is displayed
    And I verify the "120" minutes and ro "0.410" price is displayed
    And I verify the "150" minutes and ro "0.510" price is displayed
    And I verify the "180" minutes and ro "0.610" price is displayed
    And I verify the "210" minutes and ro "0.710" price is displayed
    And I verify the "240" minutes and ro "0.810" price is displayed
    And I verify the "270" minutes and ro "0.910" price is displayed
    And I verify the "300" minutes and ro "1.010" price is displayed
   