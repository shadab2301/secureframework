Feature: Verify Intial Login screen functionality

  Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen

  ################################# Login Promo #################################################
  #Combine Case
  #Dashboard: To verify that the Dashboard promo popup is displayed
  #Dashboard:To verify that the promo pop-up is closed after clicking "X" button
   @LoginPromoJenkins @10152 
  Scenario: [C6563,C6566]
    Given I update the test rail case ID "10152"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    And I verify promo popup coach marks is displayed on screen
    When I close the Welcome popup    
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    And I verify user is on dashboard
 
 #Dashboard:To verify that the promo pop-up is displayed after user refuse the promo
 @LoginPromoJenkins @6567
  Scenario: Dashboard:To verify that the promo pop-up is displayed after user refuse the promo
    Given I update the test rail case ID "6567"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    #Then I verify "Welcome" Screen appears using value
    And I verify promo popup coach marks is displayed on screen
    
 #Dashboard: To verify that Dashboard success climb pop-up is displayed 
 @LoginPromoJenkins @6564
 Scenario: Dashboard: To verify that Dashboard success climb pop-up is displayed 
    Given I update the test rail case ID "6564"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    And I verify promo popup coach marks is displayed on screen
    When I click on claim button in the popup
    And I verify success popup shows 
 
 #Dashboard:To verify that the user is navigated to Dashboard if  promo data is claimed 
@LoginPromoJenkins @6568
Scenario: Dashboard:To verify that the user is navigated to Dashboard if  promo data is claimed 
    Given I update the test rail case ID "6568"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    And I verify promo popup coach marks is displayed on screen  
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    And I verify user is on dashboard

    
    
