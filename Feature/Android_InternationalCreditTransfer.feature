Feature: Verify International Credit Transfer screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	
	#combined cases
	#To verify if the International Credit Transfer screen is displayed
	#To verify if the user is navigated to the Other Services screen after clicking on the Back button from the International Credit transfer screen
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that Top up button navigates to the Top up screen
	#To verify if a field with help text "Transfer credit to..." is displayed on the Credit Transfer screen
	#To verify if the user is able to input data in the field with help text "Transfer credit to..."
	#To verify if the help text will disappear when the user enters data in the field
	#To verify if the field with help text "Transfer credit to..." is having phone number validation
	#International Credit transfer screen - field with help text "Transfer credit to..." - the field is having character limit - min
	#International Credit transfer screen - field with help text "Transfer credit to..." - the field is having character limit - max	
	#To verify if Text "The number should start with the country code in any of these formats: +968 or 00968" is displayed on international credit transfer screen.
	#To verify if a CONTINUE button is displayed on the International Credit Transfer screen
	#To verify if the CONTINUE  button is enabled only when there is enough money in the wallet and entered phone number in the field is in correct format
	#International Credit Transfer - Selection Screen is displayed after clicking on the CONTINUE button 
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that current wallet balance (Total balance) is displayed in the header
	#To verify that Top up button navigates to the Top up screen
	#To verify that phone number is displayed
	#To verify that the International Credit Transfer Screen is displayed with clicking the EDIT button
	#To verify that the default value under the total balance screen is displayed
	#To verify that the dropdown list with predefined values of destination currency amounts should be expanded
	#To verify that the balance that will remain in the wallet if the transfer was executed is displayed
	#To verify if the user is redirected to the summary screen when the user clicks on the Transfer button after selecting the SAR value from the dropdown.
	#To verify that clicking the TRANSFER button ,user navigated to Summary Screen.
	#To verify if a You'll transfer : RO x.xx is displayed
	#To verify if the user is redirected to the verification screen when the user clicks on the Confirm button from Summary screen 
	#To verify if an OTP field is displayed on the screen
	#To verify if the user is able to input data in the OTP field
	#To verify if a CONFIRM button is displayed
@Android_InternationalCreditTransfer 
@test8958 
Scenario: 
	Given I update the test rail case ID "8958" 
	When I click on International Credit Transfer button 
	Then I verify I am on "International Credit Transfer" screen 
	When I tap back button present screen 
	Then I verify I am on "Other services" screen 
	When I click on International Credit Transfer button 
	Then I verfiy "TOTAL BALANCE" text is displayed 
	When I tap on Top up Button present 
	Then I verify I am on "Recharge" screen 
	When I tap back button present screen 
	Then I verfiy "Transfer credit to..." text is displayed 
	And I verify able to input "1234" data in number field 
	And I verify help text disappeared 
	And I verify transfer credit to is having phone number validaton 
	And I verify transfer credit to having minimum and maximum char limit 
	And I verfiy "The international number should start with the country code in any of these formats: +(country code) or 00(country code)" text is displayed 
	And I verify "CONTINUE" button is Displayed 
	When I enter number "966570738031" into number field 
	Then I verify Continue button enabled 
	When I click on "CONTINUE" button 
	Then I verfiy "Maximum amount you can transfer in a month is RO 30." text is displayed 
	And I verfiy "TOTAL BALANCE" text is displayed 
	And I verify current wallet balance display 
	When I tap on Top up Button present 
	Then I verify I am on "Recharge" screen 
	When I tap back button present screen 
	Then I verfiy Mobile number is displayed 
	When I click on "EDIT" button 
	Then I verfiy "The international number should start with the country code in any of these formats: +(country code) or 00(country code)" text is displayed 
	When I click on "CONTINUE" button 
	Then I verify default value display 
	When I choose RO value 
	Then I verify dropdown closes 
	And I verfiy remaining balance text is displayed 
	When I click on "TRANSFER" button 
	Then I verify I am on Summary screen 
	And I verfiy "You’ll transfer" text is displayed 
	When I click on "CONFIRM" button 
	Then I verfiy "Enter the six digit-code you have received in an SMS and confirm verification." text is displayed 
	Then I verify OTP field displayed 
	And I verify able to input "123456" data in OTP field 
	Then I verify "CONFIRM" button is Displayed 
	
	#Combined case
	#To verify if the user is navigated to the native Contacts application after clicking on the button
	#To verify if the phone number is displayed in the field after selecting a number from the native Contacts application
@Android_InternationalCreditTransfer 
@test8957 
Scenario: 
	Given I update the test rail case ID "8957" 
	When I click on International Credit Transfer button 
	When I tap on contact button present on screen 
	Then I verify phone contact screen is displayed 
	And I verify phone number is displayed in the field 
	
	#combined cases
	#To verify that clicking the TRANSFER button a pop up is displayed that the user does not have sufficient balance in the wallet
	#To verify that clicking the CANCEL button, the pop up is closed
	#To verify that clicking the TOP UP button, the user is navigated to the TOP UP screens
@Android_InternationalCreditTransfer 
@test8956 
Scenario: 
	Given I update the test rail case ID "8956" 
	When I click on International Credit Transfer button 
	When I enter number "966570738031" into number field 
	When I click on "CONTINUE" button 
	When I select RO value 
	When I click on transfer button 
	Then I verify insufficient balance popup display 
	When I click on CANCEL button 
	Then I verfiy Maximum amount you can transfer in a month is RO 30 text is displayed 
	When I click on transfer button 
	When I click on RECHARGE button 
	Then I verify I am on Recharge screen 
	
	#combined cases
	#To verify if the International Credit Transfer screen is displayed
	#To verify if an error message with Cancel button is displayed after clicking on the CONTINUE  button with not valid phone number
	#To verify if the error message pop-up will disappear if the user clicks on the OK button
	#To verify if the user is navigated to the Previous Services screen after clicking on the Back button from the International Credit transfer screen
	#International credit transfer:  verify if the user is navigated to the Credit Transfer screen after clicking on the Back button 
	#To verify if the user is navigated to the Credit Transfer Confirmation screen 
	#To verify if the OTP field is having OTP format validation
	#To verify if an error pop-up message that the OTP is not correct is displayed after clicking on the CONFIRM button with invalid OTP
	#International credit transfer: verify if the pop up is closed after clicking on the OK button under Error pop up 
	#To verify if the user will receive an OTP by SMS
@Android_InternationalCreditTransfer 
@test11218 
Scenario: 
	Given I update the test rail case ID "11218" 
	When I click on International Credit Transfer button 
	Then I verify I am on "International Credit Transfer" screen 
	When I enter number "965570738031" into number field 
	When I click on "CONTINUE" button 
	Then I verfiy "Sorry, the credit transfer service is not available to this country/operator at the moment." text is displayed 
	When I click on "OK" button 
	When I enter number "966570738031" into number field 
	When I click on "CONTINUE" button 
	Then I verfiy "Maximum amount you can transfer in a month is RO 30." text is displayed 
	When I tap back button present screen 
	Then I verify i am on previous screen 
	When I click on "CONTINUE" button 
	When I click on "TRANSFER" button 
	Then I verify I am on Summary screen 
	When I tap back button present screen 
	Then I verify default value display 
	When I click on "TRANSFER" button 
	Then I verify I am on Summary screen 
	When I click on "CONFIRM" button 
	Then I verfiy "Enter the six digit-code you have received in an SMS and confirm verification." text is displayed 
	When I tap back button present screen 
	Then I verify I am on Summary screen 
	When I click on "CONFIRM" button 
	Then I verfiy "Enter the six digit-code you have received in an SMS and confirm verification." text is displayed 
	#And I verify if OTP field has format validation 
	When I enter invalid OTP in field 
	When I click on "CONFIRM" button 
	Then I verfiy "The code you’ve entered is not valid. Please check the SMS you received and try again." text is displayed 
	When I click on "OK" button 
	Then I verfiy "Enter the six digit-code you have received in an SMS and confirm verification." text is displayed 
	Then I verify send again button display
	