Feature: Verify More Payment screen functionality

  Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen
    When I tap on EXISTING CUSTOMER button
    When I enter mobile number
    When I tap on next button
    When I enter verification code
    When I click on next button

  #combine Case
  #1.TC01_01 Usage History - Check if the Usage History screen is displayed by clicking on the "History" from the navigation bar
  #2.TC01_02 Usage History - Check if the Help button navigates the user to FAQ section
  #3.TC01_03 Usage History - Check if the user can navigates back to the Usage History screen from the FAQ section when clicks on the back button
  #4.TC02_01 Usage History - Data Usage - Check if the Data Usage screen is displayed by clicking on the "Data" block under usage history section
  @UsageHistory @7977
  Scenario: TC01_01,TC01_02,TC01_03,TC02_02
    Given I update the test rail case ID "7977"
    When I close the Welcome popup
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    When I click on "History" button present on screen
    Then I verify "Usage History" Screen appears
    When I click on help button present
    Then I verify "FAQ" Screen appears
    When I tap on back button
    Then I verify "Usage History" Screen appears
    #Then I verify "Data" text is present on screen using value
