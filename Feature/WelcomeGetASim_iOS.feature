Feature: Verify Welcome get a sim screen functionality

  ################################# Welcome & Get a SIM screens ####################################
  #Combine Case
  #1. Welcome: To verify if there is an "EXISTING CUSTOMER" button displayed on the Welcome screen and if clicking on the "EXISTING CUSTOMER" button user navigates to the sign in screen.
  #2. Welcome: To verify if the Splash screen is displayed once the application is started
  #3. Welcome: To verify if the Welcome screen is displayed once the splash screen is finished
  @WelcomeSignInJenkinsiOS @6573
  Scenario: Welcome: To verify if the Splash screen is displayed once the application is started
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen
    Given I update the test rail case ID "6573"
    When I tap on EXISTING CUSTOMER button
    When I enter mobile number
    When I tap on next button
    When I enter verification code
    When I click on next button
    When I close the Welcome popup
    When I tap for guide popup
    When I tap for guide popup
    When I tap for guide popup
    And I verify user is on dashboard

  # Welcome: To verify if there is an "SEND ME A SIM" button displayed on the Welcome screen and if clicking on the "SEND ME A SIM" button navigates the user to the Get a SIM Screen
  # Get a SIM: To verify that clicking the Back button opens the Welcome Screen is displayed
  # Get a SIM: To verify the user can enter text in the full name field
  # Get a SIM: To verify the user can enter text in the email field
  # Get a SIM: To verify if clicking on "Prefix" code dropdown opens a list with countries
  # Get a SIM: To verify if "GET A SIM" screen is displayed when user click on "X" button.
  # Get a SIM: To verify if the user can input data in the "Find country" field
  # Get a SIM:  To verify if "Selected country prefix code" appears when user selects a country.
  # Get a SIM: To verify if the user can input data in the field
  @WelcomeSignInJenkinsiOS @9896
  Scenario: C6577,C6578,C6579,C6580,C6581,C6582,C6583,C6582,C6583,C6584,C6585
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen
    Given I update the test rail case ID "9896"
    Then I verify SEND ME A SIM button displayed
    When I tap on SEND ME A SIM button
    Then I verify "Get a SIM" Screen appears
    When I tap on back button
    Then I verify I am on splash screen
    And I verify SEND ME A SIM button displayed
    When I tap on SEND ME A SIM button
    Then I verify "Get a SIM" Screen appears
    And I verify user is able to enter "Test100" this name
    And I verify user is able to enter "uattest@mailinator.com" this email
    And I verify user is able to enter "9687956" this number
    #And I verify user is able to enter "riyadh" this location
    When I tap on dropdown Arrow
    Then I verify "Select Country" Screen appears
    And I verify user can enter "afghanistan" this country name
    And I verify "Afghanistan" Screen appears using value
    When I click on "Afghanistan" button using value
    Then I verify "+93" Screen appears using value
