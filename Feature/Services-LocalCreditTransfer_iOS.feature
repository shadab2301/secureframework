Feature: Verify Local credit transfer screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	############################## Services - Local Credit Transfer #################################################
	#Combine Case
	#Services>>Local credit: verify if the Credit Transfer screen is displayed
	#Services>>Local credit: verify if the user is navigated to the Other Services screen after clicking on the Back button from the Credit transfer screen
	#Services>>Local credit: verify if the user is navigated to the popup screen after clickng on the Services>>Local credit:p-up button
	#Services>>Local credit: verify if an RO input field is display on the Credit Transfer screen
	#Services>>Local credit: verify if the user is able to input and view the value within the  RO input value field.
	#Services>>Local credit: verify if a field with help text "Tap in number" is displayed on the Credit Transfer screen
	#Services>>Local credit: verify if the user is able to input data in the field with help text "Tap in number"
	#Services>>Local credit: verify if the help text will disappear when the user enters data in the field
	#Services>>Local credit: verify if the field with help text "Tap in number" is having phone number validation
	#Services>>Local credit: verify if the user is navigated to the native Contacts application after clicking on the button
	#Services>>Local credit: verify if the phone number is displayed in the field after selecting a number from the native Contacts application
	#Services>>Local credit: verify if a TRANSFER button is displayed on the Credit Transfer screen
	#Services>>Local credit: verify if the entered amount is above my wallet balance and the TRANSFER button is disabled, user will see an error message
	#Services>>Local credit: verify if the TRANSFER button is enabled only when there is enough money in the wallet and entered phone number in the field is in correct format
	#Services>>Local credit: verify if an error message with Cancel button is displayed after clicking on the TRANSFER button with not valid phone number
	#Services>>Local credit: verify if the error message pop-up will disappear if the user clicks on the Ok button
	#Services>>Local credit: verify if the user is navigated to the Credit Transfer Confirmation screen after clicking on the TRANSFER button 
	#Services>>Local credit: verify if the user is navigated to the Credit Transfer screen after clicking on the Back button 
	#Services>>Local credit: verify if a total Amount drop-down is displayed
	#Services>>Local credit: verify if the total amount details are displayed after clicking on the credit total Amount drop-down
	#Services>>Local credit: verify if a CONFIRM button is displayed on the screen
	#Services>>Local credit: verify if the user is navigated to the Credit Transfer Confirmation screen 
	#Services>>Local credit: verify if an OTP field is displayed on the screen
	#Services>>Local credit: verify if the user is able to input data in the OTP field
	#Services>>Local credit: verify if the user is navigated to the Credit Transfer Confirmation screen 
	
@LocalCreditTransferJenkins @10150 
Scenario: 
	[C7447,C7448,C7449,C7450,C7451,C7452,C7453,C7454,C7455,C7456,C7457,C7458,C7459,C7460,C7461,C7462,C7463,C7464,C7465,C7466,C7467,C7468,C7469] 
	Given I update the test rail case ID "10150" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	And I verify user is on dashboard 
	When I tap for guide popup 
	When I tap for guide popup
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "Local Credit Transfer" button present on screen 
	Then I verify "Credit transfer" Screen appears 
	When I tap on back button 
	When I click on "Local Credit Transfer" button present on screen 
	Then I verify "Credit transfer" Screen appears 
	And I verify user can enter input in ro 
	And I verify "Tap in number" text is present on screen using value 
	And I verify user can enter "12345678910" this data in the tap in number field 
	And I verify the help text in the field in disappeared 
	And I verify the tranfer button is disabled 
	When I click on plus button 
	Then I verify "Recharge" Screen appears 
	When I tap on back button 
	Then I verify contacts button is displayed 
	When I click on contacts button 
	Then I verify "Contacts" Screen appears 
	When I click on "Anna Haro" button present on screen 
	Then I verify "Credit transfer" Screen appears 
	And I verify "+9685555228243" text is present on screen using value 
	Then I verify "TRANSFER" button is present on screen 
	When I enter this "999999999999999" value in ro field 
	Then I verify "Sorry, you don’t have sufficient credit in your main account" text is present on screen using value 
	When I enter this "1000" value in ro field 
	Then I verify user can enter "96879566999" this data in the tap in number field 
	And I verify the tranfer button is enabled 
	When I click on "TRANSFER" button present on screen 
	Then I verify "Error" text present on screen 
	And I verify "Oops! You can only transfer credit to a FRiENDi mobile customer. (-575)" text is present on screen using value 
	When I click on "Close" button present on screen 
	Then I verify "Credit transfer" Screen appears
	Then I verify user can enter "96879566458" this data in the tap in number field 
	And I verify the tranfer button is enabled 
	When I click on "TRANSFER" button present on screen 
	Then I verify "Summary" Screen appears 
	And I verify total amount dropdown is displayed 
	When I click on total amount dropdown 
	Then I verify "Transfer fees" text is present on screen using value 
	#When I click on "CONFIRM" button present on screen
	#Then I verify "Verification" Screen appears
	#When I tap on back button
	#Then I verify "Summary" Screen appears
	
	#Combined cases
	#To verify if an OTP field is displayed on the screen
	#To verify if the user is able to input data in the OTP field
	#To verify if the OTP field is having OTP format validation
	#To verify if a CONFIRM button is displayed
	#To verify if an error message that the OTP is not correct is displayed after clicking on the CONFIRM button with invalid OTP
	#To verify if the user is navigated to the Dashboard after clicking on the CONFIRM button with valid OTP
	
@LocalCreditTransferJenkins 
@test10748 
Scenario: 
	Given I update the test rail case ID "10748" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number in field 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	And I verify user is on dashboard 
	When I tap for guide popup 
	When I tap for guide popup
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "Local Credit Transfer" button present on screen 
	Then I verify "Credit transfer" Screen appears 
	When I enter this "1000" value in ro field 
	Then I verify user can enter "96879566458" this data in the tap in number field 
	When I click on "TRANSFER" button present on screen 
	Then I verify "Summary" Screen appears 
	When I click on "CONFIRM" button present on screen 
	Then I verify "Verification" Screen appears 
	Then I verify OTP field display 
	And I verify able to input data in OTP field 
	#And I verify if OTP field has character validation 
	Then I verify "NEXT" text present on screen 
	When I enter invalid OTP in OTP field 
	When I click on "NEXT" button present on screen 
	Then I verify "Error" text is present on screen using value 
	When I click on "OK" button present on screen 
	Then I verify "Verification" Screen appears 
	When I enter valid OTP in OTP field 
	When I click on "NEXT" button present on screen 
	Then I verify user is on dashboard 