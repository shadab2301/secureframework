Feature: Verify Services-SMS Parking screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	
	#Combined cases
	#To verify if the SMS Parking screen is displayed after clicking on the option
	#To verify if the "Hassle free SMS parking service from FRiENDi mobile. Type your car number followed by the car code and SMS it to 90091" text is displayed
	#To verify if the minimum rate is 30Minutes
	#To verify if the rates [minutes and price] are displayed
@Android_SMSParking 
@test8304 
Scenario: 
	Given I update the test rail case ID "8304" 
	When I scroll the screen upwards 
	When I click on "SMS Parking" button 
	Then I verify I am on "SMS Parking" screen 
	Then I verfiy "Hassle free SMS parking service from FRiENDi mobile. Type your car number followed by the car code and SMS it to 90091." text is displayed 
	Then I verfiy "Minimum parking time: 30 Minutes" text is displayed 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	Then I verify Rates Minutes and price display 
	
	