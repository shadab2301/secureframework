Feature: Verify Plans screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	
	#Combined Cases
	#To verify that the Service Subtype Slider Coach Mark is displayed
	#To verify that the Pack Coach Mark is displayed
	#To verify that the Coach Mark are not displayed
@Test11992 
@Plans 
Scenario: 
	Given I update the test rail case ID "11992" 
	When I tap on Plans option 
	Then I verify Service Subtype Slider Coach Mark is displayed 
	When I tap on screen 
	Then I verify that the Pack Coach Mark is displayed 
	When I tap on screen 
	Then I verify that the Coach Mark Disappears 
	
	#Combined Cases
	#To verify that the Plan Details Coach Mark is displayed
	#To verify that the Coach Mark are not displayed
@Test11993 
@Plans 
Scenario: 
	Given I update the test rail case ID "11993" 
	When I tap on Buy button option 
	When I click any pack Type 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on one of the offers 
	Then I verify plan details coach mark Displayed 
	When I tap for guide popup 
	Then I verify that the Coach Mark Disappears 
	
	#Combined Cases
	#To verify that the "BUY" button is displayed
	#To verify that the "BUY" button opens more options of pack types
@Test11994 
@Plans 
Scenario: 
	Given I update the test rail case ID "11994" 
	Then I verify buy plan button displayed 
	When I tap on Buy button option 
	Then I verify it opens more options of pack types 
	
	#Combined Cases
	#To verify that the "Data" pack is displayed
	#To verify that the "Data" pack navigates to Plans screen with Data tab selected
@Test11995 
@Plans 
Scenario: 
	Given I update the test rail case ID "11995" 
	When I tap on Buy button option 
	Then I verify data pack displayed 
	When I tap on data pack button 
	Then I verify pack navigates to Plans screen with Data tab selected 
	
	#Combined Cases
	#To verify that the "Local Calls" pack is displayed
	#To verify that the "Local Calls" pack navigates to Plans screen with Call tab selected
@Test11996 
@Plans 
Scenario: 
	Given I update the test rail case ID "11996" 
	When I tap on Buy button option 
	Then I verify local calls pack displayed 
	When I tap on local calls button 
	Then I verify pack navigates to Plans screen with local calls tab selected 
	
	#Combined Cases
	#To verify that the "International Calls" pack is displayed
	#To verify that the "International Calls" button navigates to Plans screen with Call tab selected
@Test11997 
@Plans 
Scenario: 
	Given I update the test rail case ID "11997" 
	When I tap on Buy button option 
	Then I verify international calls pack displayed 
	When I tap on international calls button 
	Then I verify pack navigates to Plans screen with international calls tab selected 
	
	#Combined Cases
	#To verify that the "Mix" pack is displayed
	#To verify that the "Mix" button navigates to Plans screen with Mix tab selected
@Test11998 
@Plans 
Scenario: 
	Given I update the test rail case ID "11998" 
	When I tap on Buy button option 
	Then I verify mix pack displayed 
	When I tap on mix button 
	Then I verify pack navigates to Plans screen with mix tab selected 
	
	#Combined Cases
	#To verify that the Plans screen is displayed
	#To verify if the 'Mobile' text is displayed in the header
	#To verify if the MSISDN is displayed in the header
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that current wallet balance is displayed in the header
	#To verify that Top up button is displayed in the header
	#To verify that Top up button navigates to the Top up screen
@Test11999 
@Plans 
Scenario: 
	Given I update the test rail case ID "11999" 
	When I tap on Buy button option 
	When I tap on data pack button 
	Then I verify Plan Screen displayed 
	And I verify "Mobile" text displayed 
	And I verify if the MSISDN is displayed in the header 
	And I verify TOTAL BALANCE text displayed 
	And I verify current wallte balance displayed 
	And I verify that Top up button is displayed 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on Top up button 
	Then I verify I am on "Recharge" screen 
	
	#combined cases
	#To verify that Data tab is displayed
	#To verify that Data tab displays Data packs
	#To verify if the validity, allowance and price are displayed for all packs
	#DataPacks-To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify that Minutes tab is displayed
	#To verify that Minutes tab displays Call packs
	#To verify if the validity, allowance and price are displayed for all packs
	#CallPacks-To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify that Mix tab is displayed
	#To verify that Mix tab displays Mix packs
	#To verify if the validity, allowance and price are displayed for all packs
	#MixPacks-To verify if the user is navigated to the Summary screen after clicking on the pack from the list
@Test12000 
@Plans 
Scenario: 
	Given I update the test rail case ID "12000" 
	When I tap on Buy button option 
	When I tap on data pack button 
	Then I verify data tab displayed 
	And I verify Data tab displays Data packs 
	And I verify for data validity, allowance and price displayed 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	When I refresh the screen 
	When I tap summary screen back button 
	Then I verify Minutes tab is displayed 
	When I tap on minutes tab 
	Then I verify Minutes tab displays Call packs 
	And I verify for calls validity, allowance and price displayed 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	When I refresh the screen 
	When I tap summary screen back button 
	Then I verify that Mix tab is displayed 
	When I tap on Mix tab 
	Then I verify Mix tab displays Mix packs 
	And I verify for Mix packs validity, allowance and price displayed 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	
	#combined cases
	#To verify that carousel is displayed
	#To verify that carousel can be scrolled
	
@Test12001 
@Plans 
Scenario: 
	Given I update the test rail case ID "12001" 
	When I tap on Buy button option 
	When I tap on data pack button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify carousel is displayed 
	When I scroll the carousel 
	Then I verify i am able to scroll carousel 
	
	#Combined Cases
	#To verify if the data pack category "All Data Plans" is displayed
	#To verify if the data packs from the All Data Plans category are displayed
	#To verify if the data pack category "Freedom Data Plans" is displayed
	#To verify if the data packs from the Freedom Data Plans category are displayed
	#To verify if the data pack category "Saver Data Plans" is displayed
	#To verify if the data packs from the Saver Data Plans category are displayed
	
@Test12002 
@Plans 
Scenario: 
	Given I update the test rail case ID "12002" 
	When I tap on Buy button option 
	When I tap on data pack button 
	Then I verify data pack category All Data Plans is displayed 
	And I verify All data packs displayed 
	When I tap on Freedom Data Plans button 
	Then I verify data pack category Freedom Data Plans is displayed 
	And I verify Freedom data packs displayed 
	When I tap on Saver Data Plans button 
	Then I verify data pack category Saver Data Plans is displayed 
	And I verify Saver data packs displayed 
	
	#Combined Cases
	#To verify if the call pack category "Freedom local calls" and "Saver local calls" are displayed
	#To verify if the call packs from the Freedom local calls category are displayed
	#To verify if the call packs from the Saver local calls category are displayed
	#To verify if the call pack category "International Calls" is displayed
	#To verify if the call packs from the International Calls category should not be displayed
	#To verify if a Select a country button is displayed under the  International Calls category    
	#To verify if the user is navigated to the Select Country after clicking on the Select a country button 
	#To verify if the Find country search field is having character validation
	#To verify if a X button is displayed in the header of the Select Country screen                                                                                    
	#To verify if the Packs screen is displayed after clicking on the X button from the Select Country screen
	#To verify if a "Find country" search field is displayed on the Select Country screen
	#To verify if the list of countries is displayed under the Find country search field
	#To verify if the user is able to input data in the Find country search field
	#To verify if the user is navigated to the Country screen whit only PAYG rate by clicking on the country from the list
	#To verify if the search process is activated when the user enters data in the Find country search field
	#To verify if there is only the list of countries that are matching with data entered in the search field when the user is searching
	#To verify if the user is navigated to the Packs screen by clicking on the country from the list
	#To verify if the user is navigated to the pack screen with available plans by clicking on the country from the list
	#To verify if the drop down for the included countries is displayed
	#To verify if the drop down for the included countries shows the included countries in the pack
@Test12003 
@Plans 
Scenario: 
	Given I update the test rail case ID "12003" 
	When I tap on Buy button option 
	When I tap on data pack button 
	When I tap on minutes tab 
	Then I verify call pack category Freedom Calls is displayed 
	And I verify Freedom call packs displayed 
	When I tap on Saver Calls Plans button 
	Then I verify call pack category Saver Calls Plans is displayed 
	And I verify Saver call packs displayed 
	When I tap on International Call button 
	Then I verify call pack category International Calls is displayed 
	And I verify international call packs should not be displayed 
	And I verify Select a country button is displayed 
	When I tap on Select a country button 
	Then I verify I am on "Select Country" screen 
	And I verify search bar has character validation 
	And I verify cross button displayed 
	When I click on the X button from the Select Country screen 
	Then I verify Plan Screen displayed 
	When I tap on Select a country button 
	Then I verify if a Find country search field is displayed 
	And I verify the list of countries is displayed 
	When I type "India" in the textfield displayed 
	Then I verify user can input data in the find country field 
	And I verify that the search list with "YEMEN" in it 
	When I enter "Japan" country in enter country field 
	When I select country 
	Then I verfiy "Peak 6AM-6PM" text is displayed 
	And I verfiy "Off-Peak" text is displayed 
	When I tap on back button present screen 
	#And I verify All countries statrts with "N" is listed on the screen 
	Then I verify I am on "Select Country" screen 
	When I tap on one of the country from list 
	Then I verify Plan Screen displayed 
	And I verify selected country display on the pack screen with available packs 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	Then I verify "Included" button is Displayed 
	When I click on "Included" button 
	Then I verify included country display 
	
	#Combined cases
	#To verify if the mix pack category "Freedom Mix Plans" is displayed
	#To verify if the mix pack category "Saver Mix Plans" is displayed
	#To verify if the data packs from the Saver Mix Plans category are displayed
	#To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify if back button is displayed on Summary screen
	#To verify if clicking the Back button navigates to Packs screen
@Test12004 
@Plans 
Scenario: 
	Given I update the test rail case ID "12004" 
	When I tap on Buy button option 
	When I tap on mix button 
	#Then I verify Mix pack category freedom mix plans is displayed 
	#When I tap on Saver Mix Plans button 
	Then I verify Mix pack category Saver mix plans is displayed 
	And I verify Saver Mix packs displayed 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	And I verify back button displayed 
	When I tap on back button present screen 
	Then I verify Plan Screen displayed 
	
	
	#COMBINED CASES
	#To verify if Summary screen is displayed when the user select pack
	#To verify if back button is displayed on Summary screen
	#To verify if clicking the Back button navigates to Packs screen
	#To verify if the selected pack is displayed
	#To verify if the "PLAN DETAILS" button is displayed
	#To verify if the "PLAN DETAILS" button opens a PLAN DETAILS screen with the details of the plan
	#To verify that the X icon is displayed on the Plan Details Screen .
	#To verify that clicking the X icon, the Summary Screen is displayed
	#To verify that clicking the  device back button, the Summary Screen is displayed
	#To verify if the "CONFIRM" button is displayed
	#To verify if Confirmation pop up is displayed when the user has enough wallet balance
	#To verify if the Done button is displayed over the Confirmation pup
	#To verify if the Done button is clicked over the Confirmation pup
	#To verify if the user can buy Data pack
	#To verify if the user can buy the same Data pack twice
	#To verify if the user can buy two different Data Packs
@Test12005 
@Plans 
Scenario: 
	Given I update the test rail case ID "12005" 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap on Buy button option 
	When I tap on data pack button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on one of the offers 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	When I refresh the screen 
	Then I verify summary back button displayed 
	When I tap summary screen back button 
	Then I verify Plan Screen displayed 
	When I tap on one of the offers 
	Then I verify selected pack display 
	And I verify "PLAN DETAILS" button is Displayed 
	When I click on "PLAN DETAILS" button 
	Then I verify I am on "Plan Details" screen 
	And I verify cross button display 
	When I tap on cross Button 
	Then I verify I am on "Summary" screen 
	When I click on "PLAN DETAILS" button 
	When I tap on device back button 
	Then I verify I am on "Summary" screen 
	And I verify "CONFIRM" button is Displayed 
	When I click on "CONFIRM" button 
	Then I verfiy "Confirmation" text is displayed 
	And I verify "DONE" button is Displayed 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy purchased data pack is displayed 
	When I click on "Plans" button 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy the same Data pack twice 
	When I click on "Plans" button 
	When I tap on different data offer 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy two different Data Packs 
	
	#combined cases
	#To verify if the user can buy Freedom Local Calls pack
	#To verify if the user can buy the same Freedom Local Calls pack twice
	#To verify if the user can buy two different Freedom Local Calls Packs
@Test12375
@Plans 
Scenario: 
	Given I update the test rail case ID "12375" 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I click on "Plans" button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on minutes tab 
	When I tap on one of the offers 
	When I tap for guide popup 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I tap on Local call Service type from graph 
	Then I verfiy purchased Freedom local calls pack is displayed 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy the same Freedom local calls pack twice 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on different Freedom call offer 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy two different Freedom call Packs 
	
	#combined cases
	#To verify if the user can buy Saver Local Calls pack
	#To verify if the user can buy the same Saver Local Calls pack twice
	#To verify if the user can buy two different Saver Local Calls Packs
@Test12376 
@Plans 
Scenario: 
	Given I update the test rail case ID "12376" 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I click on "Plans" button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on minutes tab 
	When I tap on Saver Calls Plans button 
	When I tap on one of the offers 
	When I tap for guide popup 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I tap on Local call Service type from graph 
	Then I verfiy purchased Saver local calls pack is displayed 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on Saver Calls Plans button 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy the same Saver local calls pack twice 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on Saver Calls Plans button 
	When I tap on different Saver call offer 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy two different Saver call Packs 
	
	#combined cases
	#To verify if the user can buy International Calls pack
	#To verify if the user can buy the same International Calls pack twice
	#To verify if the user can buy two different International Calls Packs
@Test12377 
@Plans 
Scenario: 
	Given I update the test rail case ID "12377" 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I click on "Plans" button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on minutes tab 
	When I tap on International Call button 
	When I tap on Select a country button 
	When I enter "Pakistan" country in enter country field 
	When I select country 
	When I tap on one of the offers 
	When I tap for guide popup 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I tap on international call Service type from graph 
	Then I verfiy purchased International call pack is displayed 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on International Call button 
	When I tap on Select a country button 
	When I enter "Pakistan" country in enter country field 
	When I select country 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy the same International call pack twice 
	When I click on "Plans" button 
	When I tap on minutes tab 
	When I tap on International Call button 
	When I tap on Select a country button 
	When I enter "Bangladesh" country in enter country field 
	When I select country 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy two different International call Packs 
	
	#combined cases
	#To verify if the user can buy Mix pack
	#To verify if the user can buy the same Mix pack twice
	#To verify if the user can buy two different Mix Packs
@Test12378 
@Plans 
Scenario: 
	Given I update the test rail case ID "12378" 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap on Buy button option 
	When I tap on mix button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on Mix tab 
	Then I verify Mix tab displays Mix packs 
	When I tap on one of the offers 
	When I tap for guide popup 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I tap on Data Service type from graph 
	Then I verfiy purchased Mix call pack is displayed 
	When I tap on Buy button option 
	When I tap on mix button 
	When I tap on Mix tab 
	When I tap on one of the offers 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy the same Mix call pack twice 
	When I tap on Buy button option 
	When I tap on mix button 
	When I tap on Mix tab 
	When I tap on different Mix call offer 
	When I click on "CONFIRM" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	Then I verfiy can buy two different Mix call Packs 
	
	#combined cases
	#To verify if top up wallet pop up is displayed when the wallet balance is lower than pack plan price
	#To verify if the "RECHARGE" button is displayed
	#To verify if the "CANCEL" button is displayed
	#To verify if the "CANCEL" button closes the pop up
	#To verify if the "RECHARGE" button navigates to Top-up screen	
	
@Test12006 
@Plans 
Scenario: 
	Given I update the test rail case ID "12006" 
	When I tap on Buy button option 
	When I tap on local calls button 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap on Mix tab 
	When I tap on offer more than wallet balance 
	Then I verify I am on "Summary" screen 
	When I tap for guide popup 
	When I click on "CONFIRM" button 
	Then I verify Recharge pop up display 
	And I verify "RECHARGE" button is Displayed 
	And I verify "CANCEL" button is Displayed 
	When I click on "CANCEL" button 
	And I verify "PLAN DETAILS" button is Displayed 
	When I click on "CONFIRM" button 
	When I click on "RECHARGE" button 
	Then I verify I am on "Recharge" screen 