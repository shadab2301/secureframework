Feature: Verify Sign In  screen functionality 

#Combined cases
#To verify that the Sign In screen is displayed 
#To verify that clicking the Back button opens the Welcome Screen 
#To verify that clicking the Help icon opens the Help Screen 
#To verify that the 'Mobile number' field is displayed 
#To verify that the help text 'Mobile number' in field is displayed 
#To verify that inputing data in the field removes the help text 'Mobile number'. 
#To verify that inputing data in the field removes the help text 'Mobile number'  when the user enter data in the field
#To verify clicking the NEXT button an error is displayed
#To verify that clicking the Face ID/Fingerprint icon opens Warning Pop up 
#To verify that clicking the OK button closes the Warning Pop up
#To verify the Legal stuff link is displayed and if clicking the Legal Stuff link opens the Legal Stuff Screen
#To verify the Legal stuff text is displayed on top of the screen
#To verify the help icon is displayed on top right corner and if clicking user redirect to support screen
#To verify the (X) button is displayed and if clicking the X button user redirect to sign In screen
#To verify if clicking the device back button user redirect to Sign In screen
#To verify clicking the NEXT button the Verification Screen is displayed 
#To verify that clicking the Back button opens the Login Screen
#To verify that clicking the Help icon opens the Support Screen
#To verify that clicking the X button closes the support screen
#To verify if the user can input data in the OTP field
#To verify that clicking the NEXT button logs in the user

@Test9888 
@Login 
Scenario: 
	Given I update the test rail case ID "9888" 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	Then I verify I am on "Sign In" screen 
	When I tap on back button 
	Then I verify Welcome screen is displayed 
	When I tap on EXISTING CUSTOMER button 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I tap on Cross button 
	Then I verify mobile number field display 
	Then I verify help text displayed 
	When I enter "96874411" mobile number 
	When I tap on next button 
	Then I verfiy "This number is not an active FRiENDi mobile number. Please try again." text is displayed 
	When I click on "OK" button 
	When I enter mobile number 
	Then I verify help text removed 
	#When I tap on fingerprint icon 
	#Then I verify warning pop up displayed 
	#When I click on "OK" button 
	When I click on "LEGAL STUFF" button 
	Then I verify I am on "Legal Stuff" screen 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I tap on Cross button 
	When I tap on Cross button 
	Then I verify I am on "Sign In" screen 
	When I click on "LEGAL STUFF" button 
	When I tap on device back button 
	Then I verify I am on "Sign In" screen 
	When I tap on next button 
	Then I verify I am on "Verification" screen 
	When I tap on back button present screen 
	Then I verify I am on "Sign In" screen 
	When I enter mobile number 
	When I tap on next button 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I tap on Cross button 
	Then I verify I am on "Verification" screen 
	And I verify able to input "123456" data in OTP field 
	When I enter verification code 
	When I click on next button 
	Then I verify I am on dashboard 
	
	#Combined cases
	#To verify that clicking the I'LL USE MY PASSWORD button opens the Sign in Screen for logging in with MSISDN and password 
	#Check if Password button is having "Enter password" help text
	#To verify the user can enter password to login
	#To verify the user can see the password when the Unmask button is clicked.
	#To verify the user can redirect to the dashboard screen successfully when the valid password is entered.
	
@Test9893 
@Login 
Scenario: 
	Given I update the test rail case ID "9893" 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter "79570784" mobile number 
	When I tap on next button 
	Then I verify I am on "Verification" screen 
	When I click "I’LL USE MY PASSWORD" text present on the screen 
	Then I verify I am on "Sign In" screen 
	Then I verify help text for password field is displayed 
	And I verify able to enter in password field 
	And I verify mask button mask and unmask the password 
	When I click on "SIGN IN" button
	Then I verify I am on dashboard 
	
	
	