Feature: Verify More Account Setting screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	#Combined cases
	#To verify if the Account settings button within More section of the Menu is clickable or not and the user is redirected to the settings screen when the user click on the Account settings button.
	#To verify If there is a Change Password button displayed in the Settings screen
	#To verify If user is redirected to Fingerprint screen 
	#To verify if the Want to use touch ID/ fingerprint through the app? text is displayed.
	#To verify if the Password field is displayed.
	#To verify if the User is successfully able to input password in the field.
	#To verify if the User is successfully able to mask/unmask the entered Password.
	#To verify if the Confirm button is displayed within fingerprint screen.
	#To verify if the Confirm button is enabled for valid password.
	#To verify if the Settings screen is displayed when user click on back arrow(<) / cancel button.
	#To verify if the Access your dashboard using your fingerprint text is displayed within settings screen.
	#To verify if the Dark Mode section within setting screen is displayed.
	#To verify if the System drop down is clickable in dark mode section.
	#To verify if the pop up is displayed when click on system drop down.
	#To verify if the pop up disappear when click on Cancel/Done button over system popup.
	#To verify if the Selected value in popup is displayed in drop down.
	#To verify if the Open source libraries option is displayed and the libraries we use screen is displayed.
	#To verify if the Settings screen is displayed.
	#To verify if the Crafted with care by your friendi mobile cyborgs text is displayed.
	#To verify if the v2.xx.0- UAT -OMAN # xxx text is displayed
	#To verify if the More screen is displayed.
	#To verify if the User is successfully logout with the id used.
@MoreAccountSetting 
Scenario: 
	Given I update the test rail case ID "2" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Settings" text is displayed 
	When I click on "Settings" button 
	Then I verify I am on "Settings" screen 
	And I verify "Change password" button is Displayed 
	When I click on "Change password" button 
	Then I verify I am on "Change password" screen 
	When I tap back button present screen 
	Then I verify I am on "Settings" screen 
	#And I verify "Fingerprint" button is Displayed 
	#When I tap on toggle button 
	#Then I verify I am on "Fingerprint" screen 
	#And I verfiy "Want to use your fingerprint through the app? Please enter your password" text is displayed 
	#And I verify the Password field is displayed 
	#And I verify i am able to enter "12345" Text in Password field 
	#And I verify mask button mask and unmask the password 
	#And I verify "CONFIRM" button is Displayed 
	#When I entered valid password "Test12345" in Password field 
	#Then I verify confirm button enabled 
	#When I tap back button present screen 
	Then I verify I am on "Settings" screen 
	#And I verfiy "Access your dashboard using your fingerprint" text is displayed 
	And I verfiy "Dark Mode" text is displayed 
	When I click on "System" button 
	Then I verify a pop up opens for theme 
	When I click on Cancel button over system popup 
	Then I verify I am on "Settings" screen 
	When I click on "System" button 
	When I click on "Always Light" button 
	Then I verify Selected value in popup is displayed in drop down 
	And I verfiy "Open Source Libraries" text is displayed 
	When I click on "Open Source Libraries" button 
	Then I verify I am on Libraries We Use screen 
	When I tap back button present screen 
	Then I verify I am on "Settings" screen 
	And I verfiy "Crafted with care by your FRiENDi mobile Cyborgs" text is displayed 
	And I verify App version is displayed 
	When I tap back button present screen 
	Then I verfiy "Services" text is displayed 
	When I click on "Settings" button 
	Then I verify "Log out" button is Displayed 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	
	
	
	
	
	
	
	