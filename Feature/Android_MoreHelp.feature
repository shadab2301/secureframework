Feature: Verify More Help screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Help center" text is displayed 
	
	#Combined Cases
	#To verify the user gets redirected to the help screen when the user click on the help icon under more screen
	#To verify if clicking the 'X' button closes the Help screen
	#To verify if the search bar get open with the blinking cursor in it on clicking the search button
	#To verify if the user is seeing the suggested and popular searches (before starting the typing)
	#To verify if there is "Back" button next to the search bar
	#To verify if clicking the "back" button closes the search bar
	#To verify if categories are displayed under the header
	#To verify if user is able to click on the first category all the categories option "Choosing FRiNDi mobile" "FRiENDi mobile stores/Xpress Counters" "Join and keep your number" "Creating your account" Option displayed
	#To verify if the user is able to observe the FAQs under every category and the user is redirected to the heading of the FAQ
	#To verify when the FAQ heading is opened from the subcategory than the category title in the header, the Back button and the answer displayed on the screen
@MoreHelp 
@test7787 
Scenario: 
	Given I update the test rail case ID "7787" 
	When I click on "Help center" button 
	Then I verify I am on "FAQ" screen 
	When I tap on Cross button 
	Then I verfiy "Services" text is displayed 
	When I click on "Help center" button 
	When I tap on search icon 
	Then I verify search field display 
	Then I verfiy "Suggested" text is displayed 
	Then I verfiy "Popular" text is displayed 
	Then I verify back button displayed 
	When I tap back button present screen 
	Then I verify I am on "FAQ" screen 
	Then I verify all categories displayed 
	When I click on "Getting Started" button 
	Then I verify all Subcategories displayed 
	When I click "Choosing FRiENDi mobile" text present on the screen 
	Then I verfiy "Why choose FRiENDi mobile?" text is displayed 
	When I click "Why choose FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	
@test7523 
@MoreHelp 
Scenario: 
	To verify if the user is able select an item from the suggested searches section 
	Given I update the test rail case ID "7523" 
	When I click on "Help center" button 
	When I tap on search icon 
	When I click "How do I port-in my number to FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	
@test7524 
@MoreHelp 
Scenario: 
	To verify if the user is able select an item from the popular searches section 
	Given I update the test rail case ID "7524" 
	When I click on "Help center" button 
	When I tap on search icon 
	When I click "I lost my SIM card, what should I do?" text present on the screen 
	Then I verify Question Answser scren display 
	
	
@test7525 
@MoreHelp 
Scenario: To verify if the user is able to input data in the search field 
	Given I update the test rail case ID "7525" 
	When I click on "Help center" button 
	When I tap on search icon 
	Then I verify i am able to enter "plan" in search field 
	
	#combined cases
	#To verify if the user is unable to search with the invalid data in the search bar and the error message appears also "Talk to us" button appears below the error message
	#To verify the popup to dial the number appear on the search screen when the user clicks at "Talk to us" button
	
@test7973 
@MoreHelp 
Scenario: 
	Given I update the test rail case ID "7973" 
	When I click on "Help center" button 
	When I tap on search icon 
	When I enter invalid data in search field 
	Then I verfiy "We couldn’t find the answer to your question :(" text is displayed 
	Then I verify "TALK TO US" button is Displayed 
	When I click on "TALK TO US" button 
	Then I verify popup to dial the number display 
	
	#combined cases
	#To verify if the user is able to search with the valid data in the search bar and the result is grouped by categories
	#To verify if the user is able to select an item form the result that appears after entering the valid data
	
@test7975 
@MoreHelp 
Scenario: 
	Given I update the test rail case ID "7975" 
	When I click on "Help center" button 
	When I tap on search icon 
	When I enter valid data in search field 
	Then I verify result is gouped by categories displayed 
	When I select an item from search list 
	Then I verify Question Answser scren display 
	
	#Combined Cases
	#To verify if the user redirected to the previous screen on clicking the back button next to the category
	#To verify the user is redirected to the FAQ heading by clicking the back button from the device 
	#To verify the user is redirected to the sub-category by clicking the back button from the FAQ's heading screen
	#To verify the user is redirected to the sub-category by clicking the device back button from the answers screen
	#To verify the user is redirected to the FAQ heading by clicking the back button from the answers screen 
	#To verify the user is redirected to the sub-category by clicking the device back button from the answers screen
	#verify if "Found your answer? text appears with yes & No buttons and when clicked on the No button, the calling option appears.
	#verify if the calling option disappear when clicked on the Yes button.
	
@test12957 
@MoreHelp 
Scenario: 
	Given I update the test rail case ID "12957" 
	When I click on "Help center" button 
	Then I verify I am on "FAQ" screen 
	When I click on "Getting Started" button 
	Then I verfiy "Choosing FRiENDi mobile" text is displayed 
	When I click on back arrow present on screen 
	Then I verify I am on "FAQ" screen 
	When I click on "Getting Started" button 
	Then I verfiy "Choosing FRiENDi mobile" text is displayed 
	When I tap on device back button 
	Then I verify I am on "FAQ" screen 
	When I click on "Getting Started" button 
	When I click "Choosing FRiENDi mobile" text present on the screen 
	Then I verfiy "Why choose FRiENDi mobile?" text is displayed 
	When I click on back arrow present on screen 
	Then I verfiy "Getting Started" text is displayed 
	When I click "Choosing FRiENDi mobile" text present on the screen 
	Then I verfiy "Why choose FRiENDi mobile?" text is displayed 
	When I tap on device back button 
	Then I verfiy "Getting Started" text is displayed 
	When I click "Choosing FRiENDi mobile" text present on the screen 
	Then I verfiy "Why choose FRiENDi mobile?" text is displayed 
	When I click "Why choose FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	When I click on back arrow present on screen 
	Then I verfiy "Choosing FRiENDi mobile" text is displayed 
	When I click "Why choose FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	When I tap on device back button 
	Then I verfiy "Choosing FRiENDi mobile" text is displayed 
	When I click "Why choose FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	Then I verfiy "Found your answer?" text is displayed 
	When I click "NO" text present on the screen 
	Then I verfiy "Sorry.." text is displayed 
	And I verfiy "CALL" text is displayed 
	When I click on back arrow present on screen 
	When I click "Why choose FRiENDi mobile?" text present on the screen 
	Then I verify Question Answser scren display 
	When I click "YES" text present on the screen 
	Then I verify Found your answer text disappears from screen 
	
	
	
