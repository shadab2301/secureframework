Feature: Verify Plans screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	#Combined cases
	#To verify that the Service Subtype Slider Coach Mark is displayed
	#To verify that the Pack Coach Mark is displayed
	#To verify that the Coach Mark are not displayed
	#To verify that the Plan Details Coach Mark is displayed
	#To verify that the Coach Mark are not displayed
	#To verify that the "BUY" button is displayed
	#To verify that the "BUY" button opens more options of pack types
	#To verify that the "Data" pack is displayed
	#To verify that the "Data" pack navigates to Plans screen with Data tab selected
	#To verify that the "Local Calls" pack is displayed
	#To verify that the "Local Calls" pack navigates to Plans screen with Call tab selected
	#To verify that the "International Calls" pack is displayed
	#To verify that the "International Calls" button navigates to Plans screen with Call tab selected
	#To verify that the "Mix" pack is displayed
	#To verify that the "Mix" button navigates to Plans screen with Mix tab selected
	
@PlansiOSJenkins 
@Test12085 
Scenario: 
	Given I update the test rail case ID "12085" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	#Then I verify Plans Screen Display 
	And I verify Service Subtype Slider Coach Mark displayed 
	When I tap for guide popup in plans screen 
	Then I verify Pack coach mark display 
	When I tap for guide popup in plans screen 
	Then I verify Pack coach mark disappear 
	When I tap one of the offers 
	Then I verify Plan Details Coach Mark display 
	When I tap for guide popup in plans screen 
	Then I verify Plan Details coach mark disappear 
	When I tap on back button 
	When I click on "Home" button present on screen 
	Then I verify Buy Plan is present on screen 
	When I click on Buy Plan button 
	And I verify "Local Calls" button is present on screen 
	And I verify "International Calls" button is present on screen 
	And I verify "Mix" button is present on screen 
	When I click on Data button from Buy plan 
	Then I verify Plans Screen Display 
	And I verify Data tab selected 
	When I click on "Home" button present on screen 
	When I click on Buy Plan button 
	When I click on Local Calls button from Buy plan 
	Then I verify Plans Screen Display 
	And I verify Minutes tab selected 
	When I click on "Home" button present on screen 
	When I click on Buy Plan button 
	When I click on International Calls button from Buy plan 
	Then I verify Plans Screen Display 
	And I verify Minutes tab selected 
	When I click on "Home" button present on screen 
	When I click on Buy Plan button 
	When I click on Mix button from Buy plan 
	Then I verify Plans Screen Display 
	And I verify Mix tab selected 
	
	#Combined cases
	#To verify that the Plans screen is displayed
	#To verify if the 'Mobile' text is displayed in the header
	#To verify if the MSISDN is displayed in the header
	#To verify that text "TOTAL BALANCE" is displayed in the header
	#To verify that current wallet balance is displayed in the header
	#To verify that Top up button is displayed in the header
	#To verify that Top up button navigates to the Top up screen
	#To verify that Data tab is displayed
	#To verify that Data tab displays Data packs
	#To verify that Minutes tab is displayed
	#To verify that Minutes tab displays Call packs
	#To verify that Mix tab is displayed
	#To verify that Mix tab displays Mix packs
	#To verify that carousel is displayed
	#To verify that carousel can be scrolled
@PlansiOSJenkins 
@Test12086 
Scenario: 
	Given I update the test rail case ID "12086" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	#And I verify "Mobile" text is present on screen 
	#And I verify MSISDN is displayed in the header 
	And I verify "TOTAL BALANCE" text is present on screen using value 
	And I verify current balance display 
	And I verify plus button display 
	When I click on plus button 
	Then I verify "Recharge" Screen appears 
	When I tap on back button 
	Then I verify Data tab displayed 
	And I verify all data packs display 
	Then I verify Minutes tab displayed 
	When I tap on Minutes tab present 
	And I verify Freedom Calls packs display 
	Then I verify Mix tab displayed 
	When I tap on Mix tab present 
	And I verify all Mix packs display 
	When I tap on Data tab present 
	And I verify that carousel is displayed 
	When I scroll the carousel present on screen 
	Then I verify Freedom Data Plans text is display 
	
	#Combined cases
	#To verify if the data pack category "All Data Plans" is displayed
	#To verify if the data packs from the All Data Plans category are displayed
	#To verify if the validity, allowance and price are displayed for all packs
	#To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify if the data pack category "Freedom Data Plans" is displayed
	#To verify if the data packs from the Freedom Data Plans category are displayed
	#To verify if the data pack category "Saver Data Plans" is displayed
	#To verify if the data packs from the Saver Data Plans category are displayed
	#To verify if the call pack category "Freedom local calls" and "Saver local calls" are displayed
	#To verify if the call packs from the Freedom local calls category are displayed
	#To verify if the validity, allowance and price are displayed for all packs
	#To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify if the call packs from the Saver local calls category are displayed
	#To verify if the call pack category "International Calls" is displayed
	#To verify if the data packs from the International Calls category are  not displayed
	#To verify if a Select a country button is displayed under the  International Calls category
	#To verify if the user is navigated to the Select Country after clicking on the Select a country button
	#To verify if a X button is displayed in the header of the Select Country screen
	#To verify if the Packs screen is displayed after clicking on the X button from the Select Country screen
	#To verify if a "Find country" search field is displayed on the Select Country screen
	#To verify if the user is able to input data in the Find country search field
	#To verify if the search process is activated when the user enters data in the Find country search field
	#To verify if the list of countries is displayed under the Find country search field
	#To verify if the user is navigated to the Packs screen by clicking on the country from the list
	#To verify if there is only the list of countries that are matching with data entered in the search field when the user is searching
	#To verify if the user is navigated to the Packs screen by clicking on the Zone from the list
	#To verify if the user is navigated to the Country screen whit only PAYG rate by clicking on the country from the list
	#To verify if the user is navigated to the packs screen whit available plans by clicking on the country from the list
	#To verify if the drop down for the included countries is displayed
	#To verify if the drop down for the included countries shows the included countries in the pack
	
@PlansiOSJenkins 
@Test12086 
Scenario: 
	Given I update the test rail case ID "12087" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap on All Data Plans button 
	Then I verify All Data Plans text is display 
	And I verify all data packs display 
	And I verify validity, allowance and price are displayed for all data packs 
	When I tap one of the offers 
	When I tap for guide popup in plans screen 
	Then I verify "Summary" Screen appears 
	When I tap on back button 
	When I tap on Freedom Data button 
	Then I verify Freedom Data Plans text is display 
	And I verify Freedom Data packs display 
	When I tap on Saver Data button 
	Then I verify Saver Data Plans text is display 
	And I verify Saver Data packs display 
	When I tap on Minutes tab present 
	Then I verify Freedom Call Plans text is display 
	And I verify Freedom Calls packs display 
	And I verify validity, allowance and price are displayed for all call packs 
	When I tap one of the offers 
	Then I verify "Summary" Screen appears 
	When I tap on back button 
	When I tap on Saver Calls button 
	Then I verify Saver Call Plans text is display 
	And I verify Saver Calls packs display 
	When I tap on International calls button 
	Then I verify International Calls text is display 
	Then I verify International call packs should not be displayed 
	Then I verify Select a country text is present on screen 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I tap on back button 
	Then I verify Plans Screen Display 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	And I verify Enter country search field display 
	#And I verify all countries list displayed 
	And I verify user can enter "INDIA" this country name 
	When I click on "India" button using value 
	Then I verify Plans Screen Display 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I enter "gcc" country name in enter country field present 
	When I click on "GCC" button using value 
	Then I verify Plans Screen Display 
	And I verify GCC text is present on screen 
	When I click on Select a country button 
	When I enter "JAPAN" country name in enter country field present 
	When I click on "Japan" button using value 
	Then I verify "Peak 6AM-6PM" text is present on screen using value 
	And I verify "Off-Peak" text is present on screen using value 
	When I tap on back button 
	Then I verify "Select Country" Screen appears 
	When I enter "United" country name in enter country field present 
	Then I verify enterd country list display onscreen 
	When I select country from list 
	Then I verify Plans Screen Display 
	And I verify United Arab Emirates text is present on screen 
	When I tap one of the offers 
	Then I verify "Included" text is present on screen using value 
	When I click on "Included" button using value 
	And I verify "United Arab Emirates" text is present on screen using value 
	When I tap on back button 
	Then I verify Plans Screen Display 
	
	
	#Combined cases
	#To verify if the mix pack category "Saver Mix Plans" is displayed
	#To verify if the data packs from the Saver Mix Plans category are displayed
	#To verify if the validity, allowance and price are displayed for all packs
	#To verify if the user is navigated to the Summary screen after clicking on the pack from the list
	#To verify if back button is displayed on Summary screen
	#To verify if clicking the Back button navigates to Packs screen
	#To verify if the selected pack is displayed
	#To verify if the "PLAN DETAILS" button is displayed
	#To verify if the "PLAN DETAILS" button opens a PLAN DETAILS screen with the details of the plan
	#To verify that the X icon is displayed on the Plan Details Screen .
	#To verify that clicking the X icon, the Summary Screen is displayed
	#To verify if the "CONFIRM" button is displayed
	#To verify if top up wallet pop up is displayed when the wallet balance is lower than pack plan price
	#To verify if the "RECHARGE" button navigates to Top-up screen
	#To verify if the "CANCEL" button closes the pop up
@PlansiOSJenkins 
@Test12363 
Scenario: 
	Given I update the test rail case ID "12363" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap on Mix tab present 
	Then I verify Saver Mix Plans text is display 
	And I verify all Mix packs display 
	And I verify validity, allowance and price are displayed for all Mix packs 
	When I tap one of the offers 
	When I tap for guide popup 
	Then I verify "Summary" Screen appears 
	When I tap on back button 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	Then I verify "Summary" Screen appears 
	#Then I verify Saver Mix Plans text is display in Summary screen 
	And I verify selected pack is display 
	And I verify "PLAN DETAILS" text is present on screen using value 
	When I click on "PLAN DETAILS" button present on screen 
	Then I verify "Plan Details" Screen appears 
	When I tap on back button 
	Then I verify "Summary" Screen appears 
	And I verify "CONFIRM" button is present on screen 
	When I click on "CONFIRM" button present on screen 
	And I verify "RECHARGE" text is present on screen using value 
	When I click on "RECHARGE" button present on screen 
	Then I verify "Recharge" Screen appears 
	When I tap on back button 
	When I click on "CONFIRM" button present on screen 
	When I click on "CANCEL" button present on screen 
	Then I verify "CONFIRM" button is present on screen 
	
	#Combined cases
	#To verify if the user can buy Data pack
	#To verify if the user can buy the same Data pack twice
	#To verify if the user can buy two different Data Packs
@PlansiOSJenkins 
@Test12368 
Scenario: 
	Given I update the test rail case ID "12368" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	When I tap for guide popup in plans screen 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I scroll the screen upward 
	Then I verify puchased data pack display 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I scroll the screen upward
	Then I verify same data pack display twice 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I select different offer 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I scroll the screen upward
	Then I verify different data pack display 
	
	#Combined cases
	#To verify if the user can buy Freedom Local Calls pack
	#To verify if the user can buy the same Freedom Local Calls pack twice
	#To verify if the user can buy two different Freedom Local Calls Packs
@PlansiOSJenkins 
@Test12369
Scenario: 
	Given I update the test rail case ID "12369" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap on Minutes tab present 
	When I tap one of the offers 
	When I tap for guide popup in plans screen 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify puchased Freedom localcall pack display 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify same Freedom localcalls pack display twice 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I select different offer 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard  
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify different Freedom localcalls pack display 
	
	#Combined cases
	#To verify if the user can buy Saver Local Calls pack
	#To verify if the user can buy the same Saver Local Calls pack twice
	#To verify if the user can buy two different Saver Local Calls Packs	
@PlansiOSJenkins 
@Test12370 
Scenario: 
	Given I update the test rail case ID "12370" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap on Minutes tab present 
	When I tap on Saver Calls button 
	Then I verify Saver Call Plans text is display 
	When I tap one of the offers 
	When I tap for guide popup in plans screen 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify puchased Saver localcall pack display 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap on Saver Calls button 
	Then I verify Saver Call Plans text is display 
	When I tap one of the offers 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify same Saver localcalls pack display twice 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap on Saver Calls button 
	Then I verify Saver Call Plans text is display 
	When I select different offer 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select local calls option from graph 
	When I scroll the screen upward
	Then I verify different Saver localcalls pack display 
	
	#Combined cases
	#To verify if the user can buy International Calls pack
	#To verify if the user can buy the same International Calls pack twice
	#To verify if the user can buy two different International Calls Packs
	#To verify if the user can buy International Calls Zone pack
@PlansiOSJenkins 
@Test12371 @Test
Scenario: 
	Given I update the test rail case ID "12371" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "Plans" button present on screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	When I tap for guide popup in plans screen 
	Then I verify Plans Screen Display 
	When I tap on Minutes tab present 
	When I tap on International calls button 
	Then I verify International Calls text is display 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I enter "United" country name in enter country field present 
	When I select country from list 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	When I tap for guide popup in plans screen 
	Then I verify "Summary" Screen appears 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select International calls option from graph 
	When I scroll the screen upward
	Then I verify puchased International calls pack display 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap on International calls button 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I enter "United" country name in enter country field present 
	When I select country from list 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	Then I verify "Summary" Screen appears 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select International calls option from graph 
	When I scroll the screen upward
	Then I verify same International calls pack display twice 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap on International calls button 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I enter "Bangla" country name in enter country field present 
	When I select country from list 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	Then I verify "Summary" Screen appears 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select International calls option from graph 
	When I scroll the screen upward
	Then I verify different International calls pack display 
	
	When I click on "Plans" button present on screen 
	Then I verify Plans Screen Display 
	When I tap on International calls button 
	When I click on Select a country button 
	Then I verify "Select Country" Screen appears 
	When I enter "gcc" country name in enter country field present 
	When I select country from list 
	Then I verify Plans Screen Display 
	When I tap one of the offers 
	Then I verify "Summary" Screen appears 
	When I click on Confirm button 
	And I verify "Confirmation" text is present on screen using value 
	When I click on "DONE" button present on screen 
	Then I verify user is on dashboard 
	When I select International calls option from graph 
	When I scroll the screen upward
	Then I verify International calls Zone pack display 
	