Feature: Verify More Payment screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	################################# Account setting ####################################
	#Combine Case
	#1. Account setting: "To verify if the Account settings button within More section of theer Menu is clickable or not and the user is redirected to the settings screen when the user click on the Account settings button"
	#2. Account setting: To verify If there is a Change Password button displayed in the Settings screen
	#3. Account setting: To verify If user is successfully able to change password by inserting valid data
	#4. Account setting: To verify if the Want to use touch ID/ fingerprint through the app? text is displayed.
	#5. Account setting: To verify if the User is successfully able to input password in the field.
	#6. Account setting: To verify if the User is successfully able to mask/unmask the entered Password.
	#7. Account setting: To verify if the Settings screen is displayed when user click on back arrow button.
	#8. Account setting: To verify if the Access your dashboard using your fingerprint text is displayed within settings screen.
	#9. Account setting: To verify if the Password field is displayed.
	#10. Account setting: To verify if the Dark Mode section within setting screen is displayed.
	#11. Account setting: To verify if the pop up is displayed when click on system drop down.
	#12. Account setting: To verify if the Open souce libraries option is displayed and the  libraries we use screen is displayed.
	#13. Account setting: To verify if the Settings screen is displayed.
	#14. Account setting: To verify if the Crafted with care by your friendi mobile cyborgs text is displayed.
	#15. Account setting: To verify if the Settings screen is displayed when user click on back arrow(<) / cancel button.
	#16. Account setting:  To verify if the User is successfully logout with the id used.
	#17. Account setting: To verify if  v2.xx.0- UAT -OMAN # xxx text is displayed
	#18. Account setting: To verify if the More screen is displayed.
@MoreScreen @test @7562 
Scenario: C7536,C7537,C7538,C7540,C7542,C7543,C7549,C7550,C7541,C7551,C7552 
	Given I update the test rail case ID "7562" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "More" button present on screen 
	Then I verify "Settings" text is present on screen using value 
	When I click on "Settings" button using value 
	Then I verify "Settings" Screen appears 
	Then I verify "Change password" button is present on screen 
	When I click on "Change password" button present on screen 
	Then I verify "Change password" Screen appears 
	When I tap on back button 
	Then I verify "Settings" Screen appears 
	#Then I verify "Fingerprint" button is present on screen 
	#When I click on toggle button 
	#Then I verify toggle popup opens 
	#And I verify user is able to enter password in toggle field 
	#Then I verify Mask button mask and unmask the password 
	#Then I verify "Cancel" button is present on screen 
	#When I click on "Cancel" button present on screen 
	#Then I verify "Settings" Screen appears 
	#And I verify "Access your dashboard using your fingerprint" text present on screen 
	Then I verify "Dark Mode" button is present on screen 
	When I click on "Dark Mode" button present on screen 
	Then I verify a drop down opens for theme 
	When I click on done button 
	Then I verify "Open Source Libraries" button is present on screen 
	When I click on "Open Source Libraries" button present on screen 
	And I verify the version on application is present on screen 
	When I tap on back button 
	Then I verify "Settings" Screen appears 
	And I verify "Crafted with care by your FRiENDi mobile Cyborgs" text present on screen 
	And I verify version name is present on screen 
	Then I verify "Log out" button is present on screen 
	When I click on "Log out" button present on screen 
	Then I verify "LOGIN" button is present on screen 
	
	
	
	####################################Help###################################################
	#Combine case
	#1. Help: verify the user gets redirected to the help screen when the user click on the help icon under more screen
	#2. Help: verify if clicking the 'X' button closes the Help screen
	#3. Help: verify if the search bar get open with the blinking cursor in it on clicking the search button
	#4. Help: verify if categories are displayed under the header
	#5. Help: verify if user is able to click on the first category all the categories option "Choosing FRiNDi mobile" "FRiENDi mobile stores/Xpress Counters" "Join and keep your number" "Creating your account" Option displayed
	#6. Help: verify if the user redirected to the previous screen on clicking the back button next to the category
	#7. Help: To verify if the user is able to oberve the FAQs under every category and the user is redirected to the heading of the FAQ
	#8. Help: verify when the FAQ heading is opened from the subcategory than the category title in the header, the Back button and the answer displayed on the screen
	#9. Help: verify the user is redirected to the FAQ heading by clicking the back button from the answers screen
	#10. Help: verify the user is redirected to the sub-category by clicking the back button from the FAQ's heading screen
	#11. Help: verify the user is redirected to the sub-category by clicking the device back button from the answers screen
	#12. Help: verify if the user is seeing the suggested and popular searches (before starting the typing)
	#13. Help: verify if the user is able select an item from the suggested searches section
	#14. Help: verify if the user is able select an item from the popular searches section
	#15. Help: verify if the user is able to input data in the search field
	#16. Help: verify if the search field is having character validation
	#17. Help: verify if the user is unable to search with the invalid data in the search bar and the error message appears also "Talk to us" button appears below the error message
	#18. Help: verify if the user is able to select an item form the result that appears after entering the valid data
@MoreScreen @MorePayment @9883 
Scenario: C7505,C7506,C7507,C7508,C7509,C7504 
	Given I update the test rail case ID "9883" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "More" button present on screen 
	Then I verify "Help center" text is present on screen using value 
	When I click on "Help center" button using value 
	Then I verify "FAQ" Screen appears 
	Then I X button is present on screen 
	And I verify Search icon present on top 
	When I click on search icon 
	Then I verify search bar should open 
	Then I verify default search option present 
	When I click one of the option from default option 
	And I verify "How do I port-in my number to FRiENDi mobile?" text present on screen 
	When I tap on back button 
	Then I verify "Help center" text is present on screen using value 
	When I click on "Help center" button using value 
	Then I verify "FAQ" Screen appears 
	And I verify categories are displaying 
	When I click on "Getting Started" button using value 
	Then I verify "Choosing FRiENDi mobile" button is present on screen 
	Then I verify "FRiENDi mobile stores/Xpress Counters" button is present on screen 
	Then I verify "Creating your account" button is present on screen 
	When I click on back button 
	Then I verify "FAQ" Screen appears 
	When I click on "Data" button using value 
	Then I verify "Phone Settings" button is present on screen 
	Then I verify "Using Internet" button is present on screen 
	Then I verify "Multiple Plans" button is present on screen 
	When I click on back button 
	Then I verify "FAQ" Screen appears 
	When I click on search icon 
	Then I verify search bar should open 
	When I click on "How long does it take to port a number?" button using value 
	Then I verify "How long does it take to port a number?" Screen appears 
	Then I verify "The entire process of porting will take (3) working days." Screen appears 
	When I click on back button 
	Then I verify search bar should open 
	When I enter "Number" this in search field 
	Then I verify "Can I change my number?" Screen appears using value 
	Then I verify "How do I port-in my number to FRiENDi mobile?" Screen appears using value 
	When I click on "Can I change my number?" button using value 
	Then I verify "Can I change my number?" Screen appears 
	When I tap on back button 
	Then I verify "Help center" text is present on screen using value 
	When I click on "Help center" button using value 
	Then I verify "FAQ" Screen appears 
	When I click on search icon 
	Then I verify search bar should open 
	When I enter "Hello world" this in search field 
	Then I verify "TALK TO US" Screen appears 
	Then I verify "We couldn’t find the answer to your question :(" Screen appears 
	
	#Combined cases
	#To verify that user is able to observe the Answer screen under every category to the heading of the FAQ and  "Found your answer? text appears with along 2 buttons yes & No "
	#verify user is able to click yes & no buttons. if user click yes button the text "Found your answer?" disappears. But if user click the no button call button appears? with sorry text.
	#verify that user able to make call by clicking call button which appears after clicking the yes button on the answer screen of help centre. 
@MoreScreen @test12308 
Scenario: 
	Given I update the test rail case ID "12308" 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "More" button present on screen 
	Then I verify "Help center" text is present on screen using value 
	When I click on "Help center" button using value 
	Then I verify "FAQ" Screen appears 
	When I click on "Getting Started" button using value 
	When I click on "Choosing FRiENDi mobile" button using value 
	When I click on "Why choose FRiENDi mobile?" button using value 
	Then I verify "Why choose FRiENDi mobile?" Screen appears 
	And I verify "Found your answer?" text present on screen 
	And I verify "NO" text present on screen 
	And I verify "YES" text present on screen 
	When I click on "YES" button present on screen 
	Then I verify Found your answer text disappears 
	When I click on back button 
	Then I verify "Choosing FRiENDi mobile" text is present on screen using value 
	When I click on "Why choose FRiENDi mobile?" button using value 
	When I click on "NO" button present on screen 
	Then I verify "Sorry.." text is present on screen using value 
	And I verify "CALL" button is present on screen 
	
	
	
