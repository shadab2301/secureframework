Feature: Verify Dashboard screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	
	############################### CreditBalance and RechargeScreen ################################
	#combined cases
	#Verify the Credit Balance Section is displayed below the Manage SIMs section
	#Verify the Credit Balance is displayed below the Manage SIMs section
	#Verify the Add Credit button is displayed below the Manage SIMs section
	#Verify if clicking the add credit button should displayed the Recharge screen
	#Verify that the Voucher Number field is displayed
	#Verify that the user can input data in the field
	#Verify that the field has character validation
	#Verify that the field has character limit
	#Recharge screen - Voucher - Use Voucher button displayed
	#Verify that the Use Voucher button is disabled
	#Verify that the Use Voucher button is enabled
	#Verify that the error message is displayed after clicking the Use Voucher button when the voucher code is incorrect
	#Verify that the error message is displayed when user tries to use the voucher that was already used
	#Verify the One Time Top-up tab is displayed
	#Verify the Enter Amount field is displayed
	#Verify that the field has character validation
	#To verify if the user is able to input data in the field
	#PAY button should be displayed
	#PAY button should be disabled when no payment method is selected
	#PAY button should be enabled when payment method is selected
	#User should be able to manually add the desire RO amount and user should see the cash back amount dynamically displayed on the same screen
	#The Add new Card Screen should be displayed
	#Verify  user clicks on "cancel" button , user fails to pay from card and card will  not be added
	#User is not able to add credit card though "+Add new card"
	#To verify that the user is navigated to Add debit card Webview
	#Done button should be displayed 
	
@Android_Dashboard 
@Test9402 
Scenario: 
	Given I update the test rail case ID "9402" 
	Then I verfiy Credit balance section is displayed 
	And I verify credit balance is displayed 
	And I verify "ADD CREDIT" button is Displayed 
	When I click on "ADD CREDIT" button 
	Then I verify I am on "Recharge" screen 
	And I verify voucher number field display 
	And I verify i am able to enter in field 
	#And I verify there is a character validation in voucher field
	And I verify there is a character limit in voucher field 
	And I verify "USE VOUCHER" button is Displayed 
	When I enter "Test" in the VOUCHER NUMBER field 
	Then I verify USE VOUCHER button is disabled 
	When I enter "123456789074" in the VOUCHER NUMBER field 
	Then I verify USE VOUCHER button is enabled 
	When I click on "USE VOUCHER" button 
	Then I verify Error Message display 
	When I click on "OK" button 
	When I enter "580717557166" in the VOUCHER NUMBER field 
	When I click on "USE VOUCHER" button 
	Then I verfiy "This recharge voucher has already been used or is invalid. Please try to input the voucher again or use another one." text is displayed 
	When I click on "OK" button 
	Then I verify one time topup field display 
	And I verify enter amount field display 
	And I verify there is a character validation in amount field 
	And I verify i am able to enter in oneTime topUp field 
	And I verify PAY button is Displayed 
	And I verify PAY button is disabled if payment method not selected 
	And I verify PAY button is enabled if payment method selected 
	When I enter "12" in oneTime topUp field 
	Then I verfiy "RO 0.600" text is displayed 
	When I click on "PAY USING" button 
	Then I verfiy "Select your payment method" text is displayed 
	When I click on "+ Add new card" button 
	Then I verify add new card screen appears 
	When I enter invalid details in fields 
	Then I verify add button is disable 
	When I click on "CANCEL" button 
	Then I verfiy "Select your payment method" text is displayed 
	When I click on "+ Add new card" button 
	Then I verify add new card screen appears 
	When I enter valid details in fields 
	Then I verify add button is Enable 
	When I click on "CANCEL" button 
	Then I verfiy "Select your payment method" text is displayed 
	When I click on "Pay using Omani Debit Card" button 
	#Then I verify add debit card webview display 
	When I tap cross button 
	Then I verify I am on "Recharge" screen 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I enter "1.5" in oneTime topUp field 
	When I click on "PAY | RO 1.500" button 
	Then I verify "DONE" button is Displayed 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	
	#Combined cases
	#Verify that clicking the Use Voucher button with valid data entered in the field, the recharge voucher should be added 
	#Recharge wallet screen - Voucher - ADD ANOTHER VOUCHER button is clickable.
	#Verify that clicking the Use Voucher button with valid data entered in the field, the Data voucher should be added 
	#Verify that clicking the ADD ANOTHER VOUCHER button user should land back to the Recharge screen
	#Verify that clicking the View button user should land on the data pack details screen
	#Verify that clicking the X button the window will be closed and user should land back to the Recharge screen
	
@Android_Dashboard 
@Test12939 
Scenario: 
	Given I update the test rail case ID "12939" 
	#	When I click on "ADD CREDIT" button 
	#	Then I verify I am on "Recharge" screen 
	#	When I enter "533631288455" in the VOUCHER NUMBER field 
	#	When I click on "USE VOUCHER" button 
	#	Then I verfiy "Successful Recharge" text is displayed 
	#	Then I verfiy "DONE" text is displayed 
	#	When I click on "ADD ANOTHER RECHARGE CARD" button 
	#	Then I verify I am on "Recharge" screen 
	#	When I enter "454411860508" in the VOUCHER NUMBER field 
	#	Then I verfiy "Awesome" text is displayed 
	#	Then I verfiy "You got 1 GB extra, with unlimited validity" text is displayed 
	#	Then I verfiy "ADD ANOTHER VOUCHER" text is displayed 
	#	When I click on "VIEW" button 
	#	Then I verify Data extended view is displayed 
	#	When I click on cross Button 
	#	Then I verify I am on "Recharge" screen 
	#	When I tap back button present screen 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I tap on any one data pack 
	Then I verify Data extended view is displayed 
	And I verify pi chart and remaining data display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity period is displayed 
	And I verify valid until display 
	And I verify price should not display for promo pack 
	When I click on cross Button 
	Then I verify I am on dashboard 
	
	#Combined cases
	#User should do the top up with 0.5 RO
	#User should do the top up with 1 RO
	#User should do the top up with 2 RO
	#User should do the top up with 3 RO
	#User should do the top up with 4 RO
	#User should do the top up with 5 RO
	#User should do the top up with 10 RO
	#User should do the top up with 20 RO
	#User should do the top up with 50 RO
	
@Android_Dashboard 
@Test9408 
Scenario: 
	Given I update the test rail case ID "9408" 
	When I click on "ADD CREDIT" button 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	Then I verfiy "0.5" text is displayed 
	When I click on "PAY | RO 0.500" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	When I click on "ADD CREDIT" button 
	When I click on positive button 
	Then I verfiy "1" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 1.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "2" times on positive button 
	Then I verfiy "2" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 2.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "3" times on positive button 
	Then I verfiy "3" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 3.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "4" times on positive button 
	Then I verfiy "4" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 4.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "5" times on positive button 
	Then I verfiy "5" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 5.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "6" times on positive button 
	Then I verfiy "10" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 10.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "7" times on positive button 
	Then I verfiy "20" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 20.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	When I click on "ADD CREDIT" button 
	When I click "8" times on positive button 
	Then I verfiy "50" text is displayed 
	When I click on "PAY USING" button 
	When I click on Radio button to select payment card 
	When I click on "PAY | RO 50.000" button 
	When I click on "DONE" button 
	Then I verify I am on dashboard 
	Then I verfiy "Credit balance" text is displayed 
	
	############################### BonusCreditBalance and YourRewardScreen  ################################		
	#Combined cases
	#Verify the Bonus Wallet Section is displayed below the Wallet Balance section
	#Verify the Bonus Wallet explanation is displayed below the Wallet Balance section
	#Verify the Bonus Wallet arrow should navigate to Your Rewards screen
	#Verify if Dashboard is displayed when the back button is clicked
	#Verify the Bonus Wallet arrow should navigate to Your Rewards screen with one top-up bonus Promo active (5%)
	#Verify if the Previous months' bonuses and the Help button is displayed when History button is clicked
	#Verify if the Your Rewards screen is displayed when Back button is clicked
	#Verify if the detailed view of promo bonus per month is listed
	#Verify if the detailed view of promo bonus per month is closed
	#Verify if the detailed view of promo bonus can be opened for more than one month at a time
	#Verify if the amounts per days are correctly summed up in the monthly amount 
	#Verify if the Recharge screen where one-off top-up and voucher top-up can be done is displayed
@Android_Dashboard 
@Test9403 
Scenario: 
	Given I update the test rail case ID "9403" 
	Then I verfiy "Your Bonus Credit Balance" text is displayed 
	And I verfiy "5% bonus credit" text is displayed 
	When I tap on Arrow button 
	Then I verify I am on "Your rewards" screen 
	And I verfiy 5% bonus dial disaply 
	When I tap back button present screen 
	Then I verify I am on dashboard 
	When I tap on Arrow button 
	When I click on "HISTORY" button 
	Then I verify I am on "Rewards history" screen 
	When I tap on back button present screen 
	Then I verify I am on "Your rewards" screen 
	When I click on "HISTORY" button 
	Then I verify previous month bonus display 
	When I tap arrow button 
	Then I verify detailed view of promo bonus per month is closed 
	When I tap arrow button for first month 
	When I scroll the screen upwards 
	When I scroll the screen upwards till specific location 
	When I tap arrow button for second month 
	Then I verify detailed view of promo bonus can be opened for more than one month at a time 
	And I verify amounts per days are correctly summed up in the monthly amount 
	When I tap back button from recharge screen 
	When I click "Recharge your account" text present on the screen 
	Then I verify I am on "Recharge" screen 
	
	############################### Dashboard-HSNotifications ################################
	#combined cases
	#To verify if the home screen notifications are displayed under the Recharge section
	#To verify that the arrow for expanding should be displayed and if clicked expands the notifications
	#To verify if the home screen notifications are stacked one on top of other
@Android_Dashboard 
@Test9404 
Scenario: 
	Given I update the test rail case ID "9404" 
	Then I verify Home screen notification display 
	Then I verify notifications are stacked one on top of other 
	When I tap on arrow in notification section 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	Then I verify notifications expanded 
	
	#combined cases
	#To verify that the CLEAR ALL/X button should be displayed and if clicked on CLEAR ALL/X button all home screen notifications cleared
	#To verify that the CLEAR  button should be displayed and if clicked on CLEAR  button all home screen notifications cleared
@Android_Dashboard 
@Test10153 
Scenario: 
	Given I update the test rail case ID "10153" 
	Then I verify Home screen notification display 
	And I verify cross button display in home notification 
	When I tap on cross button under home notification 
	Then I verify all home screen notifications cleared 
	
	
	############################### Dashboard-MyPlan ################################	
	#Combined cases
	#To verify that the Graph with Details is displayed
	#To verify that the Graph with Details is grouped by service type
	#To verify that the Remaining and Total Balance is displayed for each service type	
	#To verify that the graph with details 3 different service types
	#To verify that the Purchased Active Packs are displayed below the graph
	#To verify that selecting service type from the graph shows only the selected service type on the dashboard
	#To verify that details of the list item are displayed
	#To verify that 'Auto-Renew' Status icon is filled if the auto renew is turned on
@Android_Dashboard 
@Test9406 
Scenario: 
	Given I update the test rail case ID "9406" 
	Then I verify graph with details display 
	And I verify graph details group by service type 
	And I verify remainaing balance and total balance display 
	Then I verfiy "Data" text is displayed 
	Then I verfiy "Calls" text is displayed 
	Then I verfiy "Intl. Calls" text is displayed 
	When I scroll the screen upwards 
	Then I verify active packs display 
	When I tap on Data Service type from graph 
	Then I verify data packs display on dashboard 
	When I tap on Local call Service type from graph 
	#Then I verify Local call packs display on dashboard 
	When I tap on international call Service type from graph 
	#Then I verify international call packs display on dashboard 
	When I tap on Data Service type from graph 
	When I scroll the screen upwards 
	Then I verify remaining balance is display 
	And I verify total balance is display 
	And I verfiy validity is displayed for all itme in the list 
	And I verify auto renew icon display for latest purchase of unlimited validity pack 
	
	
	
	############################### Dashboard-RechargeOffer ################################		
	
	#Combined cases
	#To verify that the free WhatsApp and free on-net minutes view is displayed on dashboard when the user scrolls down 
	#To verify that the status is displayed when one of the recharge promos has expired/was consumed
	#To verify that the status is displayed when both of the recharge promos has expired/was consumed
	#To verify if the WhatsApp extended view is displayed
	#To verify if the WhatsApp Logo is displayed
	#To verify if the Remaining data is displayed
	#To verify if the validity period is displayed
	#To verify if the valid until date is displayed
	#To verify if the price is displayed
	#To verify if the "Recharge account and Try Again" button is hidden
	#To verify if the "X" button in the WhatsApp extended view is displayed and if clicking the "X" button in the WhatsApp extended view navigates the user to the Dashboard
	#To verify if the On-net calls extended view is displayed
	#To verify if the FRIENDi Logo is displayed
	#To verify if the Remaining minutes are displayed
	#To verify if the validity period is displayed
	#To verify if the valid till date is displayed
	#To verify if the price is displayed
	#To verify if the "Recharge Account and try again" button is hidden
	#To verify if the "X" button in the On-net calls extended view is displayed and if clicking the "X" button in the On-net calls extended view navigates the user to the Dashboard
	#To verify if the Remaining period and validity till are displayed empty when user still haven’t done any top-up above the defined threshold (still haven’t got the promo)
	#To verify if the "Recharge account and Try Again" button is displayed when user still haven’t done any top-up above the defined threshold (still haven’t got the promo)
	#To verify if the Remaining period and validity till are displayed empty when user still haven’t done any top-up above the defined threshold (still haven’t got the promo)
	#To verify if the "Recharge Account and Try Again" button is displayed when user still haven’t done any top-up above the defined threshold (still haven’t got the promo)
	#To verify if the "Recharge Account and Try Again" button navigates the user to the one off top-up screen
@Android_Dashboard 
@Test9405 
Scenario: 
	Given I update the test rail case ID "9405" 
	When I scroll the screen upwards 
	Then I verify free whatsapp and free friendi call display 
	And I verify the status of free whatsapp and free friendi call 
	When I click on Free WhatsApp button 
	Then I verify "Free WhatsApp" screen display 
	Then I verify app logo display 
	And I verify remaining data display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity Time period is displayed 
	And I verfiy "Valid until" text is displayed 
	And I verify vaid untill date display 
	And I verfiy "Price" text is displayed 
	And I verfiy "Free" text is displayed 
	And I verify Recharge account and Try Again button hidden if app status is ON 
	When I click on cross Button 
	Then I verify I am on dashboard 
	And I verify the status of free whatsapp and free friendi call 
	When I click on Free FRIENDI Calls button 
	Then I verify "Free FRiENDi Calls" screen display 
	Then I verify app logo display 
	And I verify remaining Minutes display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity Time period is displayed 
	And I verfiy "Valid until" text is displayed 
	And I verify vaid untill date display 
	And I verfiy "Price" text is displayed 
	And I verfiy "Free" text is displayed 
	And I verify Recharge account and Try Again button hidden if app status is ON 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I scroll the screen upwards 
	And I verify the status of free whatsapp and free friendi call 
	When I tap on Free Whatsapp call button 
	Then I verify Remaining period and validity until are displayed empty 
	And I verify Recharge account and Try Again button enabled if app status is OFF 
	And I verify by tap on recharge account button I am on recharge screen 
	When I tap back button of recharge screen if app offer is expired 
	Then I verify "Free WhatsApp" screen display 
	When I click on cross Button 
	Then I verify I am on dashboard 
	And I verify the status of free whatsapp and free friendi call 
	When I tap Free Friendi call button 
	Then I verify Remaining period and validity until are displayed empty 
	And I verify Recharge account and Try Again button enabled if app status is OFF 
	And I verify by tap on recharge account button I am on recharge screen 
	When I tap back button of recharge screen if app offer is expired 
	Then I verify "Free FRiENDi Calls" screen display 
	
	
	############################### Dashboard-ActivePlans(DATA) ################################	
	#combined cases
	#To verify that the Active packs are displayed on the Dashboard
	#To verify if the Data extended view is displayed
	#To verify if the pie chart with the remaining data is displayed
	#To verify if the validity period is displayed
	#To verify if the valid until date is displayed
	#To verify if the price is displayed
	#To verify if the Auto renewal toggle is displayed for the latest purchased active "Freedom Data Plan" or "Freedom Mix Plan"
	#To verify if that the Auto renewal toggle button is not displayed for the "Saver Data Plan"
	#To verify if that the Auto renewal toggle button is not displayed for the "Saver Mix Plan"
	#To verify if the Auto renewal toggle is selected
	#To verify if the mixed pack Local calls is displayed and if clicking the Local calls pack navigates the user to the Local calls extended view
	#To verify if the "BUY AGAIN" button is displayed for all active plan
	#To verify if the "BUY AGAIN" button is hidden
	#To verify if clicking the "BUY AGAIN" button user navigates to the Summary screen 
	#To verify if the "X" button in the data extended view is displayed and if clicking the "X" button in the data extended view navigates the user to the Dashboard
	#Data-To verify if the "BUY AGAIN" button opens the Top-up pop-up
	#Data-To verify if clicking the "Recharge" button navigates to the Recharge screen
	#Data-To verify if the "CANCEL" button closes the pop-up
	#To verify if the PAYG rates are displayed as expected 
	#To verify if the SEE FULL DETAILS button is displayed
	#To verify if the SEE FULL DETAILS button is displayed
	#To verify if the Toggle button is displayed in the Data PAYG rates 
	#To verify if the Toggle button is selected in the Data PAYG rates the Data PAYG rates are disabled
	#To verify if all PAYG rates will be displayed by clicking on the SEE FULL DETAILS button
	#Dashboard - PAYG rates - SHOW LESS button clicked
	#To verify if the International "View Rates >" button navigates the user to the Selec Country screen
	#To verify if the selected county will show only PAYG rate when user clicks on the International "View Rates >" button
@Android_Dashboard 
@Test9407 
Scenario: 
	Given I update the test rail case ID "9407" 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	Then I verify active packs display 
	When I tap on any one data pack 
	Then I verify Data extended view is displayed 
	And I verify pi chart and remaining data display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity period is displayed 
	And I verify vaid untill date display 
	And I verfiy price is displayed 
	And I verify Auto renewal toggle is displayed for the Freedom Data Plan or Freedom Mix Plan 
	And I verify Auto renewal toggle button is not displayed for the Saver Data Plan or Saver Mix Plan 
	And I verify Auto renewal toggle button is selected 
	#When I tap on toggle button present on screen 
	#When I click on CONFIRM button 
	#When I refresh the screen 
	#When I tap on any one data pack 
	#Then I verify Auto renewal toggle button is deselected 
	#When I tap on toggle button present on screen 
	#When I click on CONFIRM button 
	#When I tap on any one data pack 
	#Then I verify Auto renewal toggle button is selected 
	And I verify mixed pack Local calls is displayed 
	When I tap on pack under mix plan 
	Then I verify Local calls extended view display 
	When I tap on pack under mix plan 
	Then I verify Buy again Butoon is hidden 
	And I verify BUY AGAIN button is Displayed 
	When I tap on buy again button 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	When I tap on back button present screen 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter "79591818" mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I scroll the screen upwards 
	When I select data pack 
	When I tap on buy again button 
	Then I verify Recharge pop up display 
	And I verify RECHARGE button is Displayed 
	And I verify CANCEL button is Displayed 
	When I click on CANCEL button 
	When I tap on buy again button 
	When I click on RECHARGE button 
	Then I verify I am on Recharge screen 
	When I tap for guide popup 
	When I tap back button present on screen 
	Then I verify Data extended view is displayed 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards till PayG rates found 
	Then I verfiy "Standard Rates" text is displayed 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	And I verify Toggle button display 
	And I verify toggle button deselected 
	When I scroll the screen upwards 
	And I verify "SEE FULL DETAILS" button is Displayed 
	When I click on "SEE FULL DETAILS" button 
	When I scroll the screen upwards 
	Then I verify all payG rates display 
	When I click on "SHOW LESS" button 
	Then I verify all payG rates collapsed 
	When I click on "SEE FULL DETAILS" button 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	When I click on "View Rates" button 
	Then I verify I am on "Select Country" screen 
	When I tap on one of the country from list 
	Then I verfiy "Rates without international packs" text is displayed 
	
	############################### Dashboard-ActivePlans(calls) ################################	
	
	#Combined cases
	#To verify if the pie chart with the remaining data is displayed
	#To verify if the unlimited icon is displayed if the pack has unlimited validity
	#To verify if the validity period is displayed
	#To verify if the price is displayed
	#To verify if the mixed pack Data is displayed and if clicking the Data pack navigates the user to the Data extended view
	#To verify if that the Auto renewal toggle button is not displayed for any Local Plan
	#To verify if the "X" button in the local calls extended view is displayed and if clicking the "X" button in the local calls extended view navigates the user to the Dashboard
	#To verify if the "BUY AGAIN" button is always displayed for all Local Calls plans which have limited validity of X days
	#To verify if the "BUY AGAIN" button is displayed for all active Local Calls plan with unlimited validity
	#To verify if the "BUY AGAIN" button navigates the user to the Summary screen 
	#LocalCall-To verify if the "BUY AGAIN" button opens the Top-up pop-up
	#LocalCall-To verify if the "Recharge button" navigates to the Recharge screen
	#LocalCall-To verify if the "CANCEL" button closes the pop-up
	
@Android_Dashboard 
@Test9414 
Scenario: 
	Given I update the test rail case ID "9414" 
	When I tap on Local call Service type from graph 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	When I select "Valid until" call pack 
	Then I verify local call extended view display 
	And I verify unlimited icon display if pack is unlimited 
	And I verify pi chart and remaining mins display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity period is displayed 
	And I verfiy Price text is displayed 
	And I verfiy price value is displayed 
	And I verfiy auto renewal toggel should not display for local call 
	And I verify mixed pack Data is displayed 
	When I tap on pack under mix plan 
	Then I verify Data extended view display 
	When I tap on pack under mix plan 
	And I verify "BUY AGAIN" button is Displayed 
	When I tap on buy again button 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	When I tap on back button present screen 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I select "Unlimited Validity" call pack 
	And I verify "BUY AGAIN" button is Displayed 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter "79591818" mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I scroll the screen upwards 
	#When I tap on Local call Service type from graph 
	When I select local call pack 
	When I tap on buy again button 
	Then I verify Recharge pop up display 
	And I verify RECHARGE button is Displayed 
	And I verify CANCEL button is Displayed 
	When I click on CANCEL button 
	When I tap on buy again button 
	When I click on RECHARGE button 
	Then I verify I am on Recharge screen 
	
	############################### Dashboard-ActivePlans(International calls) ################################	
	#COMBINEE CASES
	#To verify if the international calls extended view is displayed
	#To verify if the pie chart with the remaining international calls is displayed
	#To verify if the validity period is displayed
	#To verify if the Included countries dropdown button is displayed and if clicked on dropdown the included countries are displayed
	#To verify if the price is displayed
	#To verify if that the Auto renewal toggle button is not displayed for any International Plan
	#To verify if the "BUY AGAIN" button is displayed for all active International calls packs
	#To verify if the "BUY AGAIN" button navigates the user to the Summary screen 
	#To verify if the "X" button in the international calls extended view is displayed and if clicking the "X" button in the international calls extended view navigates the user to the Dashboard
	#To verify if the "BUY AGAIN" button is always displayed for all International Calls plans which have limited validity of X days
	#To verify if the "BUY AGAIN" button opens the Top-up pop-up
	#To verify if the "Recharge button navigates to the Recharge screen
	#To verify if the "CANCEL" button closes the pop-up
	
@Android_Dashboard 
@Test9468 
Scenario: 
	Given I update the test rail case ID "9468" 
	When I tap on international call Service type from graph 
	When I scroll the screen upwards 
	When I scroll the screen upwards 
	When I select "Unlimited Validity" call pack 
	Then I verify international call extended view display 
	And I verify pi chart and remaining mins display 
	And I verfiy "Validity" text is displayed 
	And I verfiy validity period is displayed 
	And I verify Included dropdown is Displayed 
	When I click on Included dropdown button 
	Then I verify dropdown expended 
	And I verfiy "Price" text is displayed 
	And I verfiy price value is displayed for international calls 
	And I verfiy auto renewal toggel should not display for international call 
	And I verify BUY AGAIN button is Displayed 
	When I tap on buy again button 
	Then I verify I am on Summary screen 
	When I tap for guide popup 
	When I tap on back button present screen 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	When I select "Valid until" call pack 
	#And I verify BUY AGAIN button is Displayed 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I click on "More" button 
	When I click on "Settings" button 
	When I click on "Log out" button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter "79591818" mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I scroll the screen upwards 
	#When I tap on international call Service type from graph 
	When I select International call pack 
	When I tap on buy again button 
	Then I verify Recharge pop up display 
	And I verify RECHARGE button is Displayed 
	And I verify CANCEL button is Displayed 
	When I click on CANCEL button 
	When I tap on buy again button 
	When I click on RECHARGE button 
	Then I verify I am on Recharge screen 
	
	############################### Dashboard_Navigation_Bar ################################
	
	#Combined cases
	#To verify if the Navigation bar is displayed
	#To verify if the "Home" option in the Navigation Bar is displayed and user navigates to the dashboard screen once clicked on Home option
	#To verify if the "Plans" option in the Navigation Bar is displayed and user navigates the packs screen once clicked on plans option.
	#To verify if the "History" option in the Navigation Bar is displayed and user navigates the usage history screen once clicked on History option
	#To verify if the "More" option in the Navigation Bar is displayed and user navigates the more screen once clicked on More tab
@Android_Dashboard 
@Test9411 
Scenario: 
	Given I update the test rail case ID "9411" 
	Then I verify Navigation bar display 
	When I click on "Home" button 
	Then I verify I am on dashboard 
	When I click on "Plans" button 
	Then I verify Plan Screen displayed 
	When I tap for guide popup 
	When I tap for guide popup 
	When I click on "History" button 
	Then I verify I am on "Usage History" screen 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	
	
	
	