Feature: Verify Intial Login screen functionality

  Background: 
    When I install the application
    When I allow the popup
    Then I verify I am on splash screen

  ################################# Intial Login #################################################
  #Combine Case
  #Dashboard: To verify that the promo pop-up coach marks is displayed
  #Dashboard: To verify that tapping on the screen displays the next Coach Mark
  #Dashboard: To verify that the Buy Plan Coach Mark is displayed
  @IntialLoginJenkins @10151
  Scenario: [C6534,C6535,C6536]
    Given I update the test rail case ID "10151"
    When I tap on EXISTING CUSTOMER button
    Then I verify "Sign In" Screen appears
    When I enter mobile number
    When I tap on next button
    Then I verify "Verification" Screen appears
    When I enter verification code
    When I click on next button
    And I verify promo popup coach marks is displayed on screen
    When I close the Welcome popup    
    And I verify Buy Plan Coach Mark is displayed on screen
    
    
