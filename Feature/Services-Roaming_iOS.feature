Feature: Verify Roaming Services screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	#Combined cases
	#To verify if the user is navigated to Roaming service screen from Services in More screen
	#To verify if the user is navigated to Services screen when back button is clicked
	#To verify that user is navigated to country picker screen when search field is clicked
	#To verify if the search field is working properly
	#To verify if the search field is cleared when X button is clicked
	#To verify if the Roaming package screen is displayed properly
	#To verify that Change button navigates to country picker
	
@RoamingServicesJenkins 
@test10522 
Scenario: 
	Given I update the test rail case ID "10522" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "Roaming Services" button using value 
	Then I verify "Roaming Services" Screen appears 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "Roaming Services" button using value 
	When I click on "Enter your destination" button using value 
	Then I verify "Select Country" Screen appears 
	When I enter "United" country name in enter country field present 
	Then I verify enterd country list display onscreen 
	When I click on cross button 
	Then I verify Search field cleared 
	When I enter "United" country name in enter country field present 
	When I select country from list 
	Then I verify "Roaming Services" Screen appears 
	And I verify "United Arab Emirates" text is present on screen using value 
	#And I verify country logo display onscreen 
	When I click on "CHANGE" button using value 
	Then I verify "Select Country" Screen appears 
	