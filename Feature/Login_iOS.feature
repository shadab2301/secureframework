Feature: Verify More Payment screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	################################# Login #################################################
	#Combine Case
	#Sign-in: To verify if the sign in screen displayed when  'EXISTING CUSTOMER' button clicked
	#Sign-in: To verify if the Welcome screen appears when back button is clicked.
	#Sign-in: To verify if the Support screen appears when help button is clicked.
	# Sign-in: To verify if mobile number field is displayed.
	# Sign-in: To verify clicking the NEXT button the Verification Screen is displayed once next button is clicked after entering the valid OTP
	# Sign-in: To verify if Next button is enabled/disabled with valid/invalid inputs in the mobile number field.
	# Login:  Face ID/Fingerprint icon clicked
	# Login: Face ID/Fingerprint icon clicked -  "OK" Button Closing the Warning Pop Up
	# Login:  OTP Verification Screen - displayed
	# Login:  OTP Verification Screen - Back Button displayed and clicked
	# Login:  OTP Verification Screen - Help icon displayed and clicked
	# Login:  OTP Support Screen -  (X) button displayed and clicked
	# Login:  OTP Verification - OTP field displayed and data inputted
	# Sign-in: To verify that user is successfully logged in when clicked on Next button with valid OTP.
	# Sign-in: To verify that the Sign in Screen opens when I'LL USE MY PASSWORD button is clicked for logging in with valid MSISDN and password
	# Sign-in: To verify the user can enter password to login
	# Sign-in: To verify the user if the user is redirected to the dashboard screen when a valid password is entered.
	# Sign-in: To verify if the Legal stuff screen appears when clicked on Legal stuff link.
	# Sign-in: To verify that the user redirect to sign In screen when clicked on X button within the Legal stuff screen
	# Sign-in: To verify the Legal stuff text is displayed on top of the screen
	# Sign-in: To verify if the user is redirected to the Sign in screen when the device back button is clicked within the Legal stuff screen.
	# Sign-in: To verify if the user is redirected to the support screen when clicked on Help icon within the Legal stuff screen.
  # Sign-in: To verify if mobile number help text is displayed.
  # Sign-in: To verify if mobile number help text is removed when clicked on the mobile number help text.
  # Login: Password Verification - enter password
  # Login: Password Verification - password field is having "Enter password" help text
@LoginJenkins @10027 
Scenario: C7536,C7537,C7538,C7540,C7542,C7543,C7549,C7550,C7541,C7551,C7552 
	Given I update the test rail case ID "10027" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I tap on back button 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	And I verify help icon is present 
	When I click on help icon 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "Sign In" Screen appears 
	And I verify "NEXT" button is present on screen 
	And I verify "Mobile number" text is present on screen using value
	And I verify mobile number field display 
	When I enter invalid mobile number 
	Then I verify next button is disabled 
	When I tap on back button 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	And I verify "79570784" this mobile number is entered in field 
	Then I verify next button is enabled 
	#When I tap on fingerprint icon 
	#Then I verify "Warning" Screen appears using value 
	#And I verify warning pop up displayed 
	#And I verify "OK" button is present on screen 
	#When I click on "OK" button present on screen 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	And I verify help icon is present 
	When I click on help icon 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "Verification" Screen appears 
	And I verify "NEXT" button is present on screen 
	When I tap on back button 
	Then I verify "Sign In" Screen appears 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I click on "Done" button present on screen 
	When I click on "I’LL USE MY PASSWORD" button present on screen 
	Then I verify "Sign In" Screen appears 
	When I click on "Once you continue, you agree to the Legal stuff" button present on screen 
	Then I verify "Legal Stuff" Screen appears 
	When I tap on back button 
	Then I verify "Sign In" Screen appears 
	When I click on help button present 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "Sign In" Screen appears 
	And I verify "Enter password" text is present on screen using value
	And I verify "Test12345" this password is entered in the field 
	When I click on "SIGN IN" button present on screen
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 