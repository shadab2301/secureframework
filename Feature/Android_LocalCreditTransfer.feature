Feature: Verify Local Credit Transfer screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	
	#Combined cases
	#To verify if the Credit Transfer screen is displayed
	#To verify if the user is navigated to the Other Services screen after clicking on the Back button from the Credit transfer screen
	#To verify if the user is navigated to the Top-up screen after clicking on the Top-up button
	#To verify if an RO input field is display on the Credit Transfer screen
	#To verify if the user is able to input and view the value within the  RO input value field.
	#o verify if the entered amount is above my wallet balance and the TRANSFER button is disabled, user will see an error message
	#To verify if a field with help text "Tap in number" is displayed on the Credit Transfer screen
	#To verify if a TRANSFER button is displayed on the Credit Transfer screen
	#To verify if the user is able to input data in the field with help text "Tap in number"
	#To verify if the help text will disappear when the user enters data in the field
	#To verify if the field with help text "Tap in number" is having phone number validation
	#To verify if an error message with Cancel button is displayed after clicking on the TRANSFER button with not valid phone number
	#To verify if the error message pop-up will disappear if the user clicks on the Ok button	
	#To verify if the TRANSFER button is enabled only when there is enough money in the wallet and entered phone number in the field is in correct format
	#To verify if the user is navigated to the Credit Transfer Confirmation screen after clicking on the TRANSFER button 
	#To verify if the user is navigated to the Credit Transfer screen after clicking on the Back button 
	#To verify if a Total Amount drop-down is displayed
	#To verify if the total amount details are displayed after clicking on the Total Amount drop-down
	#To verify if a CONFIRM button is displayed on the screen
	#o verify if the user is navigated to the Credit Transfer Confirmation screen 
	#To verify if an OTP field is displayed on the screen
	#To verify if the OTP field is having OTP format validation
	#To verify if the user is able to input data in the OTP field
	#To verify if a CONFIRM button is displayed
	#o verify if the user will receive an OTP by SMS
	#To verify if an error message that the OTP is not correct is displayed after clicking on the CONFIRM/NEXT button with invalid OTP
	#To verify if the user is navigated to the Dashboard after clicking on the CONFIRM/NEXT button with valid OTP
@Android_LocalCreditTransfer 
@test7974 
Scenario: 
	Given I update the test rail case ID "7974" 
	When I click on "Local Credit Transfer" button 
	Then I verify I am on "Credit transfer" screen 
	When I tap back button present screen 
	Then I verify I am on "Other services" screen 
	When I click on "Local Credit Transfer" button 
	When I tap on Top up Button 
	Then I verify I am on "Recharge" screen 
	When I tap back button present screen 
	Then I verify RO input field display 
	And I verify able to input in RO field 
	When I enter "99999999" data in RO field 
	Then I verify error message display 
	When I enter "1000" data in RO field 
	And I verify help text "Tap in number" displayed 
	And I verify "TRANSFER" button is Displayed 
	And I verify able to input "1234" data in number field 
	And I verify help text disappeared 
	#And I verify if text field has character validation 
	When I enter "96879566999" number in tap in number field 
	Then I verify Transfer button enabled 
	When I click on "TRANSFER" button 
	Then I verfiy "Oops! The number you’re trying to send credit to (96879566999) is invalid." text is displayed 
	When I click on "OK" button 
	When I enter "96979570784" number in tap in number field 
	When I click on "TRANSFER" button 
	Then I verify I am on "Summary" screen 
	When I tap back button present screen 
	Then I verify I am on "Credit transfer" screen 
	When I click on "TRANSFER" button 
	Then I verify Total Amount dropdown display 
	When I click on "Total amount" button 
	Then I verify total amount details are displayed 
	And I verify "CONFIRM" button is Displayed 
	When I click on "CONFIRM" button 
	Then I verify I am on "Verification" screen 
	When I tap back button present screen 
	Then I verify I am on "Summary" screen 
	When I click on "CONFIRM" button 
	Then I verify I am on "Verification" screen 
	Then I verify OTP field displayed 
	#And I verify if OTP field has format validation 
	And I verify "CONFIRM" button is Displayed 
	And I verify able to input "987654" data in OTP field 
	When I click on "CONFIRM" button 
	Then I verfiy "The code you’ve entered is not valid. Please check the SMS you received and try again" text is displayed 
	Then I verify send again button display 
	When I click on "SEND AGAIN" button 
	And I verify able to input "123456" data in OTP field
	When I click on "CONFIRM" button 
	Then I verify I am on dashboard 
	
	#combined cases
	#To verify if the user is navigated to the native Contacts application after clicking on the button 
	#To verify if the phone number is displayed in the field after selecting a number from the native Contacts application
	
@Android_LocalCreditTransfer 
@test8293 
Scenario: 
	Given I update the test rail case ID "8293" 
	When I click on "Local Credit Transfer" button 
	When I tap on contact button present 
	Then I verify phone contact screen is displayed 
	And I verify phone number is displayed in the field 
	
	
	
	
	
	
	