Feature: Verify International Call Rates screen functionality 

Background: 
	When I install the application 
	When I allow the popup 
	Then I verify I am on splash screen 
	
	#Combined cases
	#To verify if the user is navigated to International Calls service screen from Services in More screen
	#To verify if the Back button navigates the user to the previous Other services screen
	#To verify if the Help button navigates the user to Help screen
	#To verify if the eligible country list will be displayed when the search icon is clicked
	#To verify if the eligible country list will be displayed once a text is typed into the input search field
	#To verify if the detailed view of international calls' offers for the selected country will be displayed
	#To verify if the Back button navigates back to the Country Picker screen
	#To verify if the Help button navigates to the Help screen
	#To verify if the Change button navigates the user back to the Country Picker screen
	#To verify if clicking the "Without plan" tab displays the PAYG offers and prices for the selected country
	#To verify if selecting a country would result in peak and off peak text with other details
	#To verify if the user can click and see all the countries included in the currently selected international plan
	#To verify if the user can search if a country is a part of the 38 eligible countries
	#To verify if the user won't find the country in the list if it not a part of the eligible countries
@InternationalCallsJenkins 
@test10516 
Scenario: 
	Given I update the test rail case ID "10516" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "International Call Rates" button using value 
	Then I verify "International Calls" Screen appears 
	When I tap on back button 
	Then I verify "Other services" Screen appears 
	When I click on "International Call Rates" button using value 
	When I click on help button present 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "International Calls" Screen appears 
	When I click on "Enter country" button using value 
	Then I verify "Select Country" Screen appears 
	#And I verify all countries list displayed 
	When I enter "United" country name in enter country field present 
	Then I verify enterd country list display onscreen 
	When I select country from list 
	Then I verify "International Calls" Screen appears 
	And I verify "United Arab Emirates" text is present on screen using value 
	#And I verify country logo display onscreen 
	When I tap on back button 
	Then I verify "Select Country" Screen appears 
	When I select country from list 
	Then I verify "International Calls" Screen appears 
	When I click on help button present 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "International Calls" Screen appears 
	When I click on "CHANGE" button using value 
	Then I verify "Select Country" Screen appears 
	When I select country from list 
	Then I verify "International Calls" Screen appears 
	When I click on "WITHOUT PLAN" button using value 
	Then I verify "From Oman to United Arab Emirates" text is present on screen using value 
	And I verify "Rates without international packs" text is present on screen using value 
	And I verify payG rates for SMS and calls disolay 
	When I click on "WITH PLAN" button using value 
	When I click on list of eligible countries button 
	Then I verify "5 supported countries" text is present on screen using value 
	And I verify all included countried displayed 
	When I enter "United Arab Emirates" country name in enter country field present 
	Then I verify enterd country present in the included list 
	When I click on cross button 
	When I enter "India" country name in enter country field present 
	Then I verify enterd country not an eligile country 
	
	#Combined cases
	#To verify if "With plan" tab displays the offers according to the available validities.
	#To verify if the available offers are scrollable and their price and name are changing dinamically
	#To verify if the available unlimited packages are displayed if present in the particular tab	
	#To verify user can navigate to summary screen when click on Pay | SR X" button
	#To verify if the "Included" dropdown button displayed the countries
	#To verify  if the "Back" button navigates to the previous screen
	#To verify  if the "Help" button navigates to the Help screen
	#To verify if the "PLAN DETAILS" button displayed the detailed plan detail
@InternationalCallsJenkins 
@test10518 
Scenario: 
	Given I update the test rail case ID "10518" 
	When I tap on EXISTING CUSTOMER button 
	Then I verify "Sign In" Screen appears 
	When I enter mobile number 
	When I tap on next button 
	Then I verify "Verification" Screen appears 
	When I enter verification code 
	When I click on next button 
	When I close the Welcome popup 
	When I tap for guide popup 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify user is on dashboard 
	When I click on "More" button present on screen 
	Then I verify "Services" text is present on screen using value 
	When I click on "Services" button using value 
	Then I verify "Other services" Screen appears 
	When I click on "International Call Rates" button using value 
	Then I verify "International Calls" Screen appears 
	When I click on "Enter country" button using value 
	When I enter "Bangladesh" country name in enter country field present 
	When I select country from list 
	Then I verify "International Calls" Screen appears 
	When I click on "WITH PLAN" button using value 
	Then I verify "UNLIMITED" text is present on screen using value 
	And I verify "Valid for UNLIMITED" text is present on screen using value 
	When I click on "30 days" button using value 
	Then I verify "Valid for 30 days" text is present on screen using value 
	And I verify "RO 1.000" text is present on screen using value 
	When I scroll the offer 
	Then I verify "RO 3.000" text is present on screen using value 
	When I scroll the screen upwards 
	When I click on "PAY | 3.000" button using value 
	Then I verify "Summary" Screen appears 
	When I click on "Included" button using value 
	Then I verify "Bangladesh" text is present on screen using value 
	When I tap on back button 
	Then I verify "International Calls" Screen appears 
	When I scroll the screen upwards 
	When I click on "PAY | 3.000" button using value 
	When I click on help button present 
	Then I verify "FAQ" Screen appears 
	When I tap on back button 
	Then I verify "Summary" Screen appears 
	When I click on "PLAN DETAILS" button using value 
	Then I verify "Plan Details" Screen appears 
	When I tap on back button 
	Then I verify "Summary" Screen appears 
	
	