Feature: Verify Welcome and GetAsim screen functionality 

Background: 
	When I install the application 
	
	#COMBINED CASES
	#To verify if the Splash screen is displayed once the application is started  
	#To verify if the Welcome screen is displayed once the splash screen is finished 
	#To verify if there is an "EXISTING CUSTOMER" button displayed on the Welcome screen 
	
@Test9885 
@WelcomGetAsim 
Scenario: 
	Given I update the test rail case ID "9885" 
	Then I verify I am on splash screen 
	Then I verify Welcome screen is displayed 
	Then I verify EXISTING CUSTOMER button displayed 
	When I tap on EXISTING CUSTOMER button 
	Then I verify I am on "Sign In" screen 
	
	#COMBINED CASES
	#To verify if there is an "SEND ME A SIM" button displayed on the Welcome screen and  if clicking on the "SEND ME A SIM" button navigates the user to the Get a SIM Screen 
	#To verify the user can enter text in the full name field 
	#To verify the user can enter text in the email field 
	#To verify the user can enter text in the location field 
	#To verify if the user can input data in the Number field 
	#To verify if clicking on "Prefix" code dropdown opens a list with countries 
	#To verify if the user can input data in the "Find country" field 
	#To verify if there is a help button in the header.
	#"X" button is displayed and closed when clicked. 
	#To verify if "Selected country prefix code" appears when user selects a country.  
	#To verify that clicking the SUBMIT button a pop up is displayed 
	#To verify that clicking the Ok button on the pop up opens the Welcome Screen 
@Test9886 
@WelcomGetAsim 
Scenario: 
	Given I update the test rail case ID "9886" 
	Then I verify SEND ME A SIM button displayed 
	When I tap on SEND ME A SIM button 
	Then I verify I am on "Get a SIM" screen 
	When I enter text in the full Name field 
	Then I verify i am able to enter text 
	When I enter text in the email field 
	Then I verify i am able to enter email 
	When I enter location in the location field 
	Then I verify i am able to enter location
	When I enter input in number field 
	Then I verify i am able to enter number  
	When I tap on dropdown Arrow 
	Then I verify I am on "Select Country" screen 
	When I enter text in enter country field 
	Then I verify i am able to enter country name 
	Then I verify help button displayed 
	Then I verify X button displayed 
	When I tap on X button 
	Then I verify I am on "Get a SIM" screen 
	When I tap on dropdown Arrow 
	When I enter country name in enter country field 
	When I select country 
	Then I verify Selected country prefix code appears 
	When I tap on submit button 
	Then I verify pop up Displayed 
	When I click on "OK" button 
	Then I verify Welcome screen is displayed 
	
@Test6590 
@WelcomGetAsim 
Scenario: 
	To verify that clicking the SUBMIT button remain disabled when the user enters invalid data 
	Given I update the test rail case ID "6590" 
	When I tap on SEND ME A SIM button 
	When I enter invalid text in the full Name field 
	When I enter invalid location in the location field 
	When I enter invalid input in number field 
	Then I verify submit button remain disabled 