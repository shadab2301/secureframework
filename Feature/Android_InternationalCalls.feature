Feature: Verify International Calls screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	
	#combined case
	#To verify if the user is navigated to International Calls service screen from Services in More screen
	#To verify if the Help button navigates to the Help screen	 
	#To verify if the eligible country list will be displayed once a text is typed into the input search field 
	#To verify if the eligible country list will be displayed when the search icon is clicked
	#To verify if the detailed view of international calls' offers for the selected country will be displayed
	#To verify if the available unlimited packages are displayed if present in the particular tab
	#To verify if the Help button navigates to the Help screen	
	#To verify if the Change button navigates the user back to the Country Picker screen	
	#To verify if the user can click and see all the countries included in the currently selected international plan		
	#To verify if clicking the "Without plan" tab displays the PAYG offers and prices for the selected country	
	#To verify if selecting a country would result in peak and off peak text with other details
	
@test9098 
@Android_InternationalCalls 
Scenario: 
	Given I update the test rail case ID "9098" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "International Call Rates" button 
	Then I verify I am on "International Calls" screen 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I tap on Cross button 
	When I click on "Enter country" button 
	When I enter country name in enter country field 
	Then I verify enterd country list display 
	When I refresh the screen 
	When I tap back button present screen 
	When I tap on search icon present on screen 
	Then I verify the list of countries is displayed 
	When I enter country name in enter country field 
	When I select country 
	Then I verify country logo display 
	Then I verify "CHANGE" button is Displayed 
	And I verfiy "WITH PLAN" text is displayed 
	And I verfiy "WITHOUT PLAN" text is displayed 
	And I verfiy "Unlimited" text is displayed 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I refresh the screen 
	When I tap on Cross button 
	When I click on "CHANGE" button 
	Then I verify country list screen display 
	When I select country 
	When I click on "WITHOUT PLAN" button 
	Then I verfiy "From Oman to United Arab Emirates" text is displayed 
	And I verify PAYG prices display for the selected country 
	When I refresh the screen 
	When I tap back button present screen 
	When I select country without having available plan 
	Then I verfiy "From Oman to Afghanistan" text is displayed 
	And I verify PAYG prices display for the selected country 
	
	#combined case	
	#To verify if the user can search if a country is a part of the eligible countries	
	#To verify if the user won't find the country in the list if it not a part of the eligible countries
	#To verify user can navigate to summary screen when click on Pay | SR X" button
	#To verify if the "Included countries" dropdown button displayed the countries
	#To verify  if the "Help" button navigates to the Help screen
	#To verify if the "PLAN DETAIL" button displayed the detailed plan detail
	#To verify  if the "Back" button navigates to the previous screen	
	#To verify if the "Confirm" button will buy the package with sufficient wallet balance
	#To verify if the boughten offer is displayed on Dashboard
	
@test9099 
@Android_InternationalCalls 
Scenario: 
	Given I update the test rail case ID "9099" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "International Call Rates" button 
	When I tap on search icon present on screen 
	When I enter country name in enter country field 
	When I select country 
	When I click on "list of eligible countries" button 
	Then I verify supported countries display 
	And I verify enterd country "United Arab Emirates" is part of eligible countries 
	And I verify enterd country "India" is not a part of eligible countries 
	When I tap on device back button 
	When I scroll the screen upwards 
	When I click on "PAY | RO 1.000" button 
	Then I verify I am on "Summary" screen 
	And I verify "Included" button is Displayed 
	When I click on "Included" button 
	Then I verify dropdown expended 
	When I click on "Included" button 
	Then I verify dropdown collapsed 
	When I tap on help Icon 
	Then I verify I am on help screen 
	When I tap on Cross button 
	When I click on "PLAN DETAILS" button 
	Then I verify I am on "Plan Details" screen 
	When I tap on Cross button 
	Then I verify I am on "Summary" screen 
	When I click on "CONFIRM" button 
	Then I verify I am on dashboard 
	When I scroll the screen upwards
	When I tap on international call Service type from Graph 
	Then I verfiy purchased pack display 
	
	#Combined case
	#To verify if "With plan" tab displays the offers according to the available Validities. Default options: 1 day, 7 days, 30 days displayed as seprate tabs with separete offers
	#To verify if the available offers are scrollable and their price and name are changing dynamically
@test9193 
@Android_InternationalCalls 
Scenario: 
	Given I update the test rail case ID "9193" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "International Call Rates" button 
	When I tap on search icon present on screen 
	When I enter "Bangladesh" country in enter country field 
	When I select country 
	Then I verfiy "Unlimited" text is displayed 
	And I verfiy "30 Days" text is displayed 
	When I click on "30 Days" button 
	Then I verfiy "1.000" text is displayed 
	When I scroll the offers 
	Then I verfiy "3.000" text is displayed 
	
	#combinedCases
	#To verify if the Back button navigates the user to the previous Other services screen
	#To verify if the Back button navigates back to the Country Picker screen
	#To verify if the "Confirm" button will buy the package
	
@test10734
@Android_InternationalCalls 
Scenario: 
	Given I update the test rail case ID "10734" 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "International Call Rates" button 
	Then I verify I am on "International Calls" screen 
	When I tap back button present screen 
	Then I verify I am on "Other Services" screen 
	When I click on "International Call Rates" button 
	Then I verify I am on "International Calls" screen 
	When I click on "Enter country" button 
	When I enter the country name in enter country field 
	When I select the country 
	Then I verify country logo display 
	When I tap back button present screen 
	Then I verify I am on "International Calls" screen 
	When I select the country 
	Then I verfiy "WITH PLAN" text is displayed 
	When I scroll the screen upward 
	When I click on "PAY | RO 0.500" button 
	Then I verfiy "Summary" text is displayed 
	And I verfiy "International Pack" text is displayed 
	And I verfiy "0.500" text is displayed 
	And I verfiy "Included" text is displayed 
	And I verfiy "Validity" text is displayed 
	And I verfiy "Unlimited" text is displayed 
	And I verfiy "You’ll pay" text is displayed 
	And I verfiy "PLAN DETAILS" text is displayed 
	And I verfiy "CONFIRM" text is displayed 
	When I click on "CONFIRM" button 
	Then I verify I am on dashboard 
	
	
	
	
	
	
	
	
	
	
	
	
	