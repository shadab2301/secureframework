Feature: Verify Data transfer screen functionality 

#Combined cases
#To verify if the Data Transfer screen is displayed
#To verify if the Data Transfer Shares screen is displayed
#To verify if the Help screen is displayed
#To verify if the Back button navigates the user to the Services screen
#To verify if the Close button navigates the user to the Services screen
@Android_DataTransfer 
@test10728 
Scenario: 
	Given I update the test rail case ID "10728" 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	Then I verify I am on "Sign In" screen 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	Then I verify I am on dashboard 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	Then I verify I am on "Other services" screen 
	When I click on "Data Transfer" button 
	Then I verify I am on "Share your data" screen 
	And I verify You cannot transfer data that already transferred to you Text display 
	When I tap back button present screen 
	Then I verify I am on "Other services" screen 
	When I click on "Data Transfer" button 
	Then I verify I am on "Share your data" screen 
	When I tap on help Icon 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	Then I verify I am on "Share your data" screen 
	And I verify close button display 
	When I tap close button 
	Then I verify Other services Screen display 
	
	#Combined cases
	#To verify if the Data Transfer screen is displayed
	#To verify if the Help screen is displayed
	#To verify if the Back button navigates the user to the Services screen
	#To verify if Learn more will open Data transfer FAQ section
	#To verify if the Data carousel should be scrollable displaying predefined MB values
	#To verify if the data validity is correctly displayed (limited or unlimited)
	#To verify if the Continue button will navigate the user to the next Enter number screen
	#To verify if the Help screen is displayed
	#To verify if the Back button navigates the user to the previous screen
	#To verify if the input field is enabled to put data into
	#To verify if clicking the Recently used list will display the numbers that previously data is transferred to
	#To verify if the Recently used list can be collapsed once expanded
	#To verify if the valid number will enable the Continue button to proceed to next screen
	#To verify if the MBs value is correctly displayed
	#To verify if the Help screen is displayed
	#To verify if the selected data can be reviewed and changed 
	#To verify if the GBs value is correctly displayed
	#To verify if the entered number can be reviewed and changed 
	#To verify if the Share data button will transfer the data to the selected number
	#To verify if the user will be navigated to the Help screen
	#To verify if the user will know if the transfer was successful
	
@Android_DataTransfer 
@test9412 
Scenario: 
	Given I update the test rail case ID "9412" 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number in field 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	When I click on "More" button 
	Then I verfiy "Services" text is displayed 
	When I click on "Services" button 
	When I click on "Data Transfer" button 
	Then I verify I am on "Share your data" screen 
	When I tap on help Icon 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	When I tap back button present screen 
	Then I verify I am on "Other services" screen 
	When I click on "Data Transfer" button 
	Then I verify I am on "Share your data" screen 
	When I click on Learn more button 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	Then I verify carousel display 
	And I verfiy "VALIDITY" text is displayed 
	And I verfiy "10 DAYS" text is displayed 
	When I click on "CONTINUE" button 
	And I verfiy Add your friends number text is displayed 
	When I tap on help Icon 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	When I tap back button present screen 
	Then I verfiy "Know how much can you share." text is displayed 
	When I click on "CONTINUE" button 
	Then I verify able to input "1234" data in number field 
	When I tap on recently used dropdown 
	Then I verify previously used number display 
	When I tap on recently used dropdown 
	Then I verify previously used number list collapsed 
	When I enter number "79566458" into number field 
	When I click on "CONTINUE" button 
	Then I verfiy "Data you’re going to share" text is displayed 
	Then I verfiy "50 MB" text is displayed 
	When I tap on help Icon 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	When I tap on data Change button 
	Then I verify carousel display 
	When I scroll the data carousel 
	When I scroll the data carousel 
	When I click on "CONTINUE" button 
	Then I verfiy Add your friends number text is displayed 
	When I click on "CONTINUE" button 
	When I tap on number Change button 
	Then I verfiy "Recently used" text is displayed 
	When I click on "CONTINUE" button 
	Then I verfiy "1 GB" text is displayed 
	When I click on "SHARE DATA" button 
	Then I verfiy "You’ve successfully shared" text is displayed 
	When I tap on help Icon 
	Then I verfiy "Data Transfer Service" text is displayed 
	When I tap on Cross button 
	Then I verify "AWESOME" button is Displayed 
	When I click on "AWESOME" button 
	Then I verify I am on "Other services" screen 
	
	#Combined cases
	#To verify if the successfully transferred data with limited validity is properly displayed on receiver's Dashboard
	#To verify if the data amount is coloured red and underlined, with a disabled Continue button when the user does not have enough data to transfer
	#To verify if buying a booster will enable data transfer
@Android_DataTransfer 
@test9884 
Scenario: 
	Given I update the test rail case ID "9884" 
	When I install the application 
	Then I verify I am on splash screen 
	When I tap on EXISTING CUSTOMER button 
	When I enter "79566458" mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I close Welcome popup 
	When I tap for guide popup 
	Then I verify I am on dashboard 
	When I scroll the screen upwards 
	And I verify transfered data displayed on dashboard 
	When I tap on received data over dashboard 
	Then I verfiy "Valid until" text is displayed 
	When I click on cross Button 
	Then I verify I am on dashboard 
	When I click on "More" button 
	When I click on "Services" button 
	When I click on "Data Transfer" button 
	Then I verify I am on "Share your data" screen 
	When I scroll the data carousel 
	When I scroll the data carousel 
	Then I verify BUY DATA button is Displayed 
	When I tap on buy data button 
	When I tap for guide popup 
	When I tap for guide popup 
	Then I verify Plans Screen display
	
	
	
	
	