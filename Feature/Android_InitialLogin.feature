Feature: Verify Initial login screen functionality 

Background: 
	When I install the application 
	Then I verify I am on splash screen 
	
@Test6534 
@InitialLogin 
Scenario: To verify that the promo pop-up coach marks is displayed 
	Given I update the test rail case ID "6534" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	Then I verify promo popup coach marks is displayed 
	
@Test6536 
@InitialLogin 
Scenario: To verify that the Buy Plan Coach Mark is displayed 
	Given I update the test rail case ID "6536" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I tap on cross button 
	Then I verify Buy Plan Coach Mark is displayed 
	
@Test6537 
@InitialLogin
Scenario: To verify that the Coach Mark are not displayed 
	Given I update the test rail case ID "6537" 
	When I tap on EXISTING CUSTOMER button 
	When I enter mobile number 
	When I tap on next button 
	When I enter verification code 
	When I click on next button 
	When I tap on cross button 
	Then I verify Buy Plan Coach Mark is displayed 
	When I tap on Buy button option 
	Then I verify that the Coach Mark Disappears 
	
	
	