package com.friendimobile.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
/**
 * 
 * @author nimit.jain
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(

		 plugin = { "progress", "html:target/Cucumber3", "json:target/cucumber-3.json",

		"html:target/cucumberr-report3.html", "junit:target/cucumber-report3.xml","rerun:target/failedRerun.txt" },
		 
		 glue = {"com.friendimobile.stepdefinitions"}

		  ,tags= {"@InternationalCallsJenkins"}  
		 ,features = {"Feature/"}
		 
	
		 )

public class CukeRunner {

}

