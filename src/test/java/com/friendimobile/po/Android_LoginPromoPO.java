package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_LoginPromoPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_LoginPromoPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/LoginPromo");
		System.out.println("Executed platform name  " + platformName);
	}

	/** tap on claim button */
	public void tapOnClaimButton() {
		try {
			if (element("claimbutton").isDisplayed()) {
				element("claimbutton").click();
			}
		} catch (Exception e) {
			System.out.println("Already Claimed");
		}
	}

	/** verify claim success pop displayed */
	public boolean claimSuccessDisplayed() {
		boolean status = false;
		try {
			if (element("claimsuccess").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			status = true;
			System.out.println("Already Claimed");
		}
		return status;
	}

	/**
	 * Verifying Dashboard screen
	 * 
	 */
	public boolean verifyingdashbaordscreendisplayed() throws InterruptedException {
		boolean status = false;
		try {
			if (element("Dashboard").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			if (element("promocoachmarkpopup").isDisplayed()) {
				System.out.println("promo Not Claimed");
				status = true;
			}
		}
		return status;
	}

	/**
	 * Verifying claimed data display
	 * 
	 */
	public boolean verifyingClaimedDataDisplay() {
		boolean status = false;
		try {
			if (element("claimededata").getText().equals("10 MB")) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("Promo Data not available");
			status = true;
		}
		return status;
	}

	/**
	 * tap on done button
	 * 
	 */
	public void tapDoneButton() {
		try {
			if (element("Donebutton").isDisplayed()) {
				element("Donebutton").click();
			}
		} catch (Exception e) {
			System.out.println("Already claimed");
		}
	}

}
