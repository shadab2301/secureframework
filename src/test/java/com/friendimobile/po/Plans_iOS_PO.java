package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class Plans_iOS_PO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Plans_iOS_PO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Plans_iOS");
		System.out.println("Executed platform name  " + platformName);
	}

	/** verify Plans Screen displayed */
	public boolean verifyPlansSecrrenDisplay() {
		return element("PlansScreen").isDisplayed();
	}

	/** verify coachMark displayed */
	public boolean coachMarkDisplayed() {
		boolean status = false;
		try {
			if (element("Coachmark").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("Coach Mark Disappeared !!!");
			status = false;
		}
		return status;
	}

	/** Tap on offer */
	public void tapOnOffer() {
		elements("offers").get(0).click();
	}

	/** verify data tab selected in plan screen */
	public boolean verifyDataTabSelected() {
		return element("dataTab").getAttribute("value").equals("1");
	}

	/** tap on plans screen */
	public void tapOnScreen() throws InterruptedException {
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(77, 186)).release().perform();
		Thread.sleep(5000);

	}

	/** verify Buy plan button display */
	public boolean verifyBuyPlanButtonDisplay() {
		return element("BuyPlan").isDisplayed();
	}

	/** tap on buy plan button */
	public void tapOnBuyPlanButton() {
		element("BuyPlan").click();
	}

	/** Tap on data button from buy plan section */
	public void tapOnDataButton() {
		element("DataButton").click();
	}

	/** Tap on local calls button from buy plan section */
	public void tapOnLocalCallsButton() {
		element("localcallsButton").click();
	}

	/** Tap on international calls button from buy plan section */
	public void tapOnInternationalCallButton() {
		element("InternationalcallsButton").click();
	}

	/** Tap on mix button from buy plan section */
	public void tapOnMixButton() {
		element("MixButton").click();
	}

	/** verify minutes tab selected in plan screen */
	public boolean verifyMitutesTabSelected() {
		return element("minutesTab").getAttribute("value").equals("1");
	}

	/** verify Mix tab selected in plan screen */
	public boolean verifyMixTabSelected() {
		return element("MixTab").getAttribute("value").equals("1");
	}

	/** verify plus button display */
	public boolean verifyPlusButtonDisplay() {
		return element("plusButton").isDisplayed();
	}

	/** verify data tab displayed in plan screen */
	public boolean verifyDataTabDisplay() {
		return element("dataTab").isDisplayed();
	}

	/** verify minutes tab displayed in plan screen */
	public boolean verifyMitutesTabDisplay() {
		return element("minutesTab").isDisplayed();
	}

	/** verify Mix tab displayed in plan screen */
	public boolean verifyMixTabDisplay() {
		return element("MixTab").isDisplayed();
	}

	/** verify All data plans display */
	public boolean verifyAllDataPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("allowance");
		List<WebElement> element2 = elements("validity");
		System.out.println("size of data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if ((element.get(i).getText().contains("MB") || element.get(i).getText().contains("GB"))
					&& (element2.get(i).getText().contains("Unlimited") || element2.get(i).getText().contains("day"))) {
				System.out.println("list of All data packs " + element.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify All Freedom plans display */
	public boolean verifyFreedomCallsPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("allowance");
		List<WebElement> element2 = elements("validity");
		System.out.println("size of Freedom call pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Minutes") && element2.get(i).getText().contains("Unlimited")) {
				System.out.println("list of Freedom Call packs " + element.get(i).getText());
				status = true;
			}

			else {
				status = false;
				break;
			}
		}
		return status;

	}

	/** verify All mix plans display */
	public boolean verifyAllMixPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("allowance");
		System.out.println("size of mix packs list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("+")) {
				System.out.println("list of Mix packs " + element.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** Tap on Minutes tab */
	public void tapOnMinutestab() {
		element("minutesTab").click();
	}

	/** Tap on Mix tab */
	public void tapOnMixtab() {
		element("MixTab").click();
	}

	/** verify current balance display */
	public boolean verifyCurrentBalanceDisplay() {
		return element("currentbalance").isDisplayed();
	}

	/** verify carousel display */
	public boolean verifyCarouselDisplay() {
		return element("BannerImage").isDisplayed();
	}

	/**
	 * scroll carousel
	 * 
	 * @throws InterruptedException
	 */
	public void scrollCarousel() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;

		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int y = (height) * 3 / 10;

		int x_right = (int) (width * 0.8);

		int x_left = (int) (width * 0.2);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x_right, y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))).moveTo(ElementOption.point(x_left, y))
				.release().perform();

		System.out.println("\nSwiping from Right to Left !!!");

		Thread.sleep(2000);
	}

	/** Tap on data tab */
	public void tapOnDatatab() {
		element("dataTab").click();
	}

	/** Tap on all data plans button */
	public void tapOnAllDataPlansButton() {
		element("AllDataplansbutton").click();
	}

	/** Tap on freedom data plans button */
	public void tapOnFreedomDataPlansButton() {
		element("freedomdataplanbutton").click();
	}

	/** Tap on saver data plans button */
	public void tapOnSaverDataPlansButton() {
		element("saverdataplanbutton").click();
	}

	/** Tap on saver call button */
	public void tapOnSaverCallsButton() {
		element("savercallplanbutton").click();
	}

	/** Tap on International calls button */
	public void tapOnInternationalCallsButton() {
		element("InternationalCallbutton").click();
	}

	/**
	 * Verify DATA validity allowance and price display
	 */
	public boolean verifyAlldataPacks() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))
					&& (element3.get(i).getText().contains("RO"))) {
				System.out.println("validity of all packs " + element.get(i).getText());
				System.out.println("allowance of all packs " + element2.get(i).getText());
				System.out.println("price of all packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/**
	 * Verify Call validity allowance and price display
	 */
	public boolean verifyAllCallPacks() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity") && element2.get(i).getText().contains("Minutes")
					&& element3.get(i).getText().contains("RO")) {
				System.out.println("validity of all packs " + element.get(i).getText());
				System.out.println("allowance of all packs " + element2.get(i).getText());
				System.out.println("price of all packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/**
	 * Verify Mix packs validity allowance and price display
	 */
	public boolean verifyAllMixPacks() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity") && element2.get(i).getText().contains("+")
					&& element3.get(i).getText().contains("RO")) {
				System.out.println("validity of Mix packs " + element.get(i).getText());
				System.out.println("allowance of Mix packs " + element2.get(i).getText());
				System.out.println("price of all Mix packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/** verify international call packs displayed */
	public boolean verifyinternationalCallacksDisplayed() {
		boolean status = false;
		try {
			List<WebElement> internationalpacks = elements("allowance");
			System.out.println("size of call pack list" + internationalpacks.size());
			for (int i = 0; i < internationalpacks.size(); i++) {

				if (internationalpacks.get(i).getText().contains("Minutes") && element("CountryLogo").isDisplayed()) {
					System.out.println("list of international Call packs " + internationalpacks.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		} catch (Exception e) {
			if (element("selectcountryText").getText().contains("Select a country")) {
				System.out.println("international call packs not display");
				status = true;
			}
		}
		return status;
	}

	/**
	 * Verify selected pack
	 */
	public boolean verifySelectedPackDisplay() {
		boolean status = false;
		if (element("pack_name").getText().contains("Local Minutes")
				&& element("validity_text").getText().contains("days") && element("price").getText().contains("RO")) {
			status = true;
		}
		return status;
	}

	/** verify MSISDN display */
	public boolean verifyMSISDNDisplay() {
		return element("MSISDN").getText().contains("‭+968 7952 5424‬");
	}

	/** verify Enter country field display */
	public boolean verifyEnterCountrySeachField() {
		return element("EnterCountryField").isDisplayed();
	}

	/** verify included countries display */
	public boolean verifyIncludedCountriedDisplay() {
		return element("includedcountries").isDisplayed();
	}

	/** verify Saver calls plans display */
	public boolean verifySaverCallsPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("allowance");
		List<WebElement> element2 = elements("validity");
		System.out.println("size of Saver call pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if ((element.get(i).getText().contains("Hours"))
					|| (element.get(i).getText().contains("Unlimited")) && element2.get(i).getText().contains("day")) {
				System.out.println("list of Freedom Call packs " + element.get(i).getText());
				status = true;
			}

			else {
				status = false;
				break;
			}
		}
		return status;

	}

	/** verify freedom data packs displayed */
	public boolean verifyFreedomDataPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		System.out.println("size of Freedom data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Unlimited")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))) {
				System.out.println("list of Freedom data packs " + element2.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify Saver data packs displayed */
	public boolean verifySaverDataPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		System.out.println("size of Saver data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("day")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))) {
				System.out.println("list of Saver data packs " + element2.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/**
	 * verify freedom data plans text button display
	 * 
	 * 
	 */
	public boolean verifyFreedomDataPlanTextDisplay() {
		return element("freedomdataplantext").getText().contains("Freedom Data Plans");
	}

	/**
	 * verify freedom data plans text button display
	 * 
	 * 
	 */
	public boolean verifySaverDataPlanTextDisplay() {
		return element("Saverdataplantext").getText().contains("Saver Data Plans");
	}

	/**
	 * verify All data plans text display
	 * 
	 * 
	 */
	public boolean verifyAllDataPlanTextDisplay() {
		return element("Alldataplantext").getText().contains("All Data Plans");
	}

	/**
	 * verify freedom Call plans button display
	 * 
	 * 
	 */
	public boolean verifyFreedomCallPlanTextDisplay() {
		return element("freedomCallplanstext").getText().contains("Freedom Call Plans");
	}

	/**
	 * verify Saver Call Plans text display
	 * 
	 * 
	 */
	public boolean verifySaverCallPlanTextDisplay() {
		return element("savercallplanstext").getText().contains("Saver Call Plans");
	}

	/**
	 * verify International Calls text display
	 * 
	 * 
	 */
	public boolean verifyInternationalCallsTextDisplay() {
		return element("internationalcallstext").getText().contains("International Calls");
	}

	/**
	 * verify International Calls text display
	 * 
	 * 
	 */
	public boolean verifySaverMixPlansTextDisplay() {
		return element("SavermixPlanstext").getText().contains("Saver Mix Plans");
	}

	/** verify mobile Text displayed */
	public boolean verifyMobileTextDisplayed(String Str) {
		boolean status = false;
		String ele = element("MobileText").getAttribute("name");
		System.out.println(ele);
		if (ele.equals(Str)) {
			status = true;
		}
		return status;
	}

	/** verify select country displayed */
	public boolean verifySelectsCountryDisplayed() {
		return element("selectcountryText").getText().contains("Select a country");
	}

	/** Tap on select country button */
	public void tapOnSSelectCountryButton() {
		element("selectcountrybutton").click();
	}

	/** verify GCC country displayed */
	public boolean verifyGCCTextDisplayed() {
		return element("countryname").getText().contains("GCC");
	}

	/** verify UAE country displayed */
	public boolean verifyUAETextDisplayed() {
		return element("countryname").getText().contains("United Arab Emirates");
	}

	/** verify text displayed in summary screen */
	public boolean verifyTextSaverMixPlanTextDisplayInSummaryScreen() {
		return element("TextTitle").getText().contains("Saver Mix Plans");
	}

	/** Tap on confirm button */
	public void tapOnConfirmButton() {
		element("ConfirmButton").click();
	}

	/** verify purchased data pack display */
	public boolean verifyDatapack() {
		return elements("pack").get(0).getText().contains("250 MB");
	}

	/** verify same data pack twice display */
	public boolean verifySameDatapackTwice() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("250 MB")
				&& elements("pack").get(1).getText().contains("250 MB")) {
			status = true;
		}
		return status;
	}

	/** Tap on different offer */
	public void tapOnDifferentOffer() {
		elements("offers").get(1).click();
	}

	/** verify different data pack display */
	public boolean verifyDifferentDatapack() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("950 MB")
				&& elements("pack").get(1).getText().contains("250 MB")) {
			status = true;
		}
		return status;
	}

	/**
	 * select local calls option from graph
	 * 
	 * @throws InterruptedException
	 */
	public void selectLocalCallsFromGraph() throws InterruptedException {
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(127, 420)).release().perform();
		Thread.sleep(5000);
		// element("graphlocalcalls").click();
	}

	/** verify purchased Freedom local call pack display */
	public boolean verifyFreedomLocalCallspack() {
		return elements("pack").get(0).getText().contains("60 mins");
	}

	/** verify same freedom local calls pack twice display */
	public boolean verifySameFreedomLocalCallpackTwice() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("60 mins")
				&& elements("pack").get(1).getText().contains("60 mins")) {
			status = true;
		}
		return status;
	}

	/** verify different freedom local calls pack display */
	public boolean verifyDifferentFreedomLocalCallpack() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("200 min")
				&& elements("pack").get(1).getText().contains("60 mins")) {
			status = true;
		}
		return status;
	}

	/** verify purchased Saver local call pack display */
	public boolean verifySaverLocalCallspack() {
		return elements("pack").get(0).getText().contains("28.5 hours");
	}

	/** verify same Saver local calls pack twice display */
	public boolean verifySameSaverLocalCallpackTwice() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("28.5 hours")
				&& elements("pack").get(1).getText().contains("28.5 hours")) {
			status = true;
		}
		return status;
	}

	/** verify different saver local calls pack display */
	public boolean verifyDifferentSaverLocalCallpack() {
		boolean status = false;
		if (element("UnlimitedPack").getText().contains("Unlimited")
				&& elements("pack").get(0).getText().contains("28.5 hours")) {
			status = true;
		}
		return status;
	}

	/**
	 * select International calls option from graph
	 * 
	 * @throws InterruptedException
	 */

	public void selectInternationalCallsFromGraph() throws InterruptedException {
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(165, 465)).release().perform();
		Thread.sleep(5000);
		// element("graphInternationalcalls").click();

	}

	/** verify purchased International calls pack display */
	public boolean verifyInternationalCallspack() {
		return elements("pack").get(0).getText().contains("15 mins");
	}

	/** verify same International calls pack twice display */
	public boolean verifySameInternationalCallspackTwice() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("15 mins")
				&& elements("pack").get(1).getText().contains("15 mins")) {
			status = true;
		}
		return status;
	}

	/** verify different International calls pack display */
	public boolean verifyDifferentInternationalCallspack() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("48 mins")
				&& elements("pack").get(1).getText().contains("15 mins")) {
			status = true;
		}
		return status;
	}

	/** verify International calls zone pack display */
	public boolean verifyInternationalZonepack() {
		boolean status = false;
		if (elements("pack").get(0).getText().contains("15 mins")) {
			status = true;
		}
		return status;
	}
}
