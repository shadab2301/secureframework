package com.friendimobile.po;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class MoreScreeniOSPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public MoreScreeniOSPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/MoreScreeniOS");
		System.out.println("Executed platform name  " + platformName);
	}

	/**
	 * Close Welcome Pop up
	 */
	public void closeWelcomePopup() {
		try {
			WebDriverWait wait = new WebDriverWait(session.driver, 180);
			WebElement locate = wait.until(ExpectedConditions.elementToBeClickable(element("closeWelcomePopup")));
			locate.click();
		} catch (Exception e) {
			System.out.println("already closed");
		}
	}

	/**
	 * Dyanmic method to click
	 */
	public void clickButton(String button) throws InterruptedException {
		Thread.sleep(5000);
		// XCUIElementTypeButton[@name="AUTO TOP-UP"]
		String x = "//*[@name='";
		String y = "']";
		String xpath = x + button + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.elementToBeClickable((By.xpath(xpath))));
		locate.click();

	}

	/**
	 * Dyanmic method to verify Screen title
	 */
	public boolean verifyScreen(String screen) {
		System.out.println("waiting");
		// XCUIElementTypeOther[@name="Top-up My Wallet"]
		String x = "//*[@name='";
		String y = "']";
		String xpath = x + screen + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Screen");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isDisplayed();
	}

	/**
	 * Dyanmic method to click using value
	 */
	public void clickButtonValue(String button) throws InterruptedException {
		Thread.sleep(3000);
		// XCUIElementTypeButton[@value="AUTO TOP-UP"]
		String x = "//*[@value='";
		String y = "']";
		String xpath = x + button + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.elementToBeClickable((By.xpath(xpath))));
		locate.click();

	}

	/**
	 * Dyanmic method to verify Button
	 */
	public boolean verifyButton(String button) {
		System.out.println("waiting");
		// XCUIElementTypeOther[@name="Top-up My Wallet"]
		String x = "//*[@name='";
		String y = "']";
		String xpath = x + button + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Screen");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isDisplayed();
	}

	/**
	 * to click on toggle
	 */
	public void clickOnToggle() {
		element("toggle").click();

	}

	/**
	 * dynamic methos to verify text
	 */
	public boolean verifyText(String text) {
		System.out.println("waiting");
		// XCUIElementTypeOther[@name="Top-up My Wallet"]
		String x = "//*[@name='";
		String y = "']";
		String xpath = x + text + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Screen");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isEnabled();

	}

	public void allowPopup() throws InterruptedException {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.elementToBeClickable(element("allowPopup")));
		locate.click();

	}

	/**
	 * to tap on screen
	 */
	public void tapOnScreen() throws InterruptedException {
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(77, 186)).release().perform();
		Thread.sleep(5000);

	}

	/**
	 * verify toggle display
	 */
	public boolean togglePopup() {
		return element("maskButton").isDisplayed();

	}

	/**
	 * verify user is able to enter password
	 */
	public boolean verifyUserAbleToEnterPass() {
		element("passwordField").click();
		element("passwordField").sendKeys("12345");
		try {
			if (element("passwordField").isDisplayed()) {
				return false;
			}

		} catch (Exception e) {
			System.out.println("Password entered");

		}

		return true;
	}

	/**
	 * mask and unmask button verify
	 */
	public boolean verifyMaskButtonMaskAndUnmask() {
		element("maskButton").click();
		String pass1 = element("passwordField2").getText();
		if (pass1.equals("12345")) {
			return true;

		}
		return false;
	}

	public boolean verifyDropDownOpens() {
		return element("dropDownOpen").isDisplayed();
	}

	/**
	 * click on done
	 */
	public void clickOnDone() {
		element("dropDownOpen").click();

	}

	public boolean verifyVersion() {
		return element("versionName").isDisplayed();
	}

	/**
	 * verify x button is present
	 */
	public boolean verifyXbutton() {
		return element("backbutton").isDisplayed();
	}

	/**
	 * verify search button is present
	 */
	public boolean verifySearchbutton() {
		return element("searchIcon").isDisplayed();
	}

	/**
	 * open search button is present
	 */
	public void searchIconOpen() {
		element("searchIcon").click();

	}

	/**
	 * verify search bar is present
	 */
	public boolean verifySearchbarOpen() throws InterruptedException {
		Thread.sleep(1000);
		return element("searchBar").isDisplayed();

	}

	/**
	 * verify category
	 */
	public boolean verifyCategories() {
		boolean status = false;
		List<WebElement> Categories = elements("categoriesPresent");
		for (int i = 0; i < Categories.size(); i++) {
			if (Categories.get(i).isDisplayed()) {
				System.out.println(Categories.get(i).getText());
				status = true;
			} else {
				break;
			}
		}
		return status;
	}

	/**
	 * click on back button
	 */
	public void clickOnBackButton() {
		try {
			element("backButtonOnCategory").click();
		} catch (Exception e) {
			element("otheBackButtonOnSubCategory").click();
		}
	}

	/**
	 * verify default search
	 */
	public boolean verifyDefaultSearch() {
		return element("defaultSearch").isDisplayed();
	}

	/**
	 * click on Default screen
	 */
	public void clickOnDefaultScreen() {
		elements("defaultSearch").get(0).click();
	}

	/**
	 * To enter data in search field
	 */
	public void enterInput(String strArg1) {
		element("searchBar").sendKeys(strArg1);
	}

	/**
	 * Dynamic method to verify screen using value
	 */
	public boolean verifyScreenUsingValue(String strArg1) {
		System.out.println("waiting");
		// XCUIElementTypeOther[@value="Top-up My Wallet"]
		String x = "//*[@value='";
		String y = "']";
		String xpath = x + strArg1 + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Screen");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isDisplayed();
	}

	/**
	 * verify text disappears
	 */
	public boolean verifyTextDisappears() {
		return element("foundyouranswertext").isDisplayed();
	}

	/**
	 * Verify Text displays entered in field
	 */
	public boolean verifyTextInfield(String strArg1) {
	if(element("redeemCodeField").getText().equals("CODE HERE"))
	{System.out.println("field is already empty!!!!!!!!!!!!!!!");	
	element("redeemCodeField").click();
	element("redeemCodeField").sendKeys(strArg1);	
	return element("redeemCodeField").getText().equals(strArg1);}
	else {
	element("redeemCodeField").clear();
	if(element("redeemCodeField").getText().equals("CODE HERE"))
	{System.out.println("field is empty now!!!!!!!!!!!!!!!");}	
	element("redeemCodeField").click();
	element("redeemCodeField").sendKeys(strArg1);
	return element("redeemCodeField").getText().equals(strArg1);}
	}

	/**
	 * To click on back btn of screen
	 */
	public void clickBack() {
	try {	element("popupBackBtn").click();
	}
	catch(Exception e)
	{element("backBtn").click();
	}
	}
	

}
