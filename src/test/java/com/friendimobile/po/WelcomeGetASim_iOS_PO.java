package com.friendimobile.po;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class WelcomeGetASim_iOS_PO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public WelcomeGetASim_iOS_PO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/WelcomeGetASim_iOS");
		System.out.println("Executed platform name  " + platformName);
	}

	/*
	 * To verify user is on dashboard
	 */
	public boolean verifyDashboardScreen() throws InterruptedException {
		boolean status = false;
		WebDriverWait wait = new WebDriverWait(session.driver, 30);
		if (wait.until(ExpectedConditions.visibilityOf(element("dashboardHeader"))).isDisplayed()) {
			status = true;
			System.out.println("/n On Dashboard screen !!");
			Thread.sleep(4000);
		} 

		return status;
	}

	/* 
	 * To verify user is able to enter name 
	 */
	public boolean verifyName(String strArg1) {
	element("nameField").click();
	element("nameField").sendKeys(strArg1);
	if (element("nameField").getText().equals(strArg1))
	return true;
	else
	return false;
	}

	/*
	 * To verify user is able to enter email
	 */
	public boolean verifyEmail(String strArg1) {
		element("emailField").click();
		element("emailField").sendKeys(strArg1);
		if (element("emailField").getText().equals(strArg1))
		return true;
		else
		return false;
	}

/*
 * To verify user is able to enter number
 */
	public boolean verifyNumber(String strArg1) {
	element("numberField").click();
	element("numberField").sendKeys(strArg1);
	if (element("numberField").getText().equals(strArg1))
	return true;
	else
	return false;
	}

/*
 * To verify user is able to enter location	
 */
	public boolean verifyLocation(String strArg1) {
		element("locationField").click();
		element("locationField").sendKeys(strArg1);
		System.out.println("already present text is " +element("locationField").getText());
		if (element("locationField").getText().equals(strArg1))
		return true;
		else
		return false;
	}

	/*
	 * To verify user is able to enter country name in the field
	 */
	public boolean verifyCountryName(String strArg1) {
		element("enterCountryField").click();
		element("enterCountryField").sendKeys(strArg1);
		if (element("enterCountryField").getText().equals(strArg1))
		return true;
		else
		return false;
	}

}
