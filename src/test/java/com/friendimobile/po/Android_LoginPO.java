package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;


public class Android_LoginPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_LoginPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Login");
		System.out.println("Executed platform name  " + platformName);
	}

	/** tap on back button */
	public void tapBackButton() {
		element("backbutton").click();
	}

	/** tap on help button */
	public void tapOnHelpIcon() {
		element("helpIcon").click();
	}

	/** verify mobile no field display */
	public boolean mobileNumberFieldDisplayed() {
		return element("MobileNumberField").isDisplayed();
	}

	/** verify help text display */
	public boolean helpTextDisplayed() {
		return element("MobileNumberField").getText().equals("Mobile number");
	}

	/** enter mobile no */
	public void enterMobileNumber(String id) {
		if (platformName.equals("android") || platformName.equals("sauceAndroid")) {
		element("MobileNumberField").click();
		element("MobileNumberField").sendKeys(id);}
		else if (platformName.equals("iPhone") || platformName.equals("sauceiOS")) {	
		element("MobileNumberField").click();
		element("MobileNumberField").sendKeys(id);}
		}

	/** verify help text removed */
	public boolean verifyHelpTextRemoved() {
		boolean status = false;
		try {
			if (element("MobileNumberField").getText().equalsIgnoreCase("Mobile number")) {
				status = false;
			} else {
				status = true;
				System.out.println("Help text is removed !!");
			}
		} catch (Exception e) {
			status = true;
			System.out.println("Help text is removed !!");
		}
		return status;
	}

	/** verify tap on next button */
	public void tapNextButton() {
		element("nextbutton").click();
	}

	/** verify back button of verification screen */
	public void tapBackbutton() {
		element("verificationscrnBackButton").click();
	}

	/** tap on fingerprint icon */
	public void tapOnFingerprintIcon() {
		element("fingerprinticon").click();
	}

	/** verify warning pop up displayed */
	public boolean verifyWarningpopUpDisplayed() {
		return element("warningpopup").isDisplayed();
	}

	/** verify password field help text displayed */
	public boolean verifypasswordHelpTextDisplayed() {
		return element("PasswordField").getText().equals("Enter password");
	}

	public boolean verifyAbletoEnterPasswordfield() {
		element("PasswordField").sendKeys("Test12345");
		return element("PasswordField").getText().equals("Test12345");
	}
}
