package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Login_iOS_PO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Login_iOS_PO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Login_iOS");
		System.out.println("Executed platform name  " + platformName);
	}

	/**
	 * To verify help icon is present
	 */
	public boolean verifyHelpIcon() {
		return element("helpIcon").isDisplayed();
	}

	/**
	 * To click help icon
	 */
	public void clickOnHelpIcon() {
		element("helpIcon").click();
	}

	/**
	 * To enter invalid number in field
	 */
	public void enterInvalidNo() {
		element("mobileNumberField").click();
		element("mobileNumberField").sendKeys("1234567891");
	}

	/**
	 * To verify button is disabled
	 */
	public boolean verifyNextBtn() {
		return element("nextBtn").isEnabled();
	}

	/**
	 * To click on i use my password button
	 */
	public void clickOnBtn() {
		element("myPassBtn").click();
	}

	/**
	 * To verify the entered password is displayed 
	 */
	public boolean verifyPass(String strArg1) {
	  element("passField").click();
	  element("passField").sendKeys(strArg1);
      return element("passField").getText().equals(strArg1);	
	}

	/**
	 * To scroll upward
	 */
	public void scrollUpward(String strArg1) throws InterruptedException {
		for(int i=0;i<strArg1.length();i++) {
		Thread.sleep(2000);
		CommonGestures.longpressScrollUpwardSimultaneously();
		CommonGestures.longpressScrollUpwardSimultaneously();
		CommonGestures.longpressScrollUpwardSimultaneously();}
	}

	/**
	 * To verify promo popup coach marks is displayed
	 */
	public boolean verifyPromoCoachMark() {
		try {
			return element("promoCoachMark").isDisplayed();
		} catch (Exception e) {
			System.out.println("promo is already claimed");
			return true;
		}
	}

	/**
	 * To verify buy plan coach marks is displayed
	 */
	public boolean verifyBuyPlanCoachMark() {
		return element("buyPlanCoachMark").isDisplayed();
	}

	/**
	 * To click on claim btn
	 */
	public void clickClaimButton() {
		try {
			element("claimButton").click();
		} catch (Exception e) {
			System.out.println("claimed!!!!!!");
		}
	}

	/*
	 * # To verify success popup is showed
	 */
	public boolean verifySuccessPopup() {
		try {
			return element("successPopup").isDisplayed();
		} catch (Exception e) {
			System.out.println("claimed!!!!!!");
			return true;
		}
	}

	/**To verify entered no. is displayed */
	public boolean verifyMobilenumber(String mobileno) {
		element("mobileNumberField").click();
		element("mobileNumberField").sendKeys(mobileno);
        return element("mobileNumberField").getText().equals(mobileno);	
	}
}
