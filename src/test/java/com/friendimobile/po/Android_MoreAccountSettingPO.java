package com.friendimobile.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_MoreAccountSettingPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_MoreAccountSettingPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_MoreAccountSetting");
		System.out.println("Executed platform name  " + platformName);
	}

	/** Tap on cross button */
	public void closeWelcomePopUp() {
		try {
			if (element("crossbutton").isDisplayed()) {
				element("crossbutton").click();
			}
		} catch (Exception e) {
			if (element("creditbalanceSection").isDisplayed()) {
				System.out.println("dashboard display");
			}
		}
	}

	/** Tap on buttons */
	public void tapOnButton(String button) {
		String x = "//*[@text='";
		String y = "']";
		String xpath = x + button + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.elementToBeClickable((By.xpath(xpath))));
		locate.click();
	}

	/** verify button displayed */
	public boolean verifyButton(String button) {
		String x = "//*[@text='";
		String y = "']";
		String xpath = x + button + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Button");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isDisplayed();
	}

	/** click on toggle button */
	public void clickOnToggle() {
		element("toggle").click();

	}

	/** verify Text present on screen displayed */
	public boolean verifyText(String text) {
		System.out.println("waiting");
		String x = "//*[@text='";
		String y = "']";
		String xpath = x + text + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Text");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isEnabled();
	}

	/** verify password field displayed */
	public boolean verifyPasswordField() {
		return element("pswdFieldInFingerprintScreen").isDisplayed();
	}

	/**
	 * verify user is able to enter password
	 */
	public boolean verifyUserAbleToEnterPass(String input) {
		boolean status = false;
		element("pswdFieldInFingerprintScreen").click();
		element("pswdFieldInFingerprintScreen").sendKeys(input);
		String orgInput = element("pswdFieldInFingerprintScreen").getText();
		if (input.equals(orgInput)) {
			System.out.println("Password Entered !!!");
			status = true;
		}
		return status;
	}

	/**
	 * mask and unmask button verify
	 */
	public boolean verifyMaskButtonMaskAndUnmask() {
		boolean status = false;
		if (element("pswdFieldInFingerprintScreen").getAttribute("password").contains("true")) {
			System.out.println("The entered password is shown in asterick");
			element("maskButton").click();
			session.driver.navigate().back();
			if (element("pswdFieldInFingerprintScreen").getAttribute("password").contains("false")) {
				status = true;
			}
		}
		return status;

	}

	/** verify Theme pop up displayed */
	public boolean verifypopupDisplayed() {
		return element("themepopup").isDisplayed();
	}

	/** tap on calcle button */
	public void tapCanclebutton() {
		element("canclebutton").click();
	}

	/**
	 * Verifying selected value under dropdown is displayed
	 * 
	 */
	public boolean verifyingSelectedValueUnderDropdown() {
		return element("Mode").getText().contains("Light");
	}

	/**
	 * Verifying Libraries we use screen displayed
	 * 
	 */
	public boolean verifyingLibrariesWeUseScreen() {
		return element("Title").getText().equals("Libraries We Use");
	}

	/**
	 * Enter valid password
	 * 
	 */
	public void enterValidPassword(String validpass) {
		element("pswdFieldInFingerprintScreen").sendKeys(validpass);
	}

	/**
	 * Verifying confirm button enabled
	 * 
	 */
	public boolean verifyConfirmButtonEnabled() {
		return element("confirmbutton").getAttribute("enabled").equals("true");
	}

	/**
	 * tap on back button
	 * 
	 */
	public void tapBackButton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("backbutton"))).click();

	}

	/**
	 * Verifying Application version display
	 * 
	 */
	public boolean verifyingAppVersion() {
		return element("versionTextCode").getText().contains("-UAT-OMAN #");
	}
	
	

}
