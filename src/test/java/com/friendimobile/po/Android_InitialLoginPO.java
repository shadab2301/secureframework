package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_InitialLoginPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_InitialLoginPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/InitialLogin");
		System.out.println("Executed platform name  " + platformName);
	}

	/** Enter verification code */
	public void enterVerificationCode() {
		element("verificationcode").click();
		element("verificationcode").clear();
		element("verificationcode").sendKeys("123456");
	}

	/** tap on next button */
	public void clickNextButton() {
		element("NextButton").click();
	}

	/** verify promo coach mark displayed */
	public boolean promoPopUpCoachMarkDisplayed() {
		boolean status = false;
		try {
			if (element("promocoachmarkpopup").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			status = true;
			System.out.println("already claimed");
		}
		return status;
	}

	/** tap on cross button */
	public void tapCrossButton() {
		try {
			if (element("crossbutton").isDisplayed()) {
				element("crossbutton").click();
			}
		} catch (Exception e) {
			System.out.println("already closed");
		}

	}

	/** verify buy plan coach mark displayed */
	public boolean buyPlanCoachMarckDisplayed() {
		return element("buyplancoachmark").isDisplayed();
	}
}
