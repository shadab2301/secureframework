package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Android_DashboardPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_DashboardPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_Dashboard");
		System.out.println("Executed platform name  " + platformName);
	}

	/** verify credit balance section display */
	public boolean verifyCreditBalanceSectiondisplay() {
		return element("creditbalanceSection").isDisplayed();
	}

	/** verify credit transfer value button */
	public boolean verifyCreditBalancedisplay() {
		return element("creditbalance").isDisplayed();
	}

	/** Tap on arrow button */
	public void tapOnArrowButton() {
		element("arrowbutton").click();
	}

	/** verify friendi and whatsapp logo display */
	public boolean verifyFreewhatsappFriendiCallDisplay() {
		boolean status = false;
		if (element("freefriendicall").isDisplayed() && element("freewhatsappcall").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** verify voucher number field display */
	public boolean verifyVoucherNumberFieldDisplay() {
		return element("vouchernumberfield").getText().equals("VOUCHER NUMBER");
	}

	/** verify able to enter into voucher number field */
	public boolean verifyableToEnterData() {
		boolean status = false;
		element("vouchernumberfield").sendKeys("1234");
		if (element("vouchernumberfield").getText().equals("1234")) {
			status = true;
		}
		return status;
	}

	/** enter data in voucher number field */
	public void enterDatainVoucherNumberField(String data) {
		element("vouchernumberfield").clear();
		element("vouchernumberfield").sendKeys(data);
	}

	/** verify use voucher button display */
	public boolean verifyUseVoucherButton() {
		return element("UseVoucherbutton").isEnabled();

	}

	/** verify error message display */
	public boolean verifyErrorMessageDisplay() {
		return element("errormessage").getText().contains("Voucher not found.");
	}

	/** verify one time top-up field display */
	public boolean verifyOneTimeTopUpFieldDisplay() {
		return element("onetimetopupfield").isDisplayed();
	}

	/** verify enter amount field display */
	public boolean verifyEnterAmountFieldDisplay() {
		return element("Enteramountfield").isDisplayed();
	}

	/** verify able to enter into one time top-up field */
	public boolean verifyAbleToEnterIntopupField() {
		boolean status = false;
		element("Enteramountfield").clear();
		element("Enteramountfield").sendKeys("123");
		if (element("Enteramountfield").getText().equals("123")) {
			status = true;
		}
		return status;
	}

	/** verify pay button display */
	public boolean verifyPayButtonDisplay() {
		return element("paybutton").isDisplayed();
	}

	/** verify add new card screen display */
	public boolean verifyAddNewCardScreenDisplay() {
		return element("addnewcardScreen").getText().equals("Select your payment method");
	}

	/** verify add Debit card webview screen display */
	public boolean verifyAddDebitCardWebviewDisplay() {
		return element("DebitCardwebview").isDisplayed();
	}

	/** tap on cross button */
	public void tapCrossButton() {
		element("crossbutton").click();
	}

	/** verify pay button status */
	public boolean verifyPayButtonstatus() {
		boolean status = false;
		try {
			if (element("+ADD").isDisplayed()) {
				element("paybutton").getAttribute("enabled").equals("false");
				System.out.println("pay button is diabled");
				status = true;
			}
		} catch (Exception e) {
			if (element("labelnumber").isDisplayed()) {
				element("paybutton").getAttribute("enabled").equals("true");
				System.out.println("pay button is enabled");
				status = true;
			}
		}
		return status;
	}

	/** enter value in top up field */
	public void enterValueTopUpfield(String value) {
		element("Enteramountfield").clear();
		element("Enteramountfield").sendKeys(value);
	}

	/** verify active packs display */
	public boolean verifyActivePacksDisplay() {
		boolean status = false;
		List<WebElement> activepacklist = elements("activeplans");
		System.out.println("size of active packs" + activepacklist.size());
		for (int i = 0; i < activepacklist.size(); i++) {

			if (activepacklist.get(i).getText().contains("MB") || activepacklist.get(i).getText().contains("GB")
					|| activepacklist.get(i).getText().contains("min")
					|| activepacklist.get(i).getText().contains("hours")) {
				System.out.println("list of active packs " + activepacklist.get(i).getText());
				status = true;
			}
		}
		return status;
	}

	/** verify graph with details display */
	public boolean verifyGraphDetailsDisplay() {
		boolean status = false;
		if (element("graph").isDisplayed() && element("graphdetails").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** verify graph details with service type display */
	public boolean verifyGraphDetailsGroupbyServiceType() {
		boolean status = false;
		List<WebElement> servicetype = elements("labeltype");
		if (element("graphdetails").isDisplayed()) {
			servicetype.get(0).getText().equals("Data");
			servicetype.get(1).getText().equals("Calls");
			servicetype.get(2).getText().equals("Intl. Calls");
			status = true;
		}
		return status;
	}

	/** verify total and remaining balance display */
	public boolean verifyTotalRemainingBalance() {
		boolean status = false;
		if (element("totalbalance").isDisplayed() && element("remainingbalance").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** tap on arrow notification section */
	public void tapOnarrowNotificationSection() {
		try {
			if (element("notificationArrow").isDisplayed()) {
				element("notificationArrow").click();
			}
		} catch (Exception e) {
			System.out.println("single or no notification is display");
		}

	}

	/** verify notification section expended */
	public boolean verifyNotificationSectionExpend() {
		boolean status = false;
		try {
			if (element("closebutton").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("single or no notification is display");
			status = true;
		}
		return status;
	}

	/** verify notifications are stacked */
	public boolean verifyNotificationsStacked() {
		boolean status = false;
		try {
			if (element("notificationArrow").isDisplayed()) {
				System.out.println("Notifications are stacked one on top of other");
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("Homenotification").isDisplayed()) {
					System.out.println("in the catch try block !!!!!!!!!!!!");
					System.out.println("single notification is display");
					status = true;
				}
			} catch (Exception e1) {
				System.out.println("in the catch block !!!!!!!!!!!!");
				System.out.println("no notifications are present");
				status = true;
			}
		}
		return status;

	}

	/** tap On Active Data Pack */
	public void tapOnActiveDataPack() {
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		wait.until(ExpectedConditions.elementToBeClickable(elements("planvalidity").get(0))).click();
		// List<WebElement> dataplans = elements("planvalidity");
		// dataplans.get(2).click();
	}

	/** verify Data Extended View Dispaly */
	public boolean verifyDataExtendedViewDispaly() {
		return element("screen").getText().equals("Data");
	}

	/** tap On cross button */
	public void taponCrossButton() {
		try {
			element("screencrossbutton").click();
		} catch (Exception e) {
			System.out.println("no cross button is displayed!!!!!!!!!!!");
		}
	}

	/** verify PI chart Display */
	public boolean verifyPIchartDispaly() {
		boolean status = false;
		if (element("pichart").isDisplayed() && (element("remainingdatavalue").getText().contains("MB")
				|| (element("remainingdatavalue").getText().contains("GB")))) {
			status = true;
		}
		return status;
	}

	/** verify Buy Again Button Display */
	public boolean verifyBuyAgainButtonDisplay() {
		boolean status = false;
		try {
			if (element("buyagainbutton").isDisplayed()) {
				status = true;
			}

		} catch (Exception e) {
			if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
				System.out.println("Bounus Minutes");
				status = true;
			}

		}
		return status;
	}

	/** tap on buy again button */
	public void taponBuyAgainButton() {
		try {
			if (element("buyagainbutton").isDisplayed()) {
				element("buyagainbutton").click();
			}

		} catch (Exception e) {
			try {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
				}
			} catch (Exception c) {
				if (element("SummaryScreen").getText().contains("Summary")) {
					System.out.println("Summary screen display");
				}
			}
		}
	}

	/** tap On Data Service Type */
	public void taoOnDataServiceType() {
		try {
			List<WebElement> servicetype = elements("labeltype");
			servicetype.get(0).click();
		} catch (Exception e) {
			System.out.println("No active data pack");
		}
	}

	/** verify Data Packs Display */
	public boolean verifyDataPacksDisplay() {
		boolean status = false;
		List<WebElement> activepacklist = elements("activeplans");
		System.out.println("size of data packs" + activepacklist.size());
		for (int i = 0; i < activepacklist.size(); i++) {

			if (activepacklist.get(i).getText().contains("MB") || activepacklist.get(i).getText().contains("GB")) {
				System.out.println("list of data packs " + activepacklist.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** tap On local call Service Type */
	public void taoOnLocalcallServiceType() {
		try {
			List<WebElement> servicetype = elements("labeltype");
			servicetype.get(1).click();
		} catch (Exception e) {
			System.out.println("No active local call pack pack");
		}

	}

	/** verify local call Packs Display */
	public boolean verifyLocalcallPacksDisplay() {
		boolean status = false;
		List<WebElement> activepacklist = elements("activeplans");
		System.out.println("size of local call packs" + activepacklist.size());
		for (int i = 0; i < activepacklist.size(); i++) {

			if (activepacklist.get(i).getText().contains("mins") || activepacklist.get(i).getText().contains("hours")
					|| element("unlimitedIcon").isDisplayed()) {
				System.out.println("list of local call packs " + activepacklist.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** tap On international call Service Type */
	public void taoOninternationalcallServiceType() {
		try {
			List<WebElement> servicetype = elements("labeltype");
			servicetype.get(2).click();
		} catch (Exception e) {
			System.out.println("No active internaitonal call pack pack");
		}
	}

	/** verify international call Packs Display */
	public boolean verifyinternationalcallPacksDisplay() {
		boolean status = false;
		List<WebElement> activepacklist = elements("internationalcallpacks");
		System.out.println("size of internatinal packs" + activepacklist.size());
		for (int i = 3; i < activepacklist.size(); i++) {
			WebDriverWait wait = new WebDriverWait(session.driver, 200);
			wait.until(ExpectedConditions.visibilityOf(element("activepacklist")));
			if (activepacklist.get(i).getText().contains("International Minutes")) {
				System.out.println("list of active packs " + activepacklist.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify validity period Display */
	public boolean verifyValidityPeriodDisplay() {
		boolean status = false;
		if (element("validityperiod").getText().contains("Day")) {
			System.out.println("Day");
			status = true;
		} else if (element("validityperiod").getText().contains("Unlimited")) {
			System.out.println("Unlimited");
			status = true;
		}
		return status;
	}

	/** verify valid until date Display */
	public boolean verifyValiduntillDisplay() {
		boolean status = false;
		try {
			if (element("validuntilldate").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("pack is unlimited ");
			System.out.println("Whatsapp offer expired");
			status = true;
		}
		return status;
	}

	/** verify price Display */
	public boolean verifyPriceDisplay() {
		boolean status = false;
		try {
			if (element("validuntilldate").isDisplayed()) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("RO")) {
					status = true;
				} else if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					status = true;
				} else if (element("Mixplan").isDisplayed()) {
					if (elements("Dataextendedscreenprice").get(4).getText().contains("RO")) {
						status = true;
					}
				}
			}
		} catch (Exception e) {
			if (elements("Dataextendedscreenprice").get(2).getText().contains("RO")) {
				status = true;
			}
		}
		return status;
	}

	/** tap on summary screen */
	public void tapbackbutton() {
		try {
			element("summaryscreenbackbutton").click();
		} catch (Exception e) {
			System.out.println("Bonus plan");
		}

	}

	/** verify home notification display */
	public boolean verifyHomeNotificationDisplay() {
		boolean status = false;
		try {
			if (element("Homenotification").isDisplayed()) {
				System.out.println("Notification is present");
				status = true;
			}
		} catch (Exception e) {
			System.out.println("Notification is not present");
			status = true;

		}
		return status;
	}

	/** verify status of free minutes on-net Friendi and whatsapp display */
	public boolean verifyStatusWhatsappandFriendicall() {
		boolean status = false;
		if (element("friendistatus").getText().equals("ON") && element("whatsappstatus").getText().equals("ON")) {
			System.out.println("status for both ON");
			status = true;
		} else if (element("friendistatus").getText().equals("ON")
				&& element("whatsappstatus").getText().equals("OFF")) {
			System.out.println("FRIENDI status is ON and WHATSAPP status is OFF");
			status = true;
		} else if (element("friendistatus").getText().equals("OFF")
				&& element("whatsappstatus").getText().equals("ON")) {
			System.out.println("FRIENDI status is OFF and WHATSAPP status is ON");
			status = true;
		} else if (element("friendistatus").getText().equals("OFF")
				&& element("whatsappstatus").getText().equals("OFF")) {
			System.out.println("status for both OFF");
			status = true;
		}
		return status;
	}

	/** verify 5% bonus dial display */
	public boolean verifyBonusDialDisplay() {
		return element("bonusdial").isDisplayed();
	}

	/** verify previous bonus list display */
	public boolean verifyPreviousBonusesDisplay() {
		try {
			return element("rewardlist").isDisplayed();
		} catch (Exception e) {
			System.out.println("detailed list is closed");
			return true;
		}
	}

	/** tap on arrow button */
	public void tapArrowButton() {
		element("rewardhistoryarrowbutton").click();
	}

	/** tap one first arrow button */
	public void tapArrowButtonFirstMonth() {
		element("arrowone").click();
	}

	/** tap on arrow button */
	public void tapArrowButtonSecondMonth() {

		element("rewardhistoryarrowbutton").click();
	}

	/** verify Sum amount display */
	public boolean verifySumUpAmountDisplay() {
		return element("sumamount").getText().contains("RO");
	}

	/** tap on positive button */
	public void clickPositive() {
		element("positiveBtn").click();
	}

	/** verify remaining balance display */
	public boolean verifyRemaingBalanceDisplay() {
		boolean status = false;
		List<WebElement> Remainingbalance = elements("remaingbalance");
		for (int i = 0; i < Remainingbalance.size(); i++) {
			Remainingbalance.get(i).isDisplayed();
			status = true;
		}
		return status;
	}

	/** verify total balance display */
	public boolean verifyTotalBalanceDisplay() {
		boolean status = false;
		List<WebElement> totalbalance = elements("Totalbalance");
		for (int i = 0; i < totalbalance.size(); i++) {
			totalbalance.get(i).isDisplayed();
			status = true;
		}
		return status;

	}

	/** verify validity display */
	public boolean verifyValidityDisplay() {
		boolean status = false;
		List<WebElement> ValidityPeriod = elements("validitylist");
		try {
			for (int i = 0; i < ValidityPeriod.size(); i++) {
				if (ValidityPeriod.get(i).getText().contains("Unlimited Validity")
						|| ValidityPeriod.get(i).getText().contains("Valid until")) {
					status = true;
				}
			}
		} catch (Exception e) {
			System.out.println("pack not displayed");
			status = true;
		}
		return status;
	}

	/** verify auto renew icon display */
	public boolean verifyAutoRenewIconDisplay() {
		boolean status = false;
		List<WebElement> validity = elements("planvalidity");
		for (int i = 0; i < validity.size(); i++) {
			if (validity.get(i).getText().contains("Unlimited Validity")) {
				if (element("AutoRenew").isDisplayed()) {
					status = true;
					break;
				}
			} else if (validity.get(i).getText().contains("Valid until")) {
				System.out.println("auto renew not display for saver plans");
				status = true;
				break;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** tap on positive button */
	public void clickPositiveButton(String number) {
		int time = Integer.parseInt(number);
		for (int i = 0; i < time; i++) {
			element("positiveBtn").click();
			System.out.println(time);
		}
	}

	/** verify navigation bar display */
	public boolean verifyNavigationBarDisplay() {
		boolean status = false;
		if (element("home").isDisplayed() && element("plans").isDisplayed() && element("history").isDisplayed()
				&& element("more").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** verify EXTENDED screen display */
	public boolean verifyScreenDisplay(String Screenname) {
		return element("extendedScreen").getText().contains(Screenname);
	}

	/** verify whatsapp logo display */
	public boolean verifyAppLogoDisplay() {
		return element("logo").isDisplayed();
	}

	/** verify remaining balance display */
	public boolean verifyRemaingDataDisplay() {
		return element("remaining").getText().contains("MB");
	}

	/** verify validity period display display */
	public boolean verifyValidityTimeDisplay() {
		return element("promovalidity").getText().contains("Days");
	}

	/** tap on whatsapp button */
	public void tapFreeWhatsappbutton() {
		element("freewhatsappcall").click();
	}

	/** scroll the screen */
	public void scrolltillElementFound() {
		boolean status = false;
		while (!status) {
			try {
				if (element("rewardhistoryarrowbutton").isDisplayed()) {
					status = true;
				}
			} catch (Exception e) {
				MobileDriver androidriver = (MobileDriver) session.driver;
				Dimension dim = session.driver.manage().window().getSize();

				int height = dim.getHeight();

				int width = dim.getWidth();

				int x = width / 2;

				int top_y = (int) (height * 0.20);
				int bottom_y = (int) (height * 0.50);

				new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
						.waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
						.moveTo(ElementOption.point(x, top_y)).release().perform();

				System.out.println("\nScrolling the screen upwards !!!");
			}
		}
	}

	/** verify recharge account and try again button display */
	public boolean verifyRechargeAccountButtonDisplay() {
		boolean status = false;
		try {
			if (element("rechargeaccountbutton").getText().equals("RECHARGE ACCOUNT AND TRY AGAIN")) {
				System.out.println("user does not have promo app offer");
				status = true;
			}
		} catch (Exception e) {
			System.out.println("Recharge account button is hidden");
			status = true;
		}
		return status;
	}

	/** verify Recharge screen display */
	public boolean verifyRechargeScreenDisplay() {
		boolean status = false;
		try {
			if (element("rechargeaccountbutton").getAttribute("enabled").equals("true")) {
				element("rechargeaccountbutton").click();
				if (element("rechargescreen").getText().equals("Recharge")) {
					System.out.println("recharge screen display");
					status = true;
				}

			}
		} catch (Exception e) {
			if (element("validity").getText().contains("Days")) {
				System.out.println("app offer is active");
				status = true;
			}
		}
		return status;
	}

	/** tap on back button of Recharge screen */
	public void tapBackButtonRechargeScreen() {
		try {
			if (element("backbuttonrechargescreen").isDisplayed()) {
				element("backbuttonrechargescreen").click();
			}
		} catch (Exception e) {
			System.out.println("app offer is active");
		}
	}

	/** tap on friendi calls button */
	public void tapFreeFriendiCallsbutton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		wait.until(ExpectedConditions.elementToBeClickable(element("freefriendicall"))).click();
	}

	/** verify remaining balance display */
	public boolean verifyRemaingMinutesDisplay() {
		return element("remaining").getText().contains("h");
	}

	/** verify remaining balance and valid until field is empty */
	public boolean verifyremaingandvaliduntillempty() {
		boolean status = false;
		if (element("remaingvalue").getText().contains("--")
				&& elements("validuntilvalue").get(1).getText().equals("--")) {
			status = true;
		}
		return status;
	}

	/** tap on free whatsapp call button */
	public void taponFreewhatsappbutton() {
		elements("screen").get(1).click();
	}

	/** tap on free On-Net call button */
	public void taponFreeOnNetbutton() {
		elements("screen").get(2).click();
	}

	/** select local call pack */
	public void selectLocalCallPack() {
		List<WebElement> dataplans = elements("planvalidity");
		dataplans.get(0).click();
	}

	/** verify local call extended screen display */
	public boolean localCallExtendedViewDisplay() {
		return element("screen").getText().equals("Local");
	}

	/** verify pi chart and remaining minutes display */
	public boolean verifyPIchartRemainingMinsDispaly() {
		boolean status = false;
		try {
			if (element("pichart").isDisplayed() && (element("remainingdatavalue").getText().contains("mins")
					|| element("remainingdatavalue").getText().contains("h"))) {
				status = true;
			}
		} catch (Exception e) {
			if (element("pichart").isDisplayed() && (element("unlimitedIcon").isDisplayed())) {
				System.out.println("pack is unlimited");
				status = true;
			}
		}

		return status;
	}

	/** verify unlimited icon display */
	public boolean verifyUnlimitedIconDispaly() {
		boolean status = false;
		try {
			if (element("unlimitedIcon").isDisplayed()) {
				System.out.println("pack is unlimited");
				status = true;
			}
		} catch (Exception e) {
			System.out.println("pack is not unlimited");
			status = true;
		}
		return status;
	}

	/** verify auto renewal toggle display */
	public boolean verifyAutorenewaltoggelDispaly() {
		boolean status = false;
		try {
			if (element("autorenewaltoggle").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("autorenewal toggle not display");
			status = true;
		}
		return status;
	}

	/** verify price display */
	public boolean verifyPriceDispaly() {
		boolean status = false;
		try {
			if (element("validuntilldate").isDisplayed()) {
				if (elements("localcallextendedscreenprice").get(3).getText().contains("RO")) {
					status = true;
				} else if (element("Mixplan").isDisplayed()) {
					if (elements("localcallextendedscreenprice").get(4).getText().contains("RO")) {
						status = true;
					}
				}
			}
		} catch (Exception e) {
			if (elements("localcallextendedscreenprice").get(2).getText().contains("RO")) {
				status = true;
			}
		}
		return status;
	}

	/** verify price display */
	public boolean verifyPriceTextDisplay() {
		boolean status = false;
		try {
			if (element("Mixplan").isDisplayed()) {
				if (elements("Pricetext").get(2).getText().contains("Price")) {
					status = true;
				}
			}
		} catch (Exception e) {
			if (elements("Pricetext").get(1).getText().contains("Price")) {
				status = true;
			}
		}
		return status;
	}

	/** select call pack */
	public void selectCallPack(String validity) {
		List<WebElement> dataplans = elements("planvalidity");
		for (int i = 0; i < dataplans.size(); i++) {
			if (dataplans.get(i).getText().equals(validity)) {
				dataplans.get(i).click();
				break;
			} else {
				System.out.println("plan not available");
			}
		}
	}

	/** verify international call extended screen display */
	public boolean internationalCallExtendedViewDisplay() {
		return element("screen").getText().equals("International calls");
	}

	/** verify Included dropdown display */
	public boolean verifyIncludedDropdownDisplay() {
		return element("includedDropdown").isDisplayed();
	}

	/** tap on included dropdown */
	public void taponIncludedDropdown() {
		element("includedDropdown").click();
	}

	/** verify price for international calls display */
	public boolean verifyPriceDispalyforInternationalcalls() {
		boolean status = false;
		try {
			if (element("validuntilldate").isDisplayed()) {
				if (elements("Dataextendedscreenprice").get(4).getText().contains("RO")) {
					System.out.println("limited pack");
					status = true;
				} else if (elements("Dataextendedscreenprice").get(4).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
					status = true;
				}
			}
		} catch (Exception e) {
			if (elements("Dataextendedscreenprice").get(3).getText().contains("RO")) {
				System.out.println("price display");
				status = true;
			}
		}
		return status;
	}

	/** verify toggle button display */
	public boolean verifytogglebuttonnDispaly() {
		return element("togglebutton").isDisplayed();
	}

	/** verify toggle button disable */
	public boolean verifytogglebuttonnDeselected() {
		return element("staus").getText().equals("(Disabled)");
	}

	/** verify all PayG rates display */
	public boolean verifyAllPayGRatesDisplay() {
		boolean status = false;
		if (elements("paygRates").get(0).getText().contains("SMS")
				&& elements("paygRates").get(1).getText().contains("Local Minutes")
				&& elements("paygRates").get(2).getText().contains("Data")
				&& elements("paygRates").get(3).getText().contains("International Minutes")) {
			status = true;
		}
		return status;

	}

	/** verify payG rates collapsed display */
	public boolean verifypayGRatesCollapsed() {
		boolean status = false;
		try {
			if (element("viewrates").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("payG rates collapsed");
			status = true;
		}
		return status;
	}

	/** scroll the screen till payG rates found */
	public void scrolltillPaygRatesFound() {
		boolean status = false;
		while (!status) {
			try {
				if (element("Standreadrates").isDisplayed()) {
					status = true;
				}
			} catch (Exception e) {
				MobileDriver androidriver = (MobileDriver) session.driver;
				Dimension dim = session.driver.manage().window().getSize();

				int height = dim.getHeight();

				int width = dim.getWidth();

				int x = width / 2;

				int top_y = (int) (height * 0.20);
				int bottom_y = (int) (height * 0.50);

				new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
						.waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
						.moveTo(ElementOption.point(x, top_y)).release().perform();

				System.out.println("\nScrolling the screen upwards !!!");
			}
		}
	}

	/** select international call pack */
	public void selectInternationalPack() {
		List<WebElement> intewnationalplans = elements("planvalidity");
		intewnationalplans.get(0).click();
	}

	/** select local call pack */
	public void selectlocalPack() {
		List<WebElement> localcall = elements("planvalidity");
		localcall.get(0).click();
	}

	/** select data pack */
	public void selectdataPack() {
		List<WebElement> Datapack = elements("planvalidity");
		Datapack.get(1).click();
	}

	/** verify auto renewal toggle display for Freedom data plan */
	public boolean verifyAutoRenewalToggleDisplay() {
		boolean status = false;
		if (element("validityperiod").getText().contains("Unlimited")) {
			if (element("autorenewaltoggle").isDisplayed()) {
				System.out.println("autorenewal toggle display");
				status = true;
			}
		} else if (element("validityperiod").getText().contains("Day")) {
			System.out.println("Saver data or Saver Mix plan");
			status = true;
		}
		return status;
	}

	/**
	 * verify auto renewal toggle should not display for saver data and saver mix
	 * plan
	 * 
	 */
	public boolean verifyAutoRenewalToggleNotDisplay() {
		boolean status = false;
		try {
			if (element("validuntilldate").isDisplayed()) {
				if (element("autorenewaltoggle").isDisplayed()) {
					System.out.println("autorenewal toggle display");
					status = true;
				}
			}
		} catch (Exception e) {
			if (element("validityperiod").getText().contains("Unlimited")) {
				System.out.println("Not a Saver data or Saver Mix plan");
				status = false;
			}
		}
		return status;
	}

	/** verify auto renewal toggle selected */
	public boolean verifyAutoRenewalToggleSelected() {
		boolean status = false;
		if (element("validityperiod").getText().contains("Unlimited")) {
			if (element("autorenewaltoggle").isDisplayed()) {
				if (element("autorenewaltoggle").getAttribute("checked").equals("true")) {
					System.out.println("autorenewal toggle selected");
					status = true;
				}
			}
		} else if (element("validityperiod").getText().contains("Day")) {
			System.out.println("Saver data or Saver Mix plan");
			status = true;
		}
		return status;
	}

	/** tap on toggle button */
	public void clickOnTogglebutton() {
		try {
			if (element("validityperiod").getText().contains("Unlimited")) {
				if (element("autorenewaltoggle").isDisplayed()) {
					element("autorenewaltoggle").click();
				}
			}
		} catch (Exception e) {
			if (element("validityperiod").getText().contains("Day")) {
				System.out.println("autorenewal toggle not displayed");
			}
		}
	}

	/** tap on confirm button */
	public void clickOnConfirmbutton() {
		try {
			if (element("confirmbutton").isDisplayed()) {
				element("confirmbutton").click();
			}
		} catch (Exception e) {
			System.out.println("confirm button not displayed");
		}
	}

	/** verify auto renewal toggle Deselected */
	public boolean verifyAutoRenewalToggleDeselected() {
		boolean status = false;
		try {
			if (element("validityperiod").getText().contains("Unlimited")) {
				if (element("autorenewaltoggle").isDisplayed()) {
					if (element("autorenewaltoggle").getAttribute("checked").equals(
							"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‎‏‏‏‎‏‎‏‎‎‎‎‎‏‎‎‎‏‏‏‎‏‏‎‎‏‏‎‏‏‎‏‎‎‎‎‎‏‏‎‏‎‏‏‎‏‏‏‎‎‏‎‎‏‏‎‎‏‏‏‎‏‏‎false")) {
						System.out.println("autorenewal toggle deselected");
						status = true;
					}
				}
			}
		} catch (Exception e) {
			if (element("validityperiod").getText().contains("Day")) {
				System.out.println("autorenewal toggle not displayed");
				status = true;
			}
		}
		return status;
	}

	/** verify local call pack display under data mix plan */
	public boolean verifyLocalCallPackDisplayUndermixPlan() {
		boolean status = false;
		try {
			if (element("Mixplan").isDisplayed()) {
				System.out.println("Mixplan");
				status = true;
			}

		} catch (Exception e) {
			System.out.println("Not a mix plan");
			status = true;
		}
		return status;
	}

	/** tap on pack under mix plan */
	public void taponPackunderMixplan() {
		try {
			if (element("Mixplan").isDisplayed()) {
				element("Mixplan").click();
			}
		} catch (Exception e) {
			System.out.println("Not a mix plan");
		}
	}

	/** verify Local call extended view from Data Mix plan */
	public boolean verifyLocalCallExtendedViewDisplay() {
		boolean status = false;
		try {
			if (element("Mixplan").isDisplayed()) {
				if (element("screen").getText().equals("Local")) {
					status = true;
				}
			}

		} catch (Exception e) {
			if (element("screen").getText().equals("Data")) {
				System.out.println("Not a mix plan");
				status = true;
			}
		}
		return status;
	}

	/** verify Data pack display under local call mix plan */
	public boolean verifyDataPackDisplayUndermixPlan() {
		boolean status = false;
		try {
			if (element("Mixplan").isDisplayed()) {
				System.out.println("Mixplan");
				status = true;
			}

		} catch (Exception e) {
			System.out.println("Not a mix plan");
			status = true;
		}
		return status;
	}

	/** verify Data extended view from Local Mix plan */
	public boolean verifyDataExtendedViewDisplay() {
		boolean status = false;
		try {
			if (element("Mixplan").isDisplayed()) {
				if (element("screen").getText().equals("Data")) {
					status = true;
				}
			}
		} catch (Exception e) {
			if (element("screen").getText().equals("Local")) {
				System.out.println("Not a mix plan");
				status = true;
			}
		}
		return status;
	}

	/** verify cross button display */
	public boolean verifycrossbuttonunderhsnotificationDisplay() {
		boolean status = false;
		try {
			if (element("clearallbutton").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("notificationArrow").isDisplayed()) {
					element("notificationArrow").click();
					if (element("clearallbutton").isDisplayed()) {
						status = true;
					}
				}
			} catch (Exception e1) {
				System.out.println("no nofitications displayed");
				status = true;
			}

		}
		return status;
	}

	/** tap on cross button */
	public void tapcrossButtonUnderHsnotification() {
		try {
			if (element("clearallbutton").isDisplayed()) {
				element("clearallbutton").click();
			}
		} catch (Exception e) {
			System.out.println("no notification present");
		}

	}

	/** verify home notification cleared */
	public boolean verifyHSnotificationCleared() {
		boolean status = false;
		try {
			if (element("Homenotification").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("Notificarions are cleared");
			status = false;
		}
		return status;
	}

	/** Verify voucher field has character validation */
	public boolean verifyTextfieldHasCharacterValidation() {
		boolean status = false;
		element("vouchernumberfield").clear();
		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";
		element("vouchernumberfield").clear();
		element("vouchernumberfield").sendKeys(st1);
		if (element("vouchernumberfield").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("vouchernumberfield").sendKeys(st2);
		if (element("vouchernumberfield").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("vouchernumberfield").sendKeys(st3);
		if (element("vouchernumberfield").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/** Verify character limit in text field */
	public boolean verifyCharacterLimit() {
		boolean status = false;

		String data1 = "111111111111";
		String data2 = "11111111111";
		String data3 = "1111111111111";

		element("vouchernumberfield").clear();
		element("vouchernumberfield").sendKeys(data1);
		if (element("UseVoucherbutton").isEnabled()) {
			status = true;
			System.out.println("Accepting 12 digits !!!");
		} else {
			status = false;
		}

		element("vouchernumberfield").clear();
		element("vouchernumberfield").sendKeys(data2);
		if (element("UseVoucherbutton").isEnabled()) {
			status = false;
		} else {
			status = true;
			System.out.println("11 digits are not allowed, should be 12 digits");
		}

		element("vouchernumberfield").clear();
		element("vouchernumberfield").sendKeys(data3);
		if (element("vouchernumberfield").getText().equals(data3)) {
			status = false;
		} else {
			status = true;
			System.out.println("13 digits are not allowed, should be 12 digits");
		}

		return status;
	}

	/** Verify topup field has character validation */
	public boolean verifyTopUpHasCharacterValidation() {
		boolean status = false;
		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";
		element("Enteramountfield").clear();
		element("Enteramountfield").sendKeys(st1);
		if (element("Enteramountfield").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("Enteramountfield").sendKeys(st2);
		if (element("Enteramountfield").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("Enteramountfield").sendKeys(st3);
		if (element("Enteramountfield").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/** tap on radio button */
	public void selectRadioButton() {
		elements("radiobutton").get(3).click();
	}

	/** Verify Recharge button display */

	public boolean verifyRechargeButtonDisplay() {
		boolean status = false;
		try {
			if (element("rechargebutton").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("SummaryScreen").getText().contains("Summary")) {
					status = true;
				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
					status = true;
				}
			}
		}
		return status;
	}

	/** Verify cancel button display */
	public boolean verifyCancelButtonDisplay() {
		boolean status = false;
		try {
			if (element("cancel").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("SummaryScreen").getText().contains("Summary")) {
					status = true;
				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
					status = true;
				}
			}
		}
		return status;
	}

	/** tap on cancel button */
	public void tapCancelButton() {
		try {
			if (element("cancel").isDisplayed()) {
				element("cancel").click();
			}
		} catch (Exception e) {
			try {
				if (element("SummaryScreen").getText().contains("Summary")) {

				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");

				}
			}
		}
	}

	/** tap on Recharge button */
	public void taponRechargebutton() {
		try {
			if (element("rechargebutton").isDisplayed()) {
				element("rechargebutton").click();
			}
		} catch (Exception e) {
			try {
				if (element("SummaryScreen").getText().contains("Summary")) {
					System.out.println("summary screen display");
				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
				}
			}
		}
	}

	/** Verify Recharge screen display */
	public boolean verifyRechargeScreen() {
		boolean status = false;
		try {
			if (element("rechargescreen").getText().equals("Recharge")) {
				System.out.println("recharge screen display");
				status = true;
			} else if (element("SummaryScreen").getText().contains("Summary")) {
				status = true;
			}
		} catch (Exception c) {
			if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
				System.out.println("Bounus Minutes");
				status = true;
			}
		}
		return status;
	}

	/** Enter invalid details */
	public void enterInvalidDetails() {
		element("cardnumber").sendKeys("1111111111111111");
		element("expirydate").sendKeys("2233");
		element("CVV").sendKeys("444");
		element("cardholderName").sendKeys("test");
	}

	/** Verify Add button remains disable */
	public boolean addButtonDisable() {
		return element("addbutton").isEnabled();
	}

	/** Verify valid until Display */
	public boolean verifyValidUntilDisplay() {
		boolean status = false;
		if (element("validityperiod").getText().contains("Day")) {
			if (element("validuntilldate").isDisplayed()) {
				status = true;
			}
		} else if (element("validityperiod").getText().contains("Unlimited")) {
			System.out.println("Unlimited");
			status = true;
		}
		return status;
	}

	/** Verify price should not Display for promo pack */
	public boolean verifyPriceNotDisplay() {
		boolean status = false;
		try {
			element("PromoPriceText").isDisplayed();
			System.out.println("Not a promo data pack");
			status = true;
		} catch (Exception e) {
			System.out.println("Promo data pack");
			status = true;
		}
		return status;
	}

	/** Enter Valid details */
	public void enterValidDetails() {
		element("cardnumber").sendKeys("4111111111111111");
		element("expirydate").sendKeys("0423");
		element("CVV").sendKeys("123");
		element("cardholderName").sendKeys("test");
	}
}
