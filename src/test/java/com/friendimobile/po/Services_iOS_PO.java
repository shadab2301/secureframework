package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Services_iOS_PO extends CommonGestures {
	public static String platformName = InitData.platformName;

	public Services_iOS_PO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Services_iOS");
		System.out.println("Executed platform name  " + platformName);
	}

	/**
	 * To click on plus button
	 */
	public void clickPlusBtn() {
		element("plusButton").click();
	}

	/**
	 * To verify user can enter data in RO field
	 */
	public boolean verifyRO() {
		element("roField").click();
		element("roField").sendKeys("12345");
		return element("roField").getText().equals("RO 12.345");
	}

	/**
	 * To verify user can enter data in Tap in number field
	 */
	public boolean verifyTapInNumberField(String strArg1) {
		try {
			element("tapInNumberField").click();
			element("tapInNumberField").click();
			Thread.sleep(3000);
			element("selectAll").click();
			element("tapInNumberField").sendKeys(Keys.DELETE);
		} catch (Exception e) {
			System.out.println("field is already empty");
		}
		element("tapInNumberField").sendKeys(strArg1 + Keys.ENTER);
		return element("tapInNumberField").getText().equals(strArg1);
	}

	/**
	 * To verify help text is disappeared
	 */
	public boolean verifyHelpTextDisapppeared() {
		return element("tapInNumberField").getText().equals("Tap in number");
	}

	/**
	 * To verify transfer button is disabled
	 */
	public boolean verifyTranferBtnDisabled() {
		return element("transferBtn").isEnabled();
	}

	/**
	 * To verify Contacts button is displayed
	 */
	public boolean verifyContactsBtn() {
		return element("contactsBtn").isDisplayed();
	}

	/**
	 * To click on contacts button
	 */
	public void clickOnContacts() {
		element("contactsBtn").click();
	}

	/**
	 * To enter value in ro field
	 */
	public void enterValueInField(String strArg1) {
		try {
			element("roField").click();
			element("roField").click();
			Thread.sleep(3000);
			element("selectAll").click();
			element("roField").sendKeys(Keys.DELETE);
		} catch (Exception e) {
			System.out.println("field is already empty");
		}
		element("roField").clear();
		element("roField").sendKeys(strArg1);
	}

	/**
	 * To verify the Total Amount DropDown button is displayed
	 */
	public boolean verifyTotalAmountDropDown() {
		return element("amountDropDown").isDisplayed();
	}

	/**
	 * To click Total Amount DropDown on plus button
	 */
	public void clickOnDropDown() {
		element("amountDropDown").click();
	}

	/**
	 * To verify rates and minutes
	 */
	public boolean verifyMinsAndPrices(String arg1, String arg2) {
		boolean status = false;
		List<WebElement> Minutes = elements("minuteslist");
		List<WebElement> price = elements("pricelist");
		for (int i = 0; i < Minutes.size(); i++) {
			System.out.println(Minutes.get(i).getText());
			System.out.println(price.get(i).getText());

			if (Minutes.get(i + 1).getText().contains(arg1) && price.get(i).getText().contains(arg2)) {
				System.out.println("Minutes and price display" + Minutes.get(i).getText() + price.get(i).getText());
				status = true;
				break;

			}
		}
		return status;

	}

	/**
	 * verify total amount field display
	 */
	public boolean verifyRoValueDisplay() {
		return element("currentbalance").isDisplayed();
	}

	/**
	 * verify help text display
	 */
	public boolean verifyTransferCreditTextDisplay() {
		return element("TransferCreditToField").getText().contains("Transfer credit to...");

	}

	/**
	 * To verify user can enter data in Transfer Credit To field
	 */
	public boolean verifyTransferCreditToField(String strArg1) {
		element("TransferCreditToField").sendKeys(strArg1);
		return element("TransferCreditToField").getText().equals(strArg1);
	}

	/**
	 * To enter valid mobile number in transfer credit to field
	 */
	public void enterValidNumber(String strArg1) {
		element("TransferCreditToField").clear();
		element("TransferCreditToField").sendKeys(strArg1);
	}

	/**
	 * verify continue button is enabled
	 */
	public boolean verifyContinueButtonEnabled() {
		return element("continuebutton").isEnabled();
	}

	/**
	 * To tap on edit button
	 */
	public void tapOnEditButton() {
		element("editbutton").click();
	}

	/**
	 * To tap on dropdown button
	 */
	public void tapOnDropdown() {
		element("dropdown").click();
	}

	/**
	 * verify predefined values display
	 */
	public boolean verifyPredefindValueDisplay() {
		return element("predefinedvalues").isDisplayed();
	}

	/**
	 * To tap on done button
	 */
	public void clickDoneButton() {
		element("donebutton").click();
	}

	/**
	 * To Scroll RO value
	 * 
	 * @throws InterruptedException
	 */
	public void scrollROvalue() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;
		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int x = width / 2;

		int top_y = (int) (height * 0.65);
		int bottom_y = (int) (height * 0.80);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500))).moveTo(ElementOption.point(x, top_y))
				.release().perform();

		System.out.println("\nScrolling the screen upwards !!!");

		Thread.sleep(1000);

	}

	/**
	 * verify OTP field display
	 */
	public boolean verifyOTPfieldDisplay() {
		return element("OTPfield").isDisplayed();
	}

	/**
	 * verify able to enter data in OTP field
	 */
	public boolean verifyOTPfield() {
		element("OTPfield").sendKeys("123456");
		return element("OTPfield").getText().contentEquals("123456");
	}

	/**
	 * To enter invalid OTP
	 */
	public void enterInvalidOTP() {
		element("OTPfield").clear();
		element("OTPfield").sendKeys("000000");
	}

	/**
	 * Verify list of countries display
	 * 
	 * 
	 */
	public boolean verifyListOfCountriesDisplay() {
		boolean status = false;

		try {

			List<WebElement> element = elements("listOfCountry");
			for (int i = 0; i < element.size(); i++) {
				if (element.get(i).isDisplayed()) {
					System.out.println(element.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Countries list not display");
		}
		return status;
	}

	/** enter country name */
	public void enterCountryName(String countryname) {
		element("entercountryField").clear();
		element("entercountryField").sendKeys(countryname);
	}

	/** verify entered countries display */
	public boolean verifyEnteredCountryListDisplay() {
		boolean status = false;
		try {

			List<WebElement> element = elements("listOfCountry");
			System.out.println("searched countries:" + element.size());
			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).getText().contains("United")) {
					System.out.println(element.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Not found country name");
		}
		return status;
	}

	/** select country from country list */
	public void selectCountry() {
		elements("listOfCountry").get(0).click();
	}

	/** verify country logo display */
	public boolean verifyCountryLogoDisplay() {
		return element("countryflag").isDisplayed();
	}

	/** verify PayG rates display */
	public boolean verifyPaygRatesDisplay() {
		boolean status = false;
		if (element("callsrate").getText().contains("min") && element("smsrate").getText().contains("sms")) {
			status = true;
		}
		return status;
	}

	/**
	 * scroll the offer
	 * 
	 * @throws InterruptedException
	 */
	public void scrollTheOffer() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;
		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int x = width / 2;

		int top_y = (int) (height * 0.40);
		int bottom_y = (int) (height * 0.60);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500))).moveTo(ElementOption.point(x, top_y))
				.release().perform();

		System.out.println("\nScrolling the screen upwards !!!");

		Thread.sleep(1000);
	}

	/** tap on eligible country list button */
	public void eligiblecountriesButton() {
		element("listofeligiblecountries").click();
	}

	/** verify all countries display */
	public boolean verifyAllIncludedCountriesDisplay() {
		boolean status = false;
		List<WebElement> element = elements("listOfCountry");
		for (int i = 0; i < element.size(); i++) {
			if (element.get(i).isDisplayed()) {
				System.out.println(element.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify entered country display */
	public boolean verifyEnteredCountryDisplay() {
		boolean status = false;
		try {

			List<WebElement> element = elements("listOfCountry");
			System.out.println("searched countries:" + element.size());
			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).getText().equals("United Arab Emirates")) {
					System.out.println(element.get(i).getText());
					status = true;
					break;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Not an eligible country");
			status = true;
		}
		return status;
	}

	/** tap on cross button */
	public void tapCrossButton() {
		element("crossbutton").click();
	}

	/** verify search field cleared */
	public boolean verifyEnteredCountrySearchFieldCleared() {
		return element("entercountryField").getText().equals("Enter country");
	}

	/** verify close button display */
	public boolean verifycloseButtondisplay() {
		boolean status = false;
		try {
			if (element("closebuton").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			if (element("carousel").isDisplayed()) {
				System.out.println("user has data to share");
				status = true;
			}
		}
		return status;
	}

	/** tap on close button */
	public void tapCloseButton() {
		try {
			if (element("closebuton").isDisplayed()) {
				element("closebuton").click();
			}
		} catch (Exception e) {
			if (element("carousel").isDisplayed()) {
				System.out.println("user has data to share");
			}
		}
	}

	/** verify other services screen display */
	public boolean verifyOtherServicesScreen() {
		boolean status = false;
		try {
			if (element("otherServices").getText().contains("Other services")) {
				status = true;
			}
		} catch (Exception e) {
			if (element("carousel").isDisplayed()) {
				System.out.println("user has data to share");
				status = true;
			}
		}
		return status;
	}

	/** verify text display */
	public boolean verifyText() {
		boolean status = false;
		try {
			if (element("text").getText().contains("Sorry, you can’t share more data!")) {
				status = true;
			}
		} catch (Exception e) {
			if (element("carousel").isDisplayed()) {
				System.out.println("user has data to share");
				status = true;
			}
		}
		return status;
	}

	/** verify remaining balance displayed */
	public boolean verifyremainingBalance() {
		return element("remainingbalanceinfo").isDisplayed();
	}

	/** scroll the data Carousel in right */
	public void scrolldatacarouselRight() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;

		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int y = (height * 3) / 5;

		int x_right = (int) (width * 0.8);

		int x_left = (int) (width * 0.5);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x_right, y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))).moveTo(ElementOption.point(x_left, y))
				.release().perform();

		System.out.println("\nSwiping from Right to Left !!!");

		Thread.sleep(2000);
	}

	/** Verify OTP field has character validation */
	public boolean verifyOTPfieldHasCharacterValidation() {
		boolean status = false;

		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";

		element("OTPfield").clear();
		element("OTPfield").sendKeys(st1);
		if (element("OTPfield").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("OTPfield").sendKeys(st2);
		if (element("OTPfield").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("OTPfield").sendKeys(st3);
		if (element("OTPfield").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/**
	 * To enter invalid OTP
	 */
	public void enterValidOTP() {
		element("OTPfield").clear();
		element("OTPfield").sendKeys("123456");
	}

	/** verify recently used number list display */
	public boolean verifyPreviousNumberDisplay() {
		try {
			return element("resentlyusednumber").isDisplayed();
		} catch (Exception e) {
			System.out.println("Resently used number list collapsed");
			return true;
		}
	}

	/**
	 * tap on change button to change data
	 */
	public void tapOnDataChangeButton() {
		elements("changebutton").get(0).click();
	}

	/** scroll the data Carousel in left */
	public void scrolldatacarouselLeft() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;

		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int y = (height * 3) / 5;

		int x_right = (int) (width * 0.8);

		int x_left = (int) (width * 0.5);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x_left, y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))).moveTo(ElementOption.point(x_right, y))
				.release().perform();

		System.out.println("\nSwiping from Right to Left !!!");

		Thread.sleep(2000);
	}

	/** verify add your friend's number screen display */
	public boolean verifyAddYourFriendsNumberScreen() {
		return element("AddFriendsNumber").isDisplayed();
	}

	/** verify how much data you want to share display */
	public boolean verifyHowMuchdataYouShareScreen() {
		return element("HowMuchdatashareScreen").isDisplayed();
	}

	/**
	 * To verify Contacts button is displayed under data transfer screen
	 */
	public boolean verifyContactsButton() {
		return element("contactsbutton").isDisplayed();
	}

	/**
	 * tap on contacts button under data transfer screen
	 */
	public void tapOnContactButton() {
		element("contactsbutton").click();
	}

	/**
	 * tap on change button to change number
	 */
	public void tapOnNumberChangeButton() {
		elements("changebutton").get(1).click();
	}

	/**
	 * tap on recently used dropdown
	 * 
	 */
	public void tapOnDropDownButton() {
		element("recentlyusedDropdown").click();
	}

	/** verify confirmation pop-up display */
	public boolean verifyConfirmationPopupDisplay() {
		boolean status = false;
		try {
			if (element("confirmationpopup").getText().contains("Confirmation")) {
				status = true;
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
				status = true;
			}
		}
		return status;
	}

	/** verify Recharge screen display */
	public boolean verifyRechargeScreenDisplay() {
		boolean status = false;
		try {
			if (element("RechargeScreen").getText().contains("Recharge")) {
				System.out.println("Recharge screen display");
				status = true;
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
				status = true;
			}
		}
		return status;
	}

	/**
	 * tap on CANCEL button
	 * 
	 */
	public void tapOnCancelButton() {
		try {
			if (element("cancel").isDisplayed()) {
				element("cancel").click();
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
			}
		}
	}

	/**
	 * tap on Transfer button
	 * 
	 */
	public void tapOnTransferButton() {
		try {
			if (element("transferbutton").isDisplayed()) {
				element("transferbutton").click();
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
			}
		}
	}

	/**
	 * tap on Recharge button
	 * 
	 */
	public void tapOnRechargeButton() {
		try {
			if (element("Reachargebutton").isDisplayed()) {
				element("Reachargebutton").click();
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
			}
		}
	}

	/** verify maximum amount info display */
	public boolean verifyMaximumAmountInfoDisplay() {
		boolean status = false;
		try {
			if (element("MaximumAmountInfo").getText()
					.contains("Maximum amount you can transfer in a month is RO 30.")) {
				status = true;
			}
		} catch (Exception e) {
			if (element("SummaryScreen").getText().contains("Summary")) {
				System.out.println("Summary screen display");
				status = true;
			}
		}
		return status;
	}

}
