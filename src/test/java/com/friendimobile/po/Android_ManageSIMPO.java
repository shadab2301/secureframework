package com.friendimobile.po;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_ManageSIMPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_ManageSIMPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_ManageSIM");
		System.out.println("Executed platform name  " + platformName);
	}

	public boolean verifyLablelDisplay(String label) {
		return element("Primarylabel").getText().equals(label);
	}

	/**
	 * Tap on expand button in top right corner
	 */
	public void tapOnExpandButtonInTopRight() throws InterruptedException {

		element("expndBtn").click();
	}

	/**
	 * Verify expand number
	 */
	public boolean verifyExpandNumber() {
		return element("bottempanel").isDisplayed();

	}

	/**
	 * Verify ManageSim OPTIONS DISPLAY
	 */
	public boolean verifyManagaeSimOptinsDisplay() {
		return element("options").isDisplayed();
	}

	/**
	 * Verify CREDIT RECHARGE OPTION DISPLY
	 */
	public boolean verifyCreditOptionDisplay() {
		List<WebElement> manageSimOptions = elements("options");
		return manageSimOptions.get(0).getText().contains("Credit");
	}

	/**
	 * TAP ON CREDIT RECHARGE OPTION
	 */
	public void taponCreditRecharge() {
		List<WebElement> manageSimOptions = elements("options");
		manageSimOptions.get(0).click();
	}

	/**
	 * VERIFY EDIT NAME POPUP DISPLAY
	 */
	public boolean verifyEditNamepopupDisplay() {
		return element("editnamepopup").isDisplayed();
	}

	/**
	 * VERIFY NAME FIELD DISPLAY
	 */
	public boolean verifyNameFieldDisplay() {
		element("namefield").clear();
		return element("namefield").getText().equals("Name");
	}

	/**
	 * VRIFY USER IS ABLE TO ENTYER TEXT IN NAME FIELD
	 */
	public boolean verifyabletoEnterText() {
		boolean status = false;
		element("namefield").clear();
		element("namefield").sendKeys("abcd");
		if (element("namefield").getText().equals("abcd")) {
			status = true;
		}
		return status;
	}

	/**
	 * ENTER TEXT IN NAME FIELD
	 */
	public void enterTextNameField(String text) {
		element("namefield").clear();
		element("namefield").sendKeys(text);
	}

	/**
	 * VERIFY SIM NAME DISPLAY IN THE HEADER
	 */
	public boolean verifyEntetredTextSaved() {
		return element("Mobilelabel").getText().equals("Mobile");
	}

	/**
	 * TAP BACK BUTTON FROM RECHARGE SCREEN
	 */
	public void tapBackbutton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("backbutton"))).click();
	}

}
