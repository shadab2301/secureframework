package com.friendimobile.po;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class Android_InternationalCreditTransferPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_InternationalCreditTransferPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_InternationalCreditTransfer");
		System.out.println("Executed platform name  " + platformName);
	}

	/** Tap on international credit transfer button */
	public void tapOnInternationalCreditTransferButton() {
		element("internationalcredittransferbutton").click();
	}

	/** Tap on contact button */
	public void tapOnContactButton() throws InterruptedException {
		element("numberfield").clear();
		session.driver.navigate().back();
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(958, 665)).release().perform();
		Thread.sleep(5000);

	}

	/** tap on top up button */
	public void tapOnTopUpbutton() {
		element("topupButton").click();
	}

	/** enter mobile number */
	public void enterMobileNumber(String Mobilenumber) {
		element("numberfield").clear();
		element("numberfield").sendKeys(Mobilenumber);

	}

	/** verify continue button enabled */
	public boolean verifyContinueButtonEnable() {
		boolean status = false;
		try {
			if (element("continuebutton").getAttribute("enabled").equals("true")) {
				status = true;
			}

		} catch (Exception e) {
			System.out.println("Mobile number is not valid");
		}
		return status;
	}

	/** select RO value */
	public void selectROvalueFromDropdown() {
		element("ROoption").click();
		element("ROVALUE2").click();
	}

	/** verify current wallet balance display */
	public boolean verifyCurrentWallateBalanceDisplay() {
		return element("walltebalance").isDisplayed();
	}

	/** verify mobile number display */
	public boolean verifyMobileNoDisplay() {
		return element("mobilenumber").getText().equals("966570738031");
	}

	/** verify default value display */
	public boolean verifyDefaultValueDisplay() {
		return element("ROoption").getText().equals("10.0 SAR(1.39 RO)");
	}

	/** verify dropdown list closes */
	public boolean verifyDropdownListCloses() {
		return element("maxtransferinfo").getText().equals("Maximum amount you can transfer in a month is RO 30.");
	}

	/** verify remaining balance text display */
	public boolean verifyRemainingBalanceTextDisplay() {
		return element("remainingBalanceText").getText().contains("Your total balance will become RO");
	}

	/** choose RO value */
	public void chooseROValue() {
		element("ROoption").click();
		element("ROVALUE3").click();
	}

	/** verify recharge pop up display */
	public boolean verifyRechargePopUpDisplay() {
		boolean status = false;
		try {
			element("rechargepopup").getText().equals("You don't have sufficient credit in your account");
			status = true;
		} catch (Exception e) {
			if (element("summaryscreen").getText().equals("Summary")) {
				System.out.println("Summary Screen appears");
				status = true;
			}
		}
		return status;
	}

	/** tap on back button */
	public void tapInternationalScreenBackButton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("backbutton"))).click();

	}

	/** verify Text */
	public boolean verifyText() {
		boolean status = false;
		try {
			if (element("textmessage").isDisplayed()) {
				status = true;
			}

		} catch (Exception e) {
			if (element("summaryscreen").getText().equals("Summary")) {
				System.out.println("Summary Screen appears");
				status = true;
			}
		}
		return status;
	}

	/** tap on transfer button */
	public void tapTransferButton() {
		try {
			if (element("transfer").isDisplayed()) {
				element("transfer").click();
			}
		} catch (Exception e) {
			if (element("summaryscreen").getText().equals("Summary")) {
				System.out.println("Summary Screen appears");
			}
		}
	}

	/** verify previous screen from international selection screen */
	public boolean verifyPreviousScreenDisplay() {
		boolean status = false;
		try {
			if (element("ROoption").getText().equals("10.0 SAR(1.39 RO)")) {
				status = false;
			}
		} catch (Exception e) {
			System.out.println("previous screen disply");
			status = true;
		}
		return status;
	}

	/** Verify OTP field has character validation */
	public boolean verifyOTPfieldHasformatValidation() {
		boolean status = false;

		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";

		element("OTPField").clear();
		element("OTPField").sendKeys(st1);
		if (element("OTPField").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("OTPField").sendKeys(st2);
		if (element("OTPField").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("OTPField").sendKeys(st3);
		if (element("OTPField").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/**
	 * To enter invalid OTP
	 */
	public void enterInvalidOTP() {
		element("OTPField").clear();
		element("OTPField").sendKeys("000000");
	}

	/** Verify transfer credit to field has phone number validation */
	public boolean verifyPhoneNumberFieldHasformatValidation() {
		boolean status = false;

		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";

		element("numberfield").clear();
		element("numberfield").sendKeys(st1);
		if (element("numberfield").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("numberfield").sendKeys(st2);
		if (element("numberfield").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("numberfield").sendKeys(st3);
		if (element("numberfield").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/** Verify character limit in text field */
	public boolean verifyPhoneNumberFieldHasCharlimit() {
		boolean status = false;

		String data1 = "11111111111111";
		String data2 = "1111111111";
		String data3 = "1111111111111111";

		element("numberfield").sendKeys(data1);
		if (element("continuebutton").isEnabled()) {
			status = true;
			System.out.println("Accepting 14 digits !!!");
		} else {
			status = false;
		}

		element("numberfield").clear();
		element("numberfield").sendKeys(data2);
		if (element("continuebutton").isEnabled()) {
			status = false;
		} else {
			status = true;
			System.out.println("10 digits are not allowed, should be in between 11-15");
		}

		element("numberfield").clear();
		element("numberfield").sendKeys(data3);
		if (element("continuebutton").isEnabled()) {
			status = false;
		} else {
			status = true;
			System.out.println("16 digits are not allowed, should be in between 11-15");
		}

		return status;
	}

	/** Verify send again button display */
	public boolean verifySendAgainButton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);
		return wait.until(ExpectedConditions.visibilityOf(element("sendagainbutton"))).isDisplayed();
	}
}
