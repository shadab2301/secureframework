package com.friendimobile.po;

import java.time.Duration;

import org.openqa.selenium.Dimension;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.qe.automation.framework.pagefactory.MobileWebViewPage;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Android_DataTransferPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_DataTransferPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_DataTransfer");
		System.out.println("Executed platform name  " + platformName);
	}

	/** verify Carousel display */
	public boolean verifyCarauselDisplay() {
		return element("carausel").isDisplayed();
	}

	/** tap on recently used Dropdown */
	public void tapResentlyUseddropdown() {
		element("resentlyusedDropdown").click();
	}

	/** verify recently used number list display */
	public boolean verifyPreviousNumberDisplay() {
		try {
			return element("resentlyusednumber").isDisplayed();
		} catch (Exception e) {
			System.out.println("Resently used number list collapsed");
			return true;
		}

	}

	/** tap on change button to review data */
	public void tapOnChangeBtn() {
		element("changebtnData").click();
	}

	/** tap on change button to review Mobile number */
	public void tapOnChangebtn() {
		element("changebtnNumber").click();
	}

	/** scroll the Carousel */
	public void scrolldatacarousel() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;

		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int y = height / 2;

		int x_right = (int) (width * 0.8);

		int x_left = (int) (width * 0.2);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x_right, y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))).moveTo(ElementOption.point(x_left, y))
				.release().perform();

		System.out.println("\nSwiping from Right to Left !!!");

		Thread.sleep(2000);
	}

	/** verify 1 GB display */
	public boolean verifyTransferedDataDisplay() {
		return elements("transferedData").get(0).getText().contains("1 GB");
	}

	/** verify Buy data Button display */
	public boolean verifyBuyDataButtonDisplay() {
		boolean status = false;
		try {
			if (element("BuyData").isDisplayed()) {
				status = true;
			}
		}

		catch (Exception e) {
			if (element("continuebutton").getAttribute("enabled").equals("true")) {
				status = true;
			}
		}
		return status;
	}

	/** verify text display */
	public boolean verifyTextInshareDataScreen() {
		boolean status = false;
		try {
			if (element("text").getText().contains("Sorry, you can’t share more data!")) {
				status = true;
			}
		} catch (Exception e) {
			if (element("carausel").isDisplayed()) {
				System.out.println("user has data to share");
				status = true;
			}
		}
		return status;
	}

	/** verify close button display */
	public boolean verifycloseButtondisplay() {
		boolean status = false;
		try {
			if (element("closebuton").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			if (element("carausel").isDisplayed()) {
				System.out.println("user has data to share");
				status = true;
			}
		}
		return status;
	}

	/** tap on close button */
	public void tapCloseButton() {
		try {
			if (element("closebuton").isDisplayed()) {
				element("closebuton").click();
			}
		} catch (Exception e) {
			if (element("carausel").isDisplayed()) {
				System.out.println("user has data to share");
			}
		}
	}

	/** verify other services screen display */
	public boolean verifyOtherServicesScreen() {
		boolean status = false;
		if (element("otherServices").getText().contains("Other services")) {
			status = true;
		} else if (element("carausel").isDisplayed()) {
			System.out.println("user has data to share");
			status = true;
		}
		return status;
	}

	/** tap on learn more button */
	public void tapLearnMoreButton() {
		element("learnmore").click();
	}

	/** verify text */
	public boolean verifyText() {
		return element("textmessage").isDisplayed();
	}

	/** tap on received data */
	public void tapReceivedData() {
		elements("transferedData").get(0).click();
	}

	/** tap on buy data button */
	public void tapBuyDatabutton() {
		try {
			if (element("BuyData").isDisplayed()) {
				element("BuyData").click();
			}
		}

		catch (Exception e) {
			if (element("continuebutton").getAttribute("enabled").equals("true")) {
				System.out.println("user has sufficient data");
			}
		}
	}
	
	/** verify plans screen display */
	public boolean verifyPlansScreenDisplay() {
	boolean status = false;
		try {
			if (element("planScreen").isDisplayed()) {
				status=true;
			}
		}

		catch (Exception e) {
			if (element("continuebutton").getAttribute("enabled").equals("true")) {
				System.out.println("user has sufficient data");
				status=true;
			}
		}
		return status;
	}
}
