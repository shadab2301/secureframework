package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_RoamingServicesPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_RoamingServicesPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_RoamingServices");
		System.out.println("Executed platform name  " + platformName);
	}
	
	

}
