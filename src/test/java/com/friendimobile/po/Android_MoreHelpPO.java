package com.friendimobile.po;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_MoreHelpPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_MoreHelpPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_MoreHelp");
		System.out.println("Executed platform name  " + platformName);
	}

	/**
	 * Tap on SEARCH button
	 * 
	 */
	public void clicksearchButton() {
		element("searchbtn").click();
	}

	/**
	 * VERIFY SEARCH FIELD DISPLAY
	 * 
	 */
	public boolean verifySearchFieldDisplay() {
		return element("searchfield").isDisplayed();
	}

	/**
	 * VERIFY ALL CATEGORIES DISPLAYED
	 * 
	 */
	public boolean verifyAllCategoryDisplayed() {
		boolean status = false;

		try {

			List<WebElement> Categories = elements("CategoriesList");
			for (int i = 0; i < Categories.size(); i++) {
				if (Categories.get(i).isDisplayed()) {
					System.out.println(Categories.get(i).getText());
					status = true;
				}
			}
		}

		catch (Exception e) {
			System.out.println("categories not displayed");
			status = false;
		}
		return status;
	}

	/** Tap on Text present on screen displayed */
	public void tapOnText(String text) {
		System.out.println("waiting");
		String x = "//*[@text='";
		String y = "']";
		String xpath = x + text + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Text");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		locate.click();
	}

	/** verify question answer screen displayed */
	public boolean verifyQuestionAnswerScreenDisplayed() {
		return element("questionAnswerScreen").isDisplayed();
	}

	/** tap on cross button */
	public void tapCrossButton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("crossbutton"))).click();
	}

	/** verify user able to enter text in search field */
	public boolean verifyAbleToEnter(String text) {
		boolean status = false;
		element("searchfield").sendKeys(text);
		String text1 = element("searchfield").getText();
		if (text1.equals(text)) {
			status = true;
		}
		return status;
	}

	/** enter invalid data on search field */
	public void enterInavalidData() {
		element("searchfield").sendKeys("1234");
	}

	/** verify dial pop up displayed */
	public boolean verifyDialPopupDisplay() {
		return element("dialpopup").isDisplayed();
	}

	/** verify back button displayed */
	public boolean verifyBackbuttonDisplay() {
		return element("backbutton").isDisplayed();
	}

	/** enter valid data in search field */
	public void enterValidDtaInSearchField() {
		element("searchfield").sendKeys("data");
	}

	/** verify result list displayed */
	public boolean verifyResultDisplayed() {
		boolean status = false;
		if (element("label").isDisplayed() && element("labelDiscription").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** select item from list */
	public void selectFromSearchList() {
		List<WebElement> searchlist = elements("labelDiscription");
		searchlist.get(0).click();
	}

	/** tap on Back button from question answer */
	public void tapBackButton() {
		element("Backarrow").click();
	}

	/**
	 * verify text disappears
	 */
	public boolean verifyTextDisappears() {
		boolean status = false;
		try {
			if( element("foundyouranswertext").isDisplayed()) {
				status= true;
			}
		}
		catch(Exception e) {
			System.out.println("text disappeared");
			status=false;
		}
		return status;
	}
}
