package com.friendimobile.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class UsageHistoryiOSPO extends CommonGestures {
public static String platformName = InitData.platformName;

	public UsageHistoryiOSPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/UsageHistory");
		System.out.println("Executed platform name  " + platformName);
	}
	
	/*
	 * click on help
	 */
	public void helpButton() {
		element("helpButton").click();
		
	}
	/*
	 *  dynamic method to verify Text value
	 */
	public boolean verifyTextValue(String text) {
		System.out.println("waiting");
		// XCUIElementTypeOther[@name="Top-up My Wallet"]
		String x = "//*[@value='";
		String y = "']";
		String xpath = x + text + y;
		System.out.println("+++++++++++++++++++++++++++++++++++++++");
		System.out.println(xpath);
		System.out.println("finding Screen");
		WebDriverWait wait = new WebDriverWait(session.driver, 180);
		WebElement locate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return locate.isDisplayed();
	}
}
