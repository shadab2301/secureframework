package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Android_PlansPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_PlansPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Plans");
		System.out.println("Executed platform name  " + platformName);
	}

	/** tap on plans option */
	public void tapOnPlans() {
		element("Plans").click();
	}

	/** verify subtype slider coachmark displayed */
	public boolean subtypeSlideCoachMarkDisplayed() {
		return element("subtypeSlideCoachMark").isDisplayed();
	}

	/** tap on screen */
	public void tapOnScreen() {
		element("Screen").click();
	}

	/** verify pack coachmark displayed */
	public boolean packCoachMarkDisplayed() {
		return element("packcouchmark").isDisplayed();
	}

	/** verify coachmark disappeard */
	public boolean coachMarkDisappears() {
		boolean status = false;
		try {
			if (element("packcouchmark").isDisplayed()) {
				status = false;
			}
		} catch (Exception e) {
			status = true;
			System.out.println("Coach Mark Disappeared !!!");
		}

		return status;
	}

	/** tap on buy plan coach mark */
	public void tapOnBuyPlanButton() {
		element("BuyPlanbutton").click();
	}

	/** tap on any one pack */
	public void tapOnAnyPack() {
		element("DataPack").click();
	}

	/** tap on any one offer */
	public void tapOnOneOffer() {
		try {
			elements("offers").get(0).click();
		} catch (Exception e) {

			System.out.println("Not found any offer");
		}
	}

	/** verify plan details coachmark displayed */
	public boolean planDetailsCoachMarkDisplayed() {
		return element("PlanDetailsCoachmark").isDisplayed();
	}

	/** verify buy plan button displayed */
	public boolean buyPlanButtonDisplayed() {
		return element("BuyPlanbutton").isDisplayed();
	}

	/** verify other pack options displayed */
	public boolean otherPackOptionsDisplayed() {
		return element("packoptions").isDisplayed();
	}

	/** verify DATA pack displayed */
	public boolean dataPackDisplayed() {
		return element("DataPack").isDisplayed();
	}

	/** verify plan screen displayed and data section is selected */
	public boolean verifyPlanScreenDataSelectedDisplay() {
		boolean status = false;
		if (element("planScreen").isDisplayed()) {
			element("datasection").getAttribute("selected").equals("true");
			status = true;
		}
		return status;
	}

	/** verify local calls pack displayed */
	public boolean localCallsPackDisplayed() {
		return element("localcallpack").isDisplayed();
	}

	/** tap on local calls pack */
	public void tapOnLocalCallsPack() {
		element("localcallpack").click();
	}

	/** verify plan screen displayed and local calls section is selected */
	public boolean verifyPlanScreenLocalCallSelectedDisplay() {
		boolean status = false;
		if (element("planScreen").isDisplayed()) {
			element("labelSection").getAttribute("text").equals("Local Calls");
			status = true;
		}
		return status;
	}

	/** verify international calls pack displayed */
	public boolean internationalCallsPackDisplayed() {
		return element("internationalcallpack").isDisplayed();
	}

	/** tap on international calls pack */
	public void tapOnInternationalCallsPack() {
		element("internationalcallpack").click();
	}

	/** verify plan screen displayed and international calls section is selected */
	public boolean verifyPlanScreeninterntionalCallSelectedDisplay() {
		boolean status = false;
		if (element("planScreen").isDisplayed()) {
			element("labelSection").getAttribute("text").equals("International Calls");
			status = true;
		}
		return status;
	}

	/** verify MIX pack displayed */
	public boolean mixPackDisplayed() {
		return element("Mix").isDisplayed();
	}

	/** tap on MIX pack */
	public void tapOnMixPack() {
		element("Mix").click();
	}

	/** verify plan screen displayed and MIX section is selected */
	public boolean verifyPlanScreenMixSelectedDisplay() {
		boolean status = false;
		if (element("planScreen").isDisplayed()) {
			element("mixsection").getAttribute("selected").equals("true");
			status = true;
		}
		return status;
	}

	/** verify plan screen displayed */
	public boolean verifyPlanScreenDisplay() {
		return element("planScreen").isDisplayed();
	}

	/** verify mobile Text displayed */
	public boolean verifyMobileTextDisplay(String Str) {
		boolean status = false;
		String ele = element("MobileText").getAttribute("text");
		System.out.println(ele);
		if (ele.equals(Str)) {
			status = true;
		}
		return status;
	}

	/** verify MSISDN displayed */
	public boolean verifyMSISDNDisplay() {
		return element("MSISDN").getText().equals("‭+968 7952 5424‬");
	}

	/** verify Total Balance Text displayed */
	public boolean verifyTotalBalanceTextDisplay() {
		return element("totalbalancetext").getText().equals("TOTAL BALANCE");
	}

	/** verify current wallet Balance displayed */
	public boolean verifyWallteBalnceDisplay() {
		return element("walltebalance").isDisplayed();
	}

	/** verify top up button displayed */
	public boolean verifyTopUpButtonDisplay() {
		return element("topupbutton").isDisplayed();
	}

	/** tap on top up button */
	public void tapOnTopUpButton() {
		element("topupbutton").click();
	}

	/** verify data tab displayed */
	public boolean verifyDataTabDisplay() {
		return element("datasection").getAttribute("selected").equals("true");
	}

	/** verify Minutes Tab displayed */
	public boolean verifyMinutesTabDisplay() {
		return element("minutesTab").isDisplayed();
	}

	/** tap on minutes tab */
	public void tapOnMinutesTab() {
		try {
			if (element("packcouchmark").isDisplayed()) {
				element("Screen").click();
				element("Screen").click();
				element("minutesTab").click();
			}
		} catch (Exception e) {
			element("minutesTab").click();

		}
	}

	/** verify mix tab display */
	public boolean verifyMixTabDisplay() {
		try {

			WebDriverWait wait = new WebDriverWait(session.driver, 60);
			return wait.until(ExpectedConditions.visibilityOf(element("MixTab"))).isDisplayed();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {

			System.out.println("****Catch block");

			WebDriverWait wait = new WebDriverWait(session.driver, 60);
			return wait.until(ExpectedConditions.visibilityOf(element("MixTab"))).isDisplayed();
		}
	}

	/** verify international calls pack displayed */
	public void tapOnMixTab() {
		element("MixTab").click();
	}

	/** verify mix packs displayed */
	public boolean verifyMixPacksDisplay() {
		boolean status = false;
		List<WebElement> element = elements("Mixpackslist");
		System.out.println("size of mix packs list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("+")) {
				System.out.println("list of Mix packs " + element.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify Carousel displayed */
	public boolean verifyCarouselDisplay() {
		return element("Carousel").isDisplayed();
	}

	/** scroll the Carousel */
	public void scrollcarousel() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;

		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int y = height / 4;

		int x_right = (int) (width * 0.8);

		int x_left = (int) (width * 0.2);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x_right, y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000))).moveTo(ElementOption.point(x_left, y))
				.release().perform();

		System.out.println("\nSwiping from Right to Left !!!");

		Thread.sleep(2000);
	}

	/** verify able to scroll the Carousel */
	public boolean verifyAbletoScrollCarousel() {
		return element("saverdatabanner").isDisplayed();
	}

	/** verify All Data Plans Category Displayed */
	public boolean verifyAllDataPlansCategoryDisplayed() {
		return element("labelSection").getText().equals("All Data Plans");
	}

	/** Tap on freedom data plan button */
	public void tapOnFreedomDataPlanButton() {
		try {
			if (element("subtypeSlideCoachMark").isDisplayed()) {
				element("Screen").click();
				element("Screen").click();
				element("freedomdataplanbutton").click();
			}
		} catch (Exception e) {
			element("freedomdataplanbutton").click();
		}

	}

	/** verify freedom Data Plans Category Displayed */
	public boolean verifyFreedomDataPlansCategoryDisplayed() {
		return element("labelSection").getText().equals("Freedom Data Plans");
	}

	/** Tap on saver data plan button */
	public void tapOnSaverDataPlanButton() {
		element("saverdataplanbutton").click();
	}

	/** verify saver Data Plans Category Displayed */
	public boolean verifySaverPlansCategoryDisplayed() {
		return element("labelSection").getText().equals("Saver Data Plans");
	}

	/** verify Freedom calls Category Displayed */
	public boolean verifyFreedomCallCategoryDisplayed() {
		return element("labelSection").getText().equals("Freedom Call Plans");
	}

	/** Tap on international calls button */
	public void tapinternationalCallsButton() {
		element("InternationalCallbutton").click();
	}

	/** verify international calls Category Displayed */
	public boolean verifyinternationalCallCategoryDisplayed() {
		return element("labelSection").getText().equals("International Calls");
	}

	/** verify select country button Displayed */
	public boolean verifySelectCountryButtonDisplayed() {
		return element("selectcountrybutton").isDisplayed();
	}

	/** Tap on select a country button */
	public void tapOnSelectCountryButton() {
		element("selectcountrybutton").click();
	}

	/** verify cross button Displayed */
	public boolean verifyCrossButtonDisplay() {
		return element("internationalcallscrossbutton").isDisplayed();
	}

	/** Tap on cross button */
	public void tapCrossButton() {
		element("internationalcallscrossbutton").click();
	}

	/** verify find country field Displayed */
	public boolean verifyFindCountryFieldDisplay() {
		return element("findcountryfield").isDisplayed();
	}

	/**
	 * Verify list of countries display
	 * 
	 * 
	 */
	public boolean verifyListOfCountriesDisplay() {
		boolean status = false;

		try {

			List<WebElement> element = elements("listOfCountry");
			for (int i = 0; i < element.size(); i++) {
				if (element.get(i).isDisplayed()) {
					System.out.println(element.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Countries list not display");
		}
		return status;
	}

	/**
	 * Type some country in text field
	 * 
	 * 
	 */
	public void typeCountryInTextField(String country) throws InterruptedException {
		element("findcountryfield").sendKeys(country);
	}

	/**
	 * verify input data in country field
	 * 
	 * 
	 */
	public boolean verifyInputDataInCountryField() {

		boolean status = false;

		String dataField = element("findcountryfield").getText();

		element("findcountryfield").sendKeys("Bahrain");

		String dataField2 = element("findcountryfield").getText();

		if (dataField.equals(dataField2)) {

			status = false;
		}

		else {
			status = true;
		}

		return status;
	}

	/**
	 * Verify search process
	 * 
	 */
	public boolean verifySearchProcess(String country) {

		boolean status = false;
		element("findcountryfield").sendKeys("YEMEN");
		List<WebElement> countries = elements("listOfCountry");
		for (int i = 0; i < countries.size(); i++) {
			if (countries.get(i).getText().equalsIgnoreCase(country)) {
				System.out.println("Country present: " + countries.get(i).isDisplayed());
				status = true;
				break;
			}
		}
		return status;
	}

	/**
	 * Verify country listed
	 * 
	 */
	public boolean verifyCountryListed(String country) {

		boolean status = false;
		element("findcountryfield").sendKeys(country);

		try {

			List<WebElement> element = elements("listOfCountry");
			System.out.println("searched countries:" + element.size());
			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).getText().contains(country)) {
					System.out.println(element.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Not found country name");
		}
		return status;
	}

	/**
	 * tap on country
	 * 
	 */
	public void tapOnCountry() {
		try {
			element("findcountryfield").clear();
			element("findcountryfield").sendKeys("Bahrain");
			List<WebElement> element = elements("listOfCountry");
			if (element.get(0).isDisplayed()) {
				element.get(0).click();
			}
		} catch (Exception e) {

			System.out.println("Not found country name");
		}
	}

	/**
	 * Verify freedom mix plan display
	 * 
	 */
	public boolean verifyFreedomMixPlansCategoryDisplayed() {
		return element("labelSection").getText().equals("Freedom Mix Plans");
	}

	/**
	 * tap on saver mix plan button
	 * 
	 */
	public void tapSaverMixPlansButton() {
		try {
			if (element("packcouchmark").isDisplayed()) {
				element("Screen").click();
				element("Screen").click();
				element("savermixplanbutton").click();
			}
		} catch (Exception e) {
			element("savermixplanbutton").click();
		}

	}

	/**
	 * Verify saver mix plan category display
	 * 
	 */
	public boolean verifySaverMixplanCategoryDisplayed() {
		return element("labelSection").getText().equals("Saver Mix Plans");
	}

	/**
	 * Verify DATA validity allowance and price display
	 */
	public boolean verifyDataPlanDetailsDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))
					&& (element3.get(i).getText().contains("RO"))) {
				System.out.println("validity of all packs " + element.get(i).getText());
				System.out.println("allowance of all packs " + element2.get(i).getText());
				System.out.println("price of all packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/**
	 * Verify calls validity allowance and price display
	 */
	public boolean verifyCallsPlanDetailsDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity")
					&& (element2.get(i).getText().contains("Minutes") || (element2.get(i).getText().contains("Hours"))
							|| element2.get(i).getText().contains("Unlimited"))
					&& (element3.get(i).getText().contains("RO"))) {
				System.out.println("validity of all packs " + element.get(i).getText());
				System.out.println("allowance of all packs " + element2.get(i).getText());
				System.out.println("price of all packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/**
	 * Verify Mix packs validity allowance and price display
	 */
	public boolean verifyMixPlanDetailsDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		List<WebElement> element3 = elements("price");
		System.out.println("size of packs" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Validity") && element2.get(i).getText().contains("+")
					&& element3.get(i).getText().contains("RO")) {
				System.out.println("validity of Mix packs " + element.get(i).getText());
				System.out.println("allowance of Mix packs " + element2.get(i).getText());
				System.out.println("price of all Mix packs " + element3.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}

		return status;
	}

	/** verify summary screen displayed */
	public boolean verifySummaryScreenDisplayed() {
		boolean status = false;
		try {
			if (element("summaryscreen").getText().equals("Summary")) {
				System.out.println("Summary Screen appears");
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("Rechargepopup").getText().equals("RECHARGE")) {
					status = true;
					System.out.println("insufficient balance");
				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bonus Minutes");
					status = true;
				}
			}
		}
		return status;
	}

	/** verify all data packs displayed */
	public boolean verifyAllDataPacksDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("allowance");
		List<WebElement> element2 = elements("validity");
		System.out.println("size of data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if ((element.get(i).getText().contains("MB") || element.get(i).getText().contains("GB"))
					&& (element2.get(i).getText().contains("Unlimited") || element2.get(i).getText().contains("Day"))) {
				System.out.println("list of All data packs " + element.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify freedom data packs displayed */
	public boolean verifyFreedomDataPacksDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		System.out.println("size of Freedom data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Unlimited")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))) {
				System.out.println("list of Freedom data packs " + element2.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify Saver data packs displayed */
	public boolean verifySaverPacksDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("validity");
		List<WebElement> element2 = elements("allowance");
		System.out.println("size of Saver data pack list" + element.size());
		for (int i = 0; i < element.size(); i++) {

			if (element.get(i).getText().contains("Day")
					&& (element2.get(i).getText().contains("MB") || element2.get(i).getText().contains("GB"))) {
				System.out.println("list of Saver data packs " + element2.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify Freedom call packs displayed */
	public boolean verifyFreedomCallacksDisplayed() {
		boolean status = false;
		List<WebElement> element1 = elements("Callpackslist");
		System.out.println("size of Freedom call pack list" + element1.size());
		for (int i = 0; i < element1.size(); i++) {

			if (element1.get(i).getText().contains("Minutes")
					&& elements("validity").get(i).getText().contains("Unlimited")) {
				System.out.println("list of Freedom Call packs " + element1.get(i).getText());
				status = true;
			}

			else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify international call packs displayed */
	public boolean verifyinternationalCallacksDisplayed() {
		boolean status = false;
		try {
			List<WebElement> internationalpacks = elements("Callpackslist");
			System.out.println("size of call pack list" + internationalpacks.size());
			for (int i = 0; i < internationalpacks.size(); i++) {

				if (internationalpacks.get(i).getText().contains("Minutes") && element("CountryLogo").isDisplayed()) {
					System.out.println("list of international Call packs " + internationalpacks.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("international call packs not display");
			status = true;
		}
		return status;
	}

	/** verify INCLUDED COUNTRY displayed */
	public boolean verifyIncludedCountryDisplayed() {

		return element("Countryname").getText().contains("Bahrain");
	}

	/** verify selected pack displayed */
	public boolean verifySelectedPackDisplayed() {
		return element("selectedpack").getText().equals("250 MB");
	}

	/** verify package card screen displayed */
	public boolean verifyPackageCardDisplayed() {
		return element("packagecard").isDisplayed();
	}

	/** verify cross button displayed */
	public boolean verifyCrossbuttonDisplayed() {
		return element("PackageCardcrossbutton").isDisplayed();
	}

	/** tap on cross button */
	public void tapCrossbutton() {
		element("PackageCardcrossbutton").click();
	}

	/** tap on offer */
	public void tapOnOffermorethenWalletBalance() {
		elements("offers").get(1).click();
	}

	/** verify Recharge pop up displayed */
	public boolean verifyRechargePopupDisplayed() {
		boolean status = false;
		try {
			if (element("Rechargepopup").getText().equals("RECHARGE")) {
				status = true;
			}
		} catch (Exception e) {
			try {
				if (element("SummaryScreen").getText().equals("Summary")) {
					System.out.println("Summary Screen Display");
					status = true;
				}
			} catch (Exception c) {
				if (elements("Dataextendedscreenprice").get(3).getText().contains("Free")) {
					System.out.println("Bounus Minutes");
					status = true;
				}
			}

		}
		return status;
	}

	/** verify back button displayed */
	public boolean verifybackDisplayed() {
		return element("summaryscreenbackbutton").isDisplayed();
	}

	/** tap summary screen back button */
	public void tapBackbutton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("summaryscreenbackbutton"))).click();
	}

	/** Tap on saver calls plan button */
	public void tapOnSaverCallsPlanButton() {
		element("freedomdataplanbutton").click();
	}

	/** verify saver calls Plans Category Displayed */
	public boolean verifySaverCallsPlansCategoryDisplayed() {
		return element("labelSection").getText().equals("Saver Call Plans");
	}

	/** verify Saver call packs displayed */
	public boolean verifySaverCallacksDisplayed() {
		boolean status = false;
		List<WebElement> element1 = elements("Callpackslist");
		System.out.println("size of Saver call pack list" + element1.size());
		for (int i = 0; i < element1.size(); i++) {

			if ((element1.get(i).getText().contains("Minutes") || element1.get(i).getText().contains("Unlimited"))
					&& elements("validity").get(i).getText().contains("Day")) {
				System.out.println("list of Saver Call packs " + element1.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/** verify Saver mix packs displayed */
	public boolean verifySaverMixPacksDisplayed() {
		boolean status = false;
		List<WebElement> element1 = elements("Callpackslist");
		System.out.println("size of Saver Mix pack list" + element1.size());
		for (int i = 0; i < element1.size(); i++) {

			if (element1.get(i).getText().contains("+") && elements("validity").get(i).getText().contains("Validity")) {
				System.out.println("list of Saver Mix packs " + element1.get(i).getText());
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/**
	 * Verify search field has character validation
	 */
	public boolean verifySearchBarHasCharacterValidation() {
		boolean status = false;

		String str = "abc123@";

		element("findcountryfield").sendKeys(str);
		if (element("findcountryfield").getText().equals(str)) {
			status = true;
			System.out.println("Accepting alphabets, numerals and special chars");
		} else {
			status = false;
		}

		return status;
	}

	/**
	 * Verify purchased data pack display over dashboard
	 */
	public boolean verifyPurchasedDatapackDisplay() {
		return elements("activeplans").get(0).getText().contains("250 MB");
	}

	/**
	 * Verify purchased same data pack twice
	 */
	public boolean verifyPurchaseDatapactwiceDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("250 MB")
				&& elements("activeplans").get(1).getText().contains("250 MB")) {
			status = true;
		}
		return status;
	}

	/**
	 * select different data pack
	 */
	public void tapOnDifferentDataPack() {
		elements("offers").get(1).click();
	}

	/**
	 * Verify two different data pack display
	 */
	public boolean verifyPurchasedifferentDatapackDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("950 MB")
				&& elements("activeplans").get(1).getText().contains("250 MB")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify purchased Freedom local calls pack display over dashboard
	 */
	public boolean verifyPurchasedFreedomLocalCallpackDisplay() {
		return elements("activeplans").get(0).getText().contains("60 mins");
	}

	/**
	 * Verify purchased same Freedom call pack twice
	 */
	public boolean verifyPurchaseFreedomCallpacktwiceDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("60 mins")
				&& elements("activeplans").get(1).getText().contains("60 mins")) {
			status = true;
		}
		return status;
	}

	/**
	 * select different freedom call pack
	 */
	public void tapOnDifferentCallPack() {
		elements("offers").get(1).click();
	}

	/**
	 * Verify two different Freedom call pack display
	 */
	public boolean verifyPurchasedifferentFreedomCallpackDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("200 mins")
				&& elements("activeplans").get(1).getText().contains("60 mins")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify purchased Saver local calls pack display over dashboard
	 */
	public boolean verifyPurchasedSaverLocalCallpackDisplay() {
		return elements("activeplans").get(0).getText().contains("28 hours");
	}

	/**
	 * Verify purchased same Saver call pack twice
	 */
	public boolean verifyPurchaseSaverlocalCallpacktwiceDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("28 hours")
				&& elements("activeplans").get(1).getText().contains("28 hours")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify two different Saver call pack display
	 */
	public boolean verifyPurchasedifferentSaverCallpackDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("Local Minutes")
				&& elements("activeplans").get(1).getText().contains("28 hours")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify purchased International call pack display over dashboard
	 */
	public boolean verifyPurchasedInternationalCallpackDisplay() {
		return elements("activeplans").get(0).getText().contains("19 mins");
	}

	/**
	 * Verify purchased same International call pack twice
	 */
	public boolean verifyPurchaseInternationalCallpacktwiceDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("19 mins")
				&& elements("activeplans").get(1).getText().contains("19 mins")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify two different International call pack display
	 */
	public boolean verifyPurchasedifferentInternationalCallpackDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("48 mins")
				&& elements("activeplans").get(1).getText().contains("19 mins")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify purchased Mix calls pack display over dashboard
	 */
	public boolean verifyPurchasedMixCallpackDisplay() {
		return elements("activeplans").get(0).getText().contains("12 GB");
	}

	/**
	 * Verify purchased same Freedom call pack twice
	 */
	public boolean verifyPurchaseMixCallpacktwiceDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("12 GB")
				&& elements("activeplans").get(1).getText().contains("12 GB")) {
			status = true;
		}
		return status;
	}

	/**
	 * Verify two different Mix call pack display
	 */
	public boolean verifyPurchasedifferentMixpackDisplay() {
		boolean status = false;
		if (elements("activeplans").get(0).getText().contains("24 GB")
				&& elements("activeplans").get(1).getText().contains("12 GB")) {
			status = true;
		}
		return status;
	}

}
