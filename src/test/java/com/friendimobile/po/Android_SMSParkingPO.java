package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Android_SMSParkingPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_SMSParkingPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_SMSParking");
		System.out.println("Executed platform name  " + platformName);
	}

	/**
	 * Scroll the screen upward
	 * 
	 * 
	 */
	public void scrollScreenUpward() throws InterruptedException {
		Thread.sleep(2000);
		MobileDriver androidriver = (MobileDriver) session.driver;
		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int x = width / 2;

		int top_y = (int) (height * 0.20);
		int bottom_y = (int) (height * 0.50);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500))).moveTo(ElementOption.point(x, top_y))
				.release().perform();

		System.out.println("\nScrolling the screen upwards !!!");

		Thread.sleep(1000);

	}

	/**
	 * verify minutes and price display
	 * 
	 * 
	 */
	public boolean verifyRatesMinutesPriceDisplay() throws InterruptedException {
		boolean status = false;
		List<WebElement> Minutes = elements("minuteslist");
		List<WebElement> price = elements("pricelist");
		for (int i = 0; i < Minutes.size(); i++) {
			if (Minutes.get(i).getText().contains("Minutes") && price.get(i).getText().contains("RO")) {
				status = true;
				System.out.println("Minutes and price display" + Minutes.get(i).getText() + price.get(i).getText());
			} else
				status = false;
			break;
		}
		return status;
	}

}
