package com.friendimobile.po;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

public class Android_RedeemRewardsPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_RedeemRewardsPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_RedeemRewards");
		System.out.println("Executed platform name  " + platformName);
	}

	/** verify text in Code Here field */
	public boolean verifyText(String strArg1) {
		if (element("CodeHere").getText().equals("CODE HERE")) {
			System.out.println("field is already empty!!!!!!!!!!!!!!!");
			element("CodeHere").click();
			element("CodeHere").sendKeys(strArg1);
			return element("CodeHere").getText().equals(strArg1);
		} else {
			element("CodeHere").clear();
			if (element("CodeHere").getText().equals("CODE HERE")) {
				System.out.println("field is empty now!!!!!!!!!!!!!!!");
			}
			element("CodeHere").click();
			element("CodeHere").sendKeys(strArg1);
			return element("CodeHere").getText().equals(strArg1);
		}
	}

	/** verify Congratulation pop-up display */
	public boolean verifyCongratulationpopup() {
		return element("congratulationPopup").getText().contains("Awesome! Enjoy your reward.");
	}

	/** To close the popup */
	public void tapClosebutton() {
		element("closebutton").click();
	}

	/** To tap on Save For Later button */
	public void tapSaveForLaterbutton() {
		element("saveforlaterbutton").click();
	}

	/** verify Saved pop-up display */
	public boolean verifySavedpopup() {
		return element("savedpopup").getText().contains("Saved");
	}

	/** verify apply button display for code */
	public boolean verifyApplyButtonDisplay() {
		return element("ApplybuttonforCode").isDisplayed();
	}

	/** To tap on apply button */
	public void tapApplybutton() {
		element("ApplybuttonforCode").click();
	}

	/** verify apply button display for offers */
	public boolean verifyApplyButton() {
		return element("applybuttonforOffer").isDisplayed();
	}

}
