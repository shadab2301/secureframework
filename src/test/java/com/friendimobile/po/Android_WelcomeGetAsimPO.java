package com.friendimobile.po;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class Android_WelcomeGetAsimPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_WelcomeGetAsimPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/WelcomeGetAsim");
		System.out.println("Executed platform name  " + platformName);
	}

	/** This is to confirm APK is installed */
	public void verifyingApplication() {
		System.out.println(" Application is Installed !!!");
	}

	/** This is to verify the splash screen */
	public boolean verifySplashScreen() {
		boolean status = false;

		WebDriverWait wait = new WebDriverWait(session.driver, 50);

		if (platformName.equals("android") || platformName.equals("sauceAndroid")) {
			status = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btn_signin"))).isDisplayed();
			System.out.println(" On Splash Screen !!!");
		} else if (platformName.equals("iPhone") || platformName.equals("sauceiOS")) {
			status = wait
					.until(ExpectedConditions
							.visibilityOfElementLocated(By.xpath("//XCUIElementTypeButton[@name='btn_signin']")))
					.isDisplayed();
			System.out.println(" On Splash Screen !!!");
		}

		return status;
	}

	/** This is to verify the Welcome screen */
	public boolean verifyWelcomeScreen() {
		return element("welcomescreen").isDisplayed();
	}

	/** This is to verify the existing customer button */
	public boolean verifyExistingCustomerBtn() {
		return element("existingcustomerbtn").isDisplayed();
	}

	/** This is to Tap on existing customer button */
	public void tapOnExistingCustomerbtn() {
		element("existingcustomerbtn").click();
	}

	/** This is to verify the SEND ME A SIM button */
	public boolean verifySendMeASIMbtn() {
		return element("SendMeAsim").isDisplayed();
	}

	/** This is to Tap on SEND ME A SIM button */
	public void tapOnSendMeASIMbtn() {
		element("SendMeAsim").click();
	}

	/** Verify Page Title */
	public boolean verifyPageTitle(String title) throws InterruptedException {
		Thread.sleep(2000);

		boolean status = false;
		WebDriverWait wait = new WebDriverWait(session.driver, 50);

		if (wait.until(ExpectedConditions.visibilityOf(element("pageTitle"))).getText().equalsIgnoreCase(title)) {
			status = true;
			System.out.println("Page Title: " + element("pageTitle").getText());
		} else {
			status = false;
		}
		return status;
	}

	/** For refreshing the screen */
	public static void refreshTheScreen() throws InterruptedException {
		AndroidDriver driver = (AndroidDriver) session.driver;
		try {
			Thread.sleep(2000);
			String activity = driver.currentActivity();
			((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
			driver.context(activity).switchTo();
			Thread.sleep(2000);
		} catch (Exception e) {
			Thread.sleep(2000);
			AndroidDriver driverss = (AndroidDriver) session.driver;
			((AndroidDriver) driverss).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
			Thread.sleep(2000);
		}
	}

	/** tap back button */
	public void tapOnBackButton() {
		element("backbutton").click();
	}

	/** enter text in full name field */
	public void enterTextFullNameField() {
		element("fullNameField").sendKeys("Test");
	}

	/** verify able to enter name */
	public boolean verifyabletoEnteText() {
		return element("fullNameField").getAttribute("text").equals("Test");
	}

	/** enter email in email field */
	public void enterTextInEmailField() {
		element("emailField").sendKeys("Test@gmail.com");
	}

	/** verify able to enter email */
	public boolean verifyabletoEnteEmail() {
		return element("emailField").getAttribute("text").equals("Test@gmail.com");
	}

	/** enter location in location field */
	public void enterlocationInlocationField() {
		element("locationfield").sendKeys("oman");
	}

	/** verify able to enter location */
	public boolean verifyabletoEntelocation() {
		return element("locationfield").getAttribute("text").equals("oman");
	}

	/** tap on dropdown */
	public void tapOnDropdown() {
		element("dropdown").click();
	}

	/** verify X button displayed */
	public boolean verifyXbtnDisplayed() {
		return element("Xbutton").isDisplayed();
	}

	/** tap on X button */
	public void tapOnXbtn() {
		element("Xbutton").click();
	}

	/** enter country in enter country field */
	public void enterCountryName() {
		element("entercountryField").sendKeys("oman");
	}

	/** verify able to enter text in enter country field */
	public boolean verifyableToEnterCountry() {
		return element("entercountryField").getAttribute("text").equals("oman");
	}

	/** verify Help icon Displayed */
	public boolean verifyHelpButton() {
		return element("helpIcon").isDisplayed();
	}

	/** enter input in number field */
	public void EnterNumberinNumberField() {
		element("NumberField").sendKeys("96879566458");
		session.driver.navigate().back();
	}

	/** verify able to enter input in Number field */
	public boolean verifyabletoEnterNumber() {
		return element("NumberField").getAttribute("text").equals("96879566458");
	}

	/** tap on submit button */
	public void tapOnSubmitBtn() {
		element("submitbutton").click();
	}

	/** verify pop up displayed */
	public boolean verifypopupDisplayed() {
		return element("popup").isDisplayed();
	}

	/** tap on Ok button */
	public void tapOnOKbutton() {
		element("okbutton").click();
	}

	/** enter Invalid text in full name field */
	public void enterInvalidName() {
		element("fullNameField").sendKeys("gdgdgdg");
	}

	/** enter Invalid location in location field */
	public void enterInvalidLocation() {
		element("locationfield").sendKeys("bdhbdh");
	}

	/** enter Invalid input in number field */
	public void enterInvalidNumber() {
		session.driver.navigate().back();
		element("NumberField").sendKeys("55852555555");
	}

	/** verify submit button disabled */
	public boolean verifysubmitbuttonDisabled() {
		return element("submitbutton").getAttribute("enabled").equals("false");
	}

	/** select country from country list */
	public void selectCountry() {
		element("selectcountry").click();
	}

	/** verify selected country code displayed */
	public boolean verifyselectedCountryCodeAppears() {
		return element("dropdown").getAttribute("text").contains("+971");
	}

	/** enter country name */
	public void enterCountryName2() {
		element("entercountryField").sendKeys("United");
	}

}
