package com.friendimobile.po;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class Android_InternationalCallsPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_InternationalCallsPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_InternationalCalls");
		System.out.println("Executed platform name  " + platformName);
	}

	/** enter mobile no */
	public void enterMobileNumber(String mobileno) {
		element("MobileNumberField").sendKeys(mobileno);
	}

	/** verify help screen display */
	public boolean verifyHelpScreenDisplay() {
		return element("helpscreen").isDisplayed();
	}

	/** tap on search icon */
	public void taponSearchIcon() {
		element("searchicon").click();
	}

	/** verify country logo display */
	public boolean verifyCountryLogoDisplay() {
		return element("countryflag").isDisplayed();
	}

	/** verify country list screen display */
	public boolean verifyCountryListScreenDisplay() {
		return element("countrylistscreen").isDisplayed();
	}

	/** verify supported countries display */
	public boolean verifysupportedCountriesDisplay() {
		return element("supportedcountries").getText().contains("supported countries");
	}

	/** tap on device back button */
	public void tapdeviceBackButton() {
		session.driver.navigate().back();
	}

	/** verify entered countries display */
	public boolean verifyEnteredCountryListDisplay() {
		boolean status = false;
		try {

			List<WebElement> element = elements("listOfCountry");
			System.out.println("searched countries:" + element.size());
			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).getText().contains("United")) {
					System.out.println(element.get(i).getText());
					status = true;
				} else {
					status = false;
					break;
				}
			}
		}

		catch (Exception e) {

			System.out.println("Not found country name");
		}
		return status;
	}

	/** verify entered country is not a part of eligible countries or not */
	public boolean verifySelectedCountryEligible(String countryname) {
		boolean status = false;
		try {
			element("entercountryField").clear();
			element("entercountryField").sendKeys(countryname);
			List<WebElement> element = elements("listOfCountry");
			System.out.println("searched countries:" + element.size());
			for (int i = 0; i < element.size(); i++) {

				if (element.get(i).getText().contains(countryname)) {
					System.out.println(element.get(i).getText());
					status = true;
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("entered country is not part of eligible countries");
			status = true;
		}
		return status;
	}

	/** verify included dropdown extended or collapsed */
	public boolean verifyDropDownStatus() {
		boolean status = false;
		try {
			if (element("countrylogo").isDisplayed()) {
				System.out.println("dropdown extended");
				status = true;
			}
		} catch (Exception e) {
			System.out.println("dropdown collapsed");
			status = true;
		}
		return status;
	}

	/** verify PayG calls and sms prices display */
	public boolean verifyPayGPriceDisplay() {
		boolean status = false;
		if (element("callsrate").getText().contains("RO") && element("smsrate").getText().contains("RO")) {
			status = true;
		}
		return status;
	}

	/** SELECT COUNTRY WITHOUT HAVING ANY PLAN */
	public void selectCountryWithoutHavingPlan() {
		List<WebElement> selectcountry = elements("countryList");
		element("entercountryField").clear();
		selectcountry.get(0).click();
	}

	/** TAP ON INTERNATIONAL CALLS FROM GRAPH */
	public void taoOninternationalcallServiceType() {
		try {
			if (element("internationalcall").getAttribute("focused").equals("false")) {
				element("internationalcall").click();

			}
		} catch (Exception e) {
			System.out.println("already selected");
		}

	}

	/** ENTER COUNTRY NAME IN ENTER COUNTRY FIELD */
	public void enterContry(String country) {
		element("entercountryField").sendKeys(country);
	}

	/** SCROLL THE OFFERS */
	public void scrollOffers() throws InterruptedException {
		MobileDriver androidriver = (MobileDriver) session.driver;
		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int x = width / 2;

		int top_y = (int) (height * 0.20);
		int bottom_y = (int) (height * 0.50);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500))).moveTo(ElementOption.point(x, top_y))
				.release().perform();

		System.out.println("\nScrolling the screen upwards !!!");

		Thread.sleep(1000);

	}

	/** verify purchased pack display */
	public boolean verifyPurchasedPack() {
		return elements("activeplans").get(0).getText().contains("15 mins");
	}

	/** select the country from country list */
	public void selectcountry() {
		element("selectCountry").click();
	}

	/** enter the country name */
	public void enterCountryName3() {
		element("entercountryField").sendKeys("India");
	}

	/**
	 * tap on back button
	 */

	public void tapOnBackButton() {
		WebDriverWait wait = new WebDriverWait(session.driver, 60);

		wait.until(ExpectedConditions.elementToBeClickable(element("backButton"))).click();
	}

	/**
	 * Scroll the screen upward
	 * 
	 * 
	 */
	public void scrollScreenUpwards() throws InterruptedException {
		Thread.sleep(2000);
		MobileDriver androidriver = (MobileDriver) session.driver;
		Dimension dim = session.driver.manage().window().getSize();

		int height = dim.getHeight();

		int width = dim.getWidth();

		int x = width / 2;

		int top_y = (int) (height * 0.20);
		int bottom_y = (int) (height * 0.80);

		new TouchAction((MobileDriver) session.driver).press(ElementOption.point(x, bottom_y))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500))).moveTo(ElementOption.point(x, top_y))
				.release().perform();

		System.out.println("\nScrolling the screen upwards !!!");

		Thread.sleep(1000);

	}

//	public void scrollScreenUpwards() throws InterruptedException {
//		Thread.sleep(2000);
//		MobileDriver androidriver = (MobileDriver) session.driver;
//		JavascriptExecutor js = ((JavascriptExecutor) session.driver);
//		js.executeScript("androidriver.scrollTo(0, document.body.scrollHeight)");
//	}
//	
//	

}
