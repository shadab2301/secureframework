package com.friendimobile.po;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class Android_LocalCreditTransferPO extends CommonGestures {

	public static String platformName = InitData.platformName;

	public Android_LocalCreditTransferPO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Android_LocalCreditTransfer");
		System.out.println("Executed platform name  " + platformName);
	}

	/** verify RO input field display */
	public boolean verifyROinputFieldDisplay() {
		return element("ROinputfield").isDisplayed();
	}

	/** verify HelpText display */
	public boolean verifyHelpTextDisplay(String helpText) {
		return element("numberfield").getText().equals(helpText);
	}

	/** verify able to input data in number field */
	public boolean verifyAbleToInputDataInNumberField(String data) {
		boolean status = false;
		element("numberfield").sendKeys(data);
		if (element("numberfield").getText().equals(data)) {
			status = true;
		}
		return status;
	}

	/** verify help text removed */
	public boolean verifyHelpTextRemoved() {
		boolean status = false;
		try {
			if (element("numberfield").getText().equalsIgnoreCase("Tap in number")) {
				status = false;
			} else {
				status = true;
				System.out.println("Help text is removed !!");
			}
		} catch (Exception e) {
			status = true;
			System.out.println("Help text is removed !!");
		}
		return status;
	}

	/** Verify Text field has character validation */
	public boolean verifyTextfieldHasCharacterValidation() {
		boolean status = false;

		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";

		element("numberfield").sendKeys(st1);
		if (element("numberfield").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("numberfield").sendKeys(st2);
		if (element("numberfield").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("numberfield").sendKeys(st3);
		if (element("numberfield").getText().equals(st3)) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}

		return status;
	}

	/** tap on top up button */
	public void tapOnTopUpbutton() {
		element("topupButton").click();
	}

	/** Tap on contact button */
	public void tapOnContactButton() throws InterruptedException {
		element("numberfield").clear();
		session.driver.navigate().back();
		TouchAction act = new TouchAction((MobileDriver) session.driver);
		act.press(PointOption.point(958, 1533)).release().perform();
		Thread.sleep(5000);

	}

	/** Verify contact screen */
	public boolean verifyContactScreen() {
		boolean status = false;
		try {
			if (element("contactPopUp").isDisplayed()) {
				element("allowpopup").click();
				session.driver.findElement(By.xpath("//*[@text ='Select Contacts']")).isDisplayed();
				status = true;
			}
		} catch (Exception e) {
			session.driver.findElement(By.xpath("//*[@text ='Select Contacts']")).isDisplayed();
			status = true;
		}
		return status;
	}

	/**
	 * To enter value in RO field
	 */
	public void enterValueInField(String amount) {
		// To initialize js object
		JavascriptExecutor JS = (JavascriptExecutor) session.driver;
		// To enter username
		JS.executeScript("document.getElementById('input_value').value=amount");
	}

	/** Verify able to enter in RO field */
	public boolean verifyInputInROField() {
		JavascriptExecutor JS = (JavascriptExecutor) session.driver;
		JS.executeScript("document.getElementById('input_value').value='12345'");
		// element("ROinputfield").sendKeys("12345");
		return element("ROinputfield").getText().equals("RO 12.345");
	}

	/** enter Mobile number in tap in number field */
	public void enterMobileNumber(String number) {
		element("numberfield").clear();
		element("numberfield").sendKeys(number);
	}

	/** Verify transfer button enabled */
	public boolean verifyTransferButtonEnabled() {
		boolean status = false;
		try {
			if (element("transferbutton").getAttribute("enabled").contains("true")) {
				System.out.println("transfer button enabled");
				status = true;
			}
		} catch (Exception e) {
			status = true;
			System.out.println("insufficient balance or entered Mobile number is incorrect !!!");
		}

		return status;
	}

	/** Verify total amount dropdown display */
	public boolean verifyTotalAmountDropdownDisplay() {
		return element("totalamountDropDown").isDisplayed();
	}

	/** Verify total amount details display */
	public boolean verifyTotalAmountDetailsDisplay() {
		return element("amountdetails").isDisplayed();
	}

	/** Verify OTP field displayed */
	public boolean verifyOTPFieldDisplay() {
		return element("OTPField").isDisplayed();
	}

	/** verify able to input data in number field */
	public boolean verifyAbleToInputDataInOTPField(String OTP) {
		boolean status = false;
		element("OTPField").clear();
		element("OTPField").sendKeys(OTP);
		if (element("OTPField").getText().equals(OTP)) {
			status = true;
		}
		return status;
	}

	/** verify able to input data in number field from contact list */
	public boolean verifyPhoneNumberDisplayed() {
		boolean status = false;
		List<WebElement> element = elements("Contactlist");
		element.get(3).click();
		if (element("numberfield").isDisplayed()) {
			status = true;
		}
		return status;
	}

	/** verify Error message display */
	public boolean verifyErrormessage(String error) {
		return element("ROinputfield").getText().contains(error);
	}
}
