package com.friendimobile.po;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;
import com.org.utils.InitData;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class Dashboard_iOS_PO extends CommonGestures {
	public static String platformName = InitData.platformName;

	public Dashboard_iOS_PO(TestSession session) throws Exception {
		super(session, "Applications/" + platformName + "/Dashboard_iOS");
		System.out.println("Executed platform name  " + platformName);

	}

	/**
	 * To verify MSISDN is present
	 */

	public void clickOnheaderDropDown() {

		element("headerDropDown").click();
	}


	/**
	 * To verify recharge options is displayed
	 */
	public boolean verifyRechargeOption() {
		return element("rechargeOption").isDisplayed();
	}

	/**
	 * To click on recharge options 
	 */
	public void clickOnRecharegeOption() {
	element("rechargeOption").click();
		
	}

	/**
	 * To verify a name field is present
	 */
	public boolean verifyNameField() {
	return element("nameField").isDisplayed();
	}

	/**
	 * To verify data can be entered in the name field
	 */
	public boolean verifyDataEnter(String strArg1) {
	try {
	element("nameField").click();	
	element("nameField").click();	
	Thread.sleep(3000);
	element("selectAll").click();
	element("nameField").sendKeys(Keys.DELETE);
	}catch(Exception e) {
	System.out.println("field is already empty");	
	}
	element("nameField").sendKeys(strArg1);
	return element("nameField").getText().equals(strArg1);
	}

	/**
	 * To verify total balance is showing
	 */
	public boolean verifyBalance() {
	return element("totalBalance").isDisplayed();
	}

	/**
	 * To verify Bonus wallet section is displayed
	 */
	public boolean verifyBonusWalletSection() {
	return element("bonusWalletSection").isDisplayed();
	}

	/**
	 * To click on Bonus wallet section 
	 * @throws InterruptedException 
	 */
	public void clickOnBonus() throws InterruptedException {
	Actions actions = new Actions(session.driver);
	int getX = element("bonusWalletSection").getLocation().getX();
	System.out.println("X coordinate:" + getX);
	int getY = element("bonusWalletSection").getLocation().getY();
	System.out.println("Y coordinate:" + getY);
	TouchAction act = new TouchAction<>((MobileDriver) session.driver);
	act.tap(PointOption.point(getX, getY)).perform();
	}

	/**
	 * To verify Total amount of SR and Your bonus balance is displayed
	 */
	public boolean verifyTotalAmountAndBonusBalance() {
	return element("totalAmount").isDisplayed() && element("bonusBalance").isDisplayed();
	}

	/**
	 * To verify Previous months are displayed
	 */
	public boolean verifyPreviousMonths() {
	try{List<WebElement> months = elements("previousMonth");
	for (int i = 0; i < months.size(); i++) {
	System.out.println(months.get(i).getText());
	return months.get(i).isDisplayed();	
	}return false;}
	catch(Exception e) {
	System.out.println("No History is present!!!!!!!!!!!!!!!!!!!!!!!!!");
	return true;}
	}

	/**
	 * To verify Previous months are displayed
	 */
	public boolean verifyBonuses() {
	boolean status=false;	
	try{element("previousMonth").click();
	List<WebElement> a = elements("rechargeDescription");
	List<WebElement> b = elements("rechargeValue");
	for (int i = 0; i < a.size(); i++) {
	System.out.println(a.get(i).getText());
	System.out.println(b.get(i).getText());
	if(a.get(i).isDisplayed() && b.get(i).isDisplayed())
	status=true;	
	}}catch(Exception e) {
	System.out.println("No History!!!!!!!!!!!!!!!!!!!!!!!!!");
	status=true;	
	}
	return status;
	}

	/**
	 * To verify amount field is present
	 */
	public boolean verifyAmountField() {
	return element("amountField").isDisplayed();
	}

	/**
	 * To click on Recharge Your Account button
	 */
	public void clickOnReachargeAccountButton() {
	element("rechargeAccountButton").click();
	}

	/**
	 * To verify user can input data in recharge wallet field
	 * @throws InterruptedException 
	 */
	public boolean verifyRechargeWallet(String strArg1) throws InterruptedException {
	try{
	element("rechargeWallet").click();
	element("rechargeWallet").click();
	Thread.sleep(3000);
	element("selectAll").click();
	element("rechargeWallet").sendKeys(Keys.DELETE);}
	catch(Exception e){
	System.out.println("Field is already empty!!!!!!!!!!!!!!!!!!!!!");	
	}element("rechargeWallet").clear();
	element("rechargeWallet").sendKeys(strArg1);
	element("plusBtn").click();
	return element("rechargeWallet").getText().equals(strArg1);
	}

	/**
	 * To verify character validation is present in recharge wallet field
	 * @throws InterruptedException 
	 */
	public boolean verifyCharacterValidation() {
			boolean status = false;
			element("rechargeWallet").clear();
			String st1 = "abcde";
			String st2 = "!@#$%";
			String st3 = "0571364";
			try{
				element("rechargeWallet").click();
				element("rechargeWallet").click();
				Thread.sleep(3000);
				element("selectAll").click();
				element("rechargeWallet").sendKeys(Keys.DELETE);}
				catch(Exception e){
				System.out.println("Field is already empty!!!!!!!!!!!!!!!!!!!!!");	
			element("rechargeWallet").sendKeys(st1);
			if (element("rechargeWallet").getText().equals(st1)) {
				status = false;
			} else {
				status = true;
				System.out.println("Not accepting alphabets !!!");
			}

			element("rechargeWallet").sendKeys(st2);
			if (element("rechargeWallet").getText().equals(st2)) {
				status = false;
			} else {
				status = true;
				System.out.println("Not accepting special characters !!!");
			}

			element("rechargeWallet").sendKeys(st3);
			if (element("rechargeWallet").getText().equals(st3)) {
				status = true;
				System.out.println("Accepting numbers !!!");
			} else {
				status = false;

			}	}

			return status;
		}
	

	/**
	 * To verify a payment method is present 
	 */
	public boolean verifyPaymentMethod() {
	return element("cardPresent").isDisplayed();
	}

	/**
	 * To enter wrong details
	 */
	public void enterWrongDetails() {
	element("payUsing").click();
	element("addNewCard").click();
	element("cardNumber").sendKeys("11111111111111111222333Test");
	element("addBtn").click();
	}

	/**
	 * To verify Pay button is enabled
	 */
	public boolean verifyPayBtn() {
	return element("payBtn").isEnabled();
	}

	/**
	 * To click plus button
	 */
	public void clickPlusBtn(int number) {
	for(int i=0;i<number;i++) {
	element("plusBtn").click();
	}}

	/**
	 * To verify use voucher button is disabled
	 */
	public boolean verifyVoucherBtnDisabled() {
	return element("useVoucherBtn").isEnabled();
	}

	/**
	 * To enter data in Voucher field
	 */
	public void verifyVoucherField(String strArg1) {
		element("useVoucherField").clear();	
		element("useVoucherField").click();
		element("useVoucherField").sendKeys(strArg1);
			}

	/**
	 * To verify character validation is present in the voucher field
	 * @throws InterruptedException 
	 */
	public boolean verifyCharacterValidationInVoucherField() {
		boolean status = false;
		element("useVoucherField").clear();
		String st1 = "abcde";
		String st2 = "!@#$%";
		String st3 = "0571364";
		try{
			element("useVoucherField").click();
			element("useVoucherField").click();
			Thread.sleep(3000);
			element("selectAll").click();
			element("useVoucherField").sendKeys(Keys.DELETE);}
			catch(Exception e){
			System.out.println("Field is already empty!!!!!!!!!!!!!!!!!!!!!");	
		element("useVoucherField").sendKeys(st1);
		if (element("useVoucherField").getText().equals(st1)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting alphabets !!!");
		}

		element("useVoucherField").sendKeys(st2);
		if (element("useVoucherField").getText().equals(st2)) {
			status = false;
		} else {
			status = true;
			System.out.println("Not accepting special characters !!!");
		}

		element("useVoucherField").sendKeys(st3);
		if (element("useVoucherField").getText().equals("0571-364")) {
			status = true;
			System.out.println("Accepting numbers !!!");
		} else {
			status = false;

		}	}

		return status;
	}

	/**
	 * To verify character limit is present in the voucher field
	 * @throws InterruptedException 
	 */
	public boolean verifyCharacterLimitInVoucherField() throws InterruptedException {
		boolean status = false;

		String data1 = "111111111111";
		String data2 = "11111111111";
		String data3 = "1111111111111";

		element("useVoucherField").clear();
		element("useVoucherField").sendKeys(data1);
		if (element("useVoucherField").getText().equals("1111-1111-1111")) {
			status = true;
			System.out.println("Accepting 12 digits !!!");
		} else {
			status = false;
		}

		element("useVoucherField").clear();
		element("useVoucherField").sendKeys(data2);
		if (element("useVoucherField").getText().equals("1111-1111-1111")) {
			status = false;
		} else {
			status = true;
			System.out.println("11 digits are not allowed, should be 12 digits");
		}

		element("useVoucherField").clear();
		element("useVoucherField").sendKeys(data3);
		Thread.sleep(3000);
		if (element("useVoucherField").getText().equals("1111-1111-1111")) {
			status = true;
			System.out.println("13 digits are not allowed, should be 12 digits");
		} else {
			status = false;
		}

		return status;
	}

	/**
	 * To verify notifications are displayed
	 *
	 */
	public boolean verifyNotifications() {
			boolean status = false;
			try {
				if (element("notificationArrow").isDisplayed()) {
					System.out.println("in the try block !!!!!!!!!!!!");
					System.out.println("Notifications are stacked one on top of other");
					status = true;
				}
			}  catch (Exception e) {
				try {if (element("notifications").isDisplayed()) {
					System.out.println("in the catch try block !!!!!!!!!!!!");
					System.out.println("single notification is display");
					status = true;}
				}
				catch (Exception e1){
					System.out.println("in the catch block !!!!!!!!!!!!");
					System.out.println("no notifications are present");	
					status=true;
					}
			}
			return status;
		}

	/**
	 * To verify notifications can be expanded
	 * @throws InterruptedException 
	 */
	public boolean verifyNotificationsExpands() throws InterruptedException {
		boolean status = false;
		try {
			element("notificationArrow").click();
			CommonGestures.scrollUpMobile();
			CommonGestures.scrollUpMobile();
			if (element("closebtn").isDisplayed()) {
				status = true;
			}
		} catch (Exception e) {
			System.out.println("single or no notification is displayed");
			status = true;
		}
		return status;
	}

	/**
	 * To verify all clear btn is displayed
	 * @throws InterruptedException 
	 */
	public boolean verifyClearBtn() throws InterruptedException {
	CommonGestures.scrollUpMobile();
	CommonGestures.scrollUpMobile();	
	 try{
		return element("allClearBtn").isDisplayed();
	}catch(Exception e) {
	System.out.println("no notifications are displayed!!!!!!!!!!!");
	return true;
	}
	}

	/**
	 * To click on clear btn 
	 */
	public void clickClearAllBtn() {
		try{
			element("crossbtn").click();	
			element("allClearBtn").click();	
		}catch(Exception e) {
			System.out.println("no notifications are displayed!!!!!!!!!!!");}
	}

	/**
	 * To verify all notifications are cleared
	 */
	public boolean verifyNotificationsCleared() {
	boolean status=false;	
	try{if(element("notifications").isDisplayed())
	status=false;
	}catch(Exception e) {
		System.out.println("no notifications are displayed!!!!!!!!!!!");
		status=true;
		}return status;
	}

	/**
	 * To value in the field of sign in
	 * 
	 */
	public void enterValue(String strArg1) {
	element("MobileNumberField").clear();
	element("MobileNumberField").sendKeys(strArg1);
	}

	/**
	 * To verify toggle button
	 * 
	 */
	public boolean verifyToggleBtn() {
	return element("toggleBtn").isDisplayed();
	}

	/**
	 * To verify word "disabled" is displayed
	 * 
	 */
	public boolean verifyToggleBtnDisabled() {
	try{return element("toggleDisabled").isDisplayed();}
	catch(Exception e) {
	element("toggleBtn").click();
	return element("toggleDisabled").isDisplayed();
	}
	}

	/**
	 * To verify word "enabled" is displayed
	 * @throws InterruptedException 
	 * 
	 */
	public boolean verifyToggleBtnEnabled() throws InterruptedException {
	element("toggleBtn").click();
	element("confirmBtn").click();
	WebDriverWait wait = new WebDriverWait(session.driver, 180);
	
	return element("toggleEnabled").isDisplayed();
	}

	/**
	 *To verify bonuses gets closed
	 */
	public boolean verifyBonusesClosed() {
	try{element("previousMonth").click();
	return !element("rechargeDescription").isDisplayed()&&!element("rechargeValue").isDisplayed();}
	catch(Exception e){System.out.println("No History is available!!!!!!!!!");
	return true;
	}
	}

	/**
	 *To verify second month bonus are opened
	 */	
	public boolean verifyMoreThanOneMonthCanBeOpened() throws InterruptedException  {
		boolean status=false;
		CommonGestures.longpressScrollUpwardSimultaneously();
		CommonGestures.longpressScrollUpwardSimultaneously();
		try{elements("previousMonth").get(1).click();
		CommonGestures.longpressScrollUpwardSimultaneously();
		CommonGestures.longpressScrollUpwardSimultaneously();
		List<WebElement> c = elements("rechargeDescription");
		List<WebElement> d = elements("rechargeValue");
		for (int i = 0; i < c.size(); i++) {
		if(c.get(i).isDisplayed() && d.get(i).isDisplayed())
		System.out.println("Second month bonuses are displayed");	
		status=true;	
		}}
		catch(Exception e) {
		System.out.println("No History is available!!!!!!!!!!!!!!");	
		status=true;
		}
	return status;}

	/**
	 *To verify navigation bar is present
	 */
	public boolean verifyNavigationBar() {
	return element("navigationBar").isDisplayed();
	}
    
	/**
	 *To verify amount summed are displayed correctly
	 */
	public boolean verifyAmountSummedProperly() {
	try{return element("totalAmountInMonth").getText().contains("RO");}
	catch(Exception e) {
	System.out.println("No History is present!!!!!!!!!!!!!!");	
	return true;
	}
	}

}
