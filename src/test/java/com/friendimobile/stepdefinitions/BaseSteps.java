package com.friendimobile.stepdefinitions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.org.helper.TestRail;
import com.org.utils.APIClient;
import com.org.utils.PropFileHandler;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;

/**
 *
 * @author nimit.jain
 *
 */
public class BaseSteps extends StepDefinitionInit {
	public static Scenario scenario;

	@Before
	public void setUp() throws Exception {

		System.out.println("STARTING TEST..... ");
		session = getTestSession();
		Thread.sleep(5000);

	}

	@After
	public void tearDown(Scenario scenario) throws Exception {

		this.scenario = scenario;
		String ID = PropFileHandler.readProperty("testCaseID");
		if (scenario.isFailed()) {

			logError(scenario, ID);

			// Take a screenshot...
			System.out.println("The error message is " + scenario.getName());
			System.out.println("The  message is " + scenario.getStatus());

			final byte[] screenshot = ((TakesScreenshot) session.driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png"); // ... and embed it in the report.

			InputStream in = new ByteArrayInputStream(screenshot);
			BufferedImage bImageFromConvert = ImageIO.read(in);

			ImageIO.write(bImageFromConvert, "png",

				//	new File("./Dropbox/Failed_Test_Cases/" + ID + scenario.getName().replaceAll(" ", "") + ".png"));
					new File("./screenshot/"+ID + scenario.getName().replaceAll(" ", "") + ".png"));
      
//			TestRail.addScreenshotForFailedTestCases(ID, 5,
//		
//					"Failed- Status updated automatically from Selenium test automation");

			
		} else {
			TestRail.addResultForTestCase(ID, 1, "Pass - Status updated automatically from Selenium test automation ");
			System.out.println("Updated Pass testcase with is:-" + ID);
		}
		System.out.println("I am in the tear function...");
		try {
			session.quit();
			System.out.println("Session is not terminated!!!");
		} finally {
			session = null;
		}
	}

	private void logError(Scenario scenario, String ID) {

		Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
		field.setAccessible(true);
		try {
			ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
			for (Result result : results) {
				if (result.getError() != null)
					TestRail.addResultForTestCase(ID, 5, "Error:-" + result.getErrorMessage());
				System.out.println("Updated Fail testcase with is:-" + ID);
				
			}
		} catch (Exception e) {
			
	           StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                String exceptionAsString = sw.toString();
                System.out.println(exceptionAsString);
                e.printStackTrace();
                
                System.out.println("*********************"+exceptionAsString);
                

		}
}
}