package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_InitialLoginStepDef extends StepDefinitionInit {

	@When("^I enter verification code$")
	public void i_enter_verification_code() throws Throwable {
		initialLoginPO.enterVerificationCode();
	}

	@When("^I click on next button$")
	public void i_click_on_next_button() throws Throwable {
		initialLoginPO.clickNextButton();
	}

	@Then("^I verify promo popup coach marks is displayed$")
	public void i_verify_promo_popup_coach_marks_is_displayed() throws Throwable {
		Assert.assertTrue(initialLoginPO.promoPopUpCoachMarkDisplayed());
	}

	@When("^I tap on cross button$")
	public void i_tap_on_cross_button() throws Throwable {
		initialLoginPO.tapCrossButton();
	}

	@Then("^I verify Buy Plan Coach Mark is displayed$")
	public void i_verify_buy_plan_coach_mark_is_displayed() throws Throwable {
		Assert.assertTrue(initialLoginPO.buyPlanCoachMarckDisplayed());
	}
}
