package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_ManageSIMStepDef extends StepDefinitionInit {
	@Then("^I verify Black circle with \"([^\"]*)\" label is displayed$")
	public void i_verify_black_circle_with_something_label_is_displayed(String label) throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyLablelDisplay(label));
	}

	@When("^I tap on expand button in top right corner$")
	public void i_tap_on_expand_button_in_top_right_corner() throws Throwable {
		android_ManageSimPO.tapOnExpandButtonInTopRight();
	}

	@Then("^I verify the user is able to expand the number dropdown when clicks on it$")
	public void i_verify_the_user_is_able_to_expand_the_number_dropdown_when_clicks_on_it() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyExpandNumber());
	}

	@Then("^I verify ManageSIM options displayed$")
	public void i_verify_managesim_options_displayed() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyManagaeSimOptinsDisplay());
	}

	@And("^I verify \"([^\"]*)\" display$")
	public void i_verify_something_display(String strArg1) throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyCreditOptionDisplay());
	}

	@When("^I tap on credit recharge options$")
	public void i_tap_on_credit_recharge_options() throws Throwable {
		android_ManageSimPO.taponCreditRecharge();
	}

	@Then("^I verify Edit name popup display$")
	public void i_verify_edit_name_popup_display() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyEditNamepopupDisplay());
	}

	@And("^I verify Name field display$")
	public void i_verify_name_field_display() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyNameFieldDisplay());
	}

	@And("^I verify able to enter in field$")
	public void i_verify_able_to_enter_in_field() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyabletoEnterText());
	}

	@When("^I enter \"([^\"]*)\" in name field$")
	public void i_enter_something_in_name_field(String text) throws Throwable {
		android_ManageSimPO.enterTextNameField(text);
	}

	@Then("^I verify entered text is saved$")
	public void i_verify_entered_text_is_saved() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyEntetredTextSaved());
	}

	@And("^I verify SIM Name is displayed in the header$")
	public void i_verify_sim_name_is_displayed_in_the_header() throws Throwable {
		Assert.assertTrue(android_ManageSimPO.verifyEntetredTextSaved());
	}

	@When("^I tap back button from recharge screen$")
	public void i_tap_back_button_from_recharge_screen() throws Throwable {
		android_ManageSimPO.tapBackbutton();
	}
}
