package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_DashboardStepDef extends StepDefinitionInit {

	@And("^I verify credit balance is displayed$")
	public void i_verify_credit_balance_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyCreditBalancedisplay());
	}

	@Then("^I verfiy Credit balance section is displayed$")
	public void i_verfiy_credit_balance_section_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyCreditBalanceSectiondisplay());
	}

	@When("^I tap on Arrow button$")
	public void i_tap_on_arrow_button() throws Throwable {
		android_dashboardPO.tapOnArrowButton();
	}

	@Then("^I verify free whatsapp and free friendi call display$")
	public void i_verify_free_whatsapp_and_free_friendi_call_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyFreewhatsappFriendiCallDisplay());
	}

	@And("^I verify voucher number field display$")
	public void i_verify_voucher_number_field_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyVoucherNumberFieldDisplay());
	}

	@And("^I verify i am able to enter in field$")
	public void i_verify_i_am_able_to_enter_in_field() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyableToEnterData());
	}

	@When("^I enter \"([^\"]*)\" in the VOUCHER NUMBER field$")
	public void i_enter_something_in_the_voucher_number_field(String data) throws Throwable {
		android_dashboardPO.enterDatainVoucherNumberField(data);
	}

	@Then("^I verify USE VOUCHER button is disabled$")
	public void i_verify_use_voucher_button_is_disabled() throws Throwable {
		Assert.assertFalse(android_dashboardPO.verifyUseVoucherButton());
	}

	@Then("^I verify USE VOUCHER button is enabled$")
	public void i_verify_use_voucher_button_is_enabled() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyUseVoucherButton());
	}

	@Then("^I verify Error Message display$")
	public void i_verify_error_message_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyErrorMessageDisplay());
	}

	@Then("^I verify one time topup field display$")
	public void i_verify_one_time_topup_field_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyOneTimeTopUpFieldDisplay());
	}

	@And("^I verify enter amount field display$")
	public void i_verify_enter_amount_field_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyEnterAmountFieldDisplay());
	}

	@And("^I verify i am able to enter in oneTime topUp field$")
	public void i_verify_i_am_able_to_enter_in_onetime_topup_field() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAbleToEnterIntopupField());
	}

	@And("^I verify PAY button is Displayed$")
	public void i_verify_pay_button_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPayButtonDisplay());
	}

	@Then("^I verify add new card screen appears$")
	public void i_verify_add_new_card_screen_appears() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAddNewCardScreenDisplay());
	}

	@Then("^I verify add debit card webview display$")
	public void i_verify_add_debit_card_webview_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAddDebitCardWebviewDisplay());
	}

	@When("^I tap cross button$")
	public void i_tap_cross_button() throws Throwable {
		android_dashboardPO.tapCrossButton();
	}

	@And("^I verify PAY button is disabled if payment method not selected$")
	public void i_verify_pay_button_is_disabled_if_payment_method_not_selected() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPayButtonstatus());
	}

	@And("^I verify PAY button is enabled if payment method selected$")
	public void i_verify_pay_button_is_enabled_if_payment_method_selected() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPayButtonstatus());
	}

	@When("^I enter \"([^\"]*)\" in oneTime topUp field$")
	public void i_enter_something_in_onetime_topup_field(String value) throws Throwable {
		android_dashboardPO.enterValueTopUpfield(value);
	}

	@Then("^I verify active packs display$")
	public void i_verify_active_packs_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyActivePacksDisplay());
	}

	@Then("^I verify graph with details display$")
	public void i_verify_graph_with_details_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyGraphDetailsDisplay());
	}

	@And("^I verify graph details group by service type$")
	public void i_verify_graph_detais_group_by_service_type() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyGraphDetailsGroupbyServiceType());
	}

	@And("^I verify remainaing balance and total balance display$")
	public void i_verify_remainaing_balance_and_total_balance_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyTotalRemainingBalance());
	}

	@When("^I tap on arrow in notification section$")
	public void i_tap_on_arrow_in_notification_section() throws Throwable {
		android_dashboardPO.tapOnarrowNotificationSection();
	}

	@Then("^I verify notifications expanded$")
	public void i_verify_notifications_expanded() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyNotificationSectionExpend());
	}

	@Then("^I verify notifications are stacked one on top of other$")
	public void i_verify_notifications_are_stacked_one_on_top_of_other() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyNotificationsStacked());
	}

	@When("^I tap on any one data pack$")
	public void i_tap_on_any_one_data_pack() throws Throwable {
		android_dashboardPO.tapOnActiveDataPack();
	}

	@Then("^I verify Data extended view is displayed$")
	public void i_verify_data_extended_view_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyDataExtendedViewDispaly());
	}

	@When("^I click on cross Button$")
	public void i_click_on_cross_button() throws Throwable {
		android_dashboardPO.taponCrossButton();
	}

	@And("^I verify pi chart and remaining data display$")
	public void i_verify_pi_chart_and_remaining_data_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPIchartDispaly());
	}

	@And("^I verify BUY AGAIN button is Displayed$")
	public void i_verify_buy_again_button_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyBuyAgainButtonDisplay());
	}

	@When("^I tap on buy again button$")
	public void i_tap_on_buy_again_button() throws Throwable {
		android_dashboardPO.taponBuyAgainButton();
	}

	@When("^I tap on Data Service type from graph$")
	public void i_tap_on_data_service_type_from_graph() throws Throwable {
		android_dashboardPO.taoOnDataServiceType();
	}

	@Then("^I verify data packs display on dashboard$")
	public void i_verify_data_packs_display_on_dashboard() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyDataPacksDisplay());
	}

	@When("^I tap on Local call Service type from graph$")
	public void i_tap_on_Local_call_service_type_from_graph() throws Throwable {
		android_dashboardPO.taoOnLocalcallServiceType();
	}

	@Then("^I verify Local call packs display on dashboard$")
	public void i_verify_Local_call_packs_display_on_dashboard() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyLocalcallPacksDisplay());
	}

	@When("^I tap on international call Service type from graph$")
	public void i_tap_on_international_call_service_type_from_graph() throws Throwable {
		android_dashboardPO.taoOninternationalcallServiceType();
	}

	@Then("^I verify international call packs display on dashboard$")
	public void i_verify_international_call_packs_display_on_dashboard() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyinternationalcallPacksDisplay());
	}

	@And("^I verfiy validity period is displayed$")
	public void i_verfiy_validity_period_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyValidityPeriodDisplay());
	}

	@And("^I verify vaid untill date display$")
	public void i_verify_vaid_untill_date_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyValiduntillDisplay());
	}

	@And("^I verfiy price is displayed$")
	public void i_verfiy_price_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPriceDisplay());
	}

	@When("^I tap on back button present screen$")
	public void i_tap_on_back_button_present_screen() throws Throwable {
		android_dashboardPO.tapbackbutton();
	}

	@Then("^I verify Home screen notification display$")
	public void i_verify_home_screen_notification_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyHomeNotificationDisplay());
	}

	@And("^I verify the status of free whatsapp and free friendi call$")
	public void i_verify_the_status_of_free_whatsapp_and_free_friendi_call() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyStatusWhatsappandFriendicall());
	}

	@And("^I verfiy 5% bonus dial disaply$")
	public void i_verfiy_5_bonus_dial_disaply() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyBonusDialDisplay());
	}

	@Then("^I verify previous month bonus display$")
	public void i_verify_previous_month_bonus_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPreviousBonusesDisplay());
	}

	@When("^I tap arrow button$")
	public void i_tap_arrow_button() throws Throwable {
		android_dashboardPO.tapArrowButton();
	}

	@Then("^I verify detailed view of promo bonus per month is closed$")
	public void i_verify_detailed_view_of_promo_bonus_per_month_is_closed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPreviousBonusesDisplay());
	}

	@When("^I tap arrow button for first month$")
	public void i_tap_arrow_button_for_first_month() throws Throwable {
		android_dashboardPO.tapArrowButtonFirstMonth();
	}

	@When("^I tap arrow button for second month$")
	public void i_tap_arrow_button_for_second_month() throws Throwable {
		android_dashboardPO.tapArrowButtonSecondMonth();
	}

	@Then("^I verify detailed view of promo bonus can be opened for more than one month at a time$")
	public void i_verify_detailed_view_of_promo_bonus_can_be_opened_for_more_than_one_month_at_a_time()
			throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPreviousBonusesDisplay());
	}

	@And("^I verify amounts per days are correctly summed up in the monthly amount$")
	public void i_verify_amounts_per_days_are_correctly_summed_up_in_the_monthly_amount() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifySumUpAmountDisplay());
	}

	@When("^I click on positive button$")
	public void i_click_on_positive_button() throws Throwable {
		android_dashboardPO.clickPositive();
	}

	@Then("^I verify remaining balance is display$")
	public void i_verify_remaining_balance_is_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRemaingBalanceDisplay());
	}

	@And("^I verify total balance is display$")
	public void i_verify_total_balance_is_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyTotalBalanceDisplay());
	}

	@And("^I verfiy validity is displayed for all itme in the list$")
	public void i_verfiy_validity_is_displayed_for_all_itme_in_the_list() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyValidityDisplay());
	}

	@And("^I verify auto renew icon display for latest purchase of unlimited validity pack$")
	public void i_verify_auto_renew_icon_display_for_latest_purchase_of_unlimited_validity_pack() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutoRenewIconDisplay());
	}

	@When("^I click \"([^\"]*)\" times on positive button$")
	public void i_click_something_times_on_positive_button(String number) throws Throwable {
		android_dashboardPO.clickPositiveButton(number);
	}

	@Then("^I verify Navigation bar display$")
	public void i_verify_navigation_bar_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyNavigationBarDisplay());
	}

	@Then("^I verify \"([^\"]*)\" screen display$")
	public void i_verify_something_screen_display(String Screenname) throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyScreenDisplay(Screenname));
	}

	@Then("^I verify app logo display$")
	public void i_verify_app_logo_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAppLogoDisplay());
	}

	@And("^I verify remaining data display$")
	public void i_verify_remaining_data_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRemaingDataDisplay());
	}

	@And("^I verfiy validity Time period is displayed$")
	public void i_verfiy_validity_time_period_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyValidityTimeDisplay());
	}

	@When("^I click on Free WhatsApp button$")
	public void i_click_on_free_whatsapp_button() throws Throwable {
		android_dashboardPO.tapFreeWhatsappbutton();
	}

	@When("^I scroll the screen upwards till specific location$")
	public void i_scroll_the_screen_upwards_till_specific_location() throws Throwable {
		android_dashboardPO.scrolltillElementFound();
	}

	@And("^I verify Recharge account and Try Again button hidden if app status is ON$")
	public void i_verify_recharge_account_and_try_again_button_hidden_if_app_status_is_on() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRechargeAccountButtonDisplay());
	}

	@And("^I verify by tap on recharge account button I am on recharge screen$")
	public void i_verify_by_tap_on_recharge_account_button_i_am_on_recharge_screen() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRechargeScreenDisplay());
	}

	@When("^I tap back button of recharge screen if app offer is expired$")
	public void i_tap_back_button_of_recharge_screen_if_app_offer_is_expired() throws Throwable {
		android_dashboardPO.tapBackButtonRechargeScreen();
	}

	@When("^I click on Free FRIENDI Calls button$")
	public void i_click_on_free_friendi_calls_button() throws Throwable {
		android_dashboardPO.tapFreeFriendiCallsbutton();
	}

	@And("^I verify remaining Minutes display$")
	public void i_verify_remaining_minutes_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRemaingMinutesDisplay());
	}

	@When("^I select any local call pack$")
	public void i_select_any_local_call_pack() throws Throwable {
		android_dashboardPO.selectLocalCallPack();
	}

	@Then("^I verify local call extended view display$")
	public void i_verify_local_call_extended_view_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.localCallExtendedViewDisplay());
	}

	@And("^I verify pi chart and remaining mins display$")
	public void i_verify_pi_chart_and_remaining_mins_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPIchartRemainingMinsDispaly());
	}

	@And("^I verify unlimited icon display if pack is unlimited$")
	public void i_verify_unlimited_icon_display_if_pack_is_unlimited() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyUnlimitedIconDispaly());
	}

	@And("^I verfiy auto renewal toggel should not display for local call$")
	public void i_verfiy_auto_renewal_toggel_should_not_display_for_local_call() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutorenewaltoggelDispaly());
	}

	@And("^I verfiy price value is displayed$")
	public void i_verfiy_price_value_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPriceDispaly());
	}

	@And("^I verfiy Price text is displayed$")
	public void i_verfiy_price_text_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPriceTextDisplay());
	}

	@When("^I select \"([^\"]*)\" call pack$")
	public void i_select_something_call_pack(String validity) throws Throwable {
		android_dashboardPO.selectCallPack(validity);
	}

	@Then("^I verify international call extended view display$")
	public void i_verify_international_call_extended_view_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.internationalCallExtendedViewDisplay());
	}

	@And("^I verify Included dropdown is Displayed$")
	public void i_verify_included_dropdown_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyIncludedDropdownDisplay());
	}

	@When("^I click on Included dropdown button$")
	public void i_click_on_included_dropdown_button() throws Throwable {
		android_dashboardPO.taponIncludedDropdown();
	}

	@And("^I verfiy price value is displayed for international calls$")
	public void i_verfiy_price_value_is_displayed_for_international_calls() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPriceDispalyforInternationalcalls());
	}

	@And("^I verfiy auto renewal toggel should not display for international call$")
	public void i_verfiy_auto_renewal_toggel_should_not_display_for_international_call() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutorenewaltoggelDispaly());
	}

	@And("^I verify Toggle button display$")
	public void i_verify_toggle_button_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifytogglebuttonnDispaly());
	}

	@And("^I verify toggle button deselected$")
	public void i_verify_toggle_button_deselected() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifytogglebuttonnDeselected());
	}

	@Then("^I verify all payG rates display$")
	public void i_verify_all_payg_rates_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAllPayGRatesDisplay());
	}

	@Then("^I verify all payG rates collapsed$")
	public void i_verify_all_payg_rates_collapsed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifypayGRatesCollapsed());
	}

	@When("^I scroll the screen upwards till PayG rates found$")
	public void i_scroll_the_screen_upwards_till_payg_rates_found() throws Throwable {
		android_dashboardPO.scrolltillPaygRatesFound();
	}

	@When("^I tap on Free Whatsapp call button$")
	public void i_tap_on_free_whatsapp_call_button() throws Throwable {
		android_dashboardPO.taponFreewhatsappbutton();
	}

	@Then("^I verify Remaining period and validity until are displayed empty$")
	public void i_verify_remaining_period_and_validity_until_are_displayed_empty() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyremaingandvaliduntillempty());
	}

	@And("^I verify Recharge account and Try Again button enabled if app status is OFF$")
	public void i_verify_recharge_account_and_try_again_button_enabled_if_app_status_is_off() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRechargeAccountButtonDisplay());
	}

	@When("^I tap Free Friendi call button$")
	public void i_tap_free_friendi_call_button() throws Throwable {
		android_dashboardPO.taponFreeOnNetbutton();
	}

	@When("^I select International call pack$")
	public void i_select_international_call_pack() throws Throwable {
		android_dashboardPO.selectInternationalPack();
	}

	@Then("^I verify Buy again Butoon is hidden$")
	public void i_verify_buy_again_butoon_is_hidden() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyBuyAgainButtonDisplay());
	}

	@When("^I select local call pack$")
	public void i_select_local_call_pack() throws Throwable {
		android_dashboardPO.selectlocalPack();
	}

	@When("^I select data pack$")
	public void i_select_data_pack() throws Throwable {
		android_dashboardPO.selectdataPack();
	}

	@And("^I verify Auto renewal toggle is displayed for the Freedom Data Plan or Freedom Mix Plan$")
	public void i_verify_auto_renewal_toggle_is_displayed_for_the_freedom_data_plan_or_freedom_mix_plan()
			throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutoRenewalToggleDisplay());
	}

	@And("^I verify Auto renewal toggle button is not displayed for the Saver Data Plan or Saver Mix Plan$")
	public void i_verify_auto_renewal_toggle_button_is_not_displayed_for_the_saver_data_plan_or_saver_mix_plan()
			throws Throwable {
		Assert.assertFalse(android_dashboardPO.verifyAutoRenewalToggleNotDisplay());
	}

	@And("^I verify Auto renewal toggle button is selected$")
	public void i_verify_auto_renewal_toggle_button_is_selected() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutoRenewalToggleSelected());
	}

	@When("^I tap on toggle button present on screen$")
	public void i_tap_on_toggle_button_present_on_screen() throws Throwable {
		android_dashboardPO.clickOnTogglebutton();
	}

	@When("^I click on CONFIRM button$")
	public void i_click_on_confirm_button() throws Throwable {
		android_dashboardPO.clickOnConfirmbutton();
	}

	@Then("^I verify Auto renewal toggle button is deselected$")
	public void i_verify_auto_renewal_toggle_button_is_deselected() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyAutoRenewalToggleDeselected());
	}

	@And("^I verify mixed pack Local calls is displayed$")
	public void i_verify_mixed_pack_local_calls_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyLocalCallPackDisplayUndermixPlan());
	}

	@When("^I tap on pack under mix plan$")
	public void i_tap_on_pack_under_mix_plan() throws Throwable {
		android_dashboardPO.taponPackunderMixplan();
	}

	@Then("^I verify Local calls extended view display$")
	public void i_verify_local_calls_extended_view_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyLocalCallExtendedViewDisplay());
	}

	@And("^I verify mixed pack Data is displayed$")
	public void i_verify_mixed_pack_data_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyDataPackDisplayUndermixPlan());
	}

	@Then("^I verify Data extended view display$")
	public void i_verify_data_extended_view_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyDataExtendedViewDisplay());
	}

	@And("^I verify cross button display in home notification$")
	public void i_verify_cross_button_display_in_home_notification() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifycrossbuttonunderhsnotificationDisplay());
	}

	@When("^I tap on cross button under home notification$")
	public void i_tap_on_cross_button_under_home_notification() throws Throwable {
		android_dashboardPO.tapcrossButtonUnderHsnotification();
	}

	@Then("^I verify all home screen notifications cleared$")
	public void i_verify_all_home_screen_notifications_cleared() throws Throwable {
		Assert.assertFalse(android_dashboardPO.verifyHSnotificationCleared());
	}

	@And("^I verify there is a character validation in voucher field$")
	public void i_verify_there_is_a_character_validation_in_voucher_field() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyTextfieldHasCharacterValidation());
	}

	@And("^I verify there is a character limit in voucher field$")
	public void i_verify_there_is_a_character_limit_in_voucher_field() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyCharacterLimit());
	}

	@And("^I verify there is a character validation in amount field$")
	public void i_verify_there_is_a_character_validation_in_amount_field() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyTopUpHasCharacterValidation());
	}

	@When("^I click on Radio button to select payment card$")
	public void i_click_on_radio_button_to_select_payment_card() throws Throwable {
		android_dashboardPO.selectRadioButton();
	}

	@And("^I verify RECHARGE button is Displayed$")
	public void i_verify_recharge_button_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRechargeButtonDisplay());
	}

	@And("^I verify CANCEL button is Displayed$")
	public void i_verify_cancel_button_is_displayed() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyCancelButtonDisplay());
	}

	@When("^I click on CANCEL button$")
	public void i_click_on_cancel_button() throws Throwable {
		android_dashboardPO.tapCancelButton();
	}

	@When("^I click on RECHARGE button$")
	public void i_click_on_recharge_button() throws Throwable {
		android_dashboardPO.taponRechargebutton();
	}

	@Then("^I verify I am on Recharge screen$")
	public void i_verify_i_am_on_recharge_screen() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyRechargeScreen());
	}

	@When("^I tap back button present on screen$")
	public void i_tap_back_button_present_on_screen() throws Throwable {
		android_dashboardPO.tapBackButtonRechargeScreen();
	}

	@When("^I enter invalid details in fields$")
	public void i_enter_invalid_details_in_fields() throws Throwable {
		android_dashboardPO.enterInvalidDetails();
	}

	@Then("^I verify add button is disable$")
	public void i_verify_add_button_is_disable() throws Throwable {
		Assert.assertFalse(android_dashboardPO.addButtonDisable());
	}

	@And("^I verify valid until display$")
	public void i_verify_valid_until_display() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyValidUntilDisplay());
	}

	@And("^I verify price should not display for promo pack$")
	public void i_verify_price_should_not_display_for_promo_pack() throws Throwable {
		Assert.assertTrue(android_dashboardPO.verifyPriceNotDisplay());
	}

	@When("^I enter valid details in fields$")
	public void i_enter_valid_details_in_fields() throws Throwable {
		android_dashboardPO.enterValidDetails();
	}

	@Then("^I verify add button is Enable$")
	public void i_verify_add_button_is_ensable() throws Throwable {
		Assert.assertTrue(android_dashboardPO.addButtonDisable());
	}
}
