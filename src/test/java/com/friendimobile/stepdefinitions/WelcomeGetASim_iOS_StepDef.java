package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class WelcomeGetASim_iOS_StepDef extends  StepDefinitionInit{

	
	@And("^I verify user is on dashboard$")
    public void i_verify_user_is_on_dashboard() throws Throwable {
    Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyDashboardScreen());    
    }
	@And("^I verify user is able to enter \"([^\"]*)\" this name$")
    public void i_verify_user_is_able_to_enter_something_this_name(String strArg1) throws Throwable {
		Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyName(strArg1));  
    }
	@And("^I verify user is able to enter \"([^\"]*)\" this email$")
    public void i_verify_user_is_able_to_enter_something_this_email(String strArg1) throws Throwable {
		Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyEmail(strArg1)); 
    }
	@And("^I verify user is able to enter \"([^\"]*)\" this number$")
    public void i_verify_user_is_able_to_enter_something_this_number(String strArg1) throws Throwable {
		Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyNumber(strArg1));
    }
	@And("^I verify user is able to enter \"([^\"]*)\" this location$")
    public void i_verify_user_is_able_to_enter_something_this_location(String strArg1) throws Throwable {
		Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyLocation(strArg1));
    }
	@And("^I verify user can enter \"([^\"]*)\" this country name$")
    public void i_verify_user_can_enter_something_this_country_name(String strArg1) throws Throwable {
		Assert.assertTrue(WelcomeGetASim_iOS_PO.verifyCountryName(strArg1));
    }
	
}

