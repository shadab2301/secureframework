package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_MoreAccountSettingStepDef extends StepDefinitionInit {
	@When("^I close Welcome popup$")
	public void i_close_welcome_popup() throws Throwable {
		android_MoreAccountsettingPO.closeWelcomePopUp();
	}

	@When("^I click on \"([^\"]*)\" button$")
	public void i_click_on_something_button(String button) throws Throwable {
		android_MoreAccountsettingPO.tapOnButton(button);
	}

	@Then("^I verify \"([^\"]*)\" button is Displayed$")
	public void i_verify_something_button_is_displayed(String button) throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyButton(button));
	}

	@When("^I tap on toggle button$")
	public void i_tap_on_toggle_button() throws Throwable {
		android_MoreAccountsettingPO.clickOnToggle();
	}

	@Then("^I verfiy \"([^\"]*)\" text is displayed$")
	public void i_verfiy_something_text_is_displayed(String text) throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyText(text));
	}

	@Then("^I verify the Password field is displayed$")
	public void i_verify_the_password_field_is_displayed() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyPasswordField());
	}

	@And("^I verify i am able to enter \"([^\"]*)\" Text in Password field$")
	public void i_verify_i_am_able_to_enter_something_text_in_password_field(String input) throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyUserAbleToEnterPass(input));
	}

	@Then("^I verify mask button mask and unmask the password$")
	public void i_verify_mask_button_mask_and_unmask_the_password() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyMaskButtonMaskAndUnmask());
	}

	@When("^I entered valid password \"([^\"]*)\" in Password field$")
	public void i_entered_valid_password_something_in_password_field(String validpass) throws Throwable {
		android_MoreAccountsettingPO.enterValidPassword(validpass);
	}

	@Then("^I verify confirm button enabled$")
	public void i_verify_confirm_button_enabled() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyConfirmButtonEnabled());
	}

	@Then("^I verify a pop up opens for theme$")
	public void i_verify_a_pop_up_opens_for_theme() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifypopupDisplayed());
	}

	@When("^I click on Cancel button over system popup$")
	public void i_click_on_cancel_button_over_system_popup() throws Throwable {
		android_MoreAccountsettingPO.tapCanclebutton();
	}

	@Then("^I verify Selected value in popup is displayed in drop down$")
	public void i_verify_selected_value_in_popup_is_displayed_in_drop_down() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyingSelectedValueUnderDropdown());
	}

	@Then("^I verify I am on Libraries We Use screen$")
	public void i_verify_i_am_on_libraries_we_use_screen() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyingLibrariesWeUseScreen());
	}

	@When("^I tap on Back button$")
	public void i_tap_on_back_button() throws Throwable {
		android_MoreAccountsettingPO.tapBackButton();
	}

	@And("^I verify App version is displayed$")
	public void i_verify_app_version_is_displayed() throws Throwable {
		Assert.assertTrue(android_MoreAccountsettingPO.verifyingAppVersion());
	}
}
