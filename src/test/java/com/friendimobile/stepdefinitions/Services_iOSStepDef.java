package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import com.org.utils.PropFileHandler;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Services_iOSStepDef extends StepDefinitionInit {

	@When("^I click on plus button$")
	public void i_click_on_plus_button() throws Throwable {
		Services_iOS_PO.clickPlusBtn();
	}

	@And("^I verify user can enter input in ro$")
	public void i_verify_user_can_enter_input_in_ro() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyRO());
	}

	@Then("^I verify user can enter \"([^\"]*)\" this data in the tap in number field$")
	public void i_verify_user_can_enter_something_this_data_in_the_tap_in_number_field(String strArg1)
			throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyTapInNumberField(strArg1));
	}

	@And("^I verify the help text in the field in disappeared$")
	public void i_verify_the_help_text_in_the_field_in_disappeared() throws Throwable {
		Assert.assertFalse(Services_iOS_PO.verifyHelpTextDisapppeared());
	}

	@Then("^I verify the tranfer button is disabled$")
	public void i_verify_the_tranfer_button_is_disabled() throws Throwable {
		Assert.assertFalse(Services_iOS_PO.verifyTranferBtnDisabled());
	}

	@Then("^I verify contacts button is displayed$")
	public void i_verify_contacts_button_is_displayed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyContactsBtn());
	}

	@When("^I click on contacts button$")
	public void i_click_on_contacts_button() throws Throwable {
		Services_iOS_PO.clickOnContacts();
	}

	@When("^I enter this \"([^\"]*)\" value in ro field$")
	public void i_enter_this_something_value_in_ro_field(String strArg1) throws Throwable {
		Services_iOS_PO.enterValueInField(strArg1);
	}

	@Then("^I verify the tranfer button is enabled$")
	public void i_verify_the_tranfer_button_is_enabled() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyTranferBtnDisabled());
	}

	@And("^I verify total amount dropdown is displayed$")
	public void i_verify_total_amount_dropdown_is_displayed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyTotalAmountDropDown());
	}

	@When("^I click on total amount dropdown$")
	public void i_click_on_total_amount_dropdown() throws Throwable {
		Services_iOS_PO.clickOnDropDown();
	}

	@And("^I verify the \"([^\"]*)\" minutes and ro \"([^\"]*)\" price is displayed$")
	public void i_verify_the_something_minutes_and_ro_something_price_is_displayed(String arg1, String arg2)
			throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyMinsAndPrices(arg1, arg2));
	}

	@And("^I verify RO value display over screen$")
	public void i_verify_ro_value_display_over_screen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyRoValueDisplay());
	}

	@And("^I verify Transfer credit to text present on screen$")
	public void i_verify_transfer_credit_to_text_present_on_screen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyTransferCreditTextDisplay());
	}

	@And("^I verify the help text in the field in disappeared from field$")
	public void i_verify_the_help_text_in_the_field_in_disappeared_from_field() throws Throwable {
		Assert.assertFalse(Services_iOS_PO.verifyTransferCreditTextDisplay());
	}

	@And("^I verify user can enter \"([^\"]*)\" this data in the field$")
	public void i_verify_user_can_enter_something_this_data_in_the_field(String strArg1) throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyTransferCreditToField(strArg1));
	}

	@When("^I enter number \"([^\"]*)\" in the field$")
	public void i_enter_number_something_in_the_field(String strArg1) throws Throwable {
		Services_iOS_PO.enterValidNumber(strArg1);
	}

	@Then("^I verify continue button get enabled$")
	public void i_verify_continue_button_get_enabled() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyContinueButtonEnabled());
	}

	@When("^I click on EDIT button$")
	public void i_click_on_edit_button() throws Throwable {
		Services_iOS_PO.tapOnEditButton();
	}

	@When("^I click on drop down present on screen$")
	public void i_click_on_drop_down_present_on_screen() throws Throwable {
		Services_iOS_PO.tapOnDropdown();
	}

	@Then("^I verify dropdown list with predefind values diaplayed$")
	public void i_verify_dropdown_list_with_predefind_values_diaplayed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyPredefindValueDisplay());
	}

	@When("^I click on Done button$")
	public void i_click_on_done_button() throws Throwable {
		Services_iOS_PO.clickDoneButton();
	}

	@When("^I scroll Ro value$")
	public void i_select_ro_value() throws Throwable {
		Services_iOS_PO.scrollROvalue();
	}

	@Then("^I verify OTP field display$")
	public void i_verify_otp_field_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyOTPfieldDisplay());
	}

	@And("^I verify able to input data in OTP field$")
	public void i_verify_able_to_input_data_in_otp_field() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyOTPfield());
	}

	@When("^I enter invalid OTP in OTP field$")
	public void i_enter_invalid_otp_in_otp_field() throws Throwable {
		Services_iOS_PO.enterInvalidOTP();
	}

	@Then("^I verify all countries list displayed$")
	public void i_verify_the_list_of_countries_is_displayed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyListOfCountriesDisplay());
	}

	@When("^I enter \"([^\"]*)\" country name in enter country field present$")
	public void i_enter_something_country_name_in_enter_country_field_present(String countryname) throws Throwable {
		Services_iOS_PO.enterCountryName(countryname);
	}

	@Then("^I verify enterd country list display onscreen$")
	public void i_verify_enterd_country_list_display_onscreen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyEnteredCountryListDisplay());
	}

	@When("^I select country from list$")
	public void i_select_country_from_list() throws Throwable {
		Services_iOS_PO.selectCountry();
	}

	@Then("^I verify country logo display onscreen$")
	public void i_verify_country_logo_display_onscreen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyCountryLogoDisplay());
	}

	@And("^I verify payG rates for SMS and calls disolay$")
	public void i_verify_payG_rates_for_sms_and_calls_disolayn() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyPaygRatesDisplay());
	}

	@When("^I scroll the offer$")
	public void i_scroll_the_offer() throws Throwable {
		Services_iOS_PO.scrollTheOffer();
	}

	@When("^I click on list of eligible countries button$")
	public void i_click_on_list_of_eligible_countries_button() throws Throwable {
		Services_iOS_PO.eligiblecountriesButton();
	}

	@And("^I verify all included countried displayed$")
	public void i_verify_all_included_countried_displayed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyAllIncludedCountriesDisplay());
	}

	@Then("^I verify enterd country present in the included list$")
	public void i_verify_enterd_country_present_in_the_included_list() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyEnteredCountryDisplay());
	}

	@Then("^I verify enterd country not an eligile country$")
	public void i_verify_enterd_country_not_an_eligile_country() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyEnteredCountryDisplay());
	}

	@When("^I click on cross button$")
	public void i_click_on_cross_button() throws Throwable {
		Services_iOS_PO.tapCrossButton();
	}

	@Then("^I verify Search field cleared$")
	public void i_verify_search_field_cleared() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyEnteredCountrySearchFieldCleared());
	}

	@And("^I verify CLOSE button display$")
	public void i_verify_close_button_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifycloseButtondisplay());
	}

	@When("^I tap CLOSE button$")
	public void i_tap_CLOSE_button() throws Throwable {
		Services_iOS_PO.tapCloseButton();
	}

	@Then("^I verify Other services Screen appears$")
	public void i_verify_other_services_screen_appears() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyOtherServicesScreen());
	}

	@And("^I verify You cannot transfer data that already transferred to you text display$")
	public void i_verify_you_cannot_transfer_data_that_already_transferred_to_you_text_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyText());
	}

	@When("^I enter mobile number in field$")
	public void i_enter_mobile_number() throws Throwable {
		String id = PropFileHandler.readProperty("MobileNumber1");
		loginPO.enterMobileNumber(id);
	}

	@Then("^I verify remaining balance in wallet is display$")
	public void i_verify_remaining_balance_in_wallet_is_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyremainingBalance());
	}

	@When("^I scroll the data carousel right present on screen$")
	public void i_scroll_the_data_carousel_right_present_on_screen() throws Throwable {
		Services_iOS_PO.scrolldatacarouselRight();
	}

	@When("^I scroll the data carousel left present on screen$")
	public void i_scroll_the_data_carousel_left_present_on_screen() throws Throwable {
		Services_iOS_PO.scrolldatacarouselLeft();
	}

	@And("^I verify if OTP field has character validation$")
	public void i_verify_if_otp_field_has_character_validation() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyOTPfieldHasCharacterValidation());
	}

	@When("^I enter valid OTP in OTP field$")
	public void i_enter_valid_otp_in_otp_field() throws Throwable {
		Services_iOS_PO.enterValidOTP();
	}

	@Then("^I verify previously used number display on screen$")
	public void i_verify_previously_used_number_display_on_screen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyPreviousNumberDisplay());
	}

	@Then("^I verify Recently used number list collapsed$")
	public void i_verify_recently_used_number_list_collapsed() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyPreviousNumberDisplay());
	}

	@When("^I tap on Change button to change data$")
	public void i_tap_on_change_button_to_change_data() throws Throwable {
		Services_iOS_PO.tapOnDataChangeButton();
	}

	@Then("^I verify Add your friend number screen display")
	public void i_verify_add_your_friend_number_screen_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyAddYourFriendsNumberScreen());
	}

	@Then("^I verify How much data do you want to share screen display")
	public void i_verify_how_much_data_do_you_want_to_share_screen_display() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyHowMuchdataYouShareScreen());
	}

	@Then("^I verify contacts button is displayed on data transfer screen$")
	public void i_verify_contacts_button_is_displayed_on_data_transfer_screen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyContactsButton());
	}

	@When("^I click on contacts button present on screen")
	public void i_click_on_contacts_button_present_on_screen() throws Throwable {
		Services_iOS_PO.tapOnContactButton();
	}

	@When("^I tap on Change button to change number$")
	public void i_tap_on_change_button_to_change_number() throws Throwable {
		Services_iOS_PO.tapOnNumberChangeButton();
	}

	@When("^I click on Recently used dropdown button$")
	public void i_click_on_recently_used_dropdown_button() throws Throwable {
		Services_iOS_PO.tapOnDropDownButton();
	}

	@Then("^I verify Confirmation popup is present on screen$")
	public void i_verify_confirmation_popup_is_present_on_screen() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyConfirmationPopupDisplay());
	}

	@Then("^I verify Recharge Screen appears$")
	public void i_verify_recharge_screen_appears() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyRechargeScreenDisplay());
	}

	@When("^I click on CANCEL button present on screen$")
	public void i_click_on_cancel_button_present_on_screen() throws Throwable {
		Services_iOS_PO.tapOnCancelButton();
	}

	@When("^I click on TRANSFER button present on screen$")
	public void i_click_on_transfer_button_present_on_screen() throws Throwable {
		Services_iOS_PO.tapOnTransferButton();
	}

	@When("^I click on RECHARGE button present on screen$")
	public void i_click_on_recharge_button_present_on_screen() throws Throwable {
		Services_iOS_PO.tapOnRechargeButton();
	}

	@Then("^I verify Maximum amount info is present on screen$")
	public void i_verify_maximum_amount_info_is_present_on_screens() throws Throwable {
		Assert.assertTrue(Services_iOS_PO.verifyMaximumAmountInfoDisplay());
	}

}
