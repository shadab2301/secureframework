package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_InternationalCreditTransferStepDef extends StepDefinitionInit {

	@When("^I click on International Credit Transfer button$")
	public void i_click_on_international_credit_transfer_button() throws Throwable {
		android_internationalcreditTransferPO.tapOnInternationalCreditTransferButton();
	}

	@When("^I tap on contact button present on screen$")
	public void i_tap_on_contact_button_present_on_screen() throws Throwable {
		android_internationalcreditTransferPO.tapOnContactButton();
	}

	@When("^I tap on Top up Button present$")
	public void i_tap_on_top_up_button_present() throws Throwable {
		android_internationalcreditTransferPO.tapOnTopUpbutton();
	}

	@When("^I enter number \"([^\"]*)\" into number field$")
	public void i_enter__number_something_into_number_field(String Mobilenumber) throws Throwable {
		android_internationalcreditTransferPO.enterMobileNumber(Mobilenumber);
	}

	@Then("^I verify Continue button enabled$")
	public void i_verify_continue_button_enabled() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyContinueButtonEnable());
	}

	@When("^I choose RO value$")
	public void i_choose_ro_value() throws Throwable {
		android_internationalcreditTransferPO.selectROvalueFromDropdown();
	}

	@And("^I verify current wallet balance display$")
	public void i_verify_current_wallet_balance_display() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyCurrentWallateBalanceDisplay());
	}

	@Then("^I verfiy Mobile number is displayed$")
	public void i_verfiy_mobile_number_is_displayed() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyMobileNoDisplay());
	}

	@Then("^I verify default value display$")
	public void i_verify_default_value_display() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyDefaultValueDisplay());
	}

	@Then("^I verify dropdown closes$")
	public void i_verify_dropdown_closes() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyDropdownListCloses());
	}

	@And("^I verfiy remaining balance text is displayed$")
	public void i_verfiy_remaining_balance_text_is_displayed() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyRemainingBalanceTextDisplay());
	}

	@When("^I select RO value$")
	public void i_select_ro_value() throws Throwable {
		android_internationalcreditTransferPO.chooseROValue();
	}

	@Then("^I verify insufficient balance popup display$")
	public void i_verify_insufficient_balance_popup_display() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyRechargePopUpDisplay());
	}

	@When("^I tap back button present screen$")
	public void i_tap_back_button_present_screen() throws Throwable {
		android_internationalcreditTransferPO.tapInternationalScreenBackButton();
	}

	@Then("^I verfiy Maximum amount you can transfer in a month is RO 30 text is displayed$")
	public void i_verfiy_maximum_amount_you_can_transfer_in_a_month_is_RO_30_text_is_displayed() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyText());
	}

	@When("^I click on transfer button$")
	public void i_click_on_transfer_button() throws Throwable {
		android_internationalcreditTransferPO.tapTransferButton();
	}

	@Then("^I verify i am on previous screen$")
	public void i_verify_i_am_on_previous_screen() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyPreviousScreenDisplay());
	}

	@And("^I verify if OTP field has format validation$")
	public void i_verify_if_otp_field_has_format_validation() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyOTPfieldHasformatValidation());
	}

	@When("^I enter invalid OTP in field$")
	public void i_enter_invalid_OTP_in_field() throws Throwable {
		android_internationalcreditTransferPO.enterInvalidOTP();
	}

	@And("^I verify transfer credit to is having phone number validaton$")
	public void i_verify_transfer_credit_to_is_having_phone_number_validaton() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyPhoneNumberFieldHasformatValidation());
	}

	@And("^I verify transfer credit to having minimum and maximum char limit$")
	public void i_verify_transfer_credit_to_having_minimum_and_maximum_char_limit() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifyPhoneNumberFieldHasCharlimit());
	}
	
	@Then("^I verify send again button display$")
	public void i_verify_send_again_button_display() throws Throwable {
		Assert.assertTrue(android_internationalcreditTransferPO.verifySendAgainButton());
	}
}
