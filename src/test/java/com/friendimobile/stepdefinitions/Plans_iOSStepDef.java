package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Plans_iOSStepDef extends StepDefinitionInit {
	@Then("^I verify Plans Screen Display$")
	public void i_verify_plans_screen_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyPlansSecrrenDisplay());
	}

	@And("^I verify Service Subtype Slider Coach Mark displayed$")
	public void i_verify_service_subtype_slider_coach_mark_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.coachMarkDisplayed());
	}

	@Then("^I verify Pack coach mark display$")
	public void i_verify_pack_coach_mark_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.coachMarkDisplayed());
	}

	@Then("^I verify Pack coach mark disappear$")
	public void i_verify_pack_coach_mark_disappear() throws Throwable {
		Assert.assertFalse(plansiOSPO.coachMarkDisplayed());
	}

	@When("^I tap one of the offers$")
	public void i_tap_one_of_the_offers() throws Throwable {
		plansiOSPO.tapOnOffer();
	}

	@Then("^I verify Plan Details Coach Mark display$")
	public void i_verify_plan_details_coach_mark_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.coachMarkDisplayed());
	}

	@Then("^I verify Plan Details coach mark disappear$")
	public void i_verify_plan_details_coach_mark_disappear() throws Throwable {
		Assert.assertFalse(plansiOSPO.coachMarkDisplayed());
	}

	@And("^I verify Data tab selected$")
	public void i_verify_data_tab_selected() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDataTabSelected());
	}

	@When("^I tap for guide popup in plans screen$")
	public void i_tap_for_guide_popup_in_plans_screen() throws Throwable {
		plansiOSPO.tapOnScreen();
	}

	@Then("^I verify Buy Plan is present on screen$")
	public void i_verify_buy_plan_button_is_present_on_screen() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyBuyPlanButtonDisplay());
	}

	@When("^I click on Buy Plan button$")
	public void i_click_on_buy_plan_button() throws Throwable {
		plansiOSPO.tapOnBuyPlanButton();
	}

	@When("^I click on Data button from Buy plan$")
	public void i_click_on_data_button_from_buy_plan() throws Throwable {
		plansiOSPO.tapOnDataButton();
	}

	@When("^I click on Local Calls button from Buy plan$")
	public void i_click_on_local_calls_button_from_buy_plan() throws Throwable {
		plansiOSPO.tapOnLocalCallsButton();
	}

	@When("^I click on International Calls button from Buy plan$")
	public void i_click_on_international_calls_button_from_buy_plan() throws Throwable {
		plansiOSPO.tapOnInternationalCallButton();
	}

	@When("^I click on Mix button from Buy plan$")
	public void i_click_on_mix_button_from_buy_plan() throws Throwable {
		plansiOSPO.tapOnMixButton();
	}

	@And("^I verify Minutes tab selected$")
	public void i_verify_mitutes_tab_selected() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMitutesTabSelected());
	}

	@And("^I verify Mix tab selected$")
	public void i_verify_nix_tab_selected() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMixTabSelected());
	}

	@And("^I verify plus button display$")
	public void i_verify_plus_button_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyPlusButtonDisplay());
	}

	@Then("^I verify Data tab displayed$")
	public void i_verify_data_tab_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDataTabDisplay());
	}

	@Then("^I verify Minutes tab displayed$")
	public void i_verify_mitutes_tab_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMitutesTabDisplay());
	}

	@Then("^I verify Mix tab displayed$")
	public void i_verify_nix_tab_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMixTabDisplay());
	}

	@And("^I verify all data packs display$")
	public void i_verify_all_data_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAllDataPacksDisplay());
	}

	@And("^I verify Freedom Calls packs display$")
	public void i_verify_all_calls_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyFreedomCallsPacksDisplay());
	}

	@And("^I verify all Mix packs display$")
	public void i_verify_all_mix_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAllMixPacksDisplay());
	}

	@When("^I tap on Minutes tab present$")
	public void i_tap_on_minutes_tab_present() throws Throwable {
		plansiOSPO.tapOnMinutestab();
	}

	@When("^I tap on Mix tab present$")
	public void i_tap_on_mix_tab_present() throws Throwable {
		plansiOSPO.tapOnMixtab();
	}

	@And("^I verify current balance display$")
	public void i_verify_current_balance_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyCurrentBalanceDisplay());
	}

	@And("^I verify that carousel is displayed$")
	public void i_verify_that_carousel_is_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyCarouselDisplay());
	}

	@When("^I scroll the carousel present on screen$")
	public void i_scroll_the_carousel_present_on_screen() throws Throwable {
		plansiOSPO.scrollCarousel();
	}

	@When("^I tap on Data tab present$")
	public void i_tap_on_data_tab_present() throws Throwable {
		plansiOSPO.tapOnDatatab();
	}

	@And("^I verify validity, allowance and price are displayed for all data packs$")
	public void i_verify_validity_allowance_and_price_are_displayed_for_all_data_packs() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAlldataPacks());
	}

	@And("^I verify validity, allowance and price are displayed for all call packs$")
	public void i_verify_validity_allowance_and_price_are_displayed_for_all_call_packs() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAllCallPacks());
	}

	@And("^I verify validity, allowance and price are displayed for all Mix packs$")
	public void i_verify_validity_allowance_and_price_are_displayed_for_all_mix_packs() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAllMixPacks());
	}

	@And("^I verify selected pack is display$")
	public void i_verify_selected_pack_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySelectedPackDisplay());
	}

	@And("^I verify MSISDN is displayed in the header$")
	public void I_verify_MSISDN_is_displayed_in_the_header() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMSISDNDisplay());
	}

	@And("^I verify Enter country search field display$")
	public void i_verify_enter_country_search_field_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyEnterCountrySeachField());
	}

	@And("^I verify Freedom Data packs display$")
	public void i_verify_freedom_data_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyFreedomDataPacksDisplay());
	}

	@When("^I tap on All Data Plans button$")
	public void i_tap_on_all_data_plans_button() throws Throwable {
		plansiOSPO.tapOnAllDataPlansButton();
	}

	@When("^I tap on Freedom Data button$")
	public void i_tap_on_freedom_data_button() throws Throwable {
		plansiOSPO.tapOnFreedomDataPlansButton();
	}

	@When("^I tap on Saver Data button$")
	public void i_tap_on_saver_data_button() throws Throwable {
		plansiOSPO.tapOnSaverDataPlansButton();
	}

	@When("^I tap on Saver Calls button$")
	public void i_tap_on_saver_calls_button() throws Throwable {
		plansiOSPO.tapOnSaverCallsButton();
	}

	@When("^I tap on International calls button$")
	public void i_tap_on_international_calls_button() throws Throwable {
		plansiOSPO.tapOnInternationalCallsButton();
	}

	@And("^I verify Saver Data packs display$")
	public void i_verify_saver_data_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverDataPacksDisplay());
	}

	@And("^I verify Saver Calls packs display$")
	public void i_verify_saver_calls_packs_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverCallsPacksDisplay());
	}

	@And("^I verify Freedom Data Plans text is display$")
	public void i_verify_freedom_data_plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyFreedomDataPlanTextDisplay());
	}

	@And("^I verify Saver Data Plans text is display$")
	public void i_verify_saver_data_plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverDataPlanTextDisplay());
	}

	@And("^I verify All Data Plans text is display$")
	public void i_verify_all_data_plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyAllDataPlanTextDisplay());
	}

	@And("^I verify Freedom Call Plans text is display$")
	public void i_verify_freedom_call_plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyFreedomCallPlanTextDisplay());
	}

	@And("^I verify Saver Call Plans text is display$")
	public void i_verify_Saver_call_plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverCallPlanTextDisplay());
	}

	@And("^I verify International Calls text is display$")
	public void i_verify_International_calls_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyInternationalCallsTextDisplay());
	}

	@And("^I verify Saver Mix Plans text is display$")
	public void i_verify_Saver_Mix_Plans_text_is_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverMixPlansTextDisplay());
	}

	@Then("^I verify International call packs should not be displayed$")
	public void i_verify_international_call_packs_should_not_be_displayed() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyinternationalCallacksDisplayed());
	}

	@And("^I verify \"([^\"]*)\" text is present on screen$")
	public void i_verify_something_text_is_present_on_screen(String Str) throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyMobileTextDisplayed(Str));
	}

	@Then("^I verify Select a country text is present on screen$")
	public void i_verify_select_a_country_text_is_present_on_screen() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySelectsCountryDisplayed());
	}

	@When("^I click on Select a country button$")
	public void i_click_on_select_a_country_button() throws Throwable {
		plansiOSPO.tapOnSSelectCountryButton();
	}

	@And("^I verify GCC text is present on screen$")
	public void i_verify_GCC_text_is_present_on_screen() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyGCCTextDisplayed());
	}

	@And("^I verify United Arab Emirates text is present on screen$")
	public void i_verify_United_Arab_Emirates_text_is_present_on_screen() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyUAETextDisplayed());
	}

	@Then("^I verify Saver Mix Plans text is display in Summary screen$")
	public void i_verify_saver_mix_plans_text_is_display_in_summary_screen() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyTextSaverMixPlanTextDisplayInSummaryScreen());
	}
	
	@When("^I click on Confirm button$")
	public void i_click_on_confirm_button() throws Throwable {
		plansiOSPO.tapOnConfirmButton();
	}

	@Then("^I verify puchased data pack display$")
	public void i_verify_puchased_data_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDatapack());
	}

	@Then("^I verify same data pack display twice$")
	public void i_verify_same_data_pack_display_twice() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySameDatapackTwice());
	}

	@When("^I select different offer$")
	public void i_select_different_offer() throws Throwable {
		plansiOSPO.tapOnDifferentOffer();
	}

	@Then("^I verify different data pack display$")
	public void i_verify_different_data_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDifferentDatapack());
	}

	@When("^I select local calls option from graph$")
	public void i_select_local_calls_option_from_graph() throws Throwable {
		plansiOSPO.selectLocalCallsFromGraph();
	}

	@Then("^I verify puchased Freedom localcall pack display$")
	public void i_verify_puchased_Freedom_localcall_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyFreedomLocalCallspack());
	}

	@Then("^I verify same Freedom localcalls pack display twice$")
	public void i_verify_same_Freedom_localcalls_pack_display_twice() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySameFreedomLocalCallpackTwice());
	}

	@Then("^I verify different Freedom localcalls pack display$")
	public void i_verify_different_freedom_localcalls_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDifferentFreedomLocalCallpack());
	}

	@Then("^I verify puchased Saver localcall pack display$")
	public void i_verify_puchased_saver_localcall_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySaverLocalCallspack());
	}

	@Then("^I verify same Saver localcalls pack display twice$")
	public void i_verify_same_saver_localcalls_pack_display_twice() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySameSaverLocalCallpackTwice());
	}

	@Then("^I verify different Saver localcalls pack display$")
	public void i_verify_different_saver_localcalls_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDifferentSaverLocalCallpack());
	}

	@When("^I select International calls option from graph$")
	public void i_select_international_calls_option_from_graph() throws Throwable {
		plansiOSPO.selectInternationalCallsFromGraph();
	}

	@Then("^I verify puchased International calls pack display$")
	public void i_verify_puchased_International_call_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyInternationalCallspack());
	}

	@Then("^I verify same International calls pack display twice$")
	public void i_verify_same_International_calls_pack_display_twice() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifySameInternationalCallspackTwice());
	}

	@Then("^I verify different International calls pack display$")
	public void i_verify_different_International_calls_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyDifferentInternationalCallspack());
	}
	
	@Then("^I verify International calls Zone pack display$")
	public void i_verify_International_calls_gone_pack_display() throws Throwable {
		Assert.assertTrue(plansiOSPO.verifyInternationalZonepack());
	}
}
