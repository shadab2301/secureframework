package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login_iOSStepDef extends StepDefinitionInit {

	@And("^I verify help icon is present$")
	public void i_verify_help_icon_is_present() throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyHelpIcon());
	}

	@When("^I click on help icon$")
	public void i_click_on_help_icon() throws Throwable {
		Login_iOS_PO.clickOnHelpIcon();
	}

	@When("^I enter invalid mobile number$")
	public void i_enter_invalid_mobile_number() throws Throwable {
		Login_iOS_PO.enterInvalidNo();
	}

	@Then("^I verify next button is disabled$")
	public void i_verify_next_button_is_disabled() throws Throwable {
		Assert.assertFalse(Login_iOS_PO.verifyNextBtn());
	}

	@Then("^I verify next button is enabled$")
	public void i_verify_next_button_is_enabled() throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyNextBtn());
	}

	@When("^I click \"([^\"]*)\" button$")
	public void i_click_something_button(String strArg1) throws Throwable {
		Login_iOS_PO.clickOnBtn();
	}

	@And("^I verify \"([^\"]*)\" this password is entered in the field$")
	public void i_enter_something_this_password_in_the_field(String strArg1) throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyPass(strArg1));
	}

	@When("^I scroll upward \"([^\"]*)\" times$")
    public void i_scroll_upward_something_times(String strArg1) throws Throwable {
		Login_iOS_PO.scrollUpward(strArg1);
	}

	@And("^I verify promo popup coach marks is displayed on screen$")
	public void i_verify_promo_popup_coach_marks_is_displayed_on_screen() throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyPromoCoachMark());
	}

	@And("^I verify Buy Plan Coach Mark is displayed on screen$")
	public void i_verify_buy_plan_coach_mark_is_displayed_on_screen() throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyBuyPlanCoachMark());
	}

	@When("^I click on claim button in the popup$")
	public void i_click_on_claim_button_in_the_popup() throws Throwable {
		Login_iOS_PO.clickClaimButton();
	}

	@And("^I verify success popup shows$")
	public void i_verify_success_popup_shows() throws Throwable {
		Login_iOS_PO.verifySuccessPopup();
	}

	@And("^I verify \"([^\"]*)\" this mobile number is entered in field$")
	public void i_enter_something_mobile_number_in_field(String mobileno) throws Throwable {
		Assert.assertTrue(Login_iOS_PO.verifyMobilenumber(mobileno));
	}

}
