package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_MoreHelpStepDef extends StepDefinitionInit {

	@When("^I tap on search icon$")
	public void i_tap_on_search_icon() throws Throwable {
		android_MoreHelpPO.clicksearchButton();
	}

	@Then("^I verify search field display$")
	public void i_verify_search_field_display() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifySearchFieldDisplay());
	}

	@Then("^I verify all categories displayed$")
	public void i_verify_all_categories_displayed() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyAllCategoryDisplayed());
	}

	@Then("^I verify all Subcategories displayed$")
	public void i_verify_all_subcategories_displayed() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyAllCategoryDisplayed());
	}

	@When("^I click \"([^\"]*)\" text present on the screen$")
	public void i_click_something_text_present_on_the_screen(String text) throws Throwable {
		android_MoreHelpPO.tapOnText(text);
	}

	@Then("^I verify Question Answser scren display$")
	public void i_verify_question_answser_scren_display() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyQuestionAnswerScreenDisplayed());
	}

	@When("^I tap on Cross button$")
	public void i_tap_on_cross_button() throws Throwable {
		android_MoreHelpPO.tapCrossButton();
	}

	@Then("^I verify i am able to enter \"([^\"]*)\" in search field$")
	public void i_verify_i_am_able_to_enter_something_in_search_field(String text) throws Throwable {
		android_MoreHelpPO.verifyAbleToEnter(text);
	}

	@When("^I enter invalid data in search field$")
	public void i_enter_invalid_data_in_search_field() throws Throwable {
		android_MoreHelpPO.enterInavalidData();
	}

	@Then("^I verify popup to dial the number display$")
	public void i_verify_popup_to_dial_the_number_display() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyDialPopupDisplay());
	}

	@Then("^I verify back button displayed$")
	public void i_verify_back_button_displayed() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyBackbuttonDisplay());
	}

	@When("^I enter valid data in search field$")
	public void i_enter_valid_data_in_search_field() throws Throwable {
		android_MoreHelpPO.enterValidDtaInSearchField();
	}

	@Then("^I verify result is gouped by categories displayed$")
	public void i_verify_result_is_gouped_by_categories_displayed() throws Throwable {
		Assert.assertTrue(android_MoreHelpPO.verifyResultDisplayed());
	}

	@When("^I select an item from search list$")
	public void i_select_an_item_from_search_list() throws Throwable {
		android_MoreHelpPO.selectFromSearchList();
	}

	@When("^I click on back arrow present on screen$")
	public void i_click_on_back_arrow_present_on_screen() throws Throwable {
		android_MoreHelpPO.tapBackButton();
	}

	@Then("^I verify Found your answer text disappears from screen$")
	public void i_verify_Found_your_answer_text_disappears_from_screen() throws Throwable {
		Assert.assertFalse(android_MoreHelpPO.verifyTextDisappears());
	}
}
