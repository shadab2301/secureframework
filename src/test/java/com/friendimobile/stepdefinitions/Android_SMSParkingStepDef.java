package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_SMSParkingStepDef extends StepDefinitionInit {

	@When("^I scroll the screen upwards$")
	public void i_scroll_the_screen_upwards() throws Throwable {
		android_SMSParkingPO.scrollScreenUpward();
	}

	@Then("^I verify Rates Minutes and price display$")
	public void i_verify_rates_minutes_and_price_display() throws Throwable {
		Assert.assertTrue(android_SMSParkingPO.verifyRatesMinutesPriceDisplay());
	}
}
