package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_LogInPromoStepDef extends StepDefinitionInit {

	@When("^I tap on claim button$")
	public void i_tap_on_claim_button() throws Throwable {
		loginpromoPO.tapOnClaimButton();
	}

	@Then("^I verify success with done button displayed$")
	public void i_verify_success_with_done_button_displayed() throws Throwable {
		Assert.assertTrue(loginpromoPO.claimSuccessDisplayed());
	}

	@Then("^I verify I am on dashboard$")
	public void i_verify_i_am_on_dashboard() throws Throwable {
		Assert.assertTrue(loginpromoPO.verifyingdashbaordscreendisplayed());
	}

	@And("^I verify promo data display$")
	public void i_verify_promo_data_display() throws Throwable {
		Assert.assertTrue(loginpromoPO.verifyingClaimedDataDisplay());
	}

	@When("^I tap on done button$")
	public void i_tap_on_done_button() throws Throwable {
		loginpromoPO.tapDoneButton();
	}
}
