package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import com.org.utils.PropFileHandler;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_WelcomeGetAsimStepDef extends StepDefinitionInit {
	@When("^I install the application$")
	public void i_install_the_application() throws Throwable {
		getAsimPO.verifyingApplication();
	}

	@Given("^I update the test rail case ID \"([^\"]*)\"$")
	public void i_update_the_test_rail_case_id_something(String testCaseID) throws Throwable {
		PropFileHandler.writeToFile("testCaseID", testCaseID);

	}

	@Then("^I verify I am on splash screen$")
	public void i_verify_i_am_on_splash_screen() throws Throwable {
		// dashBoardPO.clickOnOkayBtnInErrorMsg();
		Assert.assertTrue(getAsimPO.verifySplashScreen());
	}

	@Then("^I verify Welcome screen is displayed$")
	public void i_verify_welcome_screen_is_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyWelcomeScreen());
	}

	@Then("^I verify EXISTING CUSTOMER button displayed$")
	public void i_verify_existing_customer_button_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyExistingCustomerBtn());
	}

	@When("^I tap on EXISTING CUSTOMER button$")
	public void i_tap_on_existing_customer_button() throws Throwable {
		getAsimPO.tapOnExistingCustomerbtn();
	}

	@Then("^I verify SEND ME A SIM button displayed$")
	public void i_verify_send_me_a_sim_button_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifySendMeASIMbtn());
	}

	@When("^I tap on SEND ME A SIM button$")
	public void i_tap_on_send_me_a_sim_button() throws Throwable {
		getAsimPO.tapOnSendMeASIMbtn();
	}

	@Then("^I verify I am on \"([^\"]*)\" screen$")
	public void i_verify_i_am_on_something_screen(String title) throws Throwable {

		Thread.sleep(2000);
		getAsimPO.refreshTheScreen();
		getAsimPO.refreshTheScreen();
		Assert.assertTrue(getAsimPO.verifyPageTitle(title));
	}

	@When("^I refresh the page displayed$")
	public void i_refresh_the_page_displayed() throws Throwable {
		getAsimPO.refreshTheScreen();
		getAsimPO.refreshTheScreen();
	}

	@When("^I tap back button$")
	public void i_tap_back_button() throws Throwable {
		getAsimPO.tapOnBackButton();
	}

	@When("^I enter text in the full Name field$")
	public void i_enter_text_in_the_full_name_field() throws Throwable {
		getAsimPO.enterTextFullNameField();
	}

	@Then("^I verify i am able to enter text$")
	public void i_verify_i_am_able_to_enter_text() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyabletoEnteText());
	}

	@When("^I enter text in the email field$")
	public void i_enter_text_in_the_email_field() throws Throwable {
		getAsimPO.enterTextInEmailField();
	}

	@Then("^I verify i am able to enter email$")
	public void i_verify_i_am_able_to_enter_email() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyabletoEnteEmail());
	}

	@When("^I enter location in the location field$")
	public void i_enter_location_in_the_location_field() throws Throwable {
		getAsimPO.enterlocationInlocationField();
	}

	@Then("^I verify i am able to enter location$")
	public void i_verify_i_am_able_to_enter_location() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyabletoEntelocation());
	}

	@When("^I tap on dropdown Arrow$")
	public void i_tap_on_dropdown_arrow() throws Throwable {
		getAsimPO.tapOnDropdown();
	}

	@Then("^I verify X button displayed$")
	public void i_verify_x_button_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyXbtnDisplayed());
	}

	@When("^I tap on X button$")
	public void i_tap_on_x_button() throws Throwable {
		getAsimPO.tapOnXbtn();
	}

	@When("^I enter text in enter country field$")
	public void i_enter_text_in_enter_country_field() throws Throwable {
		getAsimPO.enterCountryName();
	}

	@Then("^I verify i am able to enter country name$")
	public void i_verify_i_am_able_to_enter_country_name() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyableToEnterCountry());
	}

	@Then("^I verify help button displayed$")
	public void i_verify_help_button_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyHelpButton());
	}

	@When("^I enter input in number field$")
	public void i_enter_input_in_number_field() throws Throwable {
		getAsimPO.EnterNumberinNumberField();
	}

	@Then("^I verify i am able to enter number$")
	public void i_verify_i_am_able_to_enter_number() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyabletoEnterNumber());
	}

	@When("^I tap on submit button$")
	public void i_tap_on_submit_button() throws Throwable {
		getAsimPO.tapOnSubmitBtn();
	}

	@Then("^I verify pop up Displayed$")
	public void i_verify_pop_up_displayed() throws Throwable {
		Assert.assertTrue(getAsimPO.verifypopupDisplayed());
	}

	@When("^I tap on ok button$")
	public void i_tap_on_ok_button() throws Throwable {
		getAsimPO.tapOnOKbutton();
	}

	@When("^I enter invalid text in the full Name field$")
	public void i_enter_invalid_text_in_the_full_name_field() throws Throwable {
		getAsimPO.enterInvalidName();
	}

	@When("^I enter invalid location in the location field$")
	public void i_enter_invalid_location_in_the_location_field() throws Throwable {
		getAsimPO.enterInvalidLocation();
	}

	@When("^I enter invalid input in number field$")
	public void i_enter_invalid_input_in_number_field() throws Throwable {
		getAsimPO.enterInvalidNumber();
	}

	@Then("^I verify submit button remain disabled$")
	public void i_verify_submit_button_remain_disabled() throws Throwable {
		Assert.assertTrue(getAsimPO.verifysubmitbuttonDisabled());
	}

	@When("^I select country$")
	public void i_select_country() throws Throwable {
		getAsimPO.selectCountry();
	}

	@Then("^I verify Selected country prefix code appears$")
	public void i_verify_selected_country_prefix_code_appears() throws Throwable {
		Assert.assertTrue(getAsimPO.verifyselectedCountryCodeAppears());
	}

	@When("^I enter country name in enter country field$")
	public void i_enter_country_name_in_enter_country_field() throws Throwable {
		getAsimPO.enterCountryName2();
	}
}
