package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_InternationalCallsStepDef extends StepDefinitionInit {
	@When("^I enter \"([^\"]*)\" mobile number$")
	public void i_enter_something_mobile_number(String mobileno) throws Throwable {
		android_InternationalCallsPO.enterMobileNumber(mobileno);
	}

	@Then("^I verify I am on help screen$")
	public void i_verify_i_am_on_help_screen() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyHelpScreenDisplay());
	}

	@When("^I tap on search icon present on screen$")
	public void i_tap_on_search_icon_present_on_screen() throws Throwable {
		android_InternationalCallsPO.taponSearchIcon();
	}

	@Then("^I verify country logo display$")
	public void i_verify_country_logo_display() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyCountryLogoDisplay());
	}

	@Then("^I verify country list screen display$")
	public void i_verify_country_list_screen_display() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyCountryListScreenDisplay());
	}

	@Then("^I verify supported countries display$")
	public void i_verify_supported_countries_display() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifysupportedCountriesDisplay());
	}

	@When("^I tap on device back button$")
	public void i_tap_on_device_back_button() throws Throwable {
		android_InternationalCallsPO.tapdeviceBackButton();
	}

	@Then("^I verify enterd country list display$")
	public void i_verify_enterd_country_list_display() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyEnteredCountryListDisplay());
	}

	@And("^I verify enterd country \"([^\"]*)\" is part of eligible countries$")
	public void i_verify_enterd_country_something_is_part_of_eligible_countries(String countryname) throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifySelectedCountryEligible(countryname));
	}

	@And("^I verify enterd country \"([^\"]*)\" is not a part of eligible countries$")
	public void i_verify_enterd_country_something_is_not_a_part_of_eligible_countries(String countryname)
			throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifySelectedCountryEligible(countryname));
	}

	@Then("^I verify dropdown expended$")
	public void i_verify_dropdown_expended() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyDropDownStatus());
	}

	@Then("^I verify dropdown collapsed$")
	public void i_verify_dropdown_collapsed() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyDropDownStatus());
	}

	@And("^I verify PAYG prices display for the selected country$")
	public void i_verify_payg_prices_display_for_the_selected_country() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyPayGPriceDisplay());
	}

	@When("^I select country without having available plan$")
	public void i_select_country_without_having_available_plan() throws Throwable {
		android_InternationalCallsPO.selectCountryWithoutHavingPlan();
	}

	@When("^I tap on international call Service type from Graph$")
	public void i_tap_on_international_call_service_type_from_Graph() throws Throwable {
		android_InternationalCallsPO.taoOninternationalcallServiceType();
	}

	@When("^I enter \"([^\"]*)\" country in enter country field$")
	public void i_enter_something_country_in_enter_country_field(String country) throws Throwable {
		android_InternationalCallsPO.enterContry(country);
	}

	@When("^I scroll the offers$")
	public void i_scroll_the_offers() throws Throwable {
		android_InternationalCallsPO.scrollOffers();
	}

	@Then("^I verfiy purchased pack display$")
	public void i_verfiy_purchased_pack_display() throws Throwable {
		Assert.assertTrue(android_InternationalCallsPO.verifyPurchasedPack());
	}

	@When("^I select the country$")
	public void i_select_the_country() throws Throwable {
		android_InternationalCallsPO.selectcountry();
	}

	@When("^I enter the country name in enter country field$")
	public void i_enter_the_country_name_in_enter_country_field() throws Throwable {
		android_InternationalCallsPO.enterCountryName3();
	}

	@When("^I tap on the back button$")
	public void i_tap__on_the_back_button() throws Throwable {
		android_InternationalCallsPO.tapOnBackButton();
	}

	@When("^I scroll the screen upward$")
	public void i_scroll_the_screen_upward() throws Throwable {
		android_InternationalCallsPO.scrollScreenUpwards();
	}

}
