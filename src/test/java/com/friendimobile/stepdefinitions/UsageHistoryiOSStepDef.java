package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UsageHistoryiOSStepDef extends StepDefinitionInit {
    @When("^I click on help button present$")
    public void i_click_on_help_button_present() throws Throwable {
    	usageHistoryPO.helpButton();
    }
    @Then("^I verify \"([^\"]*)\" text is present on screen using value$")
    public void i_verify_something_text_is_present_on_screen_using_value(String text) throws Throwable {
    	Assert.assertTrue(usageHistoryPO.verifyTextValue(text));
    }

   
}
