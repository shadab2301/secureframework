package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_DataTransferStepDef extends StepDefinitionInit {

	@Then("^I verify carousel display$")
	public void i_verify_carousel_display() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyCarauselDisplay());
	}

	@When("^I tap on recently used dropdown$")
	public void i_tap_on_recently_used_dropdown() throws Throwable {
		android_DataTransferPO.tapResentlyUseddropdown();
	}

	@Then("^I verify previously used number display$")
	public void i_verify_previously_used_number_display() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyPreviousNumberDisplay());
	}

	@Then("^I verify previously used number list collapsed$")
	public void i_verify_previously_used_number_list_collapsed() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyPreviousNumberDisplay());
	}

	@When("^I tap on data Change button$")
	public void i_tap_on_data_change_button() throws Throwable {
		android_DataTransferPO.tapOnChangeBtn();
	}

	@When("^I tap on number Change button$")
	public void i_tap_on_number_change_button() throws Throwable {
		android_DataTransferPO.tapOnChangebtn();
	}

	@When("^I scroll the data carousel$")
	public void i_scroll_the_data_carousel() throws Throwable {
		android_DataTransferPO.scrolldatacarousel();
	}

	@And("^I verify transfered data displayed on dashboard$")
	public void i_verify_transfered_data_displayed_on_dashboard() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyTransferedDataDisplay());
	}

	@Then("^I verify BUY DATA button is Displayed$")
	public void i_verify_BUY_DATA_button_is_Displayed() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyBuyDataButtonDisplay());
	}

	@And("^I verify You cannot transfer data that already transferred to you Text display$")
	public void i_verify_you_cannot_transfer_data_that_already_transferred_to_you_text_display() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyTextInshareDataScreen());
	}

	@And("^I verify close button display$")
	public void i_verify_close_button_display() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifycloseButtondisplay());
	}

	@When("^I tap close button$")
	public void i_tap_CLOSE_button() throws Throwable {
		android_DataTransferPO.tapCloseButton();
	}

	@Then("^I verify Other services Screen display$")
	public void i_verify_other_services_screen_appears() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyOtherServicesScreen());
	}

	@When("^I click on Learn more button$")
	public void i_click_on_learn_more_button() throws Throwable {
		android_DataTransferPO.tapLearnMoreButton();
	}

	@Then("^I verfiy Add your friends number text is displayed$")
	public void i_verfiy_add_your_friends_number_text_is_displayed() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyText());
	}

	@When("^I tap on received data over dashboard$")
	public void i_tap_on_received_data_over_dashboard() throws Throwable {
		android_DataTransferPO.tapReceivedData();
	}

	@When("^I tap on buy data button$")
	public void i_tap_on_buy_data_button() throws Throwable {
		android_DataTransferPO.tapBuyDatabutton();
	}
	
	@Then("^I verify Plans Screen display$")
    public void i_verify_plans_screen_display() throws Throwable {
		Assert.assertTrue(android_DataTransferPO.verifyPlansScreenDisplay());
    }
}
