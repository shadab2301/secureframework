package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_PlansStepDef extends StepDefinitionInit {

	@When("^I tap on Plans option$")
	public void i_tap_on_plans_option() throws Throwable {
		plansPO.tapOnPlans();
	}

	@Then("^I verify Service Subtype Slider Coach Mark is displayed$")
	public void i_verify_service_subtype_slider_coach_mark_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.subtypeSlideCoachMarkDisplayed());
	}

	@When("^I tap on screen$")
	public void i_tap_on_screen() throws Throwable {
		plansPO.tapOnScreen();
	}

	@Then("^I verify that the Pack Coach Mark is displayed$")
	public void i_verify_that_the_pack_coach_mark_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.packCoachMarkDisplayed());
	}

	@Then("^I verify that the Coach Mark Disappears$")
	public void i_verify_that_the_coach_mark_disappears() throws Throwable {
		Assert.assertTrue(plansPO.coachMarkDisappears());
	}

	@When("^I tap on Buy button option$")
	public void i_tap_on_buy_button_option() throws Throwable {
		plansPO.tapOnBuyPlanButton();
	}

	@When("^I click any pack Type$")
	public void i_click_any_pack_type() throws Throwable {
		plansPO.tapOnAnyPack();
	}

	@When("^I tap on one of the offers$")
	public void i_tap_on_one_of_the_offers() throws Throwable {
		plansPO.tapOnOneOffer();
	}

	@Then("^I verify plan details coach mark Displayed$")
	public void i_verify_plan_details_coach_mark_displayed() throws Throwable {
		Assert.assertTrue(plansPO.planDetailsCoachMarkDisplayed());
	}

	@Then("^I verify buy plan button displayed$")
	public void i_verify_buy_plan_button_displayed() throws Throwable {
		Assert.assertTrue(plansPO.buyPlanButtonDisplayed());
	}

	@Then("^I verify it opens more options of pack types$")
	public void i_verify_it_opens_more_options_of_pack_types() throws Throwable {
		Assert.assertTrue(plansPO.otherPackOptionsDisplayed());
	}

	@Then("^I verify data pack displayed$")
	public void i_verify_data_pack_displayed() throws Throwable {
		Assert.assertTrue(plansPO.dataPackDisplayed());
	}

	@When("^I tap on data pack button$")
	public void i_tap_on_data_pack_button() throws Throwable {
		plansPO.tapOnAnyPack();
	}

	@Then("^I verify pack navigates to Plans screen with Data tab selected$")
	public void i_verify_pack_navigates_to_plans_screen_with_data_tab_selected() throws Throwable {
		Assert.assertTrue(plansPO.verifyPlanScreenDataSelectedDisplay());
	}

	@Then("^I verify local calls pack displayed$")
	public void i_verify_local_calls_pack_displayed() throws Throwable {
		Assert.assertTrue(plansPO.localCallsPackDisplayed());
	}

	@When("^I tap on local calls button$")
	public void i_tap_on_local_calls_button() throws Throwable {
		plansPO.tapOnLocalCallsPack();
	}

	@Then("^I verify pack navigates to Plans screen with local calls tab selected$")
	public void i_verify_pack_navigates_to_plans_screen_with_local_calls_tab_selected() throws Throwable {
		Assert.assertTrue(plansPO.verifyPlanScreenLocalCallSelectedDisplay());
	}

	@Then("^I verify international calls pack displayed$")
	public void i_verify_international_calls_pack_displayed() throws Throwable {
		Assert.assertTrue(plansPO.internationalCallsPackDisplayed());
	}

	@When("^I tap on international calls button$")
	public void i_tap_on_international_calls_button() throws Throwable {
		plansPO.tapOnInternationalCallsPack();
	}

	@Then("^I verify pack navigates to Plans screen with international calls tab selected$")
	public void i_verify_pack_navigates_to_plans_screen_with_international_calls_tab_selected() throws Throwable {
		Assert.assertTrue(plansPO.verifyPlanScreeninterntionalCallSelectedDisplay());
	}

	@Then("^I verify mix pack displayed$")
	public void i_verify_mix_pack_displayed() throws Throwable {
		Assert.assertTrue(plansPO.mixPackDisplayed());
	}

	@When("^I tap on mix button$")
	public void i_tap_on_mix_button() throws Throwable {
		plansPO.tapOnMixPack();
	}

	@Then("^I verify pack navigates to Plans screen with mix tab selected$")
	public void i_verify_pack_navigates_to_plans_screen_with_mix_tab_selected() throws Throwable {
		Assert.assertTrue(plansPO.verifyPlanScreenMixSelectedDisplay());
	}

	@Then("^I verify Plan Screen displayed$")
	public void i_verify_plan_screen_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPlanScreenDisplay());
	}

	@Then("^I verify \"([^\"]*)\" text displayed$")
	public void i_verify_something_text_displayed(String Str) throws Throwable {
		Assert.assertTrue(plansPO.verifyMobileTextDisplay(Str));
	}

	@Then("^I verify if the MSISDN is displayed in the header$")
	public void i_verify_if_the_msisdn_is_displayed_in_the_header() throws Throwable {
		Assert.assertTrue(plansPO.verifyMSISDNDisplay());
	}

	@Then("^I verify TOTAL BALANCE text displayed$")
	public void i_verify_total_balance_text_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyTotalBalanceTextDisplay());
	}

	@Then("^I verify current wallte balance displayed$")
	public void i_verify_current_wallte_balance_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyWallteBalnceDisplay());
	}

	@Then("^I verify that Top up button is displayed$")
	public void i_verify_that_top_up_button_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyTopUpButtonDisplay());
	}

	@When("^I tap on Top up button$")
	public void i_tap_on_top_up_button() throws Throwable {
		plansPO.tapOnTopUpButton();
	}

	@Then("^I verify data tab displayed$")
	public void i_verify_data_tab_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyDataTabDisplay());
	}

	@Then("^I verify Data tab displays Data packs$")
	public void i_verify_data_tab_displays_data_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyAllDataPacksDisplayed());
	}

	@Then("^I verify Minutes tab is displayed$")
	public void i_verify_minutes_tab_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyMinutesTabDisplay());
	}

	@When("^I tap on minutes tab$")
	public void i_tap_on_minutes_tab() throws Throwable {
		plansPO.tapOnMinutesTab();
	}

	@Then("^I verify Minutes tab displays Call packs$")
	public void i_verify_minutes_tab_displays_call_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomCallacksDisplayed());
	}

	@Then("^I verify that Mix tab is displayed$")
	public void i_verify_that_mix_tab_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyMixTabDisplay());
	}

	@When("^I tap on Mix tab$")
	public void i_tap_on_mix_tab() throws Throwable {
		plansPO.tapOnMixTab();
	}

	@Then("^I verify Mix tab displays Mix packs$")
	public void i_verify_mix_tab_displays_mix_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyMixPacksDisplay());
	}

	@Then("^I verify carousel is displayed$")
	public void i_verify_carousel_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyCarouselDisplay());
	}

	@When("^I scroll the carousel$")
	public void i_scroll_the_carousel() throws Throwable {
		plansPO.scrollcarousel();
	}

	@Then("^I verify i am able to scroll carousel$")
	public void i_verify_i_am_able_to_scroll_carousel() throws Throwable {
		Assert.assertTrue(plansPO.verifyAbletoScrollCarousel());
	}

	@Then("^I verify data pack category All Data Plans is displayed$")
	public void i_verify_data_pack_category_all_data_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyAllDataPlansCategoryDisplayed());
	}

	@When("^I tap on Freedom Data Plans button$")
	public void i_tap_on_freedom_data_plans_button() throws Throwable {
		plansPO.tapOnFreedomDataPlanButton();
	}

	@Then("^I verify data pack category Freedom Data Plans is displayed$")
	public void i_verify_data_pack_category_freedom_data_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomDataPlansCategoryDisplayed());
	}

	@When("^I tap on Saver Data Plans button$")
	public void i_tap_on_saver_data_plans_button() throws Throwable {
		plansPO.tapOnSaverDataPlanButton();
	}

	@Then("^I verify data pack category Saver Data Plans is displayed$")
	public void i_verify_data_pack_category_saver_data_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverPlansCategoryDisplayed());
	}

	@Then("^I verify call pack category Freedom Calls is displayed$")
	public void i_verify_call_pack_category_local_calls_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomCallCategoryDisplayed());
	}

	@When("^I tap on International Call button$")
	public void i_tap_on_international_call_button() throws Throwable {
		plansPO.tapinternationalCallsButton();
	}

	@Then("^I verify call pack category International Calls is displayed$")
	public void i_verify_call_pack_category_international_calls_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyinternationalCallCategoryDisplayed());
	}

	@Then("^I verify Select a country button is displayed$")
	public void i_verify_select_a_country_button_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySelectCountryButtonDisplayed());
	}

	@When("^I tap on Select a country button$")
	public void i_tap_on_select_a_country_button() throws Throwable {
		plansPO.tapOnSelectCountryButton();
	}

	@Then("^I verify cross button displayed$")
	public void i_verify_cross_button_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyCrossButtonDisplay());
	}

	@When("^I click on the X button from the Select Country screen$")
	public void i_click_on_the_x_button_from_the_select_country_screen() throws Throwable {
		plansPO.tapCrossButton();
	}

	@Then("^I verify if a Find country search field is displayed$")
	public void i_verify_if_a_find_country_search_field_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFindCountryFieldDisplay());
	}

	@Then("^I verify the list of countries is displayed$")
	public void i_verify_the_list_of_countries_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyListOfCountriesDisplay());
	}

	@When("^I type \"([^\"]*)\" in the textfield displayed$")
	public void i_type_something_in_the_textfield_displayed(String country) throws Throwable {
		plansPO.typeCountryInTextField(country);
	}

	@Then("^I verify user can input data in the find country field$")
	public void i_verify_user_can_input_data_in_the_find_country_field() throws Throwable {
		Assert.assertTrue(plansPO.verifyInputDataInCountryField());
	}

	@Then("^I verify that the search list with \"([^\"]*)\" in it$")
	public void i_verify_that_the_search_list_with_something_in_it(String country) throws Throwable {

		Assert.assertTrue(plansPO.verifySearchProcess(country));
	}

	@Then("^I verify All countries statrts with \"([^\"]*)\" is listed on the screen$")
	public void i_verify_all_countries_statrts_with_something_is_listed_on_the_screen(String country) throws Throwable {
		Assert.assertTrue(plansPO.verifyCountryListed(country));
	}

	@When("^I tap on one of the country from list$")
	public void i_tap_on_one_of_the_country_from_list() throws Throwable {
		plansPO.tapOnCountry();
	}

	@Then("^I verify Mix pack category freedom mix plans is displayed$")
	public void i_verify_mix_pack_category_freedom_mix_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomMixPlansCategoryDisplayed());
	}

	@When("^I tap on Saver Mix Plans button$")
	public void i_tap_on_saver_mix_plans_button() throws Throwable {
		plansPO.tapSaverMixPlansButton();
	}

	@Then("^I verify Mix pack category Saver mix plans is displayed$")
	public void i_verify_mix_pack_category_saver_mix_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverMixplanCategoryDisplayed());
	}

	@Then("^I verify for data validity, allowance and price displayed$")
	public void i_verify_for_data_validity_allowance_and_price_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyDataPlanDetailsDisplayed());
	}

	@Then("^I verify for calls validity, allowance and price displayed$")
	public void i_verify_for_calls_validity_allowance_and_price_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyCallsPlanDetailsDisplayed());
	}

	@Then("^I verify for Mix packs validity, allowance and price displayed$")
	public void i_verify_for_mix_packs_validity_allowance_and_price_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyMixPlanDetailsDisplayed());
	}

	@Then("^I verify I am on Summary screen$")
	public void i_verify_i_am_on_summary_screen() throws Throwable {
		Assert.assertTrue(plansPO.verifySummaryScreenDisplayed());
	}

	@Then("^I verify All data packs displayed$")
	public void i_verify_all_data_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyAllDataPacksDisplayed());
	}

	@Then("^I verify Freedom data packs displayed$")
	public void i_verify_freedom_data_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomDataPacksDisplayed());
	}

	@Then("^I verify Saver data packs displayed$")
	public void i_verify_saver_data_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverPacksDisplayed());
	}

	@Then("^I verify Freedom call packs displayed$")
	public void i_verify_Freedom_call_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyFreedomCallacksDisplayed());
	}

	@Then("^I verify international call packs should not be displayed$")
	public void i_verify_international_call_packs_should_not_be_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyinternationalCallacksDisplayed());
	}

	@Then("^I verify selected country display on the pack screen with available packs$")
	public void i_verify_selected_country_display_on_the_pack_screen_with_available_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyinternationalCallacksDisplayed());
	}

	@Then("^I verify included country display$")
	public void i_verify_included_country_display() throws Throwable {
		Assert.assertTrue(plansPO.verifyIncludedCountryDisplayed());
	}

	@Then("^I verify selected pack display$")
	public void i_verify_selected_pack_display() throws Throwable {
		Assert.assertTrue(plansPO.verifySelectedPackDisplayed());
	}

	@Then("^I verify package card display$")
	public void i_verify_package_card_display() throws Throwable {
		Assert.assertTrue(plansPO.verifyPackageCardDisplayed());
	}

	@Then("^I verify cross button display$")
	public void i_verify_cross_button_display() throws Throwable {
		Assert.assertTrue(plansPO.verifyCrossbuttonDisplayed());
	}

	@When("^I tap on cross Button$")
	public void i_tap_on_cross_button() throws Throwable {
		plansPO.tapCrossbutton();
	}

	@When("^I tap on offer more than wallet balance$")
	public void i_tap_on_offer_more_than_wallet_balance() throws Throwable {
		plansPO.tapOnOffermorethenWalletBalance();
	}

	@Then("^I verify Recharge pop up display$")
	public void i_verify_recharge_pop_up_display() throws Throwable {
		Assert.assertTrue(plansPO.verifyRechargePopupDisplayed());
	}

	@Then("^I verify summary back button displayed$")
	public void i_verify_summary_back_button_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifybackDisplayed());
	}

	@When("^I tap summary screen back button$")
	public void i_tap_summary_screen_back_button() throws Throwable {
		plansPO.tapBackbutton();
	}

	@When("^I tap on Saver Calls Plans button$")
	public void i_tap_on_saver_Calls_plans_button() throws Throwable {
		plansPO.tapOnSaverCallsPlanButton();
	}

	@Then("^I verify call pack category Saver Calls Plans is displayed$")
	public void i_verify_call_pack_category_saver_calls_plans_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverCallsPlansCategoryDisplayed());
	}

	@Then("^I verify Saver call packs displayed$")
	public void i_verify_saver_call_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverCallacksDisplayed());
	}

	@Then("^I verify Saver Mix packs displayed$")
	public void i_verify_saver_mix_packs_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifySaverMixPacksDisplayed());
	}

	@And("^I verify search bar has character validation$")
	public void i_verify_search_bar_has_character_validation() throws Throwable {
		Assert.assertTrue(plansPO.verifySearchBarHasCharacterValidation());
	}

	@Then("^I verfiy purchased data pack is displayed$")
	public void i_verfiy_purchased_data_pack_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedDatapackDisplay());
	}

	@Then("^I verfiy can buy the same Data pack twice$")
	public void i_verfiy_can_buy_the_same_data_pack_twice() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchaseDatapactwiceDisplay());
	}

	@When("^I tap on different data offer$")
	public void i_tap_on_different_data_offer() throws Throwable {
		plansPO.tapOnDifferentDataPack();
	}

	@Then("^I verfiy can buy two different Data Packs$")
	public void i_verfiy_can_buy_two_different_data_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedifferentDatapackDisplay());
	}

	@Then("^I verfiy purchased Freedom local calls pack is displayed$")
	public void i_verfiy_purchased_Freedom_local_calls_pack_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedFreedomLocalCallpackDisplay());
	}

	@Then("^I verfiy can buy the same Freedom local calls pack twice$")
	public void i_verfiy_can_buy_the_same_Freedom_local_calls_pack_twice() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchaseFreedomCallpacktwiceDisplay());
	}

	@When("^I tap on different Freedom call offer$")
	public void i_tap_on_different_Freedom_call_offer() throws Throwable {
		plansPO.tapOnDifferentCallPack();
	}

	@Then("^I verfiy can buy two different Freedom call Packs$")
	public void i_verfiy_can_buy_two_different_Freedom_call_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedifferentFreedomCallpackDisplay());
	}

	@Then("^I verfiy purchased Saver local calls pack is displayed$")
	public void i_verfiy_purchased_Saver_local_calls_pack_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedSaverLocalCallpackDisplay());
	}

	@Then("^I verfiy can buy the same Saver local calls pack twice$")
	public void i_verfiy_can_buy_the_same_Saver_local_calls_pack_twice() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchaseSaverlocalCallpacktwiceDisplay());
	}

	@When("^I tap on different Saver call offer$")
	public void i_tap_on_different_Saver_call_offer() throws Throwable {
		plansPO.tapOnDifferentCallPack();
	}

	@Then("^I verfiy can buy two different Saver call Packs$")
	public void i_verfiy_can_buy_two_different_Saver_call_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedifferentSaverCallpackDisplay());
	}

	@Then("^I verfiy purchased International call pack is displayed$")
	public void i_verfiy_purchased_international_call_pack_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedInternationalCallpackDisplay());
	}

	@Then("^I verfiy can buy the same International call pack twice$")
	public void i_verfiy_can_buy_the_same_international_call_pack_twice() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchaseInternationalCallpacktwiceDisplay());
	}

	@Then("^I verfiy can buy two different International call Packs$")
	public void i_verfiy_can_buy_two_different_international_call_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedifferentInternationalCallpackDisplay());
	}

	@Then("^I verfiy purchased Mix call pack is displayed$")
	public void i_verfiy_purchased_mix_call_pack_is_displayed() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedMixCallpackDisplay());
	}

	@Then("^I verfiy can buy the same Mix call pack twice$")
	public void i_verfiy_can_buy_the_same_mix_call_pack_twice() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchaseMixCallpacktwiceDisplay());
	}

	@When("^I tap on different Mix call offer$")
	public void i_tap_on_different_mix_call_offer() throws Throwable {
		plansPO.tapOnDifferentCallPack();
	}

	@Then("^I verfiy can buy two different Mix call Packs$")
	public void i_verfiy_can_buy_two_different_mix_call_packs() throws Throwable {
		Assert.assertTrue(plansPO.verifyPurchasedifferentMixpackDisplay());
	}
}
