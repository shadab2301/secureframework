package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_RedeemRewardsStepDef extends StepDefinitionInit {

	@And("^I verify enterd \"([^\"]*)\" code display$")
	public void i_verify_enterd_something_code_display(String strArg1) throws Throwable {
		Assert.assertTrue(android_RedeemRewardspo.verifyText(strArg1));
	}

	@Then("^I verify congratulations popup display$")
	public void i_verify_congratulations_popup_display() throws Throwable {
		Assert.assertTrue(android_RedeemRewardspo.verifyCongratulationpopup());
	}

	@When("^I close the popup$")
	public void i_close_the_popup() throws Throwable {
		android_RedeemRewardspo.tapClosebutton();
	}

	@When("^I tap on Save For Later button$")
	public void i_tap_on_Save_For_Later_button() throws Throwable {
		android_RedeemRewardspo.tapSaveForLaterbutton();
	}

	@Then("^I verify Saved pop up displayed$")
	public void i_verify_saved_pop_up_displayed() throws Throwable {
		Assert.assertTrue(android_RedeemRewardspo.verifySavedpopup());
	}

	@Then("^I verfiy Apply button for code is displayed$")
	public void i_verfiy_apply_button_for_code_is_displayed() throws Throwable {
		Assert.assertTrue(android_RedeemRewardspo.verifyApplyButtonDisplay());
	}

	@When("^I click on Apply button for code$")
	public void i_click_on_apply_button_for_code() throws Throwable {
		android_RedeemRewardspo.tapApplybutton();
	}

	@Then("^I verfiy Apply button for offer displayed$")
	public void i_verfiy_apply_button_for_offer_displayed() throws Throwable {
		Assert.assertTrue(android_RedeemRewardspo.verifyApplyButton());
	}

}
