package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import com.org.utils.PropFileHandler;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_LoginStepDef extends StepDefinitionInit {

	@When("^I tap on back button$")
	public void i_tap_on_back_button() throws Throwable {
		loginPO.tapBackButton();
	}

	@When("^I tap on help Icon$")
	public void i_tap_on_help_icon() throws Throwable {
		loginPO.tapOnHelpIcon();
	}

	@Then("^I verify mobile number field display$")
	public void i_verify_mobile_number_field_display() throws Throwable {
		Assert.assertTrue(loginPO.mobileNumberFieldDisplayed());
	}

	@Then("^I verify help text displayed$")
	public void i_verify_help_text_displayed() throws Throwable {
		Assert.assertTrue(loginPO.helpTextDisplayed());
	}

	@When("^I enter mobile number$")
	public void i_enter_mobile_number() throws Throwable {
		String id = PropFileHandler.readProperty("loginID");
		loginPO.enterMobileNumber(id);
	}

	@Then("^I verify help text removed$")
	public void i_verify_help_text_removed() throws Throwable {
		Assert.assertTrue(loginPO.verifyHelpTextRemoved());
	}

	@When("^I tap on next button$")
	public void i_tap_on_next_button() throws Throwable {
		loginPO.tapNextButton();
	}

	@When("^I tap on back btn$")
	public void i_tap_on_back_btn() throws Throwable {
		loginPO.tapBackbutton();
	}

	@When("^I tap on fingerprint icon$")
	public void i_tap_on_fingerprint_icon() throws Throwable {
		loginPO.tapOnFingerprintIcon();
	}

	@Then("^I verify warning pop up displayed$")
	public void i_verify_warning_pop_up_displayed() throws Throwable {
		Assert.assertTrue(loginPO.verifyWarningpopUpDisplayed());
	}

	@When("^I refresh the screen$")
	public void i_refresh_the_screen() throws Throwable {
		getAsimPO.refreshTheScreen();
		getAsimPO.refreshTheScreen();
	}

	@Then("^I verify help text for password field is displayed$")
	public void i_verify_help_text_for_password_field_is_displayed() throws Throwable {
		Assert.assertTrue(loginPO.verifypasswordHelpTextDisplayed());
	}
	@And("^I verify able to enter in password field$")
    public void i_verify_able_to_enter_in_password_field() throws Throwable {
		Assert.assertTrue(loginPO.verifyAbletoEnterPasswordfield());
    }
}
