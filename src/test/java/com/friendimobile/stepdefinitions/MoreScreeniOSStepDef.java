package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MoreScreeniOSStepDef extends StepDefinitionInit {

	@When("^I close the Welcome popup$")
	public void i_close_the_welcome_popup() throws Throwable {
		moreScreeniOSPO.closeWelcomePopup();
	}

	@When("^I click on \"([^\"]*)\" button present on screen$")
	public void i_click_on_something_button_present_on_screen(String button) throws Throwable {
		moreScreeniOSPO.clickButton(button);
	}

	@Then("^I verify \"([^\"]*)\" Screen appears$")
	public void i_verify_something_screen_appears(String screen) throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyScreen(screen));
	}

	@When("^I click on \"([^\"]*)\" button using value$")
	public void i_click_on_something_button_using_value(String button) throws Throwable {
		moreScreeniOSPO.clickButtonValue(button);
	}

	@Then("^I verify \"([^\"]*)\" button is present on screen$")
	public void i_verify_something_button_is_present_on_screen(String button) throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyButton(button));
	}

	@When("^I click on toggle button$")
	public void i_click_on_toggle_button() throws Throwable {
		moreScreeniOSPO.clickOnToggle();
	}

	@And("^I verify \"([^\"]*)\" text present on screen$")
	public void i_verify_something_text_present_on_screen(String text) throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyText(text));
	}

	@When("^I allow the popup$")
	public void i_allow_the_popup() throws Throwable {
		moreScreeniOSPO.allowPopup();
	}

	@When("^I tap for guide popup$")
	public void i_tap_for_guide_popup() throws Throwable {
		moreScreeniOSPO.tapOnScreen();
	}

	@Then("^I verify toggle popup opens$")
	public void i_verify_toggle_popup_opens() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.togglePopup());
	}

	@And("^I verify user is able to enter password in toggle field$")
	public void i_verify_user_is_able_to_enter_password_in_toggle_field() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyUserAbleToEnterPass());
	}

	@Then("^I verify Mask button mask and unmask the password$")
	public void i_verify_mask_button_mask_and_unmask_the_password() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyMaskButtonMaskAndUnmask());
	}

	@Then("^I verify a drop down opens for theme$")
	public void i_verify_a_drop_down_opens_for_theme() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyDropDownOpens());
	}

	@When("^I click on done button$")
	public void i_click_on_done_button() throws Throwable {
		moreScreeniOSPO.clickOnDone();
	}

	@And("^I verify version name is present on screen$")
	public void i_verify_version_name_is_present_on_screen() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyVersion());
	}

	@Then("^I X button is present on screen$")
	public void i_x_button_is_present_on_screen() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyXbutton());
	}

	@And("^I verify Search icon present on top$")
	public void i_verify_search_icon_present_on_top() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifySearchbutton());
	}

	@When("^I click on search icon$")
	public void i_click_on_search_icon() throws Throwable {
		moreScreeniOSPO.searchIconOpen();
	}

	@Then("^I verify search bar should open$")
	public void i_verify_search_bar_should_open() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifySearchbarOpen());
	}

	@And("^I verify categories are displaying$")
	public void i_verify_categories_are_displaying() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyCategories());
	}

	@When("^I click on back button$")
	public void i_click_on_back_button() throws Throwable {
		moreScreeniOSPO.clickOnBackButton();
	}

	@Then("^I verify default search option present$")
	public void i_verify_default_search_option_present() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyDefaultSearch());
	}

	@When("^I click one of the option from default option$")
	public void i_click_one_of_the_option_from_default_option() throws Throwable {
		moreScreeniOSPO.clickOnDefaultScreen();
	}

	@When("^I enter \"([^\"]*)\" this in search field$")
	public void i_enter_something_this_in_search_field(String strArg1) throws Throwable {
		moreScreeniOSPO.enterInput(strArg1);
	}

	@Then("^I verify \"([^\"]*)\" Screen appears using value$")
	public void i_verify_something_screen_appears_using_value(String strArg1) throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyScreenUsingValue(strArg1));
	}

	@And("^I verify the version on application is present on screen$")
	public void i_verify_the_version_on_application_is_present_on_screen() throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyVersion());
	}

	@Then("^I verify Found your answer text disappears$")
	public void i_verify_Found_your_answer_text_disappears() throws Throwable {
		Assert.assertFalse(moreScreeniOSPO.verifyTextDisappears());
	}
	@And("^I verify entered \"([^\"]*)\" text displays in field$")
    public void i_verify_entered_something_text_displays_in_field(String strArg1) throws Throwable {
		Assert.assertTrue(moreScreeniOSPO.verifyTextInfield(strArg1));
    }
	@When("^I tap on back button on screen$")
    public void i_tap_on_back_button_on_screen() throws Throwable {
		moreScreeniOSPO.clickBack();
    }


}
