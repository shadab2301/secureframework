package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import com.org.utils.PropFileHandler;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Android_LocalCreditTransferStepDef extends StepDefinitionInit {

	@Then("^I verify RO input field display$")
	public void i_verify_ro_input_field_display() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyROinputFieldDisplay());
	}

	@Then("^I verify help text \"([^\"]*)\" displayed$")
	public void i_verify_help_text_something_displayed(String helpText) throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyHelpTextDisplay(helpText));
	}

	@And("^I verify able to input \"([^\"]*)\" data in number field$")
	public void i_verify_able_to_input_something_data_in_number_field(String data) throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyAbleToInputDataInNumberField(data));
	}

	@Then("^I verify help text disappeared$")
	public void i_verify_help_text_disappeard() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyHelpTextRemoved());
	}

	@And("^I verify if text field has character validation$")
	public void i_verify_if_text_field_has_character_validation() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyTextfieldHasCharacterValidation());
	}

	@When("^I tap on Top up Button$")
	public void i_tap_on_top_up_button() throws Throwable {
		android_creditTransferPO.tapOnTopUpbutton();
	}

	@When("^I tap on contact button present$")
	public void i_tap_on_contact_button_present() throws Throwable {
		android_creditTransferPO.tapOnContactButton();
	}

	@Then("^I verify phone contact screen is displayed$")
	public void i_verify_phone_contact_screen_is_displayed() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyContactScreen());
	}

	@When("^I enter \"([^\"]*)\" data in RO field$")
	public void i_enter_something_data_in_ro_field(String amount) throws Throwable {
		android_creditTransferPO.enterValueInField(amount);
	}

	@Then("^I verify error message display$")
	public void i_verify_error_message_display() throws Throwable {
		String error = PropFileHandler.readProperty("errormessage");
		Assert.assertTrue(android_creditTransferPO.verifyErrormessage(error));
	}

	@And("^I verify able to input in RO field$")
	public void i_verify_able_to_input_in_ro_field() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyInputInROField());
	}

	@When("^I enter \"([^\"]*)\" number in tap in number field$")
	public void i_enter_something_number_in_tap_in_number_field(String number) throws Throwable {
		android_creditTransferPO.enterMobileNumber(number);
	}

	@And("^I verify Transfer button enabled$")
	public void i_verify_transfer_button_enabled() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyTransferButtonEnabled());
	}

	@Then("^I verify Total Amount dropdown display$")
	public void i_verify_total_amount_dropdown_display() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyTotalAmountDropdownDisplay());
	}

	@Then("^I verify total amount details are displayed$")
	public void i_verify_total_amount_details_are_displayed() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyTotalAmountDetailsDisplay());
	}

	@Then("^I verify OTP field displayed$")
	public void i_verify_otp_field_displayed() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyOTPFieldDisplay());
	}

	@And("^I verify able to input \"([^\"]*)\" data in OTP field$")
	public void i_verify_able_to_input_something_data_in_otp_field(String OTP) throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyAbleToInputDataInOTPField(OTP));
	}

	@And("^I verify phone number is displayed in the field$")
	public void i_verify_phone_number_is_displayed_in_the_field() throws Throwable {
		Assert.assertTrue(android_creditTransferPO.verifyPhoneNumberDisplayed());
	}

}
