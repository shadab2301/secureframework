package com.friendimobile.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Dashboard_iOSStepDef extends StepDefinitionInit{

	@When("^I expand the header$")
    public void i_expand_the_header() throws Throwable {
		dashboardiOSPO.clickOnheaderDropDown();
    }
	@Then("^I verify recharge option is displayed$")
    public void i_verify_recharge_option_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyRechargeOption());
    } 
	@When("^I click on recharge option$")
    public void i_click_on_recharge_option() throws Throwable {
		dashboardiOSPO.clickOnRecharegeOption();
    }
	@And("^I verify name field is present$")
    public void i_verify_name_field_is_present() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyNameField());
    }
	@And("^I verify \"([^\"]*)\" this name can be entered in the field$")
    public void i_verify_something_this_name_can_be_entered_in_the_field(String strArg1) throws Throwable {
        Assert.assertTrue(dashboardiOSPO.verifyDataEnter(strArg1));
    }
	@And("^I verify total balance is displayed$")
    public void i_verify_total_balance_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyBalance());
    }
	@And("^I verify bonus wallet section is displayed$")
    public void i_verify_bonus_wallet_section_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyBonusWalletSection());
    }
	@When("^I click on bonus wallet section$")
    public void i_click_on_bonus_wallet_section() throws Throwable {
		dashboardiOSPO.clickOnBonus();
    }
	@And("^I verify Total amount of SR and Your bonus balance is displayed$")
    public void i_verify_total_amount_of_sr_and_your_bonus_balance_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyTotalAmountAndBonusBalance());
    }
	@And("^I verify the previous months are displayed$")
    public void i_verify_the_previous_months_are_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyPreviousMonths());
    }
	@And("^I verify bonuses are displayed$")
    public void i_verify_bonuses_are_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyBonuses());
    }
	@And("^I verify amount field is displayed$")
    public void i_verify_amount_field_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyAmountField());
    }
	@When("^I click on recharge your account button$")
    public void i_click_on_recharge_your_account_button() throws Throwable {
		dashboardiOSPO.clickOnReachargeAccountButton();
    }
	@And("^I verify user able to enter this \"([^\"]*)\" data in recharge wallet field$")
    public void i_verify_user_able_to_enter_this_something_data_in_recharge_wallet_field(String strArg1) throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyRechargeWallet(strArg1));
    }
	@And("^I verify there is character validation present in the field$")
    public void i_verify_there_is_character_validation_present_in_the_field() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyCharacterValidation());
    }
	@And("^I verify a payment method is displayed$")
    public void i_verify_a_payment_method_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyPaymentMethod());
    }
	@When("^I enter wrong details in add new card$")
    public void i_enter_wrong_details_in_add_new_card() throws Throwable {
		dashboardiOSPO.enterWrongDetails();
    }
	@And("^I verify pay button is enabled$")
    public void i_verify_pay_button_is_enabled() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyPayBtn());
    }
	@When("^I click on plus button on screen '(\\d+)' times$")
	public void i_click_on_plus_button_on_screen_times(int arg1) throws Throwable {
		dashboardiOSPO.clickPlusBtn(arg1);
	}
	@And("^I verify use voucher button is disabled$")
    public void i_verify_use_voucher_button_is_disabled() throws Throwable {
		Assert.assertFalse(dashboardiOSPO.verifyVoucherBtnDisabled());
    }
	@When("^User enters this \"([^\"]*)\" data in voucher field$")
    public void user_enters_this_something_data_in_voucher_field(String strArg1) throws Throwable {
		dashboardiOSPO.verifyVoucherField(strArg1);
    }
	@And("^I verify there is character validation present in the voucher field$")
    public void i_verify_there_is_character_validation_present_in_the_voucher_field() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyCharacterValidationInVoucherField());
    }
	@And("^I verify character limit is also present in the  voucher field$")
    public void i_verify_character_limit_is_also_present_in_the_voucher_field() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyCharacterLimitInVoucherField());
	}	
	@And("^I verify use voucher button is enabled$")
    public void i_verify_use_voucher_button_is_enabled() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyVoucherBtnDisabled());
    }
	@And("^I verify notifications are displayed in a stack$")
    public void i_verify_notifications_are_displayed_in_a_stack() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyNotifications());
    }
	@And("^I verify notifications can be expanded$")
    public void i_verify_notifications_can_be_expanded() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyNotificationsExpands());
    }
	@And("^I verify clear all button is displayed$")
    public void i_verify_clear_all_button_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyClearBtn());
    }
	@When("^I click on clear all button$")
    public void i_click_on_clear_all_button() throws Throwable {
		dashboardiOSPO.clickClearAllBtn();
    }
	@Then("^I verify all notifications are cleared$")
    public void i_verify_all_notifications_are_cleared() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyNotificationsCleared());
    }
	@When("^I enter this \"([^\"]*)\" mobile number in the field$")
    public void i_enter_this_something_mobile_number_in_the_field(String strArg1) throws Throwable {
		dashboardiOSPO.enterValue(strArg1);
    }
	@And("^I verify toggle button is present$")
    public void i_verify_toggle_button_is_present() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyToggleBtn());
    }
	@And("^I verify toggle button is disabled$")
    public void i_verify_toggle_button_is_disabled() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyToggleBtnDisabled());
    }
	@And("^I verify toggle is enabled now$")
    public void i_verify_toggle_is_enabled_now() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyToggleBtnEnabled());
    }
	@And("^I verify bonuses are closed now$")
    public void i_verify_bonuses_are_closed_now() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyBonusesClosed());
    }
	@And("^I verify more than one month details can be opened at a time$")
    public void i_verify_more_than_one_month_details_can_be_opened_at_a_time() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyBonuses());
		Assert.assertTrue(dashboardiOSPO.verifyMoreThanOneMonthCanBeOpened());
    }
	@Then("^I verify navigation bar is displayed$")
    public void i_verify_navigation_bar_is_displayed() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyNavigationBar());
    }
	@And("^I verify amount per day are summed correctly$")
    public void i_verify_amount_per_day_are_summed_correctly() throws Throwable {
		Assert.assertTrue(dashboardiOSPO.verifyAmountSummedProperly());
    }


}