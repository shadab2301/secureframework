
package com.friendimobile.stepdefinitions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.friendimobile.po.Android_DashboardPO;
import com.friendimobile.po.Android_DataTransferPO;
import com.friendimobile.po.Android_InitialLoginPO;
import com.friendimobile.po.Android_InternationalCallsPO;
import com.friendimobile.po.Android_InternationalCreditTransferPO;
import com.friendimobile.po.Android_LocalCreditTransferPO;
import com.friendimobile.po.Android_LoginPO;
import com.friendimobile.po.Android_LoginPromoPO;
import com.friendimobile.po.Android_ManageSIMPO;
import com.friendimobile.po.Android_MoreAccountSettingPO;
import com.friendimobile.po.Android_MoreHelpPO;
import com.friendimobile.po.Android_PlansPO;
import com.friendimobile.po.Android_RedeemRewardsPO;
import com.friendimobile.po.Android_RoamingServicesPO;
import com.friendimobile.po.Android_SMSParkingPO;
import com.friendimobile.po.Android_WelcomeGetAsimPO;
import com.friendimobile.po.Dashboard_iOS_PO;
import com.friendimobile.po.Login_iOS_PO;
import com.friendimobile.po.MoreScreeniOSPO;
import com.friendimobile.po.Plans_iOS_PO;
import com.friendimobile.po.Services_iOS_PO;
import com.friendimobile.po.UsageHistoryiOSPO;
import com.friendimobile.po.WelcomeGetASim_iOS_PO;
import com.org.automation.framework.TestSession;
import com.org.helper.CommonGestures;

/**
 * 
 * @author nimit.jain
 *
 */
public class StepDefinitionInit {

	protected static TestSession session;

	Map<String, Object> options = new HashMap<String, Object>();
	static Android_WelcomeGetAsimPO getAsimPO;
	static Android_LoginPO loginPO;
	static Android_InitialLoginPO initialLoginPO;
	static Android_LoginPromoPO loginpromoPO;
	static Android_PlansPO plansPO;
	static MoreScreeniOSPO moreScreeniOSPO;
	static Android_MoreAccountSettingPO android_MoreAccountsettingPO;
	static Android_MoreHelpPO android_MoreHelpPO;
	static Android_LocalCreditTransferPO android_creditTransferPO;
	static UsageHistoryiOSPO usageHistoryPO;
	static Android_InternationalCreditTransferPO android_internationalcreditTransferPO;
	static Android_SMSParkingPO android_SMSParkingPO;
	static Android_ManageSIMPO android_ManageSimPO;
	static Android_DashboardPO android_dashboardPO;
	static Android_InternationalCallsPO android_InternationalCallsPO;
	static Android_RoamingServicesPO android_RoamingServicesPO;
	static Android_DataTransferPO android_DataTransferPO;
	static WelcomeGetASim_iOS_PO WelcomeGetASim_iOS_PO;
	static Login_iOS_PO Login_iOS_PO;
	static Services_iOS_PO Services_iOS_PO;
	static Dashboard_iOS_PO dashboardiOSPO;
	static Plans_iOS_PO plansiOSPO;
	static Android_RedeemRewardsPO android_RedeemRewardspo;

	public TestSession getTestSession() throws Exception {
		// options.put("APPIUM_APP_FULL_RESET", true);
		// options.put("APPIUM_APP_NO_RESET", false);

		if (session == null) {
			session = new TestSession(options);
			getAsimPO = new Android_WelcomeGetAsimPO(session);
			loginPO = new Android_LoginPO(session);
			initialLoginPO = new Android_InitialLoginPO(session);
			loginpromoPO = new Android_LoginPromoPO(session);
			plansPO = new Android_PlansPO(session);
			moreScreeniOSPO = new MoreScreeniOSPO(session);
			android_MoreAccountsettingPO = new Android_MoreAccountSettingPO(session);
			android_MoreHelpPO = new Android_MoreHelpPO(session);
			android_creditTransferPO = new Android_LocalCreditTransferPO(session);
			usageHistoryPO = new UsageHistoryiOSPO(session);
			android_internationalcreditTransferPO = new Android_InternationalCreditTransferPO(session);
			android_SMSParkingPO = new Android_SMSParkingPO(session);
			android_ManageSimPO = new Android_ManageSIMPO(session);
			android_dashboardPO = new Android_DashboardPO(session);
			android_InternationalCallsPO = new Android_InternationalCallsPO(session);
			android_RoamingServicesPO = new Android_RoamingServicesPO(session);
			android_DataTransferPO = new Android_DataTransferPO(session);
			WelcomeGetASim_iOS_PO = new WelcomeGetASim_iOS_PO(session);
			Login_iOS_PO = new Login_iOS_PO(session);
			Services_iOS_PO = new Services_iOS_PO(session);
			dashboardiOSPO = new Dashboard_iOS_PO(session);
			plansiOSPO = new Plans_iOS_PO(session);
			android_RedeemRewardspo = new Android_RedeemRewardsPO(session);

		}
		for (Entry<String, Object> entry : session.config.entrySet()) {
			System.out.println("*********" + entry.getKey() + " " + entry.getValue());
		}
		return session;
	}

}
