# Set Up Instruction:-

**Required softwares:**
      * JDK : http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html 
      * Maven : https://maven.apache.org/download.cgi
      * Eclipse : http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/neon2
     
# Please add .apk file and .app file before executing the automation  
 <{Project_Base_Directory}>//src/main/resources/apk





# How to set up project

1. Clone this project - 
2. Import project in eclipse as Maven existing project
3. Make sure you have set JDK 1.8 in build path for this project.
4. Make sure you have given the path of feature folder in RunCukeTest.java file


# Run Project from command line

1. Open terminal(MAC) or CMD(windows) and go to project directory

2. To run the every feature file individually on android

Command:- 
mvn clean test -Dtest=com.friendimobile.runner.CukeRunner -Dcucumber.options="{Location of feature file}/ --tags @{Tag name}" -DfailIfNoTests=false -DplatformValue={platformName} 
platformName can be android or sauceAndroid  


3.To run the every feature file individually on iOS

Command:- 
mvn clean test -Dtest=com.friendimobile.runner.CukeRunner -Dcucumber.options="{Location of feature file}/ --tags @{Tag name}" -DfailIfNoTests=false -DplatformValue={platformName} 
platformName can be iPhone or sauceiOS

4- To run every test by giving all credentials (TestRail and sauce) through command line on android

mvn clean test -Dtest=com.friendimobile.runner.CukeRunner -Dcucumber.options="{Location of feature file}/ --tags @{Tag name}" -DtestRunID={Test Run ID} -DtestRailUsername={TestRail username} -DtestRailPassword={TestRail Password} -DtestRailEngineURL={Testrail engine URL} -DfailIfNoTests=false  -DsauceUrl={SauceDriverCreation)} -DplatformValue={platformName}

platformName can be android or sauceAndroid

5- To run every test by giving all credentials (TestRail and sauce) through command line on iOS

mvn clean test -Dtest=com.friendimobile.runner.CukeRunner -Dcucumber.options="{Location of feature file}/ --tags @{Tag name}" -DtestRunID={Test Run ID} -DtestRailUsername={TestRail username} -DtestRailPassword={TestRail Password} -DtestRailEngineURL={Testrail engine URL} -DfailIfNoTests=false  -DsauceUrl={SauceDriverCreation)} -DplatformValue={platformName}

platformName can be iPhone or sauceiOS
